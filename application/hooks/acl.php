<?php

if (!defined('BASEPATH'))
exit('ACCESO PROHIBIDO');

class Acl {

   /**
   * @desc - obtenemos la instancia de ci
   */
   public function __get($var)
   {
      return get_instance()->$var;
   }

   /**
   * @desc - obtenemos la instancia de ci sin tender que crearla
   */
   public function __construct()
   {
        !$this->load->library('session') ? $this->load->library('session') : false;
        !$this->load->helper('url') ? $this->load->helper('url') : false;
        $this->load->model('ruta/ruta_model', 'modelRuta');

        // Cache configuration for APC its fallback to FILE
        $cache['adapter'] = 'file';
        if ( function_exists('apc_cache_info') ) {
            $cache['adapter'] = 'apc';
        }
        $this->load->driver('cache', $cache);
   }

   /**
   * @desc - devuelve un array con los roles y las zonas de acceso
   * @return array
   */
   private function roles_access()
   {
        if ( !$roles = $this->cache->get('roles') )
        {
//            $rutas = $this->cache->get('role_access') ? $this->cache->get('role_access') : $this->modelRuta->todos_los_rol_rutas();
//            $roles = array_reduce($rutas, 'roles_acl');
//
//            log_message('INFO', "Roles FROM DB: ".json_encode($rutas));
//
//            // Save into the cache for 30 sec
//            $this->cache->save('role_access', $rutas, 30);
//            $this->cache->save('roles', $roles, 30);
        }

        return $roles ? $roles : array();
   }

   /**
   * @desc - por defecto, si no existe la sesión de usuario es CONSULTA
   * @return - string - sesión por defecto
   */
   private function _defaultRole()
   {
      return !$this->session->userdata("userNombreRol") ? $this->session->set_userdata("userNombreRol", "CONSULTA") : $this->session->userdata("userNombreRol");
   }

   /**
   * @desc - comprobamos si el usuario tiene acceso a una zona,
   * si no lo tiene lo dejamos en la primera de su rol con un mensaje
   */
   public function auth()
   {

    if ($this->uri->segment(1) =='api') {
        //log_message('INFO', "<<<<<<URI SEGMENT API ".$this->uri->segment(1));
        //$this->load->model('api/api_model','modelApi');
        return;
    }

        $userNombreRol = $this->_defaultRole();
        // log_message('INFO', "Roles: ".json_encode($this->roles_access()));
        // log_message('INFO', "Userdata userIdRol:  ".$this->session->userdata('userIdRol'));
        // log_message('INFO', "Userdata userNombreRol:  ".$this->session->userdata('userNombreRol'));
        foreach($this->roles_access() as $role => $areas)
        {
            if($userNombreRol == $role)
            {
                if(!in_array($this->uri->segment(1), $areas))
                {
                    $this->session->set_flashdata("denied","No puedes acceder a esta zona");

                    log_message('INFO', "User Access Forbidden:  [{$userNombreRol} => {$this->uri->uri_string()}]");
                    redirect("dashboard", "refresh");
                }
            }
        }
   }

}
