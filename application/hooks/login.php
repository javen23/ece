<?php

if (!defined('BASEPATH'))
exit('ACCESO PROHIBIDO');

class Login {

    private $ci;

    public function __construct() {
        $this->ci = & get_instance();
        !$this->ci->load->library('session') ? $this->ci->load->library('session') : FALSE;
        !$this->ci->load->helper('url') ? $this->ci->load->helper('url') : FALSE;
    }

    public function check_login() {
        if ($this->ci->uri->segment(1) =='api') {
            //log_message('INFO', "<<<<<<URI SEGMENT API ".$this->ci->uri->segment(1));
            $this->ci->load->model('api/api_model','modelApi');
            $this->ci->load->model("api/supervision_model","supervisionModel");
            return;
        }

        if (!$this->ci->session->userdata('idUserLogin') && $this->ci->uri->segment(1) != 'login' && $this->ci->uri->segment(1) != 'logout' ) {
            redirect('login');
        }


    }

}
