<?php

/**
 * Data para usar en vistas como selects input
 */
class DataSelect
{

    public function __construct()
    {
        # code...
    }

    public function pc_capacidad()
    {
        return array(
            array('id'=>'1','desc'=>'Menos de [CPU:Core-i3][RAM:2GB]'),
            array('id'=>'2','desc'=>'Más de [CPU:Core-i3][RAM:2GB]'),
        );
    }

    public function si_no()
    {
        return array(
            array('id'=>'1','desc'=>'SI'),
            array('id'=>'0','desc'=>'NO'),
        );
    }

    public function tipo_seguridad()
    {
        return array(
            array('id'=>'PARTICULAR','desc'=>'PARTICULAR'),
            array('id'=>'PNP','desc'=>'PNP'),
            array('id'=>'SERENAZGO','desc'=>'SERENAZGO'),
            array('id'=>'EMPRESA DE SEGURIDAD','desc'=>'EMPRESA DE SEGURIDAD'),
            array('id'=>'OTROS', 'desc'=>'OTROS'),
        );
    }

    public function tipo_internet()
    {
        return array(
            array('id'=>'1','desc'=>'CABLEADO'),
            array('id'=>'2','desc'=>'WIFI'),
        );
    }

    public function turnos()
    {
        return array(
            array('id'=>'2','desc'=>'2 TURNOS'),
            array('id'=>'3','desc'=>'3 TURNOS'),
            array('id'=>'4','desc'=>'4 TURNOS'),
        );
    }

    public function valores_total_viaje()
    {
        return array(
            'si'=>array('1', 'Hasta 3 horas'),
            'no'=>array('2', 'Más de 3 horas')
        );
    }

}
