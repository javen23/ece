<?php

require('DataSelect.php');

/**
 *
 */
class Graph_Generator
{
    protected $dataselect;

    public function __construct()
    {

    }

    public function generar(array $datos, $titulo)
    {
        foreach ($datos as $method => $list) {
            $graficos[$method] = $this->{'grafico_'.$method}($list, $titulo);
        }

        return $graficos;
    }

    public function grafico_seguridad($seguridad_lista, $titulo)
    {
        $this->dataselect   = new DataSelect();
        $graph_resultado    = array();

        foreach ($this->dataselect->tipo_seguridad() as $tipo_seguridad) {
            // Por cada tipo de seguridad obtenemos el total de registros y lo guarda para el grafico
            foreach ($seguridad_lista as $key => $seguridad_item) {
                if ( $seguridad_item['seguridad'] == $tipo_seguridad['id'] ) {

                    $graph_resultado[] = array(
                        'name'=>$seguridad_item['titulo'],
                        'y'=> (int)$seguridad_item['total'],
                    );
                    //Quitamos la seguridad encontrada para la sumatoria de OTROS
                    unset($seguridad_lista[$key]);
                }
            }
        }

        $total_otros        = array_suma_index($seguridad_lista, 'total');
        $graph_resultado[]  = array(
            'name'=> "OTROS [{$total_otros}]",
            'y'=> (int)$total_otros,
        );

        return array('title'=>'<strong>Seguridad del Local '.$titulo.'</strong>', 'data'=>json_encode($graph_resultado));
    }

    public function grafico_condicion_local($condicion_local_lista, $titulo)
    {
        $total_condicion     = array_count_values(array_column($condicion_local_lista, 'local_estado'));

        $graph_resultado =  array(
            array(
                'name'=> "PRÉSTAMO [{$total_condicion['Prestamo']}]",
                'y'=> (int)$total_condicion['Prestamo'],
            ),
            array(
                'name'=> "ALQUILADO [{$total_condicion['Alquilado']}]",
                'y'=> (int)$total_condicion['Alquilado'],
            ),
        );

        return array('title'=>'<strong>Condición del Local '.$titulo.'</strong>', 'data'=>json_encode($graph_resultado));
    }

    public function grafico_area_local($local_lista, $titulo)
    {
        $total_resultado = array_count_values(array_column($local_lista, 'local_area'));

        $valores = array(
            'MENOS DE 50M2' => 'Menos de 50m2',
            'ENTRE 50M2 Y 120M2' => 'Entre 50m2 y 120m2',
            'ENTRE 120M2 Y 200M2' => 'Entre 120m2 y 200m2',
            'ENTRE 200M2 Y 500M2' => 'Entre 200m2 y 500m2',
            'MÁS DE 500M2' => 'Mas de 500m2',
        );

        foreach ($valores as $title => $index) {

            if (!isset($total_resultado[$index])) $total_resultado[$index] = 0;

            $graph_resultado[] =  array(
                    'name'  => "{$title} [{$total_resultado[$index]}]",
                    'y'     => (int)$total_resultado[$index],
                );
        }

        return array('title'=>'<strong>Área del Local '.$titulo.'</strong>', 'data'=>json_encode($graph_resultado));
    }

    public function grafico_nro_ambientes($local_lista, $titulo)
    {
        $graph_resultado = array();
        foreach ($local_lista as $local) {
            $graph_resultado[] =  array(
                    'name'  => $local['titulo'],
                    'y'     => (int)$local['total'],
                );
        }

        return array('title'=>'<strong>Nro de Ambientes del Local '.$titulo.'</strong>', 'data'=>json_encode($graph_resultado));
    }

    public function grafico_consecucion_meta($local_lista, $titulo)
    {
        $regiones = array(
            'AMAZONAS', 'ANCASH-CHIMBOTE', 'ANCASH-HUARAZ', 'APURIMAC', 'AREQUIPA', 'AYACUCHO', 'CAJAMARCA', 'CALLAO', 'CUSCO', 'HUANCAVELICA', 'HUANUCO', 'ICA', 'JUNIN', 'LA LIBERTAD', 'LAMBAYEQUE', 'LIMA SEDE CENTRAL', 'LIMA PROVINCIAS CAÑETE', 'LIMA PROVINCIAS HUACHO', 'LORETO', 'MADRE DE DIOS', 'MOQUEGUA', 'PASCO', 'PIURA', 'PUNO', 'SAN MARTIN-MOYOBAMBA','SAN MARTIN-TARAPOTO', 'TACNA', 'TUMBES', 'UCAYALI',
        );

        // Total por Sede array(10,5,17,8,27,9,13,6,14,8,11,9,10,12,4,53,4,7,5,3,4,4,9,14,9,5,4,4,7),
        // Consecución Total por Región
        $graph_resultado[] = array(
            'name' => 'Total Regional',
            'data' => array(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,1,1,1,1,1,1,1,1,1,1,1,1,1),
            'stack' => 'total',
        );
        $graph_resultado[] = array(
            'name' => 'Total Provincial',
            'data' => array(9,4,16,7,10,8,12,null,13,7,10,8,9,11,3,null,3,6,4,2,3,3,8,13,8,4,3,3,6),
            'stack' => 'total',
        );
        $graph_resultado[] = array(
            'name' => 'Total Distrital',
            'data' => array(null,null,null,null,16,null,null,5,null,null,null,null,null,null,null,45,null,null,null,null,null,null,null,null,null,null,null,null,null),
            'stack' => 'total',
        );

        /*JHV-Inicio
        $graph_resultado[] = $this->meta_consecucion_regional(&$local_lista['regional']);
        $graph_resultado[] = $this->meta_consecucion_provincial($local_lista['regional'], $local_lista['provincial']);
        $graph_resultado[] = $this->meta_consecucion_distrital($local_lista['regional'], $local_lista['distrital']);
        Fin*/
        $graph_resultado[] = $this->meta_consecucion_regional($local_lista['regional']);
        //$graph_resultado[] = $this->meta_consecucion_provincial($local_lista['regional'], $local_lista['provincial']);
        //$graph_resultado[] = $this->meta_consecucion_distrital($local_lista['regional'], $local_lista['distrital']);
        
        return array('consecucion'=>json_encode($graph_resultado), 'categories'=>json_encode($regiones));
    }

    private function meta_consecucion_regional($local_lista)
    {
        //$lima_met       = array_slice($local_lista, 15, 7);
        $lima_met       = array("0"=>"aa","1"=>"bb");
        $lima_central   = array('cod_sede_operativa'=>'666', 'titulo'=>'LIMA SEDE CENTRAL', 'local'=>array_suma_index($lima_met, 'local'));
        //array_splice($local_lista, 15,7, array($lima_central));

        $meta_consecucion_regional = array_column($local_lista, 'local');

        return array(
            'name'  => 'Regional',
            'data'  => array_map('intval', $meta_consecucion_regional),
            'stack' => 'region',
        );
    }

    private function meta_consecucion_provincial($local_lista, $meta_provincial_lista)
    {
        // Obtenemos un array limpio de todas las sede operativas
        $sede_operativa = array_fill_keys(array_column($local_lista, 'cod_sede_operativa'), 0);
        $lima_met = array('151','152','153','154','155','156','157');

        foreach ($meta_provincial_lista as $index => $item) {
            foreach ($sede_operativa as $cod_sede_operativa => $valor) {
                // Sumamos todos los total de cada Sede Provincial
                if ($cod_sede_operativa === $item['cod_sede_operativa']) {
                    $sede_operativa[$cod_sede_operativa] += $item['local'];
                    continue;
                }
                if ( in_array($item['cod_sede_operativa'], $lima_met) ) {
                    $sede_operativa['666'] += $item['local'];
                }
            }
        }

        return array(
            'name'  => 'Provincial',
            'data'  => array_values($sede_operativa),
            'stack' => 'region',
        );
    }

    private function meta_consecucion_distrital($local_lista, $meta_distrital_lista)
    {
        // Obtenemos un array limpio de todas las sede operativas
        $sede_operativa = array_fill_keys(array_column($local_lista, 'cod_sede_operativa'), 0);
        $lima_met       = array('151','152','153','154','155','156','157');

        foreach ($meta_distrital_lista as $item) {
            if ( in_array($item['cod_sede_operativa'], $lima_met) ) {
                $sede_operativa['666'] += $item['local'];
                continue;
            }

            foreach ($sede_operativa as $cod_sede_operativa => $valor) {
                // Sumamos todos los total de cada Sede Distrital
                if ($cod_sede_operativa === $item['cod_sede_operativa']) {
                    $sede_operativa[$cod_sede_operativa] += $item['local'];
                }
            }
        }

        return array(
            'name'  => 'Distrital',
            'data'  => array_values($sede_operativa),
            'stack' => 'region',
        );
    }

    private function sedes_operativas()
    {

    }
}
