<?php

/**
 * Seguridad Entity
 */
class Seguridad extends Entity
{
    private $id_seguridad;
    public $dni;
    public $nombres;
    public $apellidos;
    public $telef_personal;
    public $email;
    public $estado;

    /**
     * Llenar atributos con data
     */
    public function __construct($data = array())
    {
        parent::__construct();
        foreach ($data as $key => $value) {

            if ($value !== '') {
                $this->{$key} = $value;
            }

            if ($key === 'id_seguridad') {
                $this->setId($value);
            }
        }
    }

    public function setId($id)
    {
        $this->id_seguridad = $id;
    }

    public function getId()
    {
        return $this->id_seguridad;
    }
}
