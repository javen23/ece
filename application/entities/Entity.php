<?php

/**
 * Entity
 */
class Entity
{
    protected $ci;

    public $lastIP;
    public $navegador;
    public $usuarioCrea;
    public $fechaCrea;
    public $usuarioModifica;
    public $fechaModifica;

    /**
     * Llenar atributos con data
     */
    public function __construct()
    {
        $this->ci = &get_instance();
        $this->lastIP       = $this->ci->input->ip_address();
        $this->navegador    = $this->ci->input->user_agent();
        $this->usuarioCrea  = $this->ci->session->userdata('userNameUsuario');
        $this->fechaCrea    = date('Y-m-d H:i:s');
        $this->usuarioModifica  = $this->ci->session->userdata('userNameUsuario');
        $this->fechaModifica    = date('Y-m-d H:i:s');
    }

    public function getAuditorias()
    {
        return $this->usuarioCrea .' | '. $this->fechaCrea;
    }
}
