<?php

/**
 * Computadora Entity
 */
class Computadora // extends Entity
{
    private $id_pc;
    public $modelo;
    public $capacidad;
    public $serie;

    /**
     * Llenar atributos con data
     */
    public function __construct($data = array())
    {
        // parent::__construct();
        foreach ($data as $key => $value) {

            if ($value !== '') {
                $this->{$key} = $value;
            }
            if ($key === 'id_pc') {
                $this->setId($value);
            }
        }
    }

    public function setId($id)
    {
        $this->id_pc = $id;
    }

    public function getId()
    {
        return $this->id_pc;
    }
}
