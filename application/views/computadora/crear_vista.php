<div class="titulo text-center"><?php echo mb_strtoupper($titulo, 'UTF-8') ?></div>
<div class="cuerpo" style="width: 750px">
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_computadora" method="POST" action="<?php echo $form_ruta ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-offset-1 col-xs-10">
                <?php foreach ($elementos as $key => $item): ?>
                    <div class="row">
                        <fieldset>
                            <legend class="fieldset-legend-title">INFORMACIÓN DE LA COMPUTADORA <big>#<?php echo $key; ?></big> </legend>
                            <?php if ($item->getId() !== ''): ?>
                                <input type="hidden" name="item[<?php echo $key; ?>][id_pc]" value="<?php echo $item->getId(); ?>">
                            <?php endif; ?>
                            <div class="form-group">
                                <label for="" class="col-xs-3 form-label"><span class="pull-right">MODELO</span></label>
                                <div class="col-xs-9 sinpadding">
                                    <input type="text" class="col-xs-12 form-control requerido text-uppercase" value="<?php echo $item->modelo; ?>" name="item[<?php echo $key; ?>][modelo]" id="local_pc_modelo" placeholder="LAPTOP / ESCRITORIO"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-xs-3 form-label"><span class="pull-right">CAPACIDAD</span></label>
                                <div class="col-xs-9 sinpadding">
                                    <select name="item[<?php echo $key; ?>][capacidad]" id="local_pc_capacidad" class="select2 requerido col-xs-12" title="Seleccione opción">
                                        <option value="" >-SELECCIONE-</option>
                                        <?php foreach($this->dataselect->pc_capacidad() as $elem): ?>
                                            <option
                                                value="<?php show($elem, 'id') ?>"
                                                <?php echo ($item->capacidad === $elem['id']) ? 'selected' : ''; ?>
                                            >
                                                <?php show($elem, 'desc') ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-xs-3 form-label"><span class="pull-right">N° DE SERIE</span></label>
                                <div class="col-xs-9 sinpadding">
                                    <input type="text" class="col-xs-12 form-control requerido text-uppercase onlyNumbers" value="<?php echo $item->serie; ?>" maxlength="12" name="item[<?php echo $key; ?>][serie]" id="local_pc_num_serie" placeholder="012345678"/>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                <?php endforeach; ?>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="hidden_key" type="hidden"  value="valorHiddenLocal"/>
                    <input type="submit" class="btn btn-guardar btn-flat" id="btn_guardar" data-loading-text="GUARDANDO..." value="GUARDAR" name="crearLocal" id="btnCreateLocal">
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        //$('.maskDecimal').autoNumeric('init');
        $('.select2').select2();
        $('[data-mask]').inputmask();
        var _form = $('#form_computadora');
        var btn_guardar = $('#btn_guardar');

        _form.validate();

        _form.on('submit.form', function(ev){
            ev.preventDefault();

            var data = _form.serialize();
            console.dir(data);

            $.ajax({
              type: "POST",
              url: this.action,
              data: data,
              beforeSend: function(data){
                  btn_guardar.button('loading');
              },
              success: function(data){
                  console.dir(data);

                  if (data.success) {
                    document.getElementById('listado_computadoras').value = data.computadoras;
                  }
              },
              error: function(){},
              complete: function(){
                  cerrar_facebox();
                  btn_guardar.button('reset');
              },
            });
        });

    });
</script>
