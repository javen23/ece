
<section class="content">
  <div class="box noBox">
    <div class="box-header">
      <h3 class="box-title">LISTADO DE ASISTENCIA<span class="inline-block">
          <form action="<?php echo base_url('ajax-exportar-tabla') ?>" method="post" target="_blank" id="form_exportar_tabla">
            <span class="pull-right cursor-pointer" data-toggle="tooltip" title="Descargar <?php echo $titulo ?>"><i class="fa fa-file-excel-o exportar_tabla_reporte text-green"></i></span>
            <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
            <input type="hidden" name="nombre_archivo" value="<?php echo url_amigable(notildes($titulo)) ?>">
          </form>
        </span></h3>
    </div>
    <div class="box-body table-responsive">
      <table width="100%" id="tablaListado" class="display table table-bordered table-striped table-hover" >
        <thead class="headTablaListado">
          <tr class="text-uppercase th-head-inputs">
            <th>N°</th>
            <th>LOCAL</th>
            <th>CARGO</th>
            <th>DNI</th>
            <th>APELLIDOS Y NOMBRES</th>
            <th>ASISTENCIA</th>
          </tr>
        </thead>
        <tfoot class="footTablaListado">
          <tr class="text-uppercase tfoot-placeholder">
            <th>N°</th>
            <th>LOCAL</th>
            <th>CARGO</th>
            <th>DNI</th>
            <th>APELLIDOS Y NOMBRES</th>
            <th>ASISTENCIA</th>
          </tr>
        </tfoot>
        <tbody class="bodyTablaListado">

        </tbody>
      </table>
    </div>
  </div>
</section>
<script>
    $(function () {
        tablaListadoDataTable();
    });
</script>
