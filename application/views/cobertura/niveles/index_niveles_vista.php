<section class="content">
	<div class="row">

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('cobertura/niveles/nivel-uno') ?>" data-toggle="tooltip" title="CAPACITACIÓN DE NIVEL I">
				<div class="info-box">
					<span class="info-box-icon bg-maroon"><i class="fa fa-circle-o-notch"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">NIVEL I</span>
						<span class="info-box-number">Capacitación de Nivel I</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('cobertura/niveles/nivel-dos') ?>" data-toggle="tooltip" title="CAPACITACIÓN DE NIVEL II">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="fa fa-circle-o-notch"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">NIVEL II</span>
						<span class="info-box-number">Capacitación de Nivel II</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('cobertura/niveles/nivel-tres') ?>" data-toggle="tooltip" title="CAPACITACIÓN DE NIVEL III">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-circle-o-notch"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">NIVEL III</span>
						<span class="info-box-number">Capacitación de Nivel III</span>
					</div>
				</div>
			</a>
		</div>

	</div>
</section>