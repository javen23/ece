<section class="content">
	<div class="row">

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('cobertura/directores') ?>" data-toggle="tooltip" title="CAPACITACION DE DIRECTORES">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-circle-o-notch"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">DIRECTORES</span>
						<span class="info-box-number">Capacitación de Directores</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('cobertura/instructores') ?>" data-toggle="tooltip" title="CAPACITACION DE INSTRUCTORES">
				<div class="info-box">
					<span class="info-box-icon bg-purple"><i class="fa fa-circle-o-notch"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">INSTRUCTORES</span>
						<span class="info-box-number">Capacitación de Instructores</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('cobertura/niveles') ?>" data-toggle="tooltip" title="Capacitación Nivel-I, Nivel-II, Nivel-III">
				<div class="info-box">
					<span class="info-box-icon bg-purple"><i class="fa fa-circle-o-notch"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">NIVELES</span>
						<span class="info-box-number">Capacitación Nivel-I, Nivel-II, Nivel-III</span>
					</div>
				</div>
			</a>
		</div>

	</div>
</section>
