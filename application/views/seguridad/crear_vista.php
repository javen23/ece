<div class="titulo text-center"><?php echo mb_strtoupper($titulo, 'UTF-8') ?></div>
<div class="cuerpo" style="width: 750px">
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_seguridad" method="POST" action="<?php echo $form_ruta ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-offset-1 col-xs-10">
                <?php foreach ($elementos as $key => $item): ?>
                    <div class="row">
                        <fieldset>
                            <legend class="fieldset-legend-title">INFORMACIÓN DEL PERSONAL <big>#<?php echo $key; ?></big> </legend>
                            <?php if ($item->getId() !== ''): ?>
                                <input type="hidden" name="item[<?php echo $key; ?>][id_seguridad]" value="<?php echo $item->getId(); ?>">
                            <?php endif; ?>
                            <div class="form-group">
                                <label for="" class="col-xs-3 form-label"><span class="pull-right">NOMBRES</span></label>
                                <div class="col-xs-9 sinpadding">
                                    <input type="text" class="col-xs-12 form-control requerido text-uppercase" value="<?php echo $item->nombres; ?>" name="item[<?php echo $key; ?>][nombres]" id="local_seguridad_nombres" placeholder="NOMBRES"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-xs-3 form-label"><span class="pull-right">APELLIDOS</span></label>
                                <div class="col-xs-9 sinpadding">
                                    <input type="text" class="col-xs-12 form-control requerido text-uppercase" value="<?php echo $item->apellidos; ?>" name="item[<?php echo $key; ?>][apellidos]" id="local_seguridad_apellidos" placeholder="APELLIDOS"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-xs-3 form-label"><span class="pull-right">DNI</span></label>
                                <div class="col-xs-9 sinpadding">
                                    <input type="text" class="col-xs-12 form-control requerido text-uppercase onlyNumbers" value="<?php echo $item->dni; ?>" name="item[<?php echo $key; ?>][dni]" id="local_seguridad_dni" maxlength="8" placeholder="12345678"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-xs-3 form-label"><span class="pull-right">CELULAR</span></label>
                                <div class="col-xs-9 sinpadding">
                                    <input type="text" class="col-xs-12 form-control requerido text-uppercase onlyNumbers" value="<?php echo $item->telef_personal; ?>" name="item[<?php echo $key; ?>][telef_personal]" id="local_seguridad_celular" maxlength="9" placeholder="912345678"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-xs-3 form-label"><span class="pull-right">EMAIL</span></label>
                                <div class="col-xs-9 sinpadding">
                                    <input type="text" class="col-xs-12 form-control requerido email text-uppercase" value="<?php echo $item->email; ?>" name="item[<?php echo $key; ?>][email]" id="local_seguridad_email" placeholder="EMAIL@EMAIL.COM"/>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                <?php endforeach; ?>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="hidden_key" type="hidden"  value="valorHiddenLocal"/>
                    <input type="submit" class="btn btn-guardar btn-flat" id="btn_guardar" data-loading-text="GUARDANDO..." value="GUARDAR" name="crearLocal" id="btnCreateLocal">
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        //$('.maskDecimal').autoNumeric('init');
        $('.select2').select2();
        $('[data-mask]').inputmask();
        var _form = $('#form_seguridad');
        var btn_guardar = $('#btn_guardar');

        _form.validate();

        _form.on('submit.form', function(ev){
            ev.preventDefault();

            var data = _form.serialize();
            console.dir(data);

            $.ajax({
              type: "POST",
              url: this.action,
              data: data,
              beforeSend: function(data){
                  btn_guardar.button('loading');
              },
              success: function(data){
                  console.dir(data);
                  if (data.success) {
                    document.getElementById('listado_seguridad').value = data.seguridad;
                  }

              },
              error: function(){},
              complete: function(){
                  cerrar_facebox();
                  btn_guardar.button('reset');
              },
            });
        });

    });
</script>
