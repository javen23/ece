<section class="content">
	<div class="row">

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('segmentacion/reportes/sede-operativa') ?>" data-toggle="tooltip" title="REPORTE DE PRESUPUESTO POR REGION OPERATIVA">
				<div class="info-box">
					<span class="info-box-icon bg-blue"><i class="fa fa-indent"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Región</span>
						<span class="info-box-number">Reporte de presupuesto por Región Operativa</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('segmentacion/reportes/provincia-operativa') ?>" data-toggle="tooltip" title="REPORTE DE PRESUPUESTO POR PROVINCIA OPERATIVA">
				<div class="info-box">
					<span class="info-box-icon bg-maroon"><i class="fa fa-indent"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Provincia</span>
						<span class="info-box-number">Reporte de presupuesto por Provincia Operativa</span>
					</div>
				</div>
			</a>
		</div>
<!--
		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('segmentacion/reportes/distrito-operativa') ?>" data-toggle="tooltip" title="REPORTE DE PRESUPUESTO POR DISTRITO OPERATIVA">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="fa fa-indent"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Distrito</span>
						<span class="info-box-number">Reporte de presupuesto por Distrito Operativa</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('segmentacion/reportes/asistente') ?>" data-toggle="tooltip" title="REPORTE DE PRESUPUESTO ASISTENTE">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-indent"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Asistente</span>
						<span class="info-box-number">Reporte de presupuesto por Asistente</span>
					</div>
				</div>
			</a>
		</div> -->

	</div>
</section>
