<section class="content">
   <div class="row">
      <div class="col-md-12 col-sm-12">
         <div class="box noBox">
            <div class="box-header">
               <div class="reporte-opciones margin-bottom-20 col-md-12 no-padding" style="display: table;">
                  <div class="col-md-4 col-sm-4 col-xs-12 no-padding ">
                     <label for="sede_reporte" class="col-xs-12">SEDE OPERATIVA</label>
                     <select class="col-xs-12  select2 text-uppercase" name="sede_reporte" id="sede_reporte" multiple>
                        <option value="0">TODOS</option>
                        <?php foreach ($total_sedes as $sede) : ?>
                        <option value="<?php echo $sede['cod_sede_operativa'] ?>"><?php echo strtoupper($sede['sede_operativa']) ?></option>
                        <?php endforeach; ?>
                     </select>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12 ">
                     <label for="nivel_reporte" class="col-xs-12">NIVEL</label>
                     <select class="col-xs-12 form-control text-uppercase" name="nivel_reporte" id="nivel_reporte" >
                        <option value="0" >TODOS</option>
                        <option value="Primaria" >PRIMARIA</option>
                        <option value="Secundaria" >SECUNDARIA</option>
                     </select>
                  </div>
                  <div class="col-md-4 col-sm-2 col-xs-12 text-center no-padding ">
                     <button class="btn btn-flat btn-bitbucket btn-sm" id="btn-filtrar-reporte-sede"><span class="fa fa-filter"></span> FILTRAR</button>
                     <a href="javascript:;" class="btn btn-flat btn-bitbucket btn-sm" id="btn-exportar-reporte-sede"><span class="fa fa-cloud-download"></span> EXPORTAR</a>
                  </div>
               </div>
            </div>
            <div class="box-body table-responsive" id="resultado_filtro">
               <table width="100%" id="tablaListado" class="table table-bordered table-striped table-hover" >
                  <thead class="headTablaListado">
                     <tr class="text-uppercase">
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class="border-th-header-right"></th>
                        <th colspan="7">INSTITUCIÓN EDUCATIVA</th>
                        <th class="border-th-header-left"></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class="border-th-header-left"></th>
                        <th colspan="5" class="border-th-header-right"><i class="fa fa-usd text-success"></i> Asignación (S./)</th>
                        
                     </tr>
                     <tr class="text-uppercase th-head-inputs">
                        <th>N°</th>
                        <th>Cod. <br/>Asistente</th>
                        <th>Sede <br/>Operativa</th>
                        <th>Sede <br/>Provincial</th>
                        <th>Sede <br/>Distrital</th>
                        <th>Departamento</th>
                        <th>Provincia</th>
                        <th>Nombre de UGEL</th>
                        <th>Distrito</th>
                        <th>Centro Poblado</th>
                        <th>Cod. Mod.</th>
                        <th>Nombre I.E</th>
                        <th>Dirección I.E</th>
                        <th>Área</td>
                        <th>Total Secciones</td>
                        <th>Nivel</td>
                        <th>Tipo de Evaluación</th>
                        <th>Total <br/>Aplicadores</th>
                        <th>Dias <br/>Aplicación</th>
                        <th>Dias <br/>Viaje</th>
                        <th>Pasaje</th>
                        <th>Movilidad</th>
                        <th>Total Gastos Oper.</th>
                        <th>Total Pasajes</th>
                        <th>Total Movilidad</th>
                        <th>Total Bonificación</th>
                        <th>Total I.E</th>
                     </tr>
                  </thead>
                  <tbody class="bodyTablaListado text-center">
                     <tr>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td>10</td>
                         <td></td>
                         <td></td>
                         <td>81</td>
                         <td></td>
                         <td></td>
                         <td><?php echo $ie['ie'] ?></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td><i class="fa fa-usd pull-left text-success"></i></td>
                         <td><i class="fa fa-usd pull-left text-success"></i></td>
                         <td><i class="fa fa-usd pull-left text-success"></i></td>
                         <td><i class="fa fa-usd pull-left text-success"></i></td>
                         <td><i class="fa fa-usd pull-left text-success"></i></td>
                      </tr>
                  <?php  $n = 1;
                  foreach ($reporte_asistente as $reporte) : ?>
                     <tr>
                        <td><?php echo $n++; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><i class="fa fa-usd pull-left text-success"></i></td>
                        <td><i class="fa fa-usd pull-left text-success"></i></td>
                        <td><i class="fa fa-usd pull-left text-success"></i></td>
                        <td><i class="fa fa-usd pull-left text-success"></i></td>
                        <td><i class="fa fa-usd pull-left text-success"></i></td>
                     </tr>
                  <?php endforeach;?>
                  </tbody>
               </table>
               <!-- <div class="box-body table-responsive" id="resultado_filtro">
                  <?php $this->load->view('segmentacion/reporte/asistente/asistente_operativa_vista_ajax', array('reporte_asistente'=>$reporte_asistente, 'total_reporte'=>$total_reporte)); ?>
               </div> -->
            </div>
         </div>
      </div>
   </div>
</section>
<script>
$(function () {
   $('.select2').select2();
   tablaListadoDataTable();
});
</script>
