<table width="100%" id="tablaListado" class="table table-bordered table-striped table-hover" >
                  <thead class="headTablaListado">
                     <tr class="text-uppercase">
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class="border-th-header-right"></th>
                        <th colspan="7">INSTITUCIÓN EDUCATIVA</th>
                        <th class="border-th-header-left"></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class="border-th-header-left"></th>
                        <th colspan="5" class="border-th-header-right"><i class="fa fa-usd text-success"></i> Asignación (S./)</th>
                        
                     </tr>
                     <tr class="text-uppercase th-head-inputs">
                        <th>N°</th>
                        <th>Cod. <br/>Asistente</th>
                        <th>Sede <br/>Operativa</th>
                        <th>Sede <br/>Provincial</th>
                        <th>Sede <br/>Distrital</th>
                        <th>Departamento</th>
                        <th>Provincia</th>
                        <th>Nombre de UGEL</th>
                        <th>Distrito</th>
                        <th>Centro Poblado</th>
                        <th>Cod. Mod.</th>
                        <th>Nombre I.E</th>
                        <th>Dirección I.E</th>
                        <th>Área</td>
                        <th>Total Secciones</td>
                        <th>Nivel</td>
                        <th>Tipo de Evaluación</th>
                        <th>Total <br/>Aplicadores</th>
                        <th>Dias <br/>Aplicación</th>
                        <th>Dias <br/>Viaje</th>
                        <th>Pasaje</th>
                        <th>Movilidad</th>
                        <th>Total Gastos Oper.</th>
                        <th>Total Pasajes</th>
                        <th>Total Movilidad</th>
                        <th>Total Bonificación</th>
                        <th>Total I.E</th>
                     </tr>
                  </thead>
                  <tbody class="bodyTablaListado text-center">
                     <tr>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td>10</td>
                         <td></td>
                         <td></td>
                         <td>81</td>
                         <td></td>
                         <td></td>
                         <td>99</td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td><i class="fa fa-usd pull-left text-success"></i></td>
                         <td><i class="fa fa-usd pull-left text-success"></i></td>
                         <td><i class="fa fa-usd pull-left text-success"></i></td>
                         <td><i class="fa fa-usd pull-left text-success"></i></td>
                         <td><i class="fa fa-usd pull-left text-success"></i></td>
                      </tr>
                  <?php  $n = 1;
                  foreach ($reporte_asistente as $reporte) : ?>
                     <tr>
                        <td><?php echo $n++; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><i class="fa fa-usd pull-left text-success"></i></td>
                        <td><i class="fa fa-usd pull-left text-success"></i></td>
                        <td><i class="fa fa-usd pull-left text-success"></i></td>
                        <td><i class="fa fa-usd pull-left text-success"></i></td>
                        <td><i class="fa fa-usd pull-left text-success"></i></td>
                     </tr>
                  <?php endforeach;?>
                  </tbody>
               </table>