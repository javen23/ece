<!-- <pre>
   <?php print_r($reporte_distrito[0]); ?>
</pre>
<pre>
   <?php print_r($total_reporte); ?>
</pre> -->
<section class="content">
   <div class="row">
      <div class="col-md-12 col-sm-12">
         <div class="box noBox">
            <div class="box-header">
               <div class="reporte-opciones margin-bottom-20 col-md-12 no-padding" style="display: table;">
                  <div class="col-md-4 col-sm-4 col-xs-12 no-padding ">
                     <label for="sede_reporte" class="col-xs-12">SEDE OPERATIVA</label>
                     <select class="col-xs-12  select2 text-uppercase" name="sede_reporte" id="sede_reporte" multiple>
                        <option value="0">TODOS</option>
                        <?php foreach ($total_sedes as $sede) : ?>
                        <option value="<?php echo $sede['cod_sede_operativa'] ?>"><?php echo strtoupper($sede['sede_operativa']) ?></option>
                        <?php endforeach; ?>
                     </select>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12 ">
                     <label for="nivel_reporte" class="col-xs-12">NIVEL</label>
                     <select class="col-xs-12 form-control text-uppercase" name="nivel_reporte" id="nivel_reporte" >
                        <option value="0" >TODOS</option>
                        <option value="Primaria" >PRIMARIA</option>
                        <option value="Secundaria" >SECUNDARIA</option>
                     </select>
                  </div>
                  <div class="col-md-4 col-sm-2 col-xs-12 text-center no-padding ">
                     <button class="btn btn-flat btn-bitbucket btn-sm" id="btn-filtrar-reporte-sede"><span class="fa fa-filter"></span> FILTRAR</button>
                     <a href="javascript:;" class="btn btn-flat btn-bitbucket btn-sm" id="btn-exportar-reporte-sede"><span class="fa fa-cloud-download"></span> EXPORTAR</a>

                  </div>
               </div>
            </div>
            <div class="box-body table-responsive" id="resultado_filtro">
               <table width="100%" id="tablaListado" class="table table-bordered table-striped table-hover" >
                  <thead class="headTablaListado">
                     <tr class="text-uppercase">
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class="border-th-header-right"></th>
                        <th colspan="3" class="border-th-header-right">Aplicadores Primaria</th>
                        <th colspan="2" class=" border-th-header-right">Aplicadores Secundaria</th>
                        <th class="border-th-header-right"></th>
                        <th colspan="5" class="border-th-header-right"> ASIGNACIÓN S./</th>
                     </tr>
                     <tr class="text-uppercase th-head-inputs">
                        <th>N°</th>
                        <th>Cod. Sede <br/>Operativa</th>
                        <th>Sede <br/>Operativa</th>
                        <th>Sede <br/>Provincial</th>
                        <th>Sede <br/>Distrital</th>
                        <th>Total <br/>Instituciones</th>
                        <th>Total <br/>Secciones</th>
                        <th>Total <br/>Asistentes</th>
                        <th>Cant. ECE</th>
                        <th>Cant. MC</th>
                        <th>Cant. EIB. L2</th>
                        <th>Cant. ECE</th>
                        <th>Cant. MC</th>
                        <th>Total <br/>Aplicadores</th>
                        <th>Total <br/>Gastos Operativos</th>
                        <th>Total <br/>Pasaje</th>
                        <th>Total <br/>Movilidad</th>
                        <th>Total <br/>Bonificación</th>
                        <th>Total </th>
                     </tr>
                  </thead>
                  <tbody class="bodyTablaListado text-center">
                     <tr>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td><?php echo number_format($total_reporte['total_instituciones'], 2); ?></td>
                         <td><?php echo number_format($total_reporte['total_secciones'], 2); ?></td>
                         <td><?php echo number_format($total_reporte['total_aplicadores'], 2); ?></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td><i class="fa fa-usd pull-left text-success"></i> <?php echo number_format($total_reporte['total_gastos_operativos'], 2); ?></td>
                         <td><i class="fa fa-usd pull-left text-success"></i> <?php echo number_format($total_reporte['total_pasaje'], 2); ?></td>
                         <td><i class="fa fa-usd pull-left text-success"></i> <?php echo number_format($total_reporte['total_movilidad'], 2); ?></td>
                         <td><i class="fa fa-usd pull-left text-success"></i> <?php echo number_format($total_reporte['total_bonificacion'], 2); ?></td>
                         <td><i class="fa fa-usd pull-left text-success"></i> <?php echo number_format($total_reporte['total'], 2); ?></td>
                      </tr>
                  <?php  $n = 1;
                  foreach ($reporte_distrito as $reporte) : ?>
                     <tr>
                        <td><?php echo $n++; ?></td>
                        <td><?php echo $reporte['codigo_sede'] ?></td>
                        <td class="text-left"><?php echo $reporte['nombre_sede'] ?></td>
                        <td><?php ?></td>
                        <td><?php ?></td>
                        <td><?php echo $reporte['total_instituciones'] ?></td>
                        <td><?php echo $reporte['total_secciones'] ?></td>
                        <td><?php echo $reporte['total_aplicadores'] ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><i class="fa fa-usd pull-left text-success"></i> <?php echo $reporte['total_gastos_operativos'] ?></td>
                        <td><i class="fa fa-usd pull-left text-success"></i> <?php echo $reporte['total_pasaje'] ?></td>
                        <td><i class="fa fa-usd pull-left text-success"></i> <?php echo $reporte['total_movilidad'] ?></td>
                        <td><i class="fa fa-usd pull-left text-success"></i> <?php echo $reporte['total_bonificacion'] ?></td>
                        <td><i class="fa fa-usd pull-left text-success"></i> <?php echo $reporte['total'] ?></td>
                     </tr>
                  <?php endforeach;?>
                  </tbody>
               </table>
               <!-- <div class="box-body table-responsive" id="resultado_filtro">
                  <?php $this->load->view('segmentacion/reporte/distrito/distrito_operativa_vista_ajax', array('reporte_distrito'=>$reporte_distrito, 'total_reporte'=>$total_reporte)); ?>
               </div> -->
            </div>
         </div>
      </div>
   </div>
</section>
<script>
$(function () {
   $('.select2').select2();
   tablaListadoDataTable();
});
</script>
