<section class="content">
   <div class="row">
      <div class="col-md-12 col-sm-12">
         <div class="box noBox">
            <div class="box-header">
               <div class="reporte-opciones margin-bottom-20 col-md-12 no-padding" style="display: table;">
                  <div class="col-md-3 col-sm-6 col-xs-12 no-padding ">
                     <label for="sede_reporte" class="col-xs-12">SEDE REGIÓN</label>
                     <select class="col-xs-12  select2 text-uppercase" name="sede_reporte" id="sede_reporte" multiple>
                        <option value="0">TODOS</option>
                        <?php foreach ($total_sedes as $sede) : ?>
                        <option value="<?php echo $sede['cod_sede_operativa'] ?>"><?php echo strtoupper($sede['sede_operativa']) ?></option>
                        <?php endforeach; ?>
                     </select>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12 ">
                     <label for="nivel_reporte" class="col-xs-12">NIVEL</label>
                     <select class="col-xs-12 form-control text-uppercase" name="nivel_reporte" id="nivel_reporte" >
                        <option value="0" >TODOS</option>
                        <?php $grados = array('Primaria'=>'PRIMARIA', 'Secundaria'=>'SECUNDARIA'); ?>
                        <?php foreach($grados as $key => $grado): ?>
                            <option value="<?php echo $key ?>"><?php echo $grado ?></option>
                        <?php endforeach; ?>
                     </select>
                  </div>
                  <!-- <div class="col-md-3 col-sm-6 col-xs-12 ">
                     <label for="nivel_reporte" class="col-xs-12">TIPO EVALUACIÓN</label>
                     <select class="col-xs-12 form-control text-uppercase" name="evaluacion_reporte" id="evaluacion_reporte" >
                        <option value="0" >TODOS</option>
                        <?php $evaluaciones = array('ECE'=>'ECE', 'EIB-L2'=>'EIB-L2', 'MC'=>'MC'); ?>
                        <?php foreach($evaluaciones as $key => $item): ?>
                            <option value="<?php echo $key ?>"><?php echo $item ?></option>
                        <?php endforeach; ?>
                     </select>
                  </div> -->
                  <div class="col-md-3 col-sm-6 col-xs-12 text-center no-padding ">
                     <label for="" class="col-xs-12">ACCIONES</label>
                     <div class="col-xs-12 text-center">
                        <div class="col-xs-6 text-center">
                           <button class="btn btn-flat btn-bitbucket btn-sm" id="btn-filtrar-reporte-sede"><span class="fa fa-filter"></span> FILTRAR</button>
                        </div>
                        <div class="col-xs-6 text-center">
                           <a href="javascript:;" class="btn btn-flat bg-green btn-sm" id="btn-exportar-reporte-sede"><span class="fa fa-cloud-download"></span> EXPORTAR</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-body table-responsive" id="resultado_filtro">
               <?php $this->load->view('segmentacion/reporte/sede/sede_operativa_vista_ajax', array('resultado_reporte'=>$resultado_reporte, 'total_reporte'=>$total_reporte)); ?>
            </div>
         </div>
      </div>
   </div>
</section>
<script>
$(function () {
   $('.select2').select2();
   tablaListadoDataTable();
   var listado_filter = $('#tablaListado_filter');
   listado_filter.remove();

    $('#btn-filtrar-reporte-sede').on('click.limpiar', function(){
        listado_filter.remove();
    });
});
</script>
