<table width="100%" id="tablaListado" class="table table-bordered table-striped table-hover" >
   <thead class="headTablaListado">
      <tr class="text-uppercase">
         <th colspan="6" class=" border-th-header-right"></th>
         <th colspan="5"><i class="fa fa-usd text-success"></i> ASIGNACIÓN</th>
      </tr>
      <tr class="text-uppercase th-head-inputs">
         <th>N°</th>
         <th>Cod. Sede <br/>Operativa</th>
         <th>Sede <br/>Operativa</th>
         <th>Total <br/>Instituciones</th>
         <th>Total <br/>Secciones</th>
         <th>Total <br/>Aplicadores</th>
         <th>Total <br/>Gastos Operativos</th>
         <th>Total <br/>Pasaje</th>
         <th>Total <br/>Movilidad</th>
         <th>Total <br/>Bonificación</th>
         <th>Total </th>
      </tr>
   </thead>
   <tbody class="bodyTablaListado text-center">
       <tr>
          <td></td>
          <td></td>
          <td class="text-left"></td>
          <td></td>
          <td></td>
          <td></td>
          <td><i class="fa fa-usd pull-left text-success"></i> <?php echo number_format($total_reporte['total_gastos_operativos'], 2); ?></td>
          <td><i class="fa fa-usd pull-left text-success"></i> <?php echo number_format($total_reporte['total_pasaje'], 2); ?></td>
          <td><i class="fa fa-usd pull-left text-success"></i> <?php echo number_format($total_reporte['total_movilidad'], 2); ?></td>
          <td><i class="fa fa-usd pull-left text-success"></i> <?php echo number_format($total_reporte['total_bonificacion'], 2); ?></td>
          <td><i class="fa fa-usd pull-left text-success"></i> <?php echo number_format($total_reporte['total'], 2); ?></td>
       </tr>
   <?php  $n=1;
   foreach ($resultado_reporte as $reporte) : ?>
      <tr>
         <td><?php echo $n++; ?></td>
         <td><?php echo $reporte['codigo_sede'] ?></td>
         <td class="text-left"><?php echo $reporte['nombre_sede'] ?></td>
         <td><?php echo $reporte['total_instituciones'] ?></td>
         <td><?php echo $reporte['total_secciones'] ?></td>
         <td><?php echo $reporte['total_aplicadores'] ?></td>
         <td><i class="fa fa-usd pull-left text-success"></i> <?php echo $reporte['total_gastos_operativos'] ?></td>
         <td><i class="fa fa-usd pull-left text-success"></i> <?php echo $reporte['total_pasaje'] ?></td>
         <td><i class="fa fa-usd pull-left text-success"></i> <?php echo $reporte['total_movilidad'] ?></td>
         <td><i class="fa fa-usd pull-left text-success"></i> <?php echo $reporte['total_bonificacion'] ?></td>
         <td><i class="fa fa-usd pull-left text-success"></i> <?php echo $reporte['total'] ?></td>
      </tr>
   <?php endforeach;?>
   </tbody>
</table>
