<section class="content">
	<div class="row">

		<div class="col-md-4 col-sm-12">
			<a href="<?php #echo base_url('segmentacion/avance-programacion') ?>" data-toggle="tooltip" title="AVANCE DE PROGRAMACIÓN">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-indent"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Programación</span>
						<span class="info-box-number">Avance de programación</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('segmentacion/listado-ingresos') ?>" data-toggle="tooltip" title="LISTADO DE INGRESOS SEGMENTACIÓN">
				<div class="info-box">
					<span class="info-box-icon bg-purple"><i class="fa fa-indent"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Listado</span>
						<span class="info-box-number">Listado de ingresos</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('segmentacion/reportes') ?>" data-toggle="tooltip" title="REPORTES">
				<div class="info-box">
					<span class="info-box-icon bg-maroon"><i class="fa fa-indent"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Reportes</span>
						<span class="info-box-number">Listado de reportes</span>
					</div>
				</div>
			</a>
		</div>

	</div>
</section>
