<style media="screen">
    .th-editable {
        background-color: #4ECDC4;
    }
    .th-cod-modular {
        background-color: #BF55EC;
        color: #fff;
    }
</style>
<section class="content">
    <div class="box noBox">
        <div class="box-header" id="boxHeader">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-4 text-center">
                    <!-- <?php echo date('d/m/Y'); ?> -->
                    <div class="bg-navy bold">FECHA <i class="fa fa-clock-o"></i> : <span v-text="fecha"></span></div>
                </div>
            </div>
        </div>
        <div class="box-body table-responsive" id="contenedorTable">
            <?php $this->load->view('segmentacion/programacion/datatable_vista', array('locales'=>$locales)); ?>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        llamadasAjaxUbigeo();

        var boxHeader = new Vue({
            el: '#boxHeader',
            data: {
                fecha: (new Date()).toISOString()
            },
            ready: function(el) {
                console.dir(el);
                var that = this;

                setInterval(function(){
                    that.fecha = (new Date()).toISOString();
                }, 2000);
            }
        });

        var tablaListado;
        var calcularPorcentaje = function(dividendo, divisor) {
            return (100/(dividendo/divisor)).toFixed(1);
        };

        tablaListado = new Vue({
            el: '#contenedorTable',
            data: {
                isDataVisible: false,
                primaria: {
                    porcentaje: [],
                    avance: [],
                },
                secundaria: {
                    porcentaje: [],
                    avance: [],
                },
                total: {
                    porcentaje: [],
                    avance: [],
                },
                sumatoria: <?php echo json_encode($total); ?>,
                sumatoriaBackup: <?php echo json_encode($total); ?>
            },
            computed: {},
            methods: {
                porcentajeDeAvance: function(tipo, index){
                    var programadas = +document.getElementById('td_'+tipo+'_programadas_'+(index+1)).innerHTML;
                    var avance      = +this[tipo].avance[index];

                    this.$$[tipo+'_porcentaje_'+index].innerHTML = calcularPorcentaje(programadas, avance);
                    // this.$set(tipo+'.porcentaje['+index+']', calcularPorcentaje(programadas, avance));
                    console.log(tipo.toUpperCase() +' index:'+index+' avance: ' + avance);
                },
                avanceProgramacion: function(tipo, index){
                    this.porcentajeDeAvance(tipo, index);

                    this.totalTrabajadas(index);
                    this.sumatoriaAvanceTipo(tipo);
                    this.sumatoriaTotalAvance();
                },
                totalTrabajadas: function(index){
                    this.$set('total.avance['+index+']', (+this.primaria.avance[index] + +this.secundaria.avance[index]));
                    this.porcentajeDeAvance('total', index);
                },
                sumatoriaAvanceTipo: function(tipo) {
                    console.log('sumatoriaAvanceTipo : '+tipo);
                    this.sumatoria[tipo].avance = this.sumatoriaBackup[tipo].avance + this[tipo].avance.reduce(function(val, newVal){
                        return +val + newVal;
                    });
                },
                sumatoriaTotalAvance: function() {
                    console.log('calcularSumatoriaAvance');

                    this.sumatoria.primaria.porcentaje      = calcularPorcentaje(this.sumatoria.primaria.programadas, this.sumatoria.primaria.avance);
                    this.sumatoria.secundaria.porcentaje    = calcularPorcentaje(this.sumatoria.secundaria.programadas, this.sumatoria.secundaria.avance);

                    this.sumatoria.total.avance     = this.sumatoria.primaria.avance + this.sumatoria.secundaria.avance;
                    this.sumatoria.total.porcentaje = calcularPorcentaje(this.sumatoria.total.programadas, this.sumatoria.total.avance);
                },
            }
        });

        console.dir(tablaListado);
    });
</script>
