<button type="button" class="btn bg-navy"
    v-on="click: isDataVisible = !isDataVisible">
        <i class="fa"
            v-class="fa-eye: isDataVisible, fa-eye-slash: !isDataVisible">
        </i>
        Ver información de Vue.js
</button>
<pre v-show="isDataVisible">
    {{ $data | json }}
</pre>
<table width="100%" id="tablaListado" class="display table table-bordered table-striped table-hover" >
<thead class="headTablaListado">
    <tr class="text-uppercase">
        <th colspan="4" class="border-th-header-right"></th>
        <th colspan="3" class="border-th-header-right">INSTITUCIONES PRIMARIA</th>
        <th colspan="3" class="border-th-header-right">INSTITUCIONES SECUNDARIA</th>
        <th colspan="3">TOTAL INSTITUCIONES</th>
    </tr>
    <tr class="text-uppercase th-head-inputs">
        <th>N°</th>
        <th>SEDE REGIÓN</th>
        <th>SEDE PROVINCIAL</th>
        <th class="border-th-header-right">SEDE DISTRITAL</th>
        <th>PROGRAMADAS</th>
        <th class="th-editable">TRABAJADAS</th>
        <th class="border-th-header-right">% DE AVANCE</th>
        <th>PROGRAMADAS</th>
        <th class="th-editable">TRABAJADAS</th>
        <th class="border-th-header-right">% DE AVANCE</th>
        <th>PROGRAMADAS</th>
        <th class="th-editable">TRABAJADAS</th>
        <th>% DE AVANCE</th>
    </tr>
</thead>
<tbody id="body_table_ie" class="bodyTablaListado">
    <tr>
        <td colspan="4" class="text-center"><strong>TOTAL</strong></td>
        <!-- PRIMARIA -->
        <td v-text="sumatoria.primaria.programadas" class="text-center"></td>
        <td v-text="sumatoria.primaria.avance" class="text-center"></td>
        <td v-text="sumatoria.primaria.porcentaje" class="text-center"></td>
        <!-- SECUNDARIA -->
        <td v-text="sumatoria.secundaria.programadas" class="text-center"></td>
        <td v-text="sumatoria.secundaria.avance" class="text-center"></td>
        <td v-text="sumatoria.secundaria.porcentaje" class="text-center"></td>
        <!-- TOTAL -->
        <td v-text="sumatoria.total.programadas" class="text-center"></td>
        <td v-text="sumatoria.total.avance" class="text-center"></td>
        <td v-text="sumatoria.total.porcentaje" class="text-center"></td>
    </tr>
    <?php $n=1; ?>
    <?php foreach ($locales as $local): ?>
        <?php $index = $n - 1; ?>
        <tr>
            <td class="text-center"><?php show($n) ?></td>
            <td><?php show($local, 'sede_region') ?></td>
            <td><?php show($local, 'sede_prov') ?></td>
            <td><?php show($local, 'sede_dist') ?></td>
            <!-- PRIMARIA -->
            <td class="text-center" id="td_primaria_programadas_<?php show($n) ?>">
                <?php show($local, 'primaria_programadas'); ?>
            </td>
            <td v-on="change: avanceProgramacion('primaria', <?php show($index) ?>)" class="text-center editable" id="td_primaria_avance_<?php show($n) ?>" data-resultado="primaria_porcentaje">
                <input type="text" v-model="primaria.avance[<?php show($index) ?>]" number class="form-control input-sm onlyNumbers" maxlength="3" value="<?php show($local, 'primaria_avance') ?>">
            </td>
            <td v-el="primaria_porcentaje_<?php show($index) ?>" class="text-center" id="td_primaria_porcentaje_<?php show($n) ?>"><?php echo porcentaje($local['primaria_programadas'], $local['primaria_avance']); ?></td>

            <!-- SECUNDARIA -->
            <td class="text-center" id="td_secundaria_programadas_<?php show($n) ?>">
                <?php show($local, 'secundaria_programadas') ?>
            </td>
            <td v-on="change: avanceProgramacion('secundaria', <?php show($index) ?>)" class="text-center editable" id="td_secundaria_avance_<?php show($n) ?>" data-resultado="secundaria_porcentaje"> <?php #show($local, 'secundaria_avance') ?>
                <input type="text" v-model="secundaria.avance[<?php show($index) ?>]" number class="form-control input-sm onlyNumbers" maxlength="3" value="<?php show($local, 'secundaria_avance') ?>">
            </td>
            <td v-el="secundaria_porcentaje_<?php show($index) ?>" class="text-center" id="td_secundaria_porcentaje_<?php show($n) ?>"><?php echo porcentaje($local['secundaria_programadas'], $local['secundaria_avance']); ?></td>

            <!-- TOTALES -->
            <td class="text-center" id="td_total_programadas_<?php show($n) ?>">
                <?php show($local, 'total_programadas') ?>
            </td>
            <td v-on="change: avanceProgramacion('total', <?php show($index) ?>)" class="text-center editable" id="td_total_avance_<?php show($n) ?>" data-resultado="total_porcentaje"> <?php #show($local, 'total_avance') ?>
                {{ total.avance[<?php show($index) ?>] }}
            </td>
            <td v-el="total_porcentaje_<?php show($index) ?>" class="text-center" id="td_total_porcentaje_<?php show($n) ?>"><?php echo porcentaje($local['total_programadas'], $local['total_avance']); ?></td>
        </tr>
        <?php $n++; ?>
    <?php endforeach; ?>
</tbody>
</table>
