<table width="100%" id="tablaListado" class="display table table-bordered table-striped table-hover" >
<thead class="headTablaListado">
    <tr class="text-uppercase">
        <th colspan="14" class="border-th-header-right">UBIGEO</th>
        <th colspan="5" class="border-th-header-right">INSTITUCIÓN EDUCATIVA</th>
        <th colspan="11" class="border-th-header-right">APLICACIÓN</th>
        <th colspan="5" class="border-th-header-right">ASIGNACIÓN (S./)</th>
    </tr>
    <tr class="text-uppercase th-head-inputs">
        <th>N°</th>
        <th>COD. SEDE REGIÓN</th>
        <th>SEDE REGIÓN</th>
        <th>COD. SEDE PROVINCIA</th>
        <th>SEDE PROVINCIA</th>
        <th>COD. SEDE DISTRITAL</th>
        <th>SEDE DISTRITAL</th>
        <th>COD. UGEL</th>
        <th>UGEL</th>
        <th>DEPARTAMENTO</th>
        <th>PROVINCIA</th>
        <th>DISTRITO</th>
        <th>CENTRO POBLADO</th>
        <th class="border-th-header-right">COD. LOCAL</th>
        <th class="th-cod-modular">CODIGO MODULAR</th>
        <th>NOMBRE II.EE</th>
        <th>DIRECCIÓN</th>
        <th>AREA</th>
        <th class="border-th-header-right">NIVEL</th>
        <th>COD. EVAL. 2015</th>
        <th>SECCIONES</th>
        <th>APLICADORES <br>LIDER</th>
        <th>APLICADORES</th>
        <th>TOTAL <br>APLICADORES</th>
        <th class="th-editable">COD. ASIS. SUP.</th>
        <th>TURNO</th>
        <th>DIAS <br>APLICACIÓN</th>
        <th class="th-editable">DIAS <br>VIAJE</th>
        <th class="th-editable">PASAJE</th>
        <th class="border-th-header-right th-editable">MOVILIDAD</th>
        <th>GASTOS <br>OPERATIVOS</th>
        <th>TOTAL <br>PASAJE</th>
        <th>TOTAL <br>MOVILIDAD</th>
        <th>TOTAL <br>BONIFICACIÓN</th>
        <th class="border-th-header-right">TOTAL <br>I.E</th>
        <th class="th-editable">ESTADO <br>IE</th>
        <th class="th-editable">OBSERVACION</th>
        <th>CIERRE</th>
    </tr>
</thead>
<tbody id="body_table_ie" class="bodyTablaListado">
    <?php
     $n=1;
     foreach ($locales as $local): ?>
        <tr>
           <td class="text-center"><?php echo $n++; ?></td>
           <td><?php show($local, 'sed_region') ?></td>
           <td><?php show($local, 'nom_sede_region') ?></td>
           <td><?php show($local, 'sed_prov') ?></td>
           <td><?php show($local, 'nom_sede_prov') ?></td>
           <td><?php show($local, 'sed_dist') ?></td>
           <td><?php show($local, 'nom_sede_dist') ?></td>
            <td><?php show($local, 'cod_ugel') ?></td>
            <td><?php show($local, 'ugel') ?></td>
            <!-- <td><?php show($local, 'cod_geo') ?></td> -->
            <td><?php show($local, 'departamento') ?></td>
            <td><?php show($local, 'provincia') ?></td>
            <td><?php show($local, 'distrito') ?></td>
            <td><?php show($local, 'cp') ?></td>
           <td><?php show($local, 'cod_local') ?></td>
           <td id="td_cod_modular_<?php show($n) ?>"><?php show($local, 'cod_modular') ?></td>
           <td><?php show($local, 'ie') ?></td>
           <td><?php show($local, 'direccion') ?></td>
           <td><?php show($local, 'area') ?></td>
           <td><?php show($local, 'nivel') ?></td>
           <td><?php show($local, 'cod_eva15') ?></td>
           <td><?php show($local, 'sec_prog') ?></td>
           <td class="text-center unused" id="td_aplica_lider_<?php show($n) ?>" data-resultado="aplica_total;gasto_operativa"><?php show($local, 'aplica_lider') ?></td>
           <td class="text-center unused" id="td_aplica_<?php show($n) ?>" data-resultado="aplica_total;gasto_operativa"><?php show($local, 'aplica') ?></td>
           <td id="td_aplica_total_<?php show($n) ?>"><?php show($local, 'aplica_total') ?></td>
           <td class="text-center editable" id="td_cod_asist_sup_<?php show($n) ?>"><?php show($local, 'cod_asist_sup') ?></td>
           <td><?php show($local, 'turno') ?></td>
           <td class="text-center" id="td_dias_aplica_<?php show($n) ?>"><?php show($local, 'dias_aplica') ?></td>
           <td class="text-center editable" id="td_dias_viaje_<?php show($n) ?>" data-resultado="gasto_operativa"> <?php show($local, 'dias_viaje') ?></td>
           <td class="text-center editable" id="td_pasaje_<?php show($n) ?>" data-resultado="total_pasaje"><?php show($local, 'pasaje') ?></td>
           <td class="text-center editable" id="td_movilidad_<?php show($n) ?>" data-resultado="total_movilidad"><?php show($local, 'movilidad') ?></td>
           <td class="text-center" id="td_gasto_operativa_<?php show($n) ?>"><?php show($local, 'gasto_operativa') ?></td>
           <td class="text-center" id="td_total_pasaje_<?php show($n) ?>"><?php show($local, 'total_pasaje') ?></td>
           <td class="text-center" id="td_total_movilidad_<?php show($n) ?>"><?php show($local, 'total_movilidad') ?></td>
           <td><?php show($local, 'total_bonificacion') ?></td>
           <td><?php show($local, 'total_ie') ?></td>
           <td class="text-center" id="td_estado_ie_<?php show($n) ?>"><?php show($local, 'estado_ie') ?></td>
           <td><?php show($local, 'observa') ?></td>
           <td><?php show($local, 'cierre') ?></td>
        </tr>
    <?php endforeach; ?>
</tbody>
</table>
