<style media="screen">
    .th-editable {
        background-color: #4ECDC4;
    }
    .th-cod-modular {
        background-color: #BF55EC;
        color: #fff;
    }
</style>
<section class="content">
    <div class="box noBox">
        <div class="box-header">
        </div>
        <div class="box-body">
            <form class="form" action="#" id="form_filtro" method="post">
                <div class="row margin-bottom-10">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label for="">SEDE REGIÓN</label>
                        <select name="sed_region" id="local_sede"  class="select2 requerido col-xs-12 sinpadding" title="Seleccione sede">
                            <option value="">-SELECCIONE-</option>
                            <?php foreach($sedes as $sede): ?>
                                <option value="<?php echo $sede['cod_sede_operativa'] ?>"><?php echo $sede['sede_operativa'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label for="">SEDE PROVINCIAL</label>
                        <select name="sed_prov" id="local_sede_provincial" class="select2 col-xs-12 sinpadding" value=""  title="Seleccione Sede Provincial">
                            <option value="" >
                                -SELECCIONE-
                            </option>
                        </select>
                    </div>
                    <div id="div_sede_distrital" class="col-xs-12 col-sm-6 col-md-3 hide">
                        <label for="">SEDE DISTRITAL</label>
                        <select name="sed_dist" id="local_sede_distrital" class="select2 col-xs-12 sinpadding" title="Seleccione Sede distrital">
                            <option value="" >
                                -SELECCIONE-
                            </option>
                        </select>
                    </div>
                </div>
                <div class="row margin-bottom-20">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label for="">DEPARTAMENTO</label>
                        <select name="ccdd" id="local_departamento" class="select2 col-xs-12 sinpadding" value=""  title="Seleccione departamento">
                            <option value="" >
                                -SELECCIONE-
                            </option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label for="">PROVINCIA</label>
                        <select name="ccpp" id="local_provincia" class="select2 col-xs-12 sinpadding" value=""  title="Seleccione provincia">
                            <option value="" >
                                -SELECCIONE-
                            </option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label for="">DISTRITO</label>
                        <select name="ccdi" id="local_distrito" class="select2 col-xs-12 sinpadding" title="Seleccione distrito">
                            <option value="" >
                                -SELECCIONE-
                            </option>
                        </select>
                    </div>
                </div>
                <div class="row margin-bottom-20">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label for="">NIVEL</label>
                        <select name="nivel" id="local_grado" class="select2 col-xs-12 sinpadding" value=""  title="Seleccione grado">
                            <option value="" >
                                -SELECCIONE-
                            </option>
                            <?php $grados = array('Primaria'=>'PRIMARIA', 'Secundaria'=>'SECUNDARIA'); ?>
                            <?php foreach($grados as $key => $grado): ?>
                                <option value="<?php echo $key ?>"><?php echo $grado ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <label for="">TIPO EVALUACIÓN</label>
                        <select name="cod_eva15" id="local_evaluacion" class="select2 col-xs-12 sinpadding" value=""  title="Seleccione tipo evaluación">
                            <option value="" >
                                -SELECCIONE-
                            </option>
                            <?php $evaluaciones = array('ECE'=>'ECE', 'EIB-L2'=>'EIB-L2', 'MC'=>'MC'); ?>
                            <?php foreach($evaluaciones as $key => $item): ?>
                                <option value="<?php echo $key ?>"><?php echo $item ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <button type="button" id="btn_filtrar_ie" data-loading-text="FILTRANDO..." name="button" class="btn btn-success btn-huge tbn-flat">FILTRAR</button>
                </div>
            </form>
            <div class="row margin-bottom-10">
                <div class="col-xs-10 text-center"><br>
                    Formulas:
                    <!-- <span class="label label-info" data-toggle="tooltip" title="APLICADORES LIDER + APLICADORES">TOTAL APLICADORES</span> -->
                    <span class="label label-success" data-toggle="tooltip" title="(DIAS APLICACIÓN + DIAS VIAJE) * TOTAL APLICADORES * S/.<?php echo $GASTO_OPERATIVO; ?>">GASTOS SOPERATIVOS</span>
                    <span class="label label-primary" data-toggle="tooltip" title="TOTAL APLICADORES * PASAJE">TOTAL PASAJES</span>
                    <span class="label label-warning" data-toggle="tooltip" title="DIAS APLICACIÓN * TOTAL APLICADORES * MOVILIDAD">TOTAL MOVILIDAD</span>
                    <span class="label bg-olive label-olive" data-toggle="tooltip" title="TOTAL APLICADORES * S/.<?php echo $BONO_APLICADOR; ?>">BONIFICACIÓN APLICADORES</span>
                    <span class="label bg-maroon label-maroon" data-toggle="tooltip" title="(APLICADORES * S/.<?php echo $BONO_APLICADOR; ?>) + (APLICADORES LIDER * S/.<?php echo $BONO_APLICADOR_LIDER_MC_PRIMARIA; ?>)">BONIFICACIÓN APLICADORES LIDER PRIMARIA</span>
                    <span class="label bg-navy label-navy" data-toggle="tooltip" title="(APLICADORES * S/.<?php echo $BONO_APLICADOR; ?>) + (APLICADORES LIDER * S/.<?php echo $BONO_APLICADOR_LIDER_MC_SECUNDARIA; ?>)">BONIFICACIÓN APLICADORES LIDER SECUNDARIA</span>
                </div>
            </div>
        </div>
        <div class="box-body table-responsive" id="contenedorTable">
            <?php $this->load->view('segmentacion/listado/datatable_vista', array('locales'=>$locales)); ?>
        </div>
    </div>
</section>
<script>
    $(function () {
        $('.select2').select2();
        var table_body;
        var btn_filtrar = $('#btn_filtrar_ie');

        var Formula = {
            BONO_APLICADOR:  <?php echo $BONO_APLICADOR; ?>,
            BONO_APLICADOR_LIDER_MC_PRIMARIA:   <?php echo $BONO_APLICADOR_LIDER_MC_PRIMARIA; ?>,
            BONO_APLICADOR_LIDER_MC_SECUNDARIA: <?php echo $BONO_APLICADOR_LIDER_MC_SECUNDARIA; ?>,
            GASTO_OPERATIVO: <?php echo $GASTO_OPERATIVO; ?>,
            MOVILIDAD:       <?php echo $MOVILIDAD; ?>,

            gastoOperativo: function(id) {
                var dias_aplicacion     = getValor('dias_aplica_'+id);
                var dias_viaje          = getValor('dias_viaje_'+id);
                var total_aplicadores   = getValor('aplica_total_'+id);

                var gasto_operativo     = document.getElementById('td_gasto_operativa_'+id);

                gasto_operativo.innerHTML = ((dias_aplicacion + dias_viaje) * total_aplicadores * Formula.GASTO_OPERATIVO) || 0;
            },
            movilidad: function(id) {
                var dias_aplicacion     = getValor('dias_aplica_'+id);
                var total_aplicadores   = getValor('aplica_total_'+id);
                var movilidad           = getValor('movilidad_'+id);

                var total_movilidad     = document.getElementById('td_total_movilidad_'+id);

                total_movilidad.innerHTML = (dias_aplicacion * total_aplicadores * movilidad) || 0;
            },
            pasaje: function(id) {
                var total_aplicadores   = getValor('aplica_total_'+id);
                var pasaje              = getValor('pasaje_'+id);

                var total_pasaje        = document.getElementById('td_total_pasaje_'+id);

                total_pasaje.innerHTML  = (total_aplicadores * pasaje) || 0;
            },
            totalIe: function(id) {
                var gasto_operativa     = getValor('gasto_operativa_'+id);
                var total_pasaje        = getValor('total_pasaje_'+id);
                var total_bonificacion  = getValor('total_bonificacion_'+id);
                var total_movilidad     = getValor('total_movilidad_'+id);

                var total_ie        = document.getElementById('td_total_ie_'+id);

                total_ie.innerHTML  = (gasto_operativa + total_pasaje + total_bonificacion + total_movilidad) || 0;
            },
            totalBonificacion: function(id) {
                var calc_aplica_total = {'bono':Formula.BONO_APLICADOR};
                var calc_aplica_lider = {'bono':0, 'aplicadores':0};

                var cod_eva15       = getValor('cod_eva15_'+id);
                var aplica_total    = getValor('aplica_total_'+id);
                var turno           = getValor('turno_'+id);
                var sec_prog        = getValor('sec_prog_'+id);

                var bono_aplica_mc_pri   = Formula.BONO_APLICADOR_LIDER_MC_PRIMARIA;
                var bono_aplica_mc_sec   = Formula.BONO_APLICADOR_LIDER_MC_SECUNDARIA;

                // FORMULA general = TOTAL APLICADORES * BONO APLICADOR
                calc_aplica_total.aplicadores = aplica_total;

                // console.log( cod_eva15 +'|'+ turno +'|'+ sec_prog);
                // console.log( (cod_eva15 === 'MC') +'|'+ (turno === 'Mañana-Tarde') +'|'+ (sec_prog > 1));
                // Calcular FORMULA DIFERENTE si COD. EVALUACION 2015 es MC
                if (cod_eva15 === 'MC' && turno === 'Mañana-Tarde' && sec_prog > 1) {
                    var nivel           = getValor('nivel_'+id);
                    var aplica          = getValor('aplica_'+id);
                    var aplica_lider    = getValor('aplica_lider_'+id);

                    // FORMULA DIFERENTE = (APLICADORES * BONO APLICADOR) + (APLICADORES LIDER * BONO SEGUN NIVEL)
                    calc_aplica_total.aplicadores = aplica;
                    calc_aplica_lider.aplicadores = aplica_lider;

                    switch (nivel) {
                        case 'Primaria':
                            calc_aplica_lider.bono = Formula.BONO_APLICADOR_LIDER_MC_PRIMARIA;
                            break;
                        case 'Secundaria':
                            calc_aplica_lider.bono = Formula.BONO_APLICADOR_LIDER_MC_SECUNDARIA;
                            break;
                        default:
                            break;
                    };
                };

                var calcularBonificacion = function(calc_total, calc_lider) {
                    return ( (calc_total.aplicadores * calc_total.bono) + (calc_lider.bono * calc_lider.aplicadores) );
                };

                var total_bonificacion  = document.getElementById('td_total_bonificacion_'+id);

                total_bonificacion.innerHTML    = calcularBonificacion(calc_aplica_total, calc_aplica_lider) || 0;
            },
            // totalAplicadores: function(id) {
            //     var aplicadores         = getValor('aplica_'+id);
            //     var aplicadores_lider   = getValor('aplica_lider_'+id);
            //
            //     var total_aplicadores   = document.getElementById('td_aplica_total_'+id);
            //
            //     total_aplicadores.innerHTML = (aplicadores + aplicadores_lider) || 0;
            // },
        };

        function pasarSiguienteElemento(celda) {

            table_body = $('tbody#body_table_ie');
            inputs = table_body.find(':input.form-control');

            inputs.eq( inputs.index(celda.find('input:visible')) + 1 ).focus();
        }

        function mostrarFormulario(celda, digits){
            var valores = {};
            valores._celda_id     = celda.attr('id').replace(/td_/,'');
            valores._nombre       = valores._celda_id.replace(/_\d+/g,'');
            valores._id           = celda.attr('id').match(/\d+/gi);
            valores._texto_escape = celda.text();
            valores._texto        = valores._texto_escape.replace(/,/g,'');

            celda.data('ie', valores);

            var cod_modular  = document.getElementById('td_cod_modular_'+celda.data('ie')._id).innerHTML;

            form = formBuilder(celda, cod_modular, digits);
            celda.html(form)
                .find('input[type=text]')
                // .select()
                .css('width','100%');
                // $('.maskDecimal').autoNumeric('init');

            clickEvents(celda);
        }

        function formBuilder(celda, cod_modular, digits) {

            var form = '<form action="javascript:this.preventDefault" autocomplete="off" role="form" >'+
                '<input type="hidden" value="'+cod_modular+'" name="hidden_cod_mod">'+
                '<input type="hidden" value="'+celda.data('ie')._nombre+'" name="campo_actualizado">';

            form += '<input type="text" name="ie['+celda.data('ie')._nombre+']" id="'+celda.data('ie')._celda_id+'" class="text-center form-control maskDecimal" maxlength="'+digits+'" value="'+celda.data('ie')._texto+'" />'+
            '</form>';

            return form;
        }

        function clickEvents(celda) {
            celda.on('click',function(){return false;});

            celda.on('keydown',function(e){
                // ENTER KEY PRESSED
                if(e.keyCode === 13){
                    if ( 'pasaje' == celda.data('ie')._nombre ) {
                        // console.log(celda_nombre);
                        getValor('movilidad_'+celda.data('ie')._id, '0');
                        Formula.movilidad(celda.data('ie')._id);
                    }
                    else if ( 'movilidad' == celda.data('ie')._nombre ) {
                        // console.log(celda_nombre);
                        getValor('pasaje_'+celda.data('ie')._id, '0');
                        Formula.pasaje(celda.data('ie')._id);
                    }

                    cambiarCampo(celda, celda.data('ie')._texto_escape);

                    pasarSiguienteElemento(celda);
                }
                // ESCAPE KEY PRESSED
                else if(e.keyCode === 27){
                    // console.log(celda._texto_escape);
                    celda.text(celda.data('ie')._texto_escape);
                    celda.off('click');

                    // Formula.totalAplicadores(celda._id);
                    Formula.gastoOperativo(celda.data('ie')._id);
                    Formula.movilidad(celda.data('ie')._id);
                    Formula.pasaje(celda.data('ie')._id);
                    Formula.totalBonificacion(celda.data('ie')._id);
                    Formula.totalIe(celda.data('ie')._id);
                }
                else {
                    // CALCULAR AL PRESIONAR UNA TECLA
                    celda.on('keyup', function() {
                        // Formula.totalAplicadores(celda._id);
                        Formula.gastoOperativo(celda.data('ie')._id);
                        Formula.movilidad(celda.data('ie')._id);
                        Formula.pasaje(celda.data('ie')._id);
                        Formula.totalBonificacion(celda.data('ie')._id);
                        Formula.totalIe(celda.data('ie')._id);
                    });
                }
            });
        }

        function cambiarCampo(celda,prevContent){

            celda.off('keydown');
            celda.off('keyup');

            var url         = CI.base_url+'segmentacion/actualizar';
            var form_data   = celda.find('form').serialize();

            if (celda.data('resultado')) {
                var extras = {};

                $.each(celda.data('resultado').split(';'), function(index, name){
                    extras['ie['+name+']'] = getValor(name +'_'+ celda.data('ie')._id);
                })

                form_data += '&' + $.param( extras);
            }

            $.ajax({
                url: url,
                type: 'POST',
                data: form_data,
                dataType: 'json',
                beforeSend: function () {

                },
                success: function (data) {
                    if (data.error) {
                        celda.html(prevContent);
                    } else {
                        //celda.html(data.monto);
                        celda.html(data.response.ie[data.response.campo_actualizado]);
                    }
                    $('body').notify(data.mensaje);
                },
                error: function(err){
                    celda.html(prevContent);
                    console.log(err);
                }
            });

            celda.off('click');
        }

        function getValor(elem, valor) {
            var accesor = 'value';

            if ( !document.getElementById(elem) ) {
                elem = 'td_'+elem;
                accesor = 'innerHTML';
            }

            if (valor) {
                document.getElementById(elem)[accesor] = valor;
            }

            var result = document.getElementById(elem)[accesor];

            // Si es NUMERO castear a ENTERO
            return (result % 1 === 0) ? +result : result;
        };

        var filtros_ubigeo = function(data) {

            data.filtro = {};

            data.filtro.sed_region = document.getElementById('local_sede').value;
            data.filtro.ccdd       = document.getElementById('local_departamento').value;
            data.filtro.ccpp       = document.getElementById('local_provincia').value;
            data.filtro.ccdi       = document.getElementById('local_distrito').value;

            data.filtro.sed_prov   = document.getElementById('local_sede_provincial').value;
            data.filtro.sed_dist   = document.getElementById('local_sede_distrital').value;
            data.filtro.nivel      = document.getElementById('local_grado').value;
            data.filtro.cod_eva15  = document.getElementById('local_evaluacion').value;

        };

        function tablaListadoDataTableSegmentacion() {
            var table = $('#tablaListado').dataTable({
                "oLanguage": {
                    "sSearch": "Buscar por Código Modular: ",
                    "oPaginate": {
                        "sFirst": "&lt;&lt;",
                        "sLast": "&gt;&gt;",
                        "sNext": "&gt;",
                        "sPrevious": "&lt;"
                    },
                    "sInfoEmpty": "0 registros que mostrar",
                    "sInfoFiltered": " ",
                    "sZeroRecords": "<div class='text-center'><i class='ion ion-podium fa-5x'></i><h3>No hay registro que mostrar</h3></div>",
                    "sLoadingRecords": "Por favor espere - cargando...",
                    "sLengthMenu": 'Mostrando <select class="form-control input-sm">' +
                            '<option value="10">10</option>' +
                            '<option value="20">20</option>' +
                            '<option value="30">30</option>' +
                            '<option value="40">40</option>' +
                            '<option value="50">50</option>' +
                            '<option value="500">500</option>' +
                            '</select> registros',
                    "sInfo": " _TOTAL_ registros encontrados, mostrando _START_ de _END_ registros"
                },
                serverSide: true,
                processing: true,
                ajax: {
                    url:  CI.base_url+'segmentacion/filtrar',
                    type: 'POST',
                    data: filtros_ubigeo,
                    beforeSend: function() {
                        btn_filtrar.button('loading');
                    },
                    complete: function(xhr, status) {

                        $.each($('#body_table_ie > tr[role=row]'), function(i, row){
                            var row = $(row);
                            var id  = row.children().eq(0).html(); // ROW ID

                            var elementos = [
                                { index: 14, attrib: {'id': 'td_cod_modular_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 21, attrib: {'id': 'td_aplica_lider_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 22, attrib: {'id': 'td_aplica_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 23, attrib: {'id': 'td_aplica_total_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 26, attrib: {'id': 'td_dias_aplica_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 30, attrib: {'id': 'td_gasto_operativa_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 31, attrib: {'id': 'td_total_pasaje_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 32, attrib: {'id': 'td_total_movilidad_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 18, attrib: {'id': 'td_nivel_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 19, attrib: {'id': 'td_cod_eva15_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 20, attrib: {'id': 'td_sec_prog_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 25, attrib: {'id': 'td_turno_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 33, attrib: {'id': 'td_total_bonificacion_'+id, 'class' :'text-center'}, isEditable: false},
                                { index: 34, attrib: {'id': 'td_total_ie_'+id, 'class' :'text-center'}, isEditable: false},

                                { index: 24, attrib: {'id': 'td_cod_asist_sup_'+id, 'class' :'text-center editable onlyNumbers', 'data-resultado':'total_bonificacion;total_ie', 'data-max':3}, isEditable: true},
                                { index: 27, attrib: {'id': 'td_dias_viaje_'+id,    'class':'text-center editable onlyNumbers', 'data-resultado':'gasto_operativa;total_bonificacion;total_ie', 'data-max':2}, isEditable: true},
                                { index: 28, attrib: {'id': 'td_pasaje_'+id,        'class':'text-center editable onlyNumbers', 'data-resultado':'total_pasaje;total_bonificacion;total_ie', 'data-max':4}, isEditable: true},
                                { index: 29, attrib: {'id': 'td_movilidad_'+id,     'class':'text-center editable onlyNumbers', 'data-resultado':'total_movilidad;total_bonificacion;total_ie', 'data-max':2}, isEditable: true},
                                { index: 35, attrib: {'id': 'td_estado_ie_'+id,     'class':'text-center editable onlyNumbers', 'data-resultado':'total_bonificacion;total_ie', 'data-max':1}, isEditable: true},
                                { index: 36, attrib: {'id': 'td_observa_'+id,     'class':'editable', 'data-resultado':'total_bonificacion;total_ie', 'maxlength':'1000'}, isEditable: true},
                            ];

                            $.each(elementos, function(i, elem){
                                var item = row.children().eq(elem.index);
                                item.attr(elem.attrib);

                                if (elem.isEditable) {
                                    mostrarFormulario(item, elem.attrib['data-max']);
                                }
                            });
                        });
                        //
                        $("td.editable").on('dblclick',function() {
                            var celda = $(this);
                            mostrarFormulario(celda, celda.data('max'));
                        });

                        $(".onlyNumbers").keypress(function (e) {
                            return validarNumero(e);
                        });

                        $('input[id*=estado_ie]').soloPermitir('12');

                        // tablaListadoDataTableSegmentacion();
                        btn_filtrar.button('reset');
                    }
                },
                // columns: [
                //     { "data": "first_name", "name": "first_name" },
                //     { "data": "last_name", "name": "last_name" },
                // ]
            });

            return table;
        };

        llamadasAjaxUbigeo();
        var table = tablaListadoDataTableSegmentacion();
        $('input[type=search]').attr('maxlength', '8');



        btn_filtrar.on('click.filtrar', function(ev){
            ev.preventDefault();
            //$('a[data-dt-idx=1]').trigger('click');
            table.fnDraw();

            ev.stopPropagation();
        });

    });
</script>
