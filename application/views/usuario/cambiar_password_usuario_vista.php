<div class="titulo text-center"><i class="fa fa-lock"></i> CAMBIAR CONTRASEÑA</div>
<div class="cuerpo" style="width: 400px">
   <form method="POST" action="<?php echo base_url('cambiar-password') ?>" id="form_cambiar_password" autocomplete="off" class="text-center padding-top-20"  >
      <div class="col-xs-12 ">
         <div class="col-md-4 col-xs-12 margin-bottom-20">
            <label for="user_cambiar">USUARIO</label>
            <input type="text" name="user_cambiar" value="<?php echo "" ?>" class="form-control text-center text-lowercase input-sm" id="user_cambiar" />
         </div>
         <div class="col-md-4 col-xs-12 margin-bottom-20">
            <label for="pass_cambiar">CONTRASEÑA</label>
            <input type="password" name="pass_cambiar" value="<?php echo "" ?>" class="form-control text-center input-sm" id="pass_cambiar" placeholder="INGRESE CONTRASEÑA NUEVA" />
         </div>
      </div>
      <div class="col-xs-12 ">
         <input name="valorPassword" type="hidden"  value="valorCambiarPassword"/>
         <button type="submit" class="btn btn-guardar btn-flat margin-rigth-10" name="cambiarPassword"  id="btnCambiarPassword">Cambiar</button>
         <button type="button" class="btn btn-danger btn-flat margin-left-10 cerrar " id="btnCancelPassword"><span class="fa fa-times "></span> CANCELAR</button>
      </div>
   </form>
</div>