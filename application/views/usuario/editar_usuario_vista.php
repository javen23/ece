<div class="titulo text-center"><span class="icon ion-edit"></span> EDITAR USUARIO</div>
<div class="cuerpo" style="width: 700px">
    <form method="POST" action="<?php echo base_url('usuarios/editar') ?>" id="form_editar_usuario" autocomplete="off" class="text-center padding-top-20">
        <input type="hidden" name="hidden_id_usuario_editar" value="<?php echo $usuario['id'] ?>">
        <div class="col-xs-12 no-padding">
         <div class="col-md-4 col-sm-4  margin-bottom-20">
            <label for="rol_editar" class="col-xs-12">ROL</label>
            <select class="col-xs-12 no-padding text-uppercase  selec2 input-sm" name="rol_editar" id="rol_editar" title="SELECCIONE ROL">
               <option value="">-SELECCIONE ROL-</option>
               <?php foreach ($totalRol as $rol) { ?>
                  <option value="<?php echo $rol['idRol'] ?>" <?php echo ($rol['idRol']==$usuario['id_rol'])? "selected":"" ;?>><?php echo strtoupper($rol['rol']) ?></option>
               <?php } ?>
            </select>
         </div>

         <div class="col-md-4 col-sm-4 margin-bottom-20">
            <label for="user_editar">USUARIO</label>
            <input type="text" name="user_editar" value="<?php echo $usuario['usuario'] ?>" class="form-control text-center text-lowercase input-sm" readonly="readonly" id="user_editar" placeholder="INGRESE NOMBRE USUARIO" >
         </div>

         <div class="col-md-4 col-sm-4 margin-bottom-20">
            <label for="pass_editar">PASSWORD</label>
            <input type="password" name="pass_editar" class="form-control text-center input-sm " id="pass_editar" placeholder="INGRESE CONTRASEÑA" >
         </div>
      </div>

      <div class="col-xs-12  no-padding">
         <div class="col-md-4 col-sm-4  margin-bottom-20">
            <label for="dni_editar">DNI</label>
            <input type="text" name="dni_editar" value="<?php echo $usuario['dni'] ?>" class="form-control text-center input-sm onlyNumbers" id="dni_editar" placeholder="INGRESE DNI" >
            <input type="hidden" name="dni_editar_hidden" value="<?php echo $usuario['dni'] ?>">
         </div>
         <div class="col-md-4 col-sm-4 margin-bottom-20">
            <label for="apellidos_editar">APELLIDOS</label>
            <input type="text" name="apellidos_editar" value="<?php echo $usuario['apellidos'] ?>" class="form-control text-center text-uppercase input-sm" id="apellidos_editar" placeholder="INGRESE APELLIDOS" >
         </div>
         <div class="col-md-4 col-sm-4 margin-bottom-20">
            <label for="nombres_editar">NOMBRES</label>
            <input type="text" name="nombres_editar" value="<?php echo $usuario['nombres'] ?>" class="form-control text-center text-uppercase input-sm" id="nombres_editar" placeholder="INGRESE NOMBRES" >
         </div>
      </div>

      <div class="col-xs-12 ">
         <input name="valorUsuario" type="hidden"  value="valorEditarUsuario"/>
         <button type="submit" class="btn btn-guardar btn-flat margin-rigth-10" name="editarUsuario"  id="btnEditarUsuario"> EDITAR</button>
         <button type="button" class="btn btn-danger btn-flat margin-left-10 cerrar " id="btnCancelUsuario">CANCELAR</button>
      </div>
   </form>
</div>
<script>
$(function () {
   $('.selec2').select2();
   $(".onlyNumbers").keypress(function(e){
        return validarNumero(e);
    });

   $("#form_editar_usuario").validate({
        errorClass: 'myErrorClass',
        rules: {
            user_editar: {
                required: true,
                letrasJuntas: true,
                minlength: 6
            },
            dni_editar: {
               required: true,
               number: true,
               minlength: 8,
               remote: {
                  url: CI.base_url + "usuarios/verificar",
                  type: "POST",
                  data: {
                     dni_crear: function () {
                        if($("input[name=dni_editar]").val()!==$("input[name=dni_editar_hidden]").val()){
                            return $("input[name=dni_editar]").val();
                        }
                     }
                  }
               }
            },
            apellidos_editar: "required",
            nombres_editar: "required"
        },
        messages: {
            user_editar: {
                required: "Ingrese usuario",
                minlength: "NOMBRE USUARIO MINIMO 6 CARACTERES"
            },
            dni_editar: {
               required: "INGRESE DNI",
               number: "SOLO NÚMEROS",
               minlength: "DNI 8 CARACTERES",
               remote: jQuery.validator.format("D.N.I YA EXISTE")
            },
            apellidos_editar: "INGRESE APELLIDOS",
            nombres_editar: "INGRESE NOMBRES"
        },
        submitHandler: function (form) {
            form.submit();
        }

    });
    $.validator.addMethod("letrasJuntas", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
    }, "Nombre usuario sin espacio, solo letras y/o numeros");
});
</script>
