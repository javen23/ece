<div class="titulo text-center"><span class="icon ion-person-add"></span> CREAR USUARIO</div>
<div class="cuerpo" style="width: 700px">

    <form method="POST" action="<?php echo base_url('usuarios/crear') ?>" id="form_crear_usuario" autocomplete="off" class="text-center padding-top-20"  >
        <div class="col-xs-12  no-padding">

            <div class="col-md-4 col-sm-4 margin-bottom-20">
                <label for="rol_crear">ROL</label>
                <select class="no-padding text-uppercase form-control input-sm selec2" name="rol_crear" id="rol_crear" title="SELECCIONE ROL">
                    <option value="">-SELECCIONE ROL-</option>
                    <?php foreach ($totalRol as $rol) { ?>
                        <option value="<?php echo $rol['idRol'] ?>"><?php echo strtoupper($rol['rol']) ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-4 col-sm-4 margin-bottom-20">
                <label for="user_crear">USUARIO</label>
                <input type="text" name="user_crear" class="form-control text-center text-lowercase input-sm" id="user_crear" placeholder="INGRESE NOMBRE USUARIO" >
            </div>

            <div class="col-md-4 col-sm-4 margin-bottom-20">
                <label for="pass_crear">CONTRASEÑA</label>
                <input type="password" name="pass_crear" class="form-control text-center input-sm" id="pass_crear" placeholder="INGRESE CONTRASEÑA" >
            </div>

        </div>

        <div class="col-xs-12  no-padding">
            <div class="col-md-4 col-sm-4  margin-bottom-20">
                <label for="dni_crear">DNI</label>
                <input type="text" name="dni_crear" class="form-control text-center input-sm onlyNumbers" id="dni_crear" placeholder="INGRESE DNI" maxlength="8">
            </div>
            <div class="col-md-4 col-sm-4 margin-bottom-20">
                <label for="apellidos_crear">APELLIDOS</label>
                <input type="text" name="apellidos_crear" class="form-control text-center text-uppercase input-sm" id="apellidos_crear" placeholder="INGRESE APELLIDOS" >
            </div>
            <div class="col-md-4 col-sm-4 margin-bottom-20">
                <label for="nombres_crear">NOMBRES</label>
                <input type="text" name="nombres_crear" class="form-control text-center text-uppercase input-sm" id="nombres_crear" placeholder="INGRESE NOMBRES" >
            </div>
        </div>

      <div class="col-xs-12 ">
         <input name="valorUsuario" type="hidden"  value="valorCrearUsuario"/>
         <button type="submit" class="btn btn-guardar btn-flat margin-rigth-10" name="crearUsuario"  id="btnCreateUsuario">CREAR</button>
         <button type="button" class="btn btn-danger btn-flat margin-left-10 cerrar " id="btnCancelUsuario">CANCELAR</button>
      </div>
   </form>
</div>
<script>
    $(function () {
        $('.selec2').select2();
        $(".onlyNumbers").keypress(function(e){
            return validarNumero(e);
        });

      $("#form_crear_usuario").validate({
         errorClass: 'myErrorClass',
         rules: {
            rol_crear: "required",
            user_crear: {
               required: true,
               letrasJuntas: true,
               minlength: 6,
               remote: {
                  url: CI.base_url + "usuarios/verificar",
                  type: "POST",
                  data: {
                     user_crear: function () {
                        return $("input[name=user_crear]").val();
                     }
                  }
               }
            },
            pass_crear: {
               required: true,
               minlength: 6
            },
            dni_crear: {
               required: true,
               number: true,
               minlength: 8,
               remote: {
                  url: CI.base_url + "usuarios/verificar",
                  type: "POST",
                  data: {
                     dni_crear: function () {
                        return $("input[name=dni_crear]").val();
                     }
                  }
               }
            },
            apellidos_crear: "required",
            nombres_crear: "required"
         },
         messages: {
            user_crear: {
               required: "INGRESE NOMBRE USUARIO",
               minlength: "NOMBRE USUARIO MINIMO 6 CARACTERES",
               remote: jQuery.validator.format("ESTE USUARIO YA EXISTE.")
            },
            pass_crear: {
               required: "INGRESE CONTRASEÑA",
               minlength: "CONTRASEÑA MINIMO 6 CARACTERES"
            },
            dni_crear: {
               required: "INGRESE DNI",
               number: "SOLO NÚMEROS",
               minlength: "DNI 8 CARACTERES",
               remote: jQuery.validator.format("D.N.I YA EXISTE")
            },
            apellidos_crear: "INGRESE APELLIDOS",
            nombres_crear: "INGRESE NOMBRES"
         },
         submitHandler: function () {
            $("#form_crear_usuario").ajaxSubmit();
         }

      });
      $.validator.addMethod("letrasJuntas", function (value, element) {
         return this.optional(element) || /^[a-zA-Z0-9_]+$/i.test(value);
      }, "Nombre usuario sin espacio, solo letras y/o numeros");

   });
</script>
