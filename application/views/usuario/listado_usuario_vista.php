<section class="content">
    <?php if ($msg = $this->session->flashdata('mensaje_flash')) { ?>
    <div class="alert alert-<?php echo $msg['tipo'] ?> alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Mensaje...!</h4>
        <strong><?php echo $msg['cuerpo'] ?></strong>
    </div>
   <?php } ?>
   <div class="row">
      <div class="col-md-12 col-sm-12">
         <div class="box noBox">
            <div class="box-header">
               <h3 class="box-title pull-right"><a href="<?php echo base_url('usuarios/crear'); ?>" class="btn btn-warning btn-flat awhite" rel="facebox"><span class="icon ion-person-add"></span> AGREGAR USUARIO</a></h3>
            </div>
            <div class="box-body table-responsive">
               <table width="100%" id="tablaListado" class="table table-bordered table-striped table-hover" >
                  <thead class="headTablaListado">
                     <tr class="text-uppercase">
                        <th colspan="6" class=" border-th-header-right"></th>
                        <th colspan="2">ACCIONES</th>
                     </tr>
                     <tr class="text-uppercase th-head-inputs">
                        <th>N°</th>
                        <th>USUARIO</th>
                        <th>ROL</th>
                        <th>DNI</th>
                        <th>NOMBRES Y APELLIDOS</th>
                        <th>ACTUALIZADO</th>
                        <th>ESTADO</th>
                        <th>EDITAR</th>
                     </tr>
                  </thead>
                  <tfoot class="footTablaListado">
                     <tr class="text-uppercase">
                        <th>N°</th>
                        <th>USUARIO</th>
                        <th>ROL</th>
                        <th>DNI</th>
                        <th>NOMBRES Y APELLIDOS</th>
                        <th>ACTUALIZADO</th>
                        <th>ESTADO</th>
                        <th>EDITAR</th>
                     </tr>
                  </tfoot>
                  <tbody class="bodyTablaListado">
                     <?php
                     $n=1;
                     foreach ($totalUsuarios as $usuario) { ?>
                        <tr>
                           <td class="text-center"><?php echo $n++; ?></td>
                           <td><?php echo $usuario['usuario'] ?></td>
                           <td><span class="cursor-pointer" data-toggle='tooltip' title="<?php echo $usuario['descripcion'] ?>"><?php echo $usuario['nombre_rol'] ?></span></td>
                           <td></td>
                           <td></td>
                           <td class="text-center">
                              <?php
                              if($usuario['modificado_por']=='' && $usuario['fecha_modificado']==''){
                                 $actTexto = (($usuario['fecha_creado']));
                                 $mensaje_tooltip="por " . $usuario['creado_por'];
                              }else{
                                 $actTexto = (($usuario['fecha_modificado']));
                                 $mensaje_tooltip="por " . $usuario['modificado_por'];
                              } ?>
                              <span class="cursor-pointer" data-toggle="tooltip" title="<?php echo $mensaje_tooltip ?>"><?php echo $actTexto ?></span>
                           </td>
                           <td class="text-center"><a href="<?php echo base_url('usuarios/estado'); ?>?idUsuario=<?php echo $usuario['id']?>&estado=<?php echo $usuario['estado'] ?>" data-toggle='tooltip' title="CAMBIAR ESTADO"><?php echo verificar_estado($usuario['estado']) ?></a></td>
                           <td class="text-center"><a href="<?php echo base_url('usuarios/editar'); ?>?idUsuario=<?php echo $usuario['id'] ?>" rel='facebox' data-toggle='tooltip' title="EDITAR" class="btn btn-flat btn-success"><i class="icon ion-edit"></i></a></td>
                        </tr>
                     <?php } ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</section>
<script>
$(function () {
   $('a[rel*=facebox]').facebox();
   $('#facebox').draggable({handle: 'div.titulo'});
   tablaListadoDataTable();
});
</script>
