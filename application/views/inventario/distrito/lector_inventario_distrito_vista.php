<div class="titulo text-center"><span class="fa fa-barcode"></span> LECTOR INVENTARIO REPLIEGUE</div>
<div class="cuerpo" style="width: 550px">
    <h4 class="text-center">COLOQUE EL LECTOR FRENTE AL CÓDIGO DE BARRA</h4>
    <form id="form_lector_barra" autocomplete="off" class="text-center padding-top-20">
        <div class="input-group placeholder-normal margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
            <input type="text" name="lector_barra" maxlength="8" id="lector_barra" autofocus="autofocus" class="form-control input-lg text-center text-uppercase onlyNumbers " placeholder="INGRESE CÓDIGO DE BARRA" >
        </div>
    </form>
    <div class="mensaje_loading"></div>
</div>
<script>
    $(function () {
        // $('.selec2').select2();
        var escondeAlert;
        function esconder() {
            escondeAlert = setTimeout(function () {
                $(".mensaje_loading").empty();
            }, 5000);
        }

        // $(".onlyNumbers").keypress(function (e) {
        //     return validarNumero(e);
        // });

        //Ajax para verificar el estado del código de barra.
        $("#lector_barra").on("keyup", function () {
            var lector = $(this).val();
            if (lector.length===8) {
                $.ajax({
                    url: CI.base_url + "ajax-lector-barra",
                    type: 'POST',
                    data: {codigo:lector},
                    dataType: 'json',
                    beforeSend: function () {
                        clearTimeout(escondeAlert);
                        $("#lector_barra").val("");
                        $(".mensaje_loading").empty()
                        $(".mensaje_loading").html('<div class="text-center"><span class="fa fa-spinner fa-spin fa-3x"></span></div>');
                    },
                    success: function (response) {
                        $("#lector_barra").val("");
                        if(response.resultado=="NO"){
                            $(".mensaje_loading").html('<div class="callout callout-danger bg-red animated shake text-center">' +
                                        '<h4 class="text-center color-white-messages">EL CÓDIGO DE BARRA: "<strong>' + lector + '</strong>"</h4>' +
                                        '<h3>NO EXISTE</h3>' +
                                    '</div>');
                            esconder();
                        }
                        if (response.resultado.reciente==0) {
                            $(".mensaje_loading").html('<div class="callout callout-warning bg-warning animated shake text-center">' +
                                    '<h4 class="text-center color-white-messages">EL CÓDIGO DE BARRA: "<strong>' + lector + '</strong>"</h4>' +
                                    '<h3>YA FUE LEÍDO</h3>' +
                                    '<h4>CORRESPONDE A LA SEDE "'+response.resultado.nombre_sede+'"</h4>' +
                                    '<h4>I.E: "'+response.resultado.ie_temp+'"</h4>' +
                                '</div>');
                            esconder();
                        }
                        if(response.resultado.reciente==1){
                            $(".mensaje_loading").html('<div class="callout callout-green bg-green animated shake text-center">' +
                                    '<h4 class="text-center color-white-messages">EL CÓDIGO DE BARRA: "<strong>' + lector + '</strong>"</h4>' +
                                    '<h4>CORRESPONDE A LA SEDE "'+response.resultado.nombre_sede+'"</h4>' +
                                    '<h4>I.E: "'+response.resultado.ie_temp+'"</h4>' +
                                '</div>');
                            esconder();
                        }
                    }
                });
            }
        });
    });
</script>