<?php #print_r($totalProyectosMes[0]) ?>
<section class="content">
    <div class="box noBox">
        <div class="box-header">
            <h3 class="box-title pull-right">
                <a href="<?php echo base_url('lector-inventario-sede'); ?>" class="btn btn-warning btn-flat awhite" rel="facebox"><span class="fa fa-barcode"></span> LECTOR INVENTARIO SEDE DISTRITAL</a>
            </h3>
        </div>
        <div class="box-body table-responsive">
            <table width="100%" id="tablaListado" class="display table table-bordered table-striped table-hover" >
                <thead class="headTablaListado">
                    <tr class="text-uppercase th-head-inputs">
                        <th>CORRELATIVO</th>
                        <th>SEDE REGIÓN</th>
                        <th>COD. MINEDU</th>
                        <th>COD. MODULAR</th>
                        <th>NOMBRE I.E</th>
                        <th>COD. BARRA</th>
                        <th>ESTADO</th>
                        <th>FECHA REGISTRO</th>
                    </tr>
                </thead>
                <tfoot class="footTablaListado">
                    <tr class="text-uppercase tfoot-placeholder">
                        <th>CORRELATIVO</th>
                        <th>SEDE REGIÓN</th>
                        <th>COD. MINEDU</th>
                        <th>COD. MODULAR</th>
                        <th>NOMBRE I.E</th>
                        <th>COD. BARRA</th>
                        <th>ESTADO</th>
                        <th>FECHA REGISTRO</th>
                    </tr>
                </tfoot>
                <tbody class="bodyTablaListado">
                <?php if(count($material_distrito)>0): ?>
                <?php foreach($material_distrito as $distrito): ?>
                    <tr>
                        <td class="text-center"><?php echo $distrito['correlativo'] ?></td>
                        <td><?php echo $distrito['nombre_sede'] ?></td>
                        <td class="text-center"><?php echo $distrito['cod_minedu'] ?></td>
                        <td class="text-center"><?php echo $distrito['cod_modular'] ?></td>
                        <td><?php echo $distrito['ie_temp'] ?></td>
                        <td class="text-center"><?php echo $distrito['cod_barra'] ?></td>
                        <td class="text-center"><?php echo verificar_estado($distrito['estado']) ?></td>
                        <td class="text-center"><?php echo convertirFecha($distrito['fecha_registro']) ?></td>
                    </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
    $(function () {
        $('a[rel*=facebox]').facebox();
        $('#facebox').draggable({handle: 'div.titulo'});
        tablaListadoDataTable();
    });
</script>