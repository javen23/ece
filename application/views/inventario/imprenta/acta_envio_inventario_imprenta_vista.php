<div class="titulo text-center"><span class="fa fa-file-text"></span> ORDEN DE SALIDA DE MATERIAL</div>
<div class="cuerpo" style="width: 800px">
    <div class="box noBox no-box-shadow no-margin">
        <div class="box-body">
            <form id="form_crear_acta_envio" method="POST" action="<?php echo base_url('acta-envio-inventario-imprenta') ?>" autocomplete="off" class="text-center display-inline-block">
                <div class="col-xs-12 no-padding">
                    <div class="col-md-4 col-sm-6 col-xs-12  margin-bottom-10">
                        <label for="" class="col-xs-12">SEDE</label>
                        <select name="local_sede" id="local_sede"  class="selec2 col-xs-12 width100 no-padding" title="Seleccione sede">
                            <option value="">-Seleccione Sede-</option>
                            <?php foreach ($sedes as $sede): ?>
                                <option value="<?php echo $sede['cod_sede_operativa'] ?>"><?php echo $sede['sede_operativa'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 margin-bottom-10 onlyStrings">
                        <label for="" class="col-xs-12">RESPONSABLE</label>
                        <input type="text" class="form-control input-sm text-center text-uppercase " id="" placeholder="Responsable" name="responsable_acta_envio" />
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 margin-bottom-10 onlyNumbers">
                        <label for="" class="col-xs-12">N° CAJAS APLICACIÓN</label>
                        <input type="text" class="form-control input-sm text-center " id="" placeholder="N° CAJAS APLICACIÓN" name="cajas_aplicacion_acta_envio" />
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 margin-bottom-10 onlyNumbers">
                        <label for="" class="col-xs-12">N° CAJAS INEI</label>
                        <input type="text" class="form-control input-sm text-center " id="" placeholder="N° CAJAS INEI" name="cajas_inei_acta_envio" />
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 margin-bottom-10 onlyNumbers">
                        <label for="" class="col-xs-12">N° CAJAS MONITOR MINEDU</label>
                        <input type="text" class="form-control input-sm text-center " id="" placeholder="N° CAJAS MINEDU" name="cajas_minedu_acta_envio" />
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 margin-bottom-10 onlyNumbers">
                        <label for="" class="col-xs-12">TOTAL CAJAS</label>
                        <input type="text" class="form-control input-sm text-center " id="" placeholder="TOTAL CAJAS " name="total_cajas_acta_envio" />
                    </div>
                </div>
                <div class="col-xs-12 margin-top-10 no-padding">
                    <input name="hidden_name_acta_envio" type="hidden"  value="valorHiddenActaEnvio"/>
                    <button type="button" class="btn btn-guardar btn-flat margin-right-10" name="crearActaEnvio" id="btnCreateActaEnvio">GUARDAR</button>
                    <button type="button" class="btn bg-red btn-flat cerrar margin-left-10" id="btnCreateCancelar">CANCELAR</button>
                </div>

            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.selec2').select2();

        $(".onlyNumbers").keypress(function (e) {
            return validarNumero(e);
        });

        $(".onlyStrings").keypress(function (e) {
            return validarLetras(e);
        });

        // //Ajax para traer las oficinas
        // $("#proyecto_institucion").on('change',function(){
        //     var institucion = $(this).val();
        //     if(institucion !== ""){
        //         $.ajax({
        //             url: CI.base_url+'obtener-oficinas',
        //             type:'POST',
        //             data:{idInstitucion:institucion},
        //             dataType: 'json',
        //             beforeSend: function () {

        //             },
        //             success: function (data) {
        //                 console.log(data);
        //                 var llenarOption = "";
        //                 $.each(data, function(i, name){
        //                     llenarOption +='<option value="'+name.idOfic+'">'+name.nombre+'</option>'
        //                 });

        //                 $("#proyecto_oficina").append(llenarOption);

        //             },
        //             error: function (error) {
        //                 //console.log(error);
        //             }
        //         });
        //     }else{
        //         $("#proyecto_oficina").html('<option value="" class="text-center">----</option>');
        //     }
        // });
    });
</script>