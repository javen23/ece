<section class="content">
    <div class="box noBox">
        <div class="box-header">
            <h3 class="box-title pull-right">
                <a href="<?php echo base_url('inventario/lector-inventario-imprenta'); ?>" class="btn btn-warning btn-flat awhite" rel="facebox"><span class="fa fa-barcode"></span> LECTOR INVENTARIO IMPRENTA</a>
                <a href="<?php echo base_url('inventario/acta-envio-inventario-imprenta'); ?>" class="btn btn-warning btn-flat awhite" rel="facebox"><span class="fa fa-file-text"></span> ORDEN DE SALIDA DE MATERIAL</a>
            </h3>
        </div>
        <div class="box-body table-responsive">
            <table width="100%" id="tablaListado" class="display table table-bordered table-striped table-hover" >
                <thead class="headTablaListado">
                    <tr class="text-uppercase th-head-inputs">
                        <th>CORRELATIVO</th>
                        <th>SEDE REGIÓN</th>
                        <!-- <th>SEDE PROVINCIA</th> -->
                        <th>COD. MINEDU</th>
                        <th>COD. MODULAR</th>
                        <th>NOMBRE I.E</th>
                        <th>COD. BARRA</th>
                        <th>ESTADO</th>
                        <th>FECHA REGISTRO</th>
                    </tr>
                </thead>
                <tfoot class="footTablaListado">
                    <tr class="text-uppercase tfoot-placeholder">
                        <th>CORRELATIVO</th>
                        <th>SEDE REGIÓN</th>
                        <!-- <th>SEDE PROVINCIA</th> -->
                        <th>COD. MINEDU</th>
                        <th>COD. MODULAR</th>
                        <th>NOMBRE I.E</th>
                        <th>COD. BARRA</th>
                        <th>ESTADO</th>
                        <th>FECHA REGISTRO</th>
                    </tr>
                </tfoot>
                <tbody class="bodyTablaListado">
                <?php if(count($material_imprenta)>0): ?>
                <?php foreach($material_imprenta as $imprenta): ?>
                    <tr>
                        <td class="text-center"><?php echo $imprenta['correlativo'] ?></td>
                        <td><?php echo $imprenta['nombre_sede'] ?></td>
                        <!-- <td><?php echo $imprenta['cod_sede_prov'] ?></td> -->
                        <td class="text-center"><?php echo $imprenta['cod_minedu'] ?></td>
                        <td class="text-center"><?php echo $imprenta['cod_modular'] ?></td>
                        <td><?php echo $imprenta['ie_temp'] ?></td>
                        <td class="text-center"><?php echo $imprenta['cod_barra'] ?></td>
                        <!-- <td class="text-center"><?php echo $imprenta['estado'] ?></td> -->
                        <td class="text-center"><?php echo verificar_estado($imprenta['estado']) ?></td>
                        <td class="text-center"><?php echo convertirFecha($imprenta['fecha_registro']) ?></td>
                    </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
    $(function () {
        $('a[rel*=facebox]').facebox();
        $('#facebox').draggable({handle: 'div.titulo'});
        tablaListadoDataTable();
    });
</script>