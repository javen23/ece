<div class="titulo text-center">ASIGNAR RUTAS</div>
<div class="cuerpo" style="width: 750px">
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_locales" method="POST" action="<?php echo base_url('rutas/asignar') ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-12 col-sm-4">
                    <fieldset>
                        <legend class="fieldset-legend-title">ROLES DE USUARIO</legend>
                        <div class="col-xs-12 sinpadding margin-bottom-20">
                            <label for="rol_id">ROL</label>
                            <select name="idRol" id="rol_id"  class="select2 col-xs-12 requerido sinpadding" title="Seleccione Rol">
                                <option value="">-SELECCIONE-</option>
                                <?php foreach($roles as $rol): ?>
                                    <option value="<?php echo $rol['idRol'] ?>"><?php echo $rol['rol'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </fieldset>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <fieldset>
                        <legend class="fieldset-legend-title">RUTAS DEL SISTEMA</legend>
                        <div class="col-xs-12 margin-bottom-10">
                            <label for="rutas">RUTA</label>
                            <select name="ruta[]" id="rutas"  class="select2 col-xs-12 sinpadding" multiple title="Seleccione Rutas">
                                <option value="">-SELECCIONE-</option>
                                <?php foreach($rutas as $ruta): ?>
                                    <option value="<?php echo $ruta['idRuta'] ?>"><?php echo $ruta['ruta'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">ASIGNAR TODAS LAS RUTAS: </label>
                            <input type="checkbox" name="todas_las_rutas" value="true">
                        </div>
                    </fieldset>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="hidden_name_local" type="hidden"  value="valorHiddenLocal"/>
                    <button type="submit" class="btn btn-guardar btn-flat " name="crearLocal" id="btnCreateLocal">ASIGNAR</button>
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                </div>

            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.select2').select2();
        // $('[data-mask]').inputmask();
        $('#form_locales').validate();
    });
</script>
