<div class="titulo text-center"><i class="fa fa-edit"></i> EDITAR RUTAS</div>
<div class="cuerpo" style="width: 750px">
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_locales" method="POST" action="<?php echo base_url('rutas/editar') ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-12 col-sm-8">
                    <fieldset>
                        <legend class="fieldset-legend-title">RUTAS DEL SISTEMA</legend>
                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">RUTA</label>
                            <input type="text" class="input-sm form-control requerido" value="<?php show($ruta, 'ruta'); ?>" name="ruta" id="ruta" placeholder="crear-local"/>
                        </div>
                    </fieldset>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="idRuta" type="hidden"  value="<?php show($ruta, 'idRuta'); ?>"/>
                    <button type="submit" class="btn btn-guardar btn-flat " name="editarLocal" id="btnCreateLocal">EDITAR</button>
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#form_locales').validate();
    });
</script>
