<div class="titulo text-center">ELIMINAR RUTA</div>
<div class="cuerpo" style="width: 450px">
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_crear_" method="POST" action="<?php echo base_url('rutas/eliminar') ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-12">
                    <fieldset>
                        <legend>¿ESTA SEGURO DE ELIMINAR ESTA RUTA "<?php echo $ruta['ruta']; ?>" PARA EL ROL "<?php echo $ruta['rol_nombre']; ?>"?</legend>
                        <p class="text-red">
                            Una vez eliminado esta ruta no se podrá revertir los cambios, tenga en cuenta este mensaje.
                        </p>
                    </fieldset>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="idRuta" type="hidden" value="<?php echo $ruta['ruta_id']; ?>"/>
                    <input name="idRol" type="hidden" value="<?php echo $ruta['rol_id']; ?>"/>
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                    <button type="submit" class="btn btn-guardar btn-flat " name="eliminarLocal" id="btnCreateLocal">ELIMINAR</button>
                </div>

            </form>
        </div>
    </div>
</div>
