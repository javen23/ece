<section class="content">
    <div class="box noBox">
        <?php if ($msg = $this->session->flashdata('mensaje_flash')): ?>
            <div class="alert-message">
                <div class="alert alert-<?php echo $msg['tipo'] ?> alert-dismissable">
                    <button type="button" class="close text-warning" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><?php echo $msg['cuerpo'] ?></strong>
                </div>
            </div>
        <?php endif; ?>
        <div class="box-header">
            <h3 class="box-title pull-right"><a href="<?php echo base_url('rutas/crear'); ?>" class="btn btn-success btn-flat awhite" rel="facebox"><span class="fa fa-plus-circle"></span> CREAR RUTAS</a></h3>
            <h3 class="box-title pull-right"><a href="<?php echo base_url('rutas/asignar'); ?>" class="btn btn-primary btn-flat awhite margin-right-10" rel="facebox"><span class="fa fa-check-circle"></span> ASIGNAR RUTAS</a></h3>
        </div>
        <div class="box-body table-responsive">
            <table width="100%" id="tablaListado" class="display table table-bordered table-striped table-hover" >
                <thead class="headTablaListado">
                    <tr class="text-uppercase th-head-inputs">
                        <th>N°</th>
                        <th>ROL</th>
                        <th>RUTA</th>
                        <th>EDITAR</th>
                        <th>ELIMINAR</th>
                    </tr>
                </thead>
                <tfoot class="footTablaListado">
                    <tr class="text-uppercase tfoot-placeholder">
                        <th>N°</th>
                        <th>ROL</th>
                        <th>RUTA</th>
                        <th>EDITAR</th>
                        <th>ELIMINAR</th>
                    </tr>
                </tfoot>
                <tbody class="bodyTablaListado">
                    <?php
                     $n=1;
                     foreach ($rutas as $ruta): ?>
                        <tr>
                           <td class="text-center"><?php echo $n++; ?></td>
                           <td><?php show($ruta, 'rol_nombre') ?></td>
                           <td><?php show($ruta, 'ruta') ?></td>
                           <td class="text-center"><a href="<?php echo base_url('rutas/editar'); ?>?idRuta=<?php show($ruta, 'ruta_id') ?>" rel='facebox' data-toggle='tooltip' title="EDITAR" class="btn btn-flat btn-primary"><i class="icon ion-edit"></i></a></td>
                           <td class="text-center"><a href="<?php echo base_url('rutas/eliminar'); ?>?idRuta=<?php show($ruta, 'ruta_id') ?>&idRol=<?php show($ruta, 'rol_id') ?>" rel='facebox' data-toggle='tooltip' title="ELIMINAR" class="btn btn-flat btn-danger"><i class="icon ion-trash-a"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
    $(function () {
        $('a[rel*=facebox]').facebox();
        $('#facebox').draggable({handle: 'div.titulo'});
        tablaListadoDataTable();
    });
</script>
