<?php $rol = $this->session->userdata('userIdRol'); ?>
<section class="content">
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo base_url('locales') ?>" data-toggle="tooltip" title="LOCALES">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">LOCALES</span>
						<span class="info-box-number">Registro de Locales</span>
					</div>
				</div>
			</a>
		</div>
		<!-- <div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo base_url('aplicador') ?>" data-toggle="tooltip" title="APLICADOR">
				<div class="info-box">
					<span class="info-box-icon bg-purple"><i class="fa fa-street-view"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">APLICADOR</span>
						<span class="info-box-number">Listado de Aplicadores</span>
					</div>
				</div>
			</a>
		</div> -->
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo base_url('segmentacion') ?>" data-toggle="tooltip" title="SEGMENTACIÓN">
				<div class="info-box">
					<span class="info-box-icon bg-maroon"><i class="fa fa-indent"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">SEGMENTACIÓN</span>
						<span class="info-box-number">Segmentación de II.EE</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo base_url('cobertura') ?>" data-toggle="tooltip" title="COBERTURA">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="fa fa-send"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">COBERTURA</span>
						<span class="info-box-number">Capacitación de niveles</span>
					</div>
				</div>
			</a>
		</div>
		<!-- <div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo base_url('inventario') ?>" data-toggle="tooltip" title="INVENTARIO">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-book"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">INVENTARIO</span>
						<span class="info-box-number">Inventario de materiales</span>
					</div>
				</div>
			</a>
		</div> -->
		<!-- <div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo base_url('reporte') ?>" data-toggle="tooltip" title="REPORTE">
				<div class="info-box">
					<span class="info-box-icon bg-purple"><i class="fa fa-table"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">REPORTE</span>
						<span class="info-box-number">Reporte informativo</span>
					</div>
				</div>
			</a>
		</div> -->
		<?php if($rol == 1): ?>
		<div class="col-md-3 col-sm-6 col-xs-12">
            <a href="<?php echo base_url('rutas') ?>" data-toggle="tooltip" title="RUTAS DEL SISTEMA">
                <div class="info-box">
					<span class="info-box-icon bg-maroon"><i class="fa fa-share"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">RUTAS DEL SISTEMA</span>
						<span class="info-box-number">Asignar rutas a cada perfíl</span>
					</div>
				</div>
            </a>
        </div>

		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="<?php echo base_url('usuarios') ?>" data-toggle="tooltip" title="USUARIOS">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">USUARIOS</span>
						<span class="info-box-number">Usuarios del sistema</span>
					</div>
				</div>
			</a>
		</div>

        <?php endif; ?>

	</div>

</section>
