<?php
$idLogin            = $this->session->userdata('idUserLogin');
$nameUsuario        = $this->session->userdata('userNameUsuario');
$apellidosUsuario   = $this->session->userdata('userNombreApellidos');
$dniUsuario         = $this->session->userdata('userDni');

?>
<!DOCTYPE html>
<html lang="es">
<head>
   <meta charset="UTF-8">
   <title><?php echo (isset($titulo) and $titulo != "") ? $titulo . " | " : ""; ?>INEI</title>
   <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
   <?php echo put_headersCss() ?>
   <script type="text/javascript">
      var CI = {
         'base_url' : '<?php echo base_url(); ?>',
         'site_url' : '<?php echo site_url(); ?>',
         'limite_pea' : 30
      };
   </script>
   <link rel="stylesheet" href="<?php echo base_url('assets/css/global.css') ?>"  />
   <!-- <script src="//maps.google.com/maps/api/js?sensor=false&foo=bar.js" type="text/javascript"></script> -->
   <script src="<?php echo base_url('assets/js/jquery.min.js') ?>" ></script>
   <?php echo put_headersJs_() ?>
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
   <?php if ($idLogin): ?>
    <script type="text/javascript">
        function activarSubmenu(_li) {

            if (_li.parent('ul.treeview-menu').length){
                _li.parent('ul.treeview-menu').css('display', 'block');
                activarSubmenu(_li.parent());
            }

            if (_li.parent('li').length){
                _li.parent('li').addClass('active');
                activarSubmenu(_li.parent());
            }

            return false;
        }
        $(document).on('ready', function () {
            var url = window.location;
            //console.log(url);
            var rutaMain     = $('#main-nav a[href="' + url + '"]');
            var li_parent    = rutaMain.parent();
            li_parent.addClass('active');
            // rutaMain.parent().parent('ul.treeview-menu').css('display', 'block');
            // rutaMain.parent().parent('ul.treeview-menu').parent('li.treeview').addClass('active');
            activarSubmenu(li_parent);
            //  $('#main-nav a').filter(function () {
            //     return this.href === url;
            //  }).parent().addClass('active');

            // var i = 0;
        });
    </script>
    <?php endif; ?>
   </head>
   <body class="<?php echo ($this->uri->segment(1)=='login')? 'login-page' : 'skin-green sidebar-mini fixed'?> ">
      <div class="wrapper">
         <?php if ($idLogin) : ?>
            <header class="main-header">
               <a href="<?php echo base_url() ?>" class="logo">
                  <span class="logo-lg"><b>S.I.G</b></span>
               </a>
               <nav class="navbar navbar-static-top" role="navigation">
                  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                     <span class="sr-only">Tootle navigation</span>
                  </a>
                  <div class="centrar-titulo-sistema">
                    <h3 class="text-white spacear-texto">
                        <span class="visible-xs visible-sm">ECE</span>
                        <span class="hidden-xs hidden-sm">EVALUACIÓN CENSAL DE ESTUDIANTES</span>
                    </h3>
                  </div>
                  <div class="navbar-custom-menu">
                     <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                              <img src="http://iinei.inei.gob.pe/iinei/asistencia/fotos/<?php echo ($dniUsuario!=NULL)? $dniUsuario : $nameUsuario ;?>.jpg" class="user-image " onerror="this.src='<?php echo base_url('assets/img/boy.png');?>'" alt="<?php echo $nameUsuario;?>" />
                              <span class="hidden-xs"><?php echo $nameUsuario ?> </span>
                           </a>
                           <ul class="dropdown-menu">
                              <li class="user-header">
                                 <img src="http://iinei.inei.gob.pe/iinei/asistencia/fotos/<?php echo ($dniUsuario!=NULL)? $dniUsuario : $nameUsuario;?>.jpg" class="img-circle " onerror="this.src='<?php echo base_url('assets/img/boy.png');?>'" alt="<?php echo $nameUsuario;?>" />
                                 <p>
                                    <?php echo ($apellidosUsuario!=NULL)? $apellidosUsuario : $nameUsuario ; ?>
                                    <small><?php echo $this->session->userdata('userNombreRol') ?></small>
                                 </p>
                              </li>
                              <li class="user-footer">
                                 <div class="pull-left">
                                    <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#cambiarPassword"><i class="fa fa-cog text-navy"></i> Perfíl</button>
                                 </div>
                                 <div class="pull-right">
                                    <a href="<?php echo base_url('logout')?>" class="btn btn-default btn-flat"><i class="ion ion-close-circled text-red"></i> SALIR</a>
                                 </div>
                              </li>
                           </ul>
                        </li>
                     </ul>
                  </div>
               </nav>
            </header>
         <?php endif;
