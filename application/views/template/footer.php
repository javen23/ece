<?php if ($this->session->userdata('idUserLogin')) {
	$idLogin 		= $this->session->userdata('idUserLogin');
	$nameUsuario 	= $this->session->userdata('userNameUsuario');
 ?>
</div>
</div>
<div class="modal fade" id="cambiarPassword" tabindex="-1" role="dialog" aria-labelledby="cambiarPassword" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close resetear_formulario" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="cambiarPassword"><i class="fa fa-lock"></i> CAMBIAR CONTRASEÑA</h4>
			</div>
			<form autocomplete="off" id="form_cambiar_password">
				<div class="box box-success no-border box-solid caja-mensaje">
					<div class="modal-body">
						<div class="form-group">
							<label for="user_cambiar">USUARIO</label>
							<input type="text" name="user_cambiar" value="<?php echo $nameUsuario; ?>" readonly="readonly" class="form-control text-center text-lowercase input-sm" id="user_cambiar" />
						</div>
						<div class="form-group">
							<label for="pass_cambiar">CONTRASEÑA</label>
							<input type="password" name="pass_cambiar"  class="form-control text-center input-sm" id="pass_cambiar" placeholder="INGRESE CONTRASEÑA NUEVA" />
						</div>
					</div>
					<div class="modal-footer center-important">
						<input type="hidden" name="id_hidden_usuario_cambiar" value="<?php echo $idLogin; ?>" />
						<input type="hidden" name="valorPassword" value="valorCambiarPassword" />
						<button type="button" class="btn btn-guardar btn-flat" name="cambiarPassword" id="btnCambiarPassword">CAMBIAR</button>
						<button type="button" class="btn btn-danger btn-flat resetear_formulario" data-dismiss="modal"><span class="fa fa-times "></span> CANCELAR</button>
					</div>
					<div class="overlay hide hide-verifica">
						<i class="fa fa-spinner fa-pulse"></i>
					</div>
				</div>
			</form>


		</div>
	</div>
</div>
<?php } ?>
<?php echo put_headersJs() ?>

<script src="<?php echo base_url('assets/js/bootstrap-growl.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/waves.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/global.js') ?>"></script>

<?php if ($flash_mensaje = $this->session->flashdata('mensaje')) : ?>
	<script type="text/javascript">
	    $(function () {
	        $('body').notify(<?php echo json_encode($flash_mensaje) ?>);
	    });
	</script>
<?php endif;?>

</body>
</html>
