<?php
$rol                = $this->session->userdata('userIdRol');
$nameUsuario        = $this->session->userdata('userNameUsuario');
$apellidosUsuario   = $this->session->userdata('userNombreApellidos');
$dniUsuario         = $this->session->userdata('userDni');
?>
<?php if (!isset($sidebar)): ?>
   <aside class="main-sidebar">
      <section class="sidebar">
         <div class="user-panel text-center">
            <div class=" image">
               <img src="http://iinei.inei.gob.pe/iinei/asistencia/fotos/<?php echo ($dniUsuario!=NULL)? $dniUsuario : $nameUsuario ;?>.jpg" class="img-circle " onerror="this.src='<?php echo base_url('assets/img/boy.png');?>'" alt="<?php echo $nameUsuario;?>" />
            </div>
            <div class=" info m-b-10">
               <a href="javascript:;"><i class="fa fa-circle text-success"></i> Conectado</a>
            </div>
            <div class=" info show_name_user">
                <p><?php echo ($apellidosUsuario!=NULL)? $apellidosUsuario : $nameUsuario ; ?></p>
            </div>
         </div>

         <ul id="main-nav" class="sidebar-menu">
            <li class=" treeview">
               <a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> <span>DASHBOARD</span></a>
            </li>
            <li class="treeview">
               <a href="<?php echo base_url('locales') ?>"><i class="fa fa-university"></i><span>LOCALES</span><i class="fa fa-angle-left pull-right"></i></a>
               <ul class="treeview-menu">
                   <li><a href="<?php echo base_url('locales/jurisdiccion-regional') ?>">Jurisdicción Regional</a></li>
                   <li><a href="<?php echo base_url('locales/jurisdiccion-provincial-distrital') ?>">Jurisdicción Provincial/Distrital</a></li>
                   <!-- <li><a href="<?php echo base_url('locales/preseleccion') ?>">Preselección</a></li>
                   <li><a href="<?php echo base_url('locales/capacitacion') ?>">Capacitación</a></li> -->
                   <li>
                       <a href="<?php echo base_url('locales/reportes') ?>">Reportes<i class="fa fa-angle-left pull-right"></i></a>
                       <ul class="treeview-menu">
                           <li><a href="<?php echo base_url('locales/reportes/directorio') ?>">Directorio</a></li>
                           <li><a href="<?php echo base_url('locales/reportes/mapa-regional') ?>">Indicador Regional</a></li>
                           <li><a href="<?php echo base_url('locales/reportes/mapa-provincial-distrital') ?>">Indicador Provincial/Distrital</a></li>
                       </ul>
                   </li>
               </ul>
            </li>
            <!-- <li class="treeview">
               <a href="javascript:;"><i class="fa fa-street-view"></i> <span>ASISTENTE DE SUPERVISOR</span><i class="fa fa-angle-left pull-right"></i></a>
               <ul class="treeview-menu">
                  <li><a href="<?php echo base_url('asistente-supervisor/ie') ?>">Institución Educativa</a></li>
                  <li><a href="<?php echo base_url('asistente-supervisor/ie/formato-accesibilidad') ?>">Accesibilidad</a></li>
               </ul>
            </li> -->
            <li class="treeview">
               <a href="<?php echo base_url('segmentacion') ?>"><i class="fa fa-indent"></i> <span>SEGMENTACIÓN</span><i class="fa fa-angle-left pull-right"></i></a>
               <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('segmentacion/avance-programacion') ?>">Avance de Programación</a></li>
                    <li><a href="<?php echo base_url('segmentacion/listado-ingresos') ?>">Listado de Ingresos</a></li>
                    <li>
                        <a href="<?php echo base_url('segmentacion/reportes') ?>">Reportes<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url('segmentacion/reportes/sede-operativa') ?>">Presupuesto Región</a></li>
                            <li><a href="<?php echo base_url('segmentacion/reportes/provincia-operativa') ?>">Presupuesto Provincia</a></li>
                        </ul>
                    </li>
               </ul>
            </li>
            <li class="treeview">
               <a href="<?php echo base_url('cobertura') ?>"><i class="fa fa-send"></i> <span>COBERTURA</span><i class="fa fa-angle-left pull-right"></i></a>
               <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('cobertura/directores') ?>">Directores</a></li>
                    <li><a href="<?php echo base_url('cobertura/instructores') ?>">Instructores</a></li>
                    <li><a href="<?php echo base_url('cobertura/niveles') ?>">Niveles</a></li>
               </ul>
            </li>
            <!-- <li class="treeview">
               <a href="javascript:;"><i class="fa fa-book"></i><span>INVENTARIO</span><i class="fa fa-angle-left pull-right"></i></a>
               <ul class="treeview-menu">
                  <li><a href="<?php echo base_url('inventario/imprenta') ?>">Imprenta</a></li>
                  <li><a href="<?php echo base_url('inventario/sede-regional') ?>">Sede Regional</a></li>
                  <li><a href="<?php echo base_url('inventario/sede-provincial') ?>">Sede Provincial</a></li>
                  <li><a href="<?php echo base_url('inventario/sede-distrital') ?>">Sede Distrital</a></li>
               </ul>
            </li> -->
            <?php if ($rol == 1): ?>
            <!-- <li class="treeview">
                <a href="<?php echo base_url('calendario') ?>"><i class="fa fa-calendar"></i> <span>CALENDARIO</span></a>
            </li> -->
            <li class="treeview">
                <a href="<?php echo base_url('rutas') ?>"><i class="fa fa-share"></i> <span>RUTAS</span></a>
            </li>
            <li class="treeview">
                <a href="<?php echo base_url('usuarios') ?>"><i class="fa fa-users"></i> <span>USUARIOS</span></a>
            </li>
           <?php endif; ?>
         </ul>
      </section>
   </aside>
   <div class="content-wrapper">

    <section class="content-header">
      <?php
        $uri=$this->uri->uri_string();
        $ruta_explode=explode('/',$uri);
        $last_array = end($ruta_explode);
      ?>
     <h1><?php echo $titulo?></h1>
     <ol class="breadcrumb">
        <?php $val_url = '';  ?>
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> DASHBOARD</a></li>
        <?php if(isset($ruta_explode)): ?>
            <?php if(count($ruta_explode)>0):?>
                <?php foreach ($ruta_explode as $val) :?>
                <?php $val_url .= $val;  ?>
                  <li>
                    <?php if($last_array!=$val): ?>
                    <a href="<?php echo base_url($val_url) ?>">
                        <?php echo utf8_mayusculas(replace_texto($val)) ?>
                    </a>
                    <?php else: ?>
                        <?php echo utf8_mayusculas(replace_texto($val)) ?>
                    <?php endif;?>
                  </li>
                <?php $val_url .= '/';?>
                <?php endforeach; ?>
            <?php endif;?>
        <?php endif;?>
     </ol>
    </section>

<?php endif; ?>
