<section class="content">
    <div class="box noBox">
        <form id="form_locales" method="POST" action="<?php echo base_url('asistente-supervisor/ie/crear') ?>"  autocomplete="off" >
            <div class="box-body col-md-12 text-center">
                <div class="col-md-6">
                    <fieldset class="">
                        <legend >I. INFORMACIÓN DE LA INSTITUCIÓN EDUCATIVA</legend>
                        <div class="form-group col-md-4">
                            <label for="local_sede" class="form-label">SEDE REGIONAL</label>
                            <select name="local_sede" id="local_sede"  class="select2 requerido no-padding" title="Seleccione sede" >
                                <option value="">-SELECCIONE-</option>
                                <?php foreach($sedes as $sede): ?>
                                    <option value="<?php show($sede, 'cod_sede_operativa') ?>">
                                        <?php show($sede, 'sede_operativa') ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="local_sede_provincial" class="form-label">SEDE PROVINCIAL</label>
                            <select name="local_sede_provincial" id="local_sede_provincial"  class="select2 requerido no-padding" title="Seleccione sede">
                                <option value="">-SELECCIONE-</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4" id="div_sede_distrital">
                            <label for="local_sede_distrital" class="form-label">SEDE DISTRITAL</label>
                            <select name="local_sede_distrital" id="local_sede_distrital" class="select2 requerido col-xs-12 no-padding" title="Seleccione Sede distrital">
                                <option value="">-SELECCIONE-</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="local_ie" class="form-label">INSTITUCIÓN EDUCATIVA</label>
                            <select name="local_ie" id="local_ie"  class="select2 requerido sinpadding" title="Seleccione Institución Educativa">
                                <option value="">-SELECCIONE-</option>
                                <?php foreach($elementos as $item): ?>
                                    <option value="<?php show($item, 'id_ie') ?>">
                                        <?php show($item, 'ie') ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <!--
                        <div class="form-group col-md-4">
                            <label for="local_sede" class="form-label">DEPARTAMENTO</label>
                            <select name="local_sede" id="local_sede"  class="select2 requerido no-padding" title="Seleccione sede" >
                                <option value="">-SELECCIONE-</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="local_sede_provincial" class="form-label">PROVINCIA</label>
                            <select name="local_sede_provincial" id="local_sede_provincial"  class="select2 requerido no-padding" title="Seleccione sede">
                                <option value="">-SELECCIONE-</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4" id="div_sede_distrital">
                            <label for="local_sede_distrital" class="form-label">DISTRITO</label>
                            <select name="local_sede_distrital" id="local_sede_distrital" class="select2 requerido col-xs-12 no-padding" title="Seleccione Sede distrital">
                                <option value="">-SELECCIONE-</option>
                            </select>
                        </div>
                        -->
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="">
                        <legend class="scheduler-border">II. INFORMACIÓN DEL APLICADOR</legend>
                        <div class="col-md-12">
                            <div class="form-group col-md-4">
                                <label for="nombre" class="form-label">NOMBRES</label>
                                <input type="text" name="nombre" id="nombre" class="input-sm form-control requerido text-uppercase onlyStrings" placeholder="NOMBRES...">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ap_paterno" class="form-label">APELLIDO PATERNO</label>
                                <input type="text" name="ap_paterno" id="ap_paterno" class="input-sm form-control requerido text-uppercase onlyStrings" placeholder="APELLIDO PATERNO">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ap_materno" class="form-label">APELLIDO MATERNO</label>
                                <input type="text" name="ap_materno" id="ap_materno" class="input-sm form-control requerido text-uppercase onlyStrings" placeholder="APELLIDO MATERNO">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="dni_asistente" class="form-label">DNI</label>
                                <input type="text" name="dni" id="dni_asistente" class="dni_asistente input-sm form-control requerido digits text-uppercase onlyNumbers" maxlength="8" placeholder="Ejem: 12345678">
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <div class="form-group col-md-4">
                                <label for="cod_duracion_viaje" class="form-label ">DURACIÓN DEL VIAJE </label>
                                <select name="cod_duracion_viaje" id="cod_duracion_viaje"  class="form-control input-sm requerido" width="100%" title="Seleccione una opción">
                                    <option value="">-SELECCIONE-</option>
                                    <?php foreach($this->dataselect->valores_total_viaje() as $key => $item): ?>
                                        <option value="<?php echo $item[0] ?>">
                                            <?php echo $item[1] ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="fecha_salida_addon" class="form-label">FECHA Y HORA DE SALIDA</label>
                                <!-- <div class="input-group date" id="fecha_salida_addon">
                                    <input class="form-control  input-sm requerido text-uppercase" name="fecha_salida" id="fecha_salida" type="text" value="" placeholder="Clic Aquí" readonly>
                                    <div class="input-group-addon" ><i class="fa fa-calendar-o"></i></div>
                                </div> -->
                                <div class="input-group cursor-pointer" id="date_time_picker">
                                    <input type="text" name="fecha_salida" class="form-control input-sm " id="fecha_salida_addon" placeholder="Ingrese fecha" />
                                    <div class="input-group-addon input-sm "><span class="fa fa-calendar"></span></div>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="local_cant_aplicadores" class="form-label">CANT. APLICADORES</label><!-- data-date-format="dd/mm/yyyy h:m" -->
                                <div class="input-group input-group-sm">
                                    <input name="local_cant_aplicadores" id="local_cant_aplicadores" type="text" class="form-control input-sm digits text-uppercase onlyNumbers" maxlength="2" placeholder="00"/>
                                    <span class="input-group-btn btn-flat">
                                        <a id="btn_aplicadores" class="btn btn-info btn-flat" type="button"><i class="fa fa-user"></i></a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="box-footer text-center">
                <input name="hidden_name_local" type="hidden"  value="valorHiddenLocal"/>
                <button type="submit" class="btn btn-guardar btn-flat " name="crearAsistente" id="btnCreateLocal" >CREAR REGISTRO</button>
                <button type="reset" class="btn bg-red btn-flat cerrar" onClick="history.go(-1)" id="btnCreateCancelar" >CANCELAR</button>
            </div>
        </form>
        <!-- LOCALES MODAL  -->
        <div class="modal fade" id="aplicador_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="text-center">
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                            <h3 class="text-navy">Cargando por favor espere...</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #form_locales label{
            font-size: 10px !important;
        }
        p.text-danger {
            font-size: 11px !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('a[rel*=facebox]').facebox();
            $('#facebox').draggable({handle: 'div.titulo'});
            $('.select2').select2();
            $('#form_locales').validate();
            $(".dni_asistente").rules("add", {
                 remote: {
                    url: CI.base_url + "verificar-dni-asistente",
                    type: "POST",
                    data: {
                        dni: function () {
                             return $("input[name=dni]").val();
                        }
                    }
                },
                messages: {
                    
                    remote: jQuery.validator.format("ESTE D.N.I YA EXISTE.")
                }
            });
            $('#cod_duracion_viaje').siblings().attr('style', 'width:100%');
            // $(".open_date_time_picker").on('click',function(){
            //     console.log('sorry');
            //     open_datPicker();
            // });

            llamadasAjaxUbigeo();

            var modalLocal      = $('#aplicador_modal');
            var modalContent    = $('div.modal-content');
            $('.modal-dialog').draggable({handle: 'div.modal-header'});

            $('body').on('hidden.bs.modal', '.modal', function () {
               $(this).removeData('bs.modal');
               modalContent.html('<div class="text-center">'+
                   '<i class="fa fa-spinner fa-spin fa-3x"></i>'+
                   '<h3 class="text-navy">Cargando por favor espere...</h3>'+
               '</div>');
            });
            $("#btn_aplicadores").click(function() { 
                var cant_aplicadores = $('#local_cant_aplicadores').val();
                
                modalLocal.modal({
                    show: true,
                    backdrop: 'static',
                    remote: CI.base_url+'asistente-supervisor/ie/aplicadores?aplicadores='+cant_aplicadores,
                });
            });  
        });
    </script>
</section>
