<!-- <pre>
    <?php print_r($aplicadores_dni[0]); ?>
</pre> -->
<?php $asistentes = array(); ?>
<div class="modal-header" style="cursor:move;">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-street-view"></i> <?php echo mb_strtoupper($titulo, 'UTF-8') ?></h4>
</div>
<div class="cuerpo" >
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_locales" method="POST" action="<?php echo base_url('asistente-supervisor/ie/editar?id=00034') ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-offset-1 col-xs-10">
                    <?php for ($i=1; $i <= $cant_aplicadores; $i++): ?>
                    <div class="row">
                        <fieldset>
                            <!-- <legend class="fieldset-legend-title">Aplicador <big>#<?php echo $i; ?></big> </legend> -->
                            <div class="col-md-10" id="remote">
                                <div class="col-md-6">
                                    <legend class="fieldset-legend-title">Aplicador <big>#<?php echo $i; ?></big> </legend>
                                </div>
                                <div class="col-md-6" >
                                    <input type="text" class="typeahead tt requerido text-uppercase col-md-8" id="" name="dni" placeholder="Datos del Aplicador" value="" />
                                </div>
                                <!-- <div class="row">
                                    <div class="col-md-6">
                                    <label for="">NOMBRES Y APELLIDOS</label>
                                       <input type="text" class="col-xs-6 form-control requerido text-uppercase" value="" name="Ingrese DNI" id="nombre" placeholder="Nombres"/>
                                    </div>
                                </div> -->
                            </div>
                        </fieldset>
                    </div>
                    <?php endfor; ?>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="cod_asistente" type="hidden"  value="valorHiddenLocal"/>
                    <button type="submit" class="btn btn-guardar btn-flat" name="crearAplicador_dni" id="btnCreateLocal">Guardar</button>
                    <button type="button" class="btn bg-red btn-flat" data-dismiss="modal">Cerrar</button>
                    <!--<button type="button"  class="btn bg-red btn-flat cerrar" id="">Cancelar</button>-->
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        //$('.maskDecimal').autoNumeric('init');
        $('.select2').select2();
        $('[data-mask]').inputmask();
        $('#form_locales').validate();

        $(".onlyNumbers").keypress(function (e) {
            return validarNumero(e);
        });

        $(".onlyStrings").keypress(function (e) {
            return validarLetras(e);
        });
    });
</script>
    <script>
        $(document).ready(function(){
        
        var bestPictures = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.whitespace,
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          //prefetch: CI.base_url+"ajax/aplicadores/check_dni?dni=7000",
          remote: {
            url: CI.base_url+"ajax/aplicadores/check_dni?dni=%ELVIS",
            wildcard: '%ELVIS'
          }
        });

        $('#remote .typeahead').typeahead({
              minLength: 3,
              highlight: true
            }, {
            source: bestPictures
        });

        $('#remote .typeahead').on('typeahead:select', function(ev, suggestion) {
          console.log('Selection: ' + suggestion);
          console.dir(suggestion);
        });
    });
    </script>
