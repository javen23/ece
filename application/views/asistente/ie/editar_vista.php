<section class="content">
    <div class="box noBox">
        <form id="form_locales" method="POST" action="<?php echo base_url('asistente-supervisor/ie/editar?id='.$asistentes['cod_asistente']) ?>" role="form-horizontal" autocomplete="off" >
            <div class="box-body col-md-12 text-center">
                <div class="col-md-6">
                    <fieldset class="">
                        <legend >I. INFORMACIÓN DE LA INSTITUCIÓN EDUCATIVA</legend>
                            <div class="form-group col-md-4">
                                <label for="" class="form-label"><span class="pull-left">SEDE REGIONAL</span></label>
                                <select name="local_sede" id="local_sede"  class="form-control requerido sinpadding" title="Seleccione sede" class="pull-left">
                                    <option value="">-SELECCIONE-</option>
                                    <?php foreach($sedes as $sede): ?>
                                        <option value="<?php show($sede, 'cod_sede_operativa') ?>"
                                            <?php echo ($asistentes['cod_sede_operativa'] == $sede['cod_sede_operativa']) ? 'selected' : '' ?>
                                        >
                                            <?php show($sede, 'sede_operativa') ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="" class="form-label"><span class="pull-left">SEDE PROVINCIAL</span></label>
                                <select name="local_sede_provincial" id="local_sede_provincial"  class="form-control col-xs-12 requerido sinpadding" title="Seleccione sede">
                                    <option value="">-SELECCIONE-</option>
                                    <?php foreach($sede_provinciales as $sede_prov): ?>
                                    <option
                                        value="<?php show($sede_prov, 'cod_sede_prov') ?>"
                                        <?php echo ($asistentes['cod_sede_prov'] == $sede_prov['cod_sede_prov']) ? 'selected' : '' ?>
                                    >
                                        <?php show($sede_prov, 'sede_prov') ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="" class="form-label"><span class="pull-left">INSTITUCIÓN EDUCATIVA</span></label>
                                <select name="local_ie" id="local_ie"  class="form-control requerido sinpadding" title="Seleccione Institución Educativa">
                                    <option value="">-SELECCIONE-</option>
                                    <?php foreach($ies as $item): ?>
                                        <option value="<?php show($item, 'id_ie') ?>"
                                            <?php echo ($asistentes['ie'] == $item['id_ie']) ? ' selected ' : '' ?>
                                        >
                                            <?php show($item, 'ie') ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="" class="form-label"><span class="pull-left">DURACIÓN DEL VIAJE</span></label>
                                <select name="cod_duracion_viaje" id="cod_duracion_viaje"  class="form-control requerido" width="100%" title="Seleccione una opción">
                                    <option value="">-SELECCIONE-</option>
                                    <?php foreach($this->dataselect->valores_total_viaje() as $item): ?>
                                        <option value="<?php echo $item[0] ?>"
                                            <?php echo($asistentes['viaje'] == $item[0]) ? 'selected' : '' ?>
                                        >
                                            <?php echo $item[1] ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="" class="form-label">FECHA Y HORA DE SALIDA</label>
                                <div class="input-group cursor-pointer" id="date_time_picker">
                                    <input type="text" name="fecha_salida" class="form-control input-sm " value="<?php echo $asistentes['aplicadores'] ?>" id="fecha_salida_addon" placeholder="Ingrese fecha" />
                                    <div class="input-group-addon input-sm "><span class="fa fa-calendar"></span></div>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="" class="form-label">CANT. APLICADORES</label>
                                <div class="input-group input-group-sm">
                                    <input name="local_cant_aplicadores" id="local_cant_aplicadores" type="text" readonly="readonly" class="form-control input-sm digits text-uppercase onlyNumbers" maxlength="2" value="<?php echo $asistentes['aplicadores'] ?>" placeholder="00"/>
                                    <span class="input-group-btn btn-flat">
                                        <a id="btn_aplicadores" class="btn btn-info btn-flat" type="button"><i class="fa fa-user"></i></a>
                                    </span>
                                </div>
                            </div>
                    </fieldset>
                </div>          
                <div class="col-md-6">
                    <fieldset class="">
                        <legend class="scheduler-border">II. INFORMACIÓN DEL ASISTENTE SUPERVISOR</legend>
                        <div class="col-md-12">
                            <div class="form-group col-md-4">
                                <label for="" class="form-label"><span class="pull-left">NOMBRES:</span></label>
                                <input type="text" name="nombre" value="<?php echo $asistentes['nombres'] ?>" class="input-sm form-control requerido text-uppercase" placeholder="Ejm. Juan Alarcon">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="" class="form-label"><span class="pull-left">APELLIDO PATERNO:</span></label>
                                <input type="text" name="ap_paterno" value="<?php echo $asistentes['apellido_paterno'] ?>" class="input-sm form-control requerido text-uppercase" placeholder="1er Apellido">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="" class="form-label"><span class="pull-left">APELLIDO MATERNO:</span></label>
                                <input type="text" name="ap_materno" value="<?php echo $asistentes['apellido_materno'] ?>" class="input-sm form-control requerido text-uppercase" placeholder="2° Apellido">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="" class="form-label"><span class="pull-left">DNI:</span></label>
                                <input type="text" name="dni" value="<?php echo $asistentes['dni'] ?>" class="dni_asistente input-sm form-control requerido text-uppercase onlyNumbers" maxlength="8" placeholder="Ejem: 12345678">
                                <input type="hidden" name="dni_hidden" id="dni_hidden" value="<?php echo $asistentes['dni'] ?>">
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="box-body col-md-12 text-center">
                 <div class="col-md-12">
                     <fieldset class="">
                        <legend class="scheduler-border">III. REGISTRO DE APLICADORE(S)</legend>
                    </fieldset>
                 </div>
                <!--<div class="col-xs-2" id="the-basics">
                    <input type="text" class="typeahead input-sm form-control requerido text-uppercase" maxlength="8" id="datos_dnisf" name="datos_dni[]" placeholder="DNI" value="345" />
                </div> -->
                <div id="aplicadores_lista" class="col-md-12">
                    <?php $cantidad_aplicadores = $asistentes['aplicadores']; ?>
                    <?php for ($i=1; $i <= $cantidad_aplicadores; $i++) { ?>
                    <?php
                        $apellidos = $nombres = $telefono = $dni = "";
                     
                        if(count($aplicadores)>0){
                            
                            if (isset($aplicadores[$i-1])){

                            $aplicador = $aplicadores[$i-1];
                            //print_r($aplicador);      
                            //$dni = $aplicador['DNI'];
                            $dni = $aplicador['nombres']. '&nbsp;' . $aplicador['apellidos']. '&nbsp;' . $aplicador['DNI'];
                            //$nombre = $aplicadores['nombres'];
                            //$apellido = $aplicadores['apellidos'];
                            
                            /*$nombres = $aplicador['nombres']; PENDIENTE. por falta de campos
                            $apellidos = $aplicador['apellidos'];
                            $telefono = $aplicador['telefono'];*/
                            }
                        }                        
                    ?>
                    <div id="aplicador_<?php echo $i; ?>">
                        <div class="col-md-12">
                            <div class="" id="remote">
                                <label for="">Aplicador #<?php echo $i; ?> &nbsp;&nbsp;</label>
                                <input type="text" class="typeahead tt form-control requerido text-uppercase onlyNumbers dni_aplicador"  maxlength="8" id="datos_dni[<?php echo $i; ?>]" name="datos_dni[]" placeholder="Ingrese DNI" value="<?php echo $dni  ?>" />
                            </div>
                           <!--  <div class="col-xs-1">
                                <input type="text" class="input-sm form-control requerido text-uppercase" maxlength="30" id="datos_nombres[<?php echo $i; ?>]" name="datos_nombres[]" placeholder="Nombres" value="<?php echo $nombres; ?>"/>
                            </div>
                            <div class="col-xs-1">
                                <input type="text" class="input-sm form-control requerido text-uppercase" maxlength="30" id="datos_apellidos[<?php echo $i; ?>]" name="datos_apellidos[]" placeholder="Apellidos" value="<?php echo $apellidos; ?>"/>
                            </div> -->
                             <!-- <div class="col-xs-12" id="remote">
                                <input type="text" class="typeahead tt requerido text-uppercase" maxlength="8" id="datos_dni[<?php echo $i; ?>]" name="datos_dni[]" placeholder="Datos del Aplicador" value="<?php echo $dni; ?>" />
                            </div> -->
                            <!-- <div class="col-xs-1">
                                <input type="hidden" class="input-sm form-control requerido text-uppercase" maxlength="15" id="datos_telefono[<?php echo $i; ?>]" name="datos_telefono[]" placeholder="Teléfono | Celular" value="<?php echo $telefono; ?>"/>
                            </div> -->
                            <!--  <div class="col-xs-1">
                                <input type="button" value="X" class="btn bg-red btn-flat cerrar btn-xs" onclick="aplicador_remover(<?php echo $i; ?>)" >
                            </div> -->
                        </div>
                    </div>
                    <?php } ?> 
                </div>
                <!-- <button type="button" id="btn_aplicador_agregar" class="btn btn-guardar btn-flat" >AGREGAR</button> -->
            </div>
            <div class="box-footer text-center">
                <input name="hidden_name_local" type="hidden"  value="valorHiddenLocal" />
                <button type="submit" class="btn btn-guardar btn-flat"  name="editarAsistente" id="btnCreateLocal" >GUARDAR EDICION</button>
                <button type="reset" class="btn bg-red btn-flat cerrar" onClick="history.go(-1)" id="btnCreateCancelar" >CANCELAR</button>
            </div>
        </form>
        <!-- LOCALES MODAL  -->
        <div class="modal fade" id="aplicador_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="text-center">
                            <i class="fa fa-spinner fa-spin fa-3x"></i>
                            <h3 class="text-navy">Cargando por favor espere...</h3>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #form_locales label{
            font-size: 10px !important;
        }
        p.text-danger {
            font-size: 11px !important;
        }
    </style>
    <script>
    $(document).ready(function () {
        $('a[rel*=facebox]').facebox();
        $('#facebox').draggable({handle: 'div.titulo'});
        $('.select2').select2();
        $('#form_locales').validate();
        $(".dni_asistente").rules("add", {
              remote: {
                    url: CI.base_url + "verificar-dni-asistente",
                    type: "POST",
                    data: {
                        dni: function () {
                             //return $("input[name=dni]").val();
                             if($("input[name=dni]").val() !== $("input[name=dni_hidden]").val())
                             {
                             return ($("input[name=dni]").val());
                            }
                        }
                    }
                },
                messages: {
                    
                    remote: jQuery.validator.format("ESTE D.N.I YA EXISTE.")
            }
        });
        $(".dni_aplicador").rules("add",{
                remote: {
                        url: CI.base_url + "Verificar_dni_aplicador",
                        type: "POST",
                        data: {
                            datos_dni: function(){
                                return $("input[name=datos_dni[]").val();
                            }
                        }
                    },
                    messages: {
                        remote: jQuery.validator.format("DNI DE APLICADOR NO EXISTE.")
                    }
        });
        $('#cod_duracion_viaje').siblings().attr('style', 'width:100%');
        // $(".open_date_time_picker").on('click',function(){
        //     console.log('sorry');
        //     open_datPicker();
        // });

            llamadasAjaxUbigeo();

            var modalLocal      = $('#aplicador_modal');
            var modalContent    = $('div.modal-content');
            $('.modal-dialog').draggable({handle: 'div.modal-header'});

            $('body').on('hidden.bs.modal', '.modal', function () {
               $(this).removeData('bs.modal');
               modalContent.html('<div class="text-center">'+
                   '<i class="fa fa-spinner fa-spin fa-3x"></i>'+
                   '<h3 class="text-navy">Cargando por favor espere...</h3>'+
               '</div>');
            });
            $("#btn_aplicadores").click(function() { 
                var cant_aplicadores = $('#local_cant_aplicadores').val();
                
                modalLocal.modal({
                    show: true,
                    backdrop: 'static',
                    remote: CI.base_url+'asistente-supervisor/ie/aplicadores?aplicadores='+cant_aplicadores,
                });
            });
        });
      $("#btn_aplicadores").click(function() { 
             aplicadores_agregar();
        });

        $("#btn_aplicador_agregar").click(function() { 
            aplicador_agregar();
        });

    });
    var contador = 0;
    contador = parseInt($("#local_cant_aplicadores").val());

    function aplicador_agregar()
    {           
        contador++;
        var contenido = aplicador_registro(contador + 1);
        $("#aplicadores_lista").append(contenido);
        cantidad = parseInt($("#local_cant_aplicadores").val()) + 1;
        $("#local_cant_aplicadores").val(cantidad);
        console.log("contador : " + contador + " cantidad : " + cantidad);
    }

    function aplicadores_agregar()
    {
        var cantidad = $("#local_cant_aplicadores").val();
        $("#aplicadores_lista div").remove();

        if(cantidad == 0 || cantidad == "")
            return;      

        for (var i = 1; i <= cantidad; i++) {
            var contenido = aplicador_registro(cantidad);
            $("#aplicadores_lista").append(contenido);
        };
        contador = cantidad;
    }

    function aplicador_remover(posicion)
    {        
        $( "#aplicador_" + posicion ).remove();
        var cantidad = parseInt($("#local_cant_aplicadores").val()) - 1;
        $("#local_cant_aplicadores").val(cantidad);
        console.log("contador : " + contador + " cantidad : " + cantidad);
    }

    function aplicador_registro(cantidad)
    {
        var contenido = '<div id="aplicador_' + cantidad + '" class="col-xs-12 sinpadding margin-bottom-20">' +
                        '<div class="row">' + 
                            ' <div class="col-xs-1"><input type="text" class="input-sm form-control requerido text-uppercase" maxlength="30" id="datos_nombres[' + cantidad + ']" name="datos_nombres[]" placeholder="Nombres"/></div> ' +
                            ' <div class="col-xs-1"><input type="text" class="input-sm form-control requerido text-uppercase" maxlength="30" id="datos_apellidos[' + cantidad + ']" name="datos_apellidos[]" placeholder="Apellidos"/></div> ' +
                            ' <div class="col-xs-2"><input type="text" class="typeahead tt tt-menu input-sm form-control requerido text-uppercase" maxlength="8"  id="datos_dni[' + cantidad + ']" name="datos_dni[]" placeholder="DNI"/></div> ' +
                            ' <div class="col-xs-1"><input type="text" class="input-sm form-control requerido text-uppercase" maxlength="15" id="datos_telefono[' + cantidad + ']" name="datos_telefono[]" placeholder="Teléfono | Celular"/></div> ' +
                            ' <div class="col-xs-1"><input type="button" value="X" class="btn bg-red btn-flat cerrar" onclick="aplicador_remover(' + cantidad + ')" > ' +
                            ' </div> ' +
                        '</div>' + 
                        '</div>';
        return contenido;
    }

    </script>
    <script>
            $(document).ready(function(){
            
            var bestPictures = new Bloodhound({
              datumTokenizer: Bloodhound.tokenizers.whitespace,
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              //prefetch: CI.base_url+"ajax/aplicadores/check_dni?dni=7000",
              remote: {
                url: CI.base_url+"ajax/aplicadores/check_dni?dni=%ELVIS",
                wildcard: '%ELVIS'
              }
            });

            $('#remote .typeahead').typeahead({
                  minLength: 1,
                  highlight: true
                }, {
                source: bestPictures
            });

            $('#remote .typeahead').on('typeahead:select', function(ev, suggestion) {
              console.log('Selection: ' + suggestion);
              console.dir(suggestion);
            });
        });
    </script>
</section>

