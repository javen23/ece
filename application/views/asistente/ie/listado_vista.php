<section class="content">
    <div class="box noBox">
        <?php if ($msg = $this->session->flashdata('mensaje_flash')): ?>
            <div class="alert-message">
                <div class="alert alert-<?php echo $msg['tipo'] ?> alert-dismissable">
                    <button type="button" class="close text-warning" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><?php echo $msg['cuerpo'] ?></strong>
                </div>
            </div>
        <?php endif; ?>
        <div class="box-header">
            <!-- <h3 class="box-title pull-right">
                <a href="<?php echo base_url('asistente-supervisor/ie/crear'); ?>" class="btn btn-success btn-flat awhite">
                    <span class="fa fa-plus-circle"></span> CREAR REGISTRO
                </a>
            </h3> -->
            <h3 class="box-title">
                EXPORTAR LISTADO DE INSTITUCIONES EDUCATIVAS
                <span class="inline-block">
                    <form action="<?php echo base_url('ajax-exportar-tabla') ?>" method="POST" target="_blank" id="form_exportar_tabla">
                        <span class="pull-right cursor-pointer exportar_tabla_reporte" data-toggle="tooltip" title="Descargar <?php echo $titulo ?>"><i class="fa fa-cloud-download fa-2x text-green"></i></span>
                        <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
                        <input type="hidden" name="nombre_archivo" value="<?php echo url_amigable(notildes($titulo)) ?>">
                    </form>
                </span>
            </h3>
        </div>
        <div class="box-body table-responsive">
            <table width="100%" id="tablaListado" class="display table table-bordered table-striped table-hover" >
                <thead class="headTablaListado">
                    <tr class="text-uppercase">
                        <th colspan="9" class="seccion1">I. INFORMACIÓN DE LA II.EE</th>
                        <th colspan="6" class="seccion2 border-th-header-left border-th-header-right">II. INFORMACIÓN DEL APLICADOR</th>
                        <th colspan="3" class="seccion3">ACCIONES</th>
                    </tr>
                    <tr class="text-uppercase th-head-inputs">
                        <th>N°</th>
                        <th>SEDE REGIONAL</th>
                        <th>SEDE PROVINCIAL</th>
                        <th>SEDE DISTRITAL</th>
                        <th>DEPARTAMENTO</th>
                        <th>PROVINCIA</th>
                        <th>DISTRITO</th>
                        <th>COD.MODULAR</th>
                        <th class="border-th-header-right">INSTITUCIÓN EDUCATIVA</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>DNI</th>
                        <th>CANTIDAD DE APLICADORES</th>
                        <th>DURACIÓN TOTAL DEL VIAJE <br/>SEDE A II.EE</th>
                        <th>FECHA DE SALIDA</th>
                        <th class="border-th-header-left">VER APLICADORES</th>
                        <th>EDITAR</th>
                        <th>ELIMINAR</th>
                    </tr>
                </thead>
                <tfoot class="footTablaListado">
                    <tr class="text-uppercase tfoot-placeholder">
                        <th>N°</th>
                        <th>SEDE REGIONAL</th>
                        <th>SEDE PROVINCIAL</th>
                        <th>SEDE DISTRITAL</th>
                        <th>DEPARTAMENTO</th>
                        <th>PROVINCIA</th>
                        <th>DISTRITO</th>
                        <th>COD. MODULAR</th>
                        <th>INSTITUCIÓN EDUCATIVA</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>DNI</th>
                        <th>CANTIDAD DE APLICADORES</th>
                        <th>DURACIÓN TOTAL DEL VIAJE <br/>SEDE A II.EE</th>
                        <th>FECHA DE SALIDA</th>
                    </tr>
                </tfoot>
                <tbody class="bodyTablaListado">
                    <?php $n=1; ?>
                    <?php foreach ($asistentes as $item): ?>
                        <tr class="text-center">
                           <td class="text-center"><?php echo $n++; ?></td>
                           <td><?php echo $item['sede_operativa'] ?></td>
                           <td><?php echo $item['sede_provincial'] ?></td>
                           <td><?php echo $item['sede_distrital'] ?></td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td><?php echo $item['cod_modular'] ?></td>
                           <td><?php echo $item['nombre_ie'] ?></td>
                           <td><?php echo $item['nombres'] ?></td>
                           <td><?php echo $item['apellido_paterno']. '&nbsp;' . $item['apellido_materno'] ?></td>
                           <td><?php echo $item['dni'] ?></td>
                           <td><?php echo $item['aplicadores'] ?></td>
                           <td><?php echo si_no($item['viaje'], '', $this->dataselect->valores_total_viaje()) ?></td>
                           <td><?php echo $item['salida'] ?></td>
                           <td class="text-center"><a href="<?php echo base_url('asistente-supervisor/ie/editar'); ?>?id=<?php echo $item['cod_asistente'] ?>" data-toggle='tooltip' title="VER APLICADORES" class="btn btn-flat btn-success"><i class="fa fa-eye"></i></a></td>
                           <td class="text-center"><a href="<?php echo base_url('asistente-supervisor/ie/editar'); ?>?id=<?php echo $item['cod_asistente'] ?>" data-toggle='tooltip' title="EDITAR" class="btn btn-flat btn-primary"><i class="icon ion-edit"></i></a></td>
                           <td class="text-center"><a href="<?php echo base_url('asistente-supervisor/ie/eliminar'); ?>?id=<?php echo $item['cod_asistente'] ?>" rel='facebox' data-toggle='tooltip' title="ELIMINAR" class="btn btn-flat btn-danger"><i class="icon ion-trash-a"></i></a></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
    
</section>
    <script>
        $(function () {
            $('a[rel*=facebox]').facebox();
            $('#facebox').draggable({handle: 'div.titulo'});

            tablaListadoDataTable();

                llamadasAjaxUbigeo();

            var modalLocal      = $('#aplicador_modal');
            var modalContent    = $('div.modal-content');
            $('.modal-dialog').draggable({handle: 'div.modal-header'});

            $('body').on('hidden.bs.modal', '.modal', function () {
               $(this).removeData('bs.modal');
               modalContent.html('<div class="text-center">'+
                   '<i class="fa fa-spinner fa-spin fa-3x"></i>'+
                   '<h3 class="text-navy">Cargando por favor espere...</h3>'+
               '</div>');
            });
            $("#btn_aplicadores").click(function() { 
                var cant_aplicadores = $('#local_cant_aplicadores').val();
                
                modalLocal.modal({
                    show: true,
                    backdrop: 'static',
                    remote: CI.base_url+'asistente-supervisor/ie/aplicadores?aplicadores='+cant_aplicadores,
                });
            });
        });


    </script>
