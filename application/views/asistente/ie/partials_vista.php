<?php $_local['cod_sede_operativa'] = isset($local['cod_sede_operativa']) ? $local['cod_sede_operativa'] : '-1'; ?>
<?php $_local['ccdd'] = isset($local['ccdd']) ? $local['ccdd'] : '-1'; ?>
<?php $_local['ccpp'] = isset($local['ccpp']) ? $local['ccpp'] : '-1'; ?>
<?php $_local['ccdi'] = isset($local['ccdi']) ? $local['ccdi'] : '-1'; ?>
<?php $local = isset($local) ? $local : array(); ?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" title="I. Ubicación de la Sede Operativa">Sección I</a></li>
        <li><a href="#tab_2" data-toggle="tab" title="II. Información del Local">Sección II</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1" >
            <div class="box-header page-header">
                <h3 class="box-title">I. INFORMACIÓN DE LA INSTITUCIÓN EDUCATIVA</h3>
            </div>
            <div class="box-body form-horizontal">
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">SEDE OPERATIVA</span></label>
                    <div class="col-sm-4">
                        <select name="local_sede" id="local_sede"  class="select2 col-xs-12 requerido sinpadding" title="Seleccione sede">
                            <option value="">-SELECCIONE-</option>
                            <?php foreach($sedes as $sede): ?>
                                <option
                                    value="<?php show($sede, 'cod_sede_operativa') ?>"
                                >
                                    <?php show($sede, 'sede_operativa') ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">DEPARTAMENTO</span></label>
                    <div class="col-sm-4">
                        <select name="local_departamento" id="local_departamento" class="select2 col-xs-12 requerido sinpadding" value="bagua"  title="Seleccione departamento">
                            <option value="" > -SELECCIONE- </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">PROVINCIA</span></label>
                    <div class="col-sm-4">
                        <select name="local_provincia" id="local_provincia" class="select2 col-xs-12 requerido sinpadding" value="bagua"  title="Seleccione provincia">
                            <option value="" > -SELECCIONE- </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">DISTRITO</span></label>
                    <div class="col-sm-4">
                        <select name="local_distrito" id="local_distrito" class="select2 col-xs-12 requerido sinpadding" value="distrito2"  title="Seleccione distrito">
                            <option value="" > -SELECCIONE- </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">INSTITUCIÓN EDUCATIVA</span></label>
                    <div class="col-sm-4">
                        <select name="local_ie" id="local_ie"  class="select2 col-xs-12 requerido sinpadding" title="Seleccione Institución Educativa">
                            <option value="">-SELECCIONE-</option>
                            <?php foreach($elementos as $item): ?>
                                <option
                                    value="<?php show($item, 'cod_modular') ?>"
                                    <?php echo ($_local['cod_sede_operativa'] == $item['cod_modular']) ? 'selected' : ''; ?>
                                >
                                    <?php show($item, 'nombre_ie') ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tab_2">
            <div class="box-header page-header">
                <h3 class="box-title">II. INFORMACIÓN DEL APLICADOR</h3>
            </div>
            <div class="box-body form-horizontal">
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">CANTIDAD DE APLICADORES</span></label>
                    <div class="col-sm-4">
                        <input type="text" class="input-sm form-control requerido digits text-uppercase onlyNumbers" value="<?php show($local,'cant_aplicadores') ?>" name="local_cant_aplicadores" maxlength="2" id="local_cant_aplicadores" placeholder="05"/>
                    </div>
                    <a href="<?php echo base_url('asistente-supervisor/ie/aplicadores') ?>?aplicadores=3" class="btn btn-flat btn-info btn-huge" rel="facebox" id="btn_aplicadores">Registrar Aplicadores</a>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">DURACIÓN TOTAL DEL VIAJE DE SEDE A II.EE</span></label>
                    <div class="col-sm-4">
                        <select name="cod_duracion_viaje" id="cod_duracion_viaje"  class="select2 col-xs-12 requerido" width="100%" title="Seleccione una opción">
                            <option value="">-SELECCIONE-</option>
                            <?php foreach($this->dataselect->valores_total_viaje() as $key => $item): ?>
                                <option
                                    value="<?php echo $item[0] ?>"
                                    <?php echo ($_local['cod_sede_operativa'] == $item[0]) ? 'selected' : ''; ?>
                                >
                                    <?php echo $item[1] ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">FECHA Y HORA DE SALIDA</span></label>
                    <div class="col-xs-12 col-sm-4">
                        <div class="row">
                            <div class="col-xs-6 ">
                                <input type="text" class="form-control text-center requerido input-sm text-uppercase" value="<?php show($local,'fecha_salida') ?>" data-inputmask="'mask':'99/99/9999'" data-mask name="fecha_salida" id="fecha_salida" placeholder="31/12/2015"/>
                            </div>
                            <div class="col-xs-6 ">
                                <input type="text" class="form-control text-center requerido input-sm text-uppercase" value="<?php show($local,'hora_salida') ?>" data-inputmask="'mask': '99:99'" data-mask name="hora_salida" id="hora_salida" placeholder="15:30"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    (function($){
        var btn_aplicadores     = $('#btn_aplicadores');
        var total_aplicadores   = $('#local_cant_aplicadores');
        var remote_url          = '<?php echo base_url('asistente-supervisor/ie/aplicadores') ?>';

        // btn_aplicadores.on('click', function(ev){
        //     ev.preventDefault();
        //
        //     remote_url += '?aplicadores='+total_aplicadores.val();
        //
        //     jQuery.facebox({ajax: remote_url});
        //     ev.stopPropagation();
        // });

    })(jQuery);
</script>
