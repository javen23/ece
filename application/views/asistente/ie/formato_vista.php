<section class="content">
    <div class="box noBox">
    	<form>
    		<fieldset>	
    			<div class="box-body col-md text-center">
			        <div class="box-header">
			        	<h3 class="box-title-pull-center">
			                FORMATO DE ACCESIBILIDAD A LA INSTITUCION EDUCATIVA
			            </h3>
			        </div>
			        <div class="row">
			        	<div class="col-md-6">
							<div class="box-header">
			        			<table border="1px" width="100%">
			        			<thead class="box box-danger">
			        			<tr>
			        				<th colspan="2">1. UBICACIÓN GEOGRÁFICA</th>
			        			</tr>
			        			</thead>
			        			<tbody>
			        				<tr>
				        				<td width="50%" class="form-group col-ms-6">1. SDE OPERATIVA</td>
				        				<td width="50%" class="form-group col-ms-6"></td>
					        		</tr>
					        		<tr>
					        			<td>2. DEPARTAMENTO</td>
				        				<td>&nbsp;</td>
					        		</tr>
					        		<tr>
					        			<td>3. PROVINCIA</td>
					        			<td>&nbsp;</td>
					        		</tr>
					        		<tr>
					        			<td>4. DISTRITO</td>
					        			<td>&nbsp;</td>
					        		</tr>
					        		<tr>
					        			<td>5. CENTRO POBLADO</td>
					        			<td>&nbsp;</td>
					        		</tr>
			        			</tbody>
			        			</table>
			        		</div>
			        	</div>
			        	<div class="col-md-6">
			        		<div class="box-header">
			        			<table border="1px" width="100%">
			        			<thead class="box box-primary">
									<tr>
										<th colspan="2">2. INFORMACIÓN DE LA INSTITUCIÓN EDUCATIVA</th>
									</tr>
			        			</thead>
			        			<tbody>
			        				<tr>
			        					<td  width="50%" class="form-group col-ms-6">6. Nombre de la UGEL</td>
			        					<td width="50%" >&nbsp;</td>
			        				</tr>
			        				<tr>
			        					<td class="form-group col-ms-6">7. CODIGO MODULAR</td>
			        					<td>&nbsp;</td>
			        				</tr>
			        				<tr>
			        					<td class="form-group col-ms-6">8. NOMBRE DE LA I.E</td>
			        					<td>&nbsp;</td>
			        				</tr>
			        				<tr>
			        					<td class="form-group col-ms-6">N° DE SECCIONES</td>
			        					<td>&nbsp;</td>
			        				</tr>
			        				<tr>
			        					<td class="form-group col-ms-6">N° DE APLICADORES</td>
			        					<td>&nbsp;</td>
			        				</tr>
			        			</tbody>
			        		</table>
			        		</div>
			        	</div>
			        </div>
		        </div>
    		</fieldset>
        </form>
    </div>
</section>

