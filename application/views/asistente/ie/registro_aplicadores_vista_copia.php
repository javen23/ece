<?php $local = array(); ?>
<div class="titulo text-center"><?php echo mb_strtoupper($titulo, 'UTF-8') ?></div>
<div class="cuerpo" style="width: 750px">
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_locales" method="POST" action="<?php echo base_url('asistente-supervisor/ie/aplicadores') ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-offset-1 col-xs-10">
                    <?php if($cant_aplicadores>0){ ?>
                    <?php for ($i=1; $i <= $cant_aplicadores; $i++): ?>
                    <div class="row">
                        <fieldset>
                            <legend class="fieldset-legend-title">INFORMACIÓN DEL APLICADOR <big>#<?php echo $i; ?></big> </legend>
                            <label for="">NOMBRES Y APELLIDOS</label>
                            <div class="col-xs-12 sinpadding margin-bottom-20">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="text" class="input-sm col-xs-6 form-control requerido text-uppercase" value="<?php show($local,'cant_aplicadores') ?>" name="local_cant_aplicadores" maxlength="2" id="local_cant_aplicadores" placeholder="Nombres"/>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" class="input-sm col-xs-6 form-control requerido text-uppercase" value="<?php show($local,'cant_aplicadores') ?>" name="local_cant_aplicadores" maxlength="2" id="local_cant_aplicadores" placeholder="Apellidos"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 sinpadding margin-bottom-20">
                                <label for="">DNI Y TELÉFONO</label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="text" class="input-sm form-control requerido text-uppercase" value="<?php show($local,'cant_aplicadores') ?>" name="local_cant_aplicadores" maxlength="2" id="local_cant_aplicadores" placeholder="DNI"/>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" class="input-sm form-control requerido text-uppercase" value="<?php show($local,'cant_aplicadores') ?>" name="local_cant_aplicadores" maxlength="2" id="local_cant_aplicadores" placeholder="Teléfono | Celular"/>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <?php endfor; ?>
                    <?php } else { ?>
                    <p>No se encontraron aplicadores.</p>
                    <?php } ?>
                </div>

                <div class="col-xs-12 margin-top-20">
                    <input name="hidden_name_local" type="hidden"  value="valorHiddenLocal"/>
                    <input type="submit" class="btn btn-guardar btn-flat" value="GUARDAR" name="crearLocal" id="btnCreateLocal">
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        //$('.maskDecimal').autoNumeric('init');
        $('.select2').select2();
        $('[data-mask]').inputmask();
        $('#form_locales').validate();
    });
</script>
