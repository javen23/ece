<section class="content">
	<div class="row">

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('asistente-supervisor/ie') ?>" data-toggle="tooltip" title="CAPACITACION DE DIRECTORES">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-street-view"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Instituciones educativas</span>
						<span class="info-box-number">Cronograma de Salida por Asistentes</span>
					</div>
				</div>
			</a>
		</div>

	</div>
</section>
