
<?php $aplicadores = array(); ?>
<div class="modal-header" style="cursor:move;">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-street-view"></i> <?php echo mb_strtoupper($titulo, 'UTF-8') ?></h4>
</div>
<div class="cuerpo" >
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_locales" method="POST" action="<?php echo base_url('asistente-supervisor/ie/crear') ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-offset-1 col-xs-10">
                    <?php for ($i=1; $i <= $cant_aplicadores; $i++): ?>
                    <div class="row">
                        <fieldset>
                            <legend class="fieldset-legend-title">INFORMACIÓN DEL APLICADOR <big>#<?php echo $i; ?></big> </legend>
                            <label for="">NOMBRES Y APELLIDOS</label>
                            <div class="col-xs-12 sinpadding margin-bottom-20">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="text" class="col-xs-6 form-control requerido text-uppercase onlyStrings" value="" name="nombres" id="nombre" placeholder="Nombres"/>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" class="col-xs-6 form-control requerido text-uppercase onlyStrings" value="" name="apellidos" id="apellidos" placeholder="Apellidos"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 sinpadding margin-bottom-20">
                                <label for="">DNI Y TELÉFONO</label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control requerido text-uppercase onlyNumbers" value="" name="dni" id="dni" maxlength="8" placeholder="DNI"/>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control requerido text-uppercase onlyNumbers" value="" name="cel"  id="cel" placeholder="Teléfono | Celular"/>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <?php endfor; ?>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="cod_asistente" type="hidden"  value="valorHiddenLocal"/>
                    <button type="submit" class="btn btn-guardar btn-flat" name="crearAplicador" id="btnCrarAplicador">Guardar</button>
                    <button type="button" class="btn bg-red btn-flat" data-dismiss="modal">Cerrar</button>
                    <!--<button type="button"  class="btn bg-red btn-flat cerrar" id="">Cancelar</button>-->
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        //$('.maskDecimal').autoNumeric('init');
        $('.select2').select2();
        $('[data-mask]').inputmask();
        $('#form_locales').validate();

        $(".onlyNumbers").keypress(function (e) {
            return validarNumero(e);
        });

        $(".onlyStrings").keypress(function (e) {
            return validarLetras(e);
        });
    });
</script>
