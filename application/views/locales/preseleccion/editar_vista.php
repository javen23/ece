<div class="titulo text-center">EDITAR LOCAL DE PRE SELECCIÓN</div>
<div class="cuerpo" style="width: 750px">
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_locales" method="POST" action="<?php echo base_url('locales-preseleccion/editar') ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-12 col-sm-4">
                    <?php $this->load->view('locales/ubigeo_vista'); ?>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <fieldset>
                        <legend class="fieldset-legend-title">DATOS DEL LOCAL DE PRE SELECCIÓN</legend>
                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">NOMBRE DEL LOCAL</label>
                            <input type="text" class="input-sm form-control requerido text-uppercase" value="<?php echo $local['nombre_local']; ?>" name="local_nombre" id="local_nombre" placeholder="Nombre"/>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">DIRECCIÓN</label>
                            <input type="text" class="input-sm form-control requerido text-uppercase" value="<?php echo $local['direccion']; ?>" name="local_direccion" id="local_direccion" placeholder="Dirección"/>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">REFERENCIA</label>
                            <input type="text" class="input-sm form-control requerido text-uppercase" value="<?php echo $local['referencia']; ?>" name="local_referencia" id="local_referencia" placeholder="Referencia"/>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <div class="row-fluid">
                                <div class="col-xs-3 sinpadding">
                                    <label for="">LUZ <span class="hidden-xs">ELECTRICA</span><br/>(SI = 1/NO = 0)</label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control requerido si-no text-uppercase" value="<?php echo $local['electricidad']; ?>" data-inputmask="'mask':'9', 'placeholder':''" data-mask name="local_luz" id="local_luz" placeholder="SI - NO"/>
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <label for="">SS.HH<br/> (SI = 1/NO = 0)</label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control requerido si-no text-uppercase" value="<?php echo $local['sshh']; ?>" data-inputmask="'mask':'9', 'placeholder':''" data-mask name="local_sshh" id="local_sshh" placeholder="SI - NO"/>
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <label for="">
                                        <span class="visible-xs"><br>PEA</span>
                                        <span class="hidden-xs">PEA A<br/>CAPACITAR</span>
                                    </label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control requerido text-uppercase" value="<?php echo $local['pea_programa']; ?>" data-inputmask="'mask':'999999', 'placeholder':''" data-mask name="local_pea" id="local_pea" placeholder="PEA"/>
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <label for="">
                                        <span class="visible-xs"><br>AULAS</span>
                                        <span class="hidden-xs">AULAS<br/>PROGRAMADAS</span>
                                    </label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control text-uppercase" value="<?php echo $local['aulas_program']; ?>"  readonly="readonly" name="local_aulas" id="local_aulas" placeholder="AULAS"/>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div id="container_resto_pea" class="col-xs-12 sinpadding margin-top-10" style="display:none;">
                                    <div class="alert-message">
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <strong>Tiene <span id="resto_pea">0</span> PEA sobrantes</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <div class="col-xs-offset-3 col-xs-3 sinpadding">
                                <label for="">HORA DE<br>INICIO</label>
                                <input type="text" name="local_hora_inicio" id="local_hora_inicio" data-inputmask="'mask': '99:99'" data-mask class="col-xs-12 text-center requerido input-sm form-control text-uppercase" placeholder="HORA DE INICIO" value="<?php echo $local['hora_inicio']; ?>">
                            </div>
                            <div class="col-xs-3 no-padding-right">
                                <label for="">HORA DE TÉRMINO</label>
                                <input type="text" name="local_hora_fin" id="local_hora_fin" data-inputmask="'mask': '99:99'" data-mask class="col-xs-12 text-center requerido input-sm form-control text-uppercase" placeholder="HORA DE TÉRMINO" value="<?php echo $local['hora_fin']; ?>">
                            </div>
                        </div>


                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">OBSERVACIONES</label>
                            <textarea name="local_observacion" id="local_observacion" class="input-sm form-control text-uppercase" maxlength="700" cols="30" rows="5" placeholder="OBSERVACIONES"><?php echo $local['observacion']; ?></textarea>
                        </div>

                    </fieldset>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="local_id_local" type="hidden" value="<?php echo $local['id_local']; ?>"/>
                    <button type="submit" class="btn btn-guardar btn-flat " name="editarLocal" id="btnCreateLocal">GUARDAR EDICIÓN</button>
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                </div>

            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        //$('.maskDecimal').autoNumeric('init');
        $('.select2').select2();
        $('[data-mask]').inputmask();
        $('#form_locales').validate();

        limitePeaLocales();
        llamadasAjaxUbigeo();
    });
</script>
