<div class="titulo text-center">CREAR LOCAL DE PRE SELECCIÓN</div>
<div class="cuerpo" style="width: 750px">
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_locales" method="POST" action="<?php echo base_url('locales-preseleccion/crear') ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-12 col-sm-4">
                    <fieldset>
                        <legend class="fieldset-legend-title">UBIGEO</legend>
                        <div class="col-xs-12 sinpadding margin-bottom-20">
                            <label for="">SEDE OPERATIVA</label>
                            <select name="local_sede" id="local_sede"  class="select2 requerido col-xs-12 sinpadding" title="Seleccione sede">
                                <option value="">-Seleccione-</option>
                                <?php foreach($sedes as $sede): ?>
                                    <option value="<?php echo $sede['cod_sede_operativa'] ?>"><?php echo $sede['sede_operativa'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-xs-12 sinpadding margin-bottom-20">
                            <label for="">DEPARTAMENTO</label>
                            <select name="local_departamento" id="local_departamento" class="select2 requerido col-xs-12 sinpadding" title="Seleccione departamento">
                                <option value="" >
                                    -SELECCIONE-
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-12 sinpadding margin-bottom-20">
                            <label for="">PROVINCIA</label>
                            <select name="local_provincia" id="local_provincia" class="select2 requerido col-xs-12 sinpadding"  title="Seleccione provincia">
                                <option value="" >
                                    -SELECCIONE-
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-12 sinpadding margin-bottom-20">
                            <label for="">DISTRITO</label>
                            <select name="local_distrito" id="local_distrito" class="select2 requerido col-xs-12 sinpadding" title="Seleccione distrito">
                                <option value="" >
                                    -SELECCIONE-
                                </option>
                            </select>
                        </div>
                    </fieldset>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <fieldset>
                        <legend class="fieldset-legend-title">DATOS DEL LOCAL DE PRE SELECCIÓN</legend>
                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">NOMBRE DEL LOCAL</label>
                            <input type="text" class="input-sm form-control requerido text-uppercase" value="" name="local_nombre" id="local_nombre" placeholder="Nombre"/>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">DIRECCIÓN</label>
                            <input type="text" class="input-sm form-control requerido text-uppercase" value="" name="local_direccion" id="local_direccion" placeholder="Dirección"/>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">REFERENCIA</label>
                            <input type="text" class="input-sm form-control requerido text-uppercase" value="" name="local_referencia" id="local_referencia" placeholder="Referencia"/>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <div class="row-fluid">
                                <div class="col-xs-3 sinpadding">
                                    <label for="">LUZ <span class="hidden-xs">ELÉCTRICA</span><br/>(SI = 1/NO = 0)</label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control requerido si-no text-uppercase" value="" data-inputmask="'mask':'9', 'placeholder':''" data-mask name="local_luz" id="local_luz" placeholder="SI - NO"/>
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <label for="">SS.HH<br/> (SI = 1/NO = 0)</label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control requerido si-no text-uppercase" value="" data-inputmask="'mask':'9', 'placeholder':''" data-mask name="local_sshh" id="local_sshh" placeholder="SI - NO"/>
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <label for="">
                                        <span class="visible-xs"><br>PEA</span>
                                        <span class="hidden-xs">PEA A<br/>CAPACITAR</span>
                                    </label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control requerido digits text-uppercase" value="" data-inputmask="'mask':'999999', 'placeholder':''" data-mask name="local_pea" id="local_pea" placeholder="PEA"/>
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <label for="">
                                        <span class="visible-xs"><br>AULAS</span>
                                        <span class="hidden-xs">AULAS<br/>PROGRAMADAS</span>
                                    </label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control text-uppercase" value="" readonly="readonly" name="local_aulas" id="local_aulas" placeholder="AULAS"/>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div id="container_resto_pea" class="col-xs-12 sinpadding margin-top-10" style="display:none;">
                                    <div class="alert-message">
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <strong>Tiene <span id="resto_pea">0</span> PEA sobrantes</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <div class="col-xs-offset-3 col-xs-3 sinpadding">
                                <label for="">HORA DE<br>INICIO</label>
                                <input type="text" name="local_hora_inicio" id="local_hora_inicio" data-inputmask="'mask': '99:99'" data-mask class="col-xs-12 text-center requerido input-sm form-control text-uppercase" placeholder="HORA DE INICIO" value="">
                            </div>
                            <div class="col-xs-3 no-padding-right">
                                <label for="">HORA DE TÉRMINO</label>
                                <input type="text" name="local_hora_fin" id="local_hora_fin" data-inputmask="'mask': '99:99'" data-mask class="col-xs-12 text-center requerido input-sm form-control text-uppercase" placeholder="HORA DE TÉRMINO" value="">
                            </div>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">OBSERVACIONES</label>
                            <textarea name="local_observacion" id="local_observacion" class="input-sm form-control text-uppercase" maxlength="700" cols="30" rows="5" placeholder="OBSERVACIONES"></textarea>
                        </div>

                    </fieldset>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="hidden_name_local" type="hidden"  value="valorHiddenLocal"/>
                    <button type="submit" class="btn btn-guardar btn-flat " name="crearLocal" id="btnCreateLocal">CREAR LOCAL</button>
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                </div>

            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        //$('.maskDecimal').autoNumeric('init');
        $('.select2').select2();
        $('[data-mask]').inputmask();
        $('#form_locales').validate();

        limitePeaLocales();
        llamadasAjaxUbigeo();
    });
</script>
