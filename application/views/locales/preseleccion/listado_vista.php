<section class="content">
    <div class="box noBox">
        <?php if ($msg = $this->session->flashdata('mensaje_flash')): ?>
            <div class="alert-message">
                <div class="alert alert-<?php echo $msg['tipo'] ?> alert-dismissable">
                    <button type="button" class="close text-warning" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><?php echo $msg['cuerpo'] ?></strong>
                </div>
            </div>
        <?php endif; ?>
        <div class="box-header">
            <h3 class="box-title pull-right"><a href="<?php echo base_url('locales-preseleccion/crear'); ?>" class="btn btn-success btn-flat awhite" rel="facebox"><span class="fa fa-plus-circle"></span> AGREGAR LOCAL</a></h3>
            <h3 class="box-title">
                EXPORTAR LISTADO DE LOCALES
                <span class="inline-block">
                    <form action="<?php echo base_url('ajax-exportar-tabla') ?>" method="POST" target="_blank" id="form_exportar_tabla">
                        <span class="pull-right cursor-pointer exportar_tabla_reporte" data-toggle="tooltip" title="Descargar <?php echo $titulo ?>"><i class="fa fa-cloud-download fa-2x text-green"></i></span>
                        <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
                        <input type="hidden" name="nombre_archivo" value="<?php echo url_amigable(notildes($titulo)) ?>">
                    </form>
                </span>
            </h3>
        </div>
        <div class="box-body table-responsive">
            <table width="100%" id="tablaListado" class="display table table-bordered table-striped table-hover" >
                <thead class="headTablaListado">
                    <tr class="text-uppercase">
                        <th colspan="5">UBICACIÓN DE LA SEDE OPERATIVA</th>
                        <th colspan="7" class="border-th-header-left border-th-header-right">INFORMACIÓN DEL LOCAL DE PRESELECCIÓN</th>
                        <th colspan="3" class="border-th-header-right">HORARIO DE PRUEBA</th>
                        <th colspan="2">ACCIONES</th>
                    </tr>
                    <tr class="text-uppercase th-head-inputs">
                        <th>N°</th>
                        <th>SEDE OPERATIVA</th>
                        <th>DEPARTAMENTO</th>
                        <th>PROVINCIA</th>
                        <th>DISTRITO</th>
                        <th class="border-th-header-left">NOMBRE LOCAL</th>
                        <th>DIRECCIÓN</th>
                        <th>REFERENCIA</th>
                        <th>LUZ <br>ELÉCTRICA<br>(S/N)</th>
                        <th>SS.HH <br>(S/N)</th>
                        <th>PEA <br>CAPACITAR</th>
                        <th class="border-th-header-right">AULAS <br>PROGRAMADAS</th>
                        <th>HORA <br>INICIO</th>
                        <th>HORA <br>TÉRMINO</th>
                        <th class="border-th-header-right">OBSERVACIONES</th>
                        <th>EDITAR</th>
                        <th>ELIMINAR</th>
                    </tr>
                </thead>
                <tfoot class="footTablaListado">
                    <tr class="text-uppercase tfoot-placeholder">
                        <th>N°</th>
                        <th>SEDE OPERATIVA</th>
                        <th>DEPARTAMENTO</th>
                        <th>PROVINCIA</th>
                        <th>DISTRITO</th>
                        <th>NOMBRE LOCAL</th>
                        <th>DIRECCIÓN</th>
                        <th>REFERENCIA</th>
                        <th>LUZ <br>ELÉCTRICA<br>(S/N)</th>
                        <th>SS.HH <br>(S/N)</th>
                        <th>PEA <br>CAPACITAR</th>
                        <th>AULAS<br>PROGRAMADAS</th>
                        <th>HORA <br>INICIO</th>
                        <th>HORA <br>TÉRMINO</th>
                        <th>OBSERVACIONES</th>
                        <th>EDITAR</th>
                        <th>ELIMINAR</th>
                    </tr>
                </tfoot>
                <tbody class="bodyTablaListado">
                    <?php
                     $n=1;
                     foreach ($locales as $local): ?>
                        <tr>
                           <td class="text-center"><?php echo $n++; ?></td>
                           <td><?php echo $local['sede_operativa'] ?></td>
                           <td><?php echo $local['departamento'] ?></td>
                           <td><?php echo $local['provincia'] ?></td>
                           <td><?php echo $local['distrito'] ?></td>
                           <td><?php echo $local['nombre_local'] ?></td>
                           <td><?php echo $local['direccion'] ?></td>
                           <td><?php echo $local['referencia'] ?></td>
                           <td><?php echo ($local['electricidad'] ? 'Si' : 'No') ?></td>
                           <td><?php echo ($local['sshh'] ? 'Si' : 'No') ?></td>
                           <td><?php echo $local['pea_programa'] ?></td>
                           <td><?php echo $local['aulas_program'] ?></td>
                           <td><?php echo $local['hora_inicio'] ?></td>
                           <td><?php echo $local['hora_fin'] ?></td>
                           <td><?php echo $local['observacion'] ?></td>
                           <td class="text-center"><a href="<?php echo base_url('locales-preseleccion/editar'); ?>?idLocal=<?php echo $local['id_local'] ?>" rel='facebox' data-toggle='tooltip' title="EDITAR" class="btn btn-flat btn-primary"><i class="icon ion-edit"></i></a></td>
                           <td class="text-center"><a href="<?php echo base_url('locales-preseleccion/eliminar'); ?>?idLocal=<?php echo $local['id_local'] ?>" rel='facebox' data-toggle='tooltip' title="ELIMINAR" class="btn btn-flat btn-danger"><i class="icon ion-trash-a"></i></a></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
    $(function () {
        $('a[rel*=facebox]').facebox();
        $('#facebox').draggable({handle: 'div.titulo'});

        tablaListadoDataTable();
    });
</script>
