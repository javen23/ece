<fieldset>
    <legend class="fieldset-legend-title">UBIGEO</legend>
    <div class="col-xs-12 sinpadding margin-bottom-20">
        <label for="">SEDE OPERATIVA</label>
        <select name="local_sede" id="local_sede"  class="select2 col-xs-12 requerido sinpadding" title="Seleccione sede">
            <?php foreach($sedes as $sede): ?>
                <option
                    value="<?php echo $sede['cod_sede_operativa'] ?>"
                    <?php echo ($local['cod_sede_operativa'] == $sede['cod_sede_operativa']) ? 'selected' : '' ?>
                >
                    <?php echo $sede['sede_operativa'] ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-xs-12 sinpadding margin-bottom-20">
        <label for="">DEPARTAMENTO</label>
        <select name="local_departamento" id="local_departamento" class="select2 col-xs-12 requerido sinpadding" value=""  title="Seleccione departamento">
            <?php foreach($departamentos as $departamento): ?>
                <option
                    value="<?php echo $departamento['ccdd'] ?>"
                    <?php echo ($local['ccdd'] == $departamento['ccdd']) ? 'selected' : '' ?>
                >
                    <?php echo $departamento['nombre'] ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-xs-12 sinpadding margin-bottom-20">
        <label for="">PROVINCIA</label>
        <select name="local_provincia" id="local_provincia" class="select2 col-xs-12 requerido sinpadding" value=""  title="Seleccione provincia">
            <?php foreach($provincias as $provincia): ?>
                <option
                    value="<?php echo $provincia['ccpp'] ?>"
                    <?php echo ($local['ccpp'] == $provincia['ccpp']) ? 'selected' : '' ?>
                >
                    <?php echo $provincia['nombre'] ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-xs-12 sinpadding margin-bottom-20">
        <label for="">DISTRITO</label>
        <select name="local_distrito" id="local_distrito" class="select2 col-xs-12 requerido sinpadding" value=""  title="Seleccione distrito">
            <?php foreach($distritos as $distrito): ?>
                <option
                    value="<?php echo $distrito['ccdi'] ?>"
                    <?php echo ($local['ccdi'] == $distrito['ccdi']) ? 'selected' : '' ?>
                >
                    <?php echo $distrito['nombre'] ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
    <div id="div_sede_distrital" class="col-xs-12 sinpadding margin-bottom-20 hide">
        <label for="">SEDE DISTRITAL</label>
        <select name="local_sede_distrital" id="local_sede_distrital" class="selec2 requerido col-xs-12 sinpadding" title="Seleccione Sede distrital">
            <?php foreach($sede_distritales as $sede_dist): ?>
                <option
                    value="<?php show($sede_dist, 'cod_sede_dist') ?>"
                    <?php show( show($local, 'cod_sede_dist') == $sede_dist['ccdi']) ? 'selected' : '' ?>
                >
                    <?php show($sede_dist, 'sede_dist') ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>
</fieldset>

<?php if(false): ?>
<?php $local = isset($local) ? $local : array(); ?>
<?php $sede_provinciales = isset($sede_distritales) ? $sede_distritales : array(); ?>
<?php $sede_distritales = isset($sede_distritales) ? $sede_distritales : array(); ?>

<div class="col-md-6 col-xs-12">
    <div class="form-group">
        <label for="" class="col-sm-5 form-label"><span class="pull-right">SEDE OPERATIVA</span></label>
        <div class="col-sm-7">
            <select name="local_sede" id="local_sede"  class="select2 col-xs-12 requerido sinpadding" title="Seleccione sede">
                <option value="">-SELECCIONE-</option>
                <?php foreach($sedes as $sede): ?>
                    <option
                        value="<?php show($sede, 'cod_sede_operativa') ?>"
                        <?php show( show($local, 'cod_sede_operativa') == $sede_dist['cod_sede_operativa']) ? 'selected' : '' ?>
                    >
                        <?php show($sede, 'sede_operativa') ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="" class="col-sm-5 form-label"><span class="pull-right">SEDE PROVINCIAL</span></label>
        <div class="col-sm-7">
            <select name="local_sede_provincial" id="local_sede_provincial"  class="select2 col-xs-12 requerido sinpadding" title="Seleccione sede">
                <option value="">-SELECCIONE-</option>
                <?php foreach($sede_provinciales as $sede_prov): ?>
                    <option
                        value="<?php show($sede_prov, 'cod_sede_prov') ?>"
                        <?php show( show($local, 'cod_sede_prov') == $sede_prov['cod_sede_prov']) ? 'selected' : '' ?>
                    >
                        <?php show($sede_prov, 'sede_prov') ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div id="div_sede_distrital" class="<?php echo isset($local['cod_sede_dist']) ? '' : 'hide'; ?>">
        <div class="form-group">
            <label for="" class="col-sm-5 form-label"><span class="pull-right">SEDE DISTRITAL</span></label>
            <div class="col-sm-7">
                <select name="local_sede_distrital" id="local_sede_distrital" class="select2 requerido col-xs-12 sinpadding" title="Seleccione Sede distrital">
                    <option value="">-SELECCIONE-</option>
                    <?php foreach($sede_distritales as $sede_dist): ?>
                        <option
                            value="<?php show($sede_dist, 'cod_sede_dist') ?>"
                            <?php show( show($local, 'cod_sede_dist') == $sede_dist['cod_sede_dist']) ? 'selected' : '' ?>
                        >
                            <?php show($sede_dist, 'sede_dist') ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6 col-xs-12">
    <div class="form-group">
        <label for="" class="col-sm-5 form-label"><span class="pull-right">DEPARTAMENTO</span></label>
        <div class="col-sm-7">
            <select name="local_departamento" id="local_departamento" class="select2 col-xs-12 requerido sinpadding" title="Seleccione departamento">
                <option value="">-SELECCIONE-</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="" class="col-sm-5 form-label"><span class="pull-right">PROVINCIA</span></label>
        <div class="col-sm-7">
            <select name="local_provincia" id="local_provincia" class="select2 col-xs-12 requerido sinpadding" title="Seleccione provincia">
                <option value="">-SELECCIONE-</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="" class="col-sm-5 form-label"><span class="pull-right">DISTRITO</span></label>
        <div class="col-sm-7">
            <select name="local_distrito" id="local_distrito" class="select2 col-xs-12 requerido sinpadding" title="Seleccione distrito">
                <option value="">-SELECCIONE-</option>
            </select>
        </div>
    </div>
</div>
<?php endif; ?>
