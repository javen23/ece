<section class="content">
	<div class="row">
		<div class="col-md-3 col-sm-12">
			<a href="<?php echo base_url('locales/jurisdiccion-regional') ?>" data-toggle="tooltip" title="LOCAL DE JURISDICCIÓN REGIONAL">
				<div class="info-box">
					<span class="info-box-icon bg-maroon"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text text-uppercase">Jurisdicción Regional</span>
						<span class="info-box-number">Local de jurisdicción regional</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-3 col-sm-12">
			<a href="<?php echo base_url('locales/jurisdiccion-provincial-distrital') ?>" data-toggle="tooltip" title="LOCAL DE JURISDICCIÓN PROVINCIAL / DISTRITAL">
				<div class="info-box">
					<span class="info-box-icon bg-purple"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text text-uppercase">Jurisdicción provincial / distrital</span>
						<span class="info-box-number">Local de jurisdicción provincial / distrital</span>
					</div>
				</div>
			</a>
		</div>

		<!-- <div class="col-md-3 col-sm-12">
			<a href="<?php echo base_url('locales/preseleccion') ?>" data-toggle="tooltip" title="LOCAL DE PRESELECCIÓN">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text text-uppercase">Preselección</span>
						<span class="info-box-number">Preselección</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-3 col-sm-12">
			<a href="<?php echo base_url('locales/capacitacion') ?>" data-toggle="tooltip" title="LOCAL DE CAPACITACIÓN">
				<div class="info-box">
					<span class="info-box-icon bg-blue"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text text-uppercase">capacitación</span>
						<span class="info-box-number">capacitación</span>
					</div>
				</div>
			</a>
		</div> -->

		<div class="col-md-3 col-sm-12">
			<a href="<?php echo base_url('locales/reportes') ?>" data-toggle="tooltip" title="LOCAL DE CAPACITACIÓN">
				<div class="info-box">
					<span class="info-box-icon bg-blue"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text text-uppercase">REPORTES</span>
						<span class="info-box-number">Reportes de Consecución de Locales</span>
					</div>
				</div>
			</a>
		</div>

	</div>
</section>
