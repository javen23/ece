<div class="titulo text-center">OFICIO A ENVIAR</div>
<div class="cuerpo" style="width: 750px">
    <div class="box noBox no-box-shadow">
        <div class="box-body padding-top-20">
            <!-- <form id="form_locales" method="POST" action="#" autocomplete="off" class="text-center padding-top-20"> -->
                <div class="col-xs-12 col-sm-10">
                    <fieldset>
                        <legend class="fieldset-legend-title">OFICIO</legend>
                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">DESTINATARIOS:</label>
                            <input type="text" class="input-sm form-control text-uppercase" value="<?php echo $destinatarios ?>" readonly name="destinatarios" id="destinatarios" placeholder="Destinatarios"/>
                        </div>
                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">CON COPIA A:</label>
                            <input type="text" class="input-sm form-control text-uppercase" value="" name="otros_destinatarios" id="otros_destinatarios" placeholder="<jperez@inei.gob.pe>"/>
                        </div>
                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">CUERPO DEL OFICIO</label>
                            <textarea name="oficio_mensaje" id="oficio_mensaje" class="input-sm form-control text-uppercase" cols="30" rows="10" placeholder="Oficio"><?php echo $oficio_cuerpo; ?></textarea>
                        </div>

                    </fieldset>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="hidden_name_local" type="hidden"  value="valorHiddenLocal"/>
                    <input type="submit" class="btn btn-guardar btn-flat" value="CONFIRMAR" name="crearLocal" id="btnCreateLocal">
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                </div>
            <!-- </form> -->
        </div>
    </div>
</div>
