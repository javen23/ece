<?php $_local['cod_sede_operativa'] = isset($local['cod_sede_operativa']) ? $local['cod_sede_operativa'] : '-1'; ?>
<?php $_local['cod_sede_prov'] = isset($local['cod_sede_prov']) ? $local['cod_sede_prov'] : '-1'; ?>
<?php $_local['cod_sede_dist'] = isset($local['cod_sede_dist']) ? $local['cod_sede_dist'] : '-1'; ?>
<?php $_local['pc_capacidad'] = isset($local['pc_capacidad']) ? $local['pc_capacidad'] : '-1'; ?>
<?php $_local['internet'] = isset($local['internet']) ? $local['internet'] : '-1'; ?>
<?php $_local['electricidad'] = isset($local['electricidad']) ? $local['electricidad'] : '-1'; ?>
<?php $_local['sshh'] = isset($local['sshh']) ? $local['sshh'] : '-1'; ?>
<?php $_local['internet_tipo'] = isset($local['internet_tipo']) ? $local['internet_tipo'] : '-1'; ?>
<?php $_local['seguridad'] = isset($local['seguridad']) ? $local['seguridad'] : false; ?>
<?php $_local['turnos'] = isset($local['turnos']) ? $local['turnos'] : '-1'; ?>
<?php $local = isset($local) ? $local : array() ?>

<input type="hidden" name="local_tipo" value="OF">
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="seccion1 active"><a href="#tab_1" data-toggle="tab" title="I. Ubicación de la Sede Operativa">Sección I</a></li>
        <li class="seccion2"><a href="#tab_2" data-toggle="tab" title="II. Información del Responsable">Sección II</a></li>
        <li class="seccion3"><a href="#tab_3" data-toggle="tab" title="III. Información del Local Administrativo">Sección III</a></li>
        <!-- <li class="seccion4"><a href="#tab_4" data-toggle="tab" title="IV. Fotografías del Local Administrativo">Sección IV</a></li> -->
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1" >
            <div class="box-header page-header">
                <h3 class="box-title">I. UBICACIÓN DE LA SEDE REGIONAL</h3>
            </div>
            <div class="box-body form-horizontal">
                <div class="col-sm-6 sinpadding">
                    <div class="form-group">
                        <label for="" class="col-sm-4 form-label"><span class="pull-right">SEDE REGIONAL</span></label>
                        <div class="col-sm-5">
                            <select name="local_sede" id="local_sede"  class="select2 col-xs-12 requerido sinpadding" title="Seleccione sede">
                                <option value="">-SELECCIONE-</option>
                                <?php foreach($sedes as $sede): ?>
                                    <option
                                        value="<?php show($sede, 'cod_sede_operativa') ?>"
                                        <?php echo ($_local['cod_sede_operativa'] == $sede['cod_sede_operativa']) ? 'selected' : '' ?>
                                    >
                                        <?php show($sede, 'sede_operativa') ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-4 form-label"><span class="pull-right">SEDE PROVINCIAL</span></label>
                        <div class="col-sm-5">
                            <select name="local_sede_provincial" id="local_sede_provincial"  class="select2 col-xs-12 requerido sinpadding" title="Seleccione sede">
                                <option value="">-SELECCIONE-</option>
                                <?php foreach($sede_provinciales as $sede): ?>
                                    <option
                                        value="<?php show($sede, 'cod_sede_prov') ?>"
                                        <?php echo ($_local['cod_sede_prov'] == $sede['cod_sede_prov']) ? 'selected' : '' ?>
                                    >
                                        <?php show($sede, 'sede_prov') ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <?php // echo(isset($local['cod_sede_dist']) ? '' : 'hide') ?>
                    <div id="div_sede_distrital" class="">
                        <div class="form-group">
                            <label for="" class="col-sm-4 form-label"><span class="pull-right">SEDE DISTRITAL</span></label>
                            <div class="col-sm-5">
                                <select name="local_sede_distrital" id="local_sede_distrital" class="select2 requerido col-xs-12 sinpadding" title="Seleccione Sede distrital">
                                    <option value="">-SELECCIONE-</option>
                                    <?php foreach($sede_distritales as $sede_dist): ?>
                                        <option
                                            value="<?php show($sede_dist, 'cod_sede_dist') ?>"
                                            <?php echo ($_local['cod_sede_dist'] == $sede_dist['cod_sede_dist']) ? 'selected' : '' ?>
                                        >
                                            <?php show($sede_dist, 'sede_dist') ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="" class="col-sm-4 form-label"><span class="pull-right">DEPARTAMENTO</span></label>
                        <div class="col-sm-5">
                            <select name="local_departamento" id="local_departamento" class="select2 col-xs-12 requerido sinpadding" value="bagua"  title="Seleccione departamento">
                                <option value="" >-SELECCIONE-</option>
                                <?php foreach($departamentos as $item): ?>
                                    <option
                                        value="<?php show($item, 'ccdd') ?>"
                                        <?php echo ($local['ccdd'] == $item['ccdd']) ? 'selected' : '' ?>
                                    >
                                        <?php show($item, 'nombre') ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-4 form-label"><span class="pull-right">PROVINCIA</span></label>
                        <div class="col-sm-5">
                            <select name="local_provincia" id="local_provincia" class="select2 col-xs-12 requerido sinpadding" value="bagua"  title="Seleccione provincia">
                                <option value="" >-SELECCIONE-</option>
                                <?php foreach($provincias as $item): ?>
                                    <option
                                        value="<?php show($item, 'ccpp') ?>"
                                        <?php echo ($local['ccpp']  == $item['ccpp']) ? 'selected' : '' ?>
                                    >
                                        <?php show($item, 'nombre') ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-4 form-label"><span class="pull-right">DISTRITO</span></label>
                        <div class="col-sm-5">
                            <select name="local_distrito" id="local_distrito" class="select2 col-xs-12 requerido sinpadding" value="distrito2"  title="Seleccione distrito">
                                <option value="" >-SELECCIONE-</option>
                                <?php foreach($distritos as $item): ?>
                                    <option
                                        value="<?php show($item, 'ccdi') ?>"
                                        <?php echo ($local['ccdi']  == $item['ccdi']) ? 'selected' : '' ?>
                                    >
                                        <?php show($item, 'nombre') ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-4 form-label"><span class="pull-right">DIRECCIÓN</span></label>
                        <div class="col-sm-5">
                            <input type="text" class="input-sm form-control requerido text-uppercase" value="<?php show($local, 'direccion') ?>" name="local_direccion" id="local_direccion" placeholder="Dirección de la oficina"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-4 form-label"><span class="pull-right">REFERENCIA</span></label>
                        <div class="col-sm-5">
                            <input type="text" class="input-sm form-control requerido text-uppercase" value="<?php show($local, 'referencia') ?>" name="local_referencia" id="local_referencia" placeholder="Referencia"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-4 form-label"><span class="pull-right">OBSERVACIONES</span></label>
                        <div class="col-sm-5">
                            <textarea name="local_observacion" id="local_observacion" class="input-sm form-control text-uppercase" maxlength="700" cols="30" rows="5" placeholder="OBSERVACIONES"><?php show($local, 'observacion') ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 sinpadding">
                    <div class="form-group">
                        <label for="ubicacion_georeferencia" class="col-xs-12 form-label">UBICACIÓN: <span id="map_marker" class="animated bounce infinite"><i class="fa fa-map-marker text-red fa-2x"></i></span><small class="text-blue">ARRASTRE EL MARCADOR A LA UBICACIÓN DEL LOCAL</small></label>
                        <div class="col-xs-12" >
                            <div id="map_canvas" style="width:100%;height:400px;"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lat_long" class="col-xs-12 form-label">LATITUD Y LONGITUD DEL LOCAL</label>
                        <div class="col-xs-6">
                            <input type="text" name="latitud_georeferencia" id="latitud_georeferencia" class="form-control input-sm" value="<?php show($local, 'gps_latitud') ?>" readonly="readonly" placeholder="Latitud"/>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" name="longitud_georeferencia" id="longitud_georeferencia" class="form-control input-sm" value="<?php show($local, 'gps_longitud') ?>" readonly="readonly" placeholder="Longitud"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tab_2">
            <div class="box-header page-header">
                <h3 class="box-title">II. INFORMACIÓN DEL RESPONSABLE</h3>
            </div>
            <div class="box-body form-horizontal">
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">NOMBRES Y APELLIDOS</span></label>
                    <div class="col-sm-4">
                        <input type="text" class="input-sm form-control requerido onlyStrings text-uppercase" value="<?php show($local, 'nombres') ?>" name="local_nombre_funcionario" id="local_nombre_funcionario" placeholder="Nombre del Funcionario"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">CARGO</span></label>
                    <div class="col-sm-4">
                        <input type="text" class="input-sm form-control requerido onlyStrings text-uppercase" value="<?php show($local, 'cargo') ?>" name="local_cargo_responsable" id="local_cargo_responsable" placeholder="Cargo del Responsable"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">EMAIL</span></label>
                    <div class="col-sm-4">
                        <input type="text" class="input-sm form-control requerido email text-uppercase" value="<?php show($local, 'email') ?>" name="local_email" id="local_email" placeholder="Email"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 form-label"><span class="pull-right">TELÉFONO</span></label>
                    <div class="col-xs-12 col-sm-4">
                        <div class="row">
                            <div class="col-xs-4 ">
                                <input type="text" class="form-control text-center al-menos-uno telefono telefono_fijo input-sm text-uppercase" value="<?php show($local, 'telef_fijo') ?>" data-inputmask="'mask':'(999) 999 999[9]'" data-mask data-identity="telefono" name="local_telefono_fijo" id="local_telefono_fijo" placeholder="Fijo"/>
                            </div>
                            <div class="col-xs-4 ">
                                <input type="text" class="form-control text-center al-menos-uno telefono telefono_celular input-sm text-uppercase" value="<?php show($local, 'telef_celular') ?>" data-inputmask="'mask':'999 999 999'" data-mask data-identity="telefono" name="local_telefono_celular" id="local_telefono_celular" placeholder="Celular"/>
                            </div>
                            <div class="col-xs-4 ">
                                <input type="text" class="form-control text-center al-menos-uno telefono telefono_rpm soloPermitir input-sm text-uppercase" value="<?php show($local, 'telef_rpm') ?>" data-identity="telefono" data-permitido="*#123456789" maxlength="10" name="local_telefono_rpm" id="local_telefono_rpm" placeholder="RPM"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tab_3">
            <div class="box-header page-header">
                <h3 class="box-title">III. INFORMACIÓN DEL LOCAL</h3>
            </div>
            <div class="box-body form-horizontal">
                <div class="row">

                    <fieldset class="col-xs-12 col-md-3">
                        <legend class="text-center text-light-blue"><small>A) DATOS</small></legend>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">ÁREA TOTAL M2 DEL LOCAL</span></label>
                                <input type="text" class="input-sm form-control text-uppercase no-requerido number onlyNumbers min" value="<?php show($local, 'area') ?>" name="local_area" id="local_area" data-validate-min="120" maxlength="8" placeholder="ÁREA M2"/>
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">N° DE AMBIENTES</span></label>
                                <input type="text" class="input-sm form-control text-uppercase requerido onlyNumbers digits min" value="<?php show($local, 'nro_ambiente') ?>" name="local_ambientes" id="local_ambientes" data-validate-min="2" maxlength="5" placeholder="N° DE AMBIENTES"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label pull-left">ÁREA M2 DEL ALMACÉN</label>
                                <!-- <label class="form-label pull-right">MINIMO: <br/><span id="min_area_almacen">10</span>M2</label> -->
                                <input type="text" class="input-sm form-control text-uppercase number onlyNumbers no-requerido" value="<?php show($local, 'area_almacen') ?>" name="local_area_almacen" id="local_area_almacen" data-validate-min="10" maxlength="8" placeholder="ÁREA M2"/>
                                <p id="area_almacen_msg" class="text-danger" style="display:none;">Área mínima del almacén es: <span id="min_area_almacen">10</span>m2.</p>
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">TIPO DE CONSTRUCCIÓN</span></label>
                                <input type="text" class="input-sm form-control text-uppercase requerido" value="<?php show($local, 'tipo_construc') ?>" name="local_tipo" id="local_tipo" placeholder="TIPO DE CONSTRUCCIÓN"/>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="col-xs-12 col-md-3">
                        <legend class="text-center text-red"><small>B) MOBILIARIO</small></legend>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">N° DE ESCRITORIOS</span></label>
                                <input type="text" class="input-sm form-control text-uppercase onlyNumbers requerido digits" value="<?php show($local, 'nro_escritorio') ?>" name="local_escritorios" id="local_escritorios" maxlength="5" placeholder="N° DE ESCRITORIOS"/>
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">N° DE MESAS</span></label>
                                <input type="text" class="input-sm form-control text-uppercase onlyNumbers requerido digits" value="<?php show($local, 'nro_mesa') ?>" name="local_mesas" id="local_mesas" maxlength="5" placeholder="N° DE MESAS"/>
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">N° DE SILLAS</span></label>
                                <input type="text" class="input-sm form-control text-uppercase onlyNumbers requerido digits" value="<?php show($local, 'nro_silla') ?>" name="local_sillas" id="local_sillas" maxlength="5" placeholder="N° DE SILLAS"/>
                            </div>
                        </div>
                    </fieldset>


                    <fieldset class="col-xs-12 col-md-3">
                        <legend class="text-center text-yellow"><small>C) SERVICIOS</small></legend>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">N° DE COMPUTADORAS</span></label>
                                <div class="input-group input-group-sm">
                                    <!-- <input name="local_cant_aplicadores" type="text" class="form-control input-sm digits text-uppercase onlyNumbers" maxlength="2" id="local_cant_aplicadores" placeholder="00"/> -->
                                    <input type="text" class="input-sm form-control text-uppercase requerido onlyNumbers" value="<?php show($local, 'pc') ?>" name="local_pc" id="local_pc" maxlength="2" placeholder="N° DE COMPUTADORAS"/>
                                    <span class="input-group-btn btn-flat">
                                    <?php if (isset($local['id_oficina'])): ?>
                                        <a href="<?php echo base_url('ajax/computadora/editar'); ?>?id_oficina=<?php echo $local['id_oficina']; ?>&pcs=<?php echo $local['pc']; ?>" id="btn_reg_computadoras" rel='facebox' data-toggle='tooltip' title="Registrar computadoras" class="btn btn-flat btn-info"><i class="fa fa-laptop"></i></a>
                                    <?php else: ?>
                                        <a href="#" id="btn_reg_computadoras" rel='facebox' data-toggle='tooltip' title="Registrar computadoras" disabled class="btn form-input btn-flat btn-info"><i class="fa fa-laptop"></i></a>
                                    <?php endif; ?>
                                    </span>
                                </div>
                                <!-- <input type="text" class="input-sm form-control text-uppercase requerido onlyNumbers" value="<?php show($local, 'pc') ?>" name="local_pc" id="local_pc" maxlength="2" placeholder="N° DE COMPUTADORAS"/> -->
                                <!-- <a href="#" id="btn_reg_computadoras" rel='facebox' data-toggle='tooltip' title="Registrar computadoras" disabled class="btn btn-flat form-input btn-info margin-top-10"><i class="fa fa-laptop"></i> COMPUTADORAS</a> -->

                                <input type="hidden" name="local_pc_listado" value="" id="listado_computadoras">
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">INTERNET</span></label>
                                <!-- <input type="text" class="input-sm form-control text-uppercase requerido si-no" value="<?php show($local, 'internet') ?>" name="local_internet" id="local_internet" maxlength="1" placeholder="1/0"/> -->
                                <select name="local_internet" id="local_internet" class="select2 col-xs-12 requerido" title="Seleccione opción">
                                    <option value="" >-SELECCIONE-</option>
                                    <?php foreach($this->dataselect->si_no() as $item): ?>
                                        <option
                                            value="<?php show($item, 'id') ?>"
                                            <?php echo ($_local['internet'] == $item['id']) ? 'selected' : '' ?>
                                        >
                                            <?php show($item, 'desc') ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div id="internet_datos" class="margin-left-10 col-xs-12" style="display:none;">
                                <div class="form-group">
                                    <label for="" class="col-xs-5 form-label"><span class="pull-right">TIPO DE INTERNET</span></label>
                                    <div class="col-xs-7 sinpadding">
                                        <select name="local_internet_tipo" id="local_internet_tipo" class="select2 col-xs-12" title="Seleccione opción">
                                            <option value="" >-SELECCIONE-</option>
                                            <?php foreach($this->dataselect->tipo_internet() as $item): ?>
                                                <option
                                                    value="<?php show($item, 'id') ?>"
                                                    <?php echo ($_local['internet_tipo'] == $item['id']) ? 'selected' : '' ?>
                                                >
                                                    <?php show($item, 'desc') ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="col-xs-5 form-label"><span class="pull-right">VELOCIDAD (MB/s)</span></label>
                                    <div class="col-xs-7 sinpadding">
                                        <input type="text" class="input-sm form-control text-uppercase requerido onlyNumbers" value="<?php show($local, 'internet_velocidad') ?>" name="local_internet_velocidad" id="local_internet_velocidad" maxlength="2" placeholder="N° DE COMPUTADORAS"/>
                                    </div>
                                </div>

                            </div>


                            <div class="col-sm-12  no-padding">
                                <div class="col-sm-6">

                                    <div class="form-group col-sm-12 no-padding">
                                        <label for="" class="form-label col-sm-12 text-center"><span class="">LUZ ELECTRICA</span></label>
                                        <!-- <input type="text" class="input-sm form-control text-uppercase requerido si-no" value="<?php #show($local, 'electricidad') ?>" name="local_electricidad" id="local_electricidad" maxlength="1" placeholder="1/0"/> -->
                                        <select name="local_electricidad" id="local_electricidad" class="select2 col-xs-12 no-padding requerido" title="Seleccione opción">
                                            <option value="" >-SELECCIONE-</option>
                                            <?php foreach($this->dataselect->si_no() as $item): ?>
                                                <option
                                                    value="<?php show($item, 'id') ?>"
                                                    <?php echo ($_local['electricidad'] == $item['id']) ? 'selected' : '' ?>
                                                >
                                                    <?php show($item, 'desc') ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">

                                    <div class="form-group col-sm-12 no-padding">
                                        <label for="" class="form-label col-sm-12 text-center"><span class="">SS.HH</span></label>
                                        <select name="local_sshh" id="local_sshh" class="select2 col-xs-12 no-padding requerido" title="Seleccione opción">
                                            <option value="" >-SELECCIONE-</option>
                                            <?php foreach($this->dataselect->si_no() as $item): ?>
                                                <option
                                                    value="<?php show($item, 'id') ?>"
                                                    <?php echo ($_local['sshh'] == $item['id']) ? 'selected' : '' ?>
                                                >
                                                    <?php show($item, 'desc') ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="col-xs-12 col-md-3">
                        <legend class="text-center text-green"><small>D) SEGURIDAD</small></legend>
                        <div class="col-md-12">
                            <?php if ( in_array($_local['seguridad'], array_column($this->dataselect->tipo_seguridad(), 'id')) ): ?>
                                <?php $otra_seguridad = false;?>
                            <?php else: ?>
                                <?php   $otra_seguridad = $_local['seguridad'];
                                        $_local['seguridad'] = 'OTROS';
                                    ?>
                            <?php endif; ?>
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">TIPO DE SEGURIDAD</span></label>
                                <select name="local_seguridad" id="local_seguridad" class="select2 col-xs-12 requerido" title="Seleccione opción">
                                    <option value="" >-SELECCIONE-</option>
                                    <?php foreach($this->dataselect->tipo_seguridad() as $item): ?>
                                        <option
                                            value="<?php show($item, 'id') ?>"
                                            <?php echo ($_local['seguridad'] == $item['id']) ? 'selected' : '' ?>
                                        >
                                            <?php show($item, 'desc') ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <?php if ($otra_seguridad): ?>
                                <div id="local_seguridad_datos" class="margin-left-10 margin-top-20 col-xs-12" style="">
                                    <div class="form-group">
                                        <label for="local_seguridad" class="col-xs-5 form-label sinpadding"><span class="pull-right">SEGURIDAD</span></label>
                                        <div class="col-xs-7">
                                            <input name="local_seguridad" class="input-sm form-control text-uppercase onlyStrings requerido" value="<?php echo $otra_seguridad ?>" maxlength="30" />
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">CANTIDAD DE TURNOS</span></label>
                                <!-- <input type="text" class="input-sm form-control text-uppercase digits onlyNumbers requerido" value="<?php show($local, 'turnos') ?>" name="local_turnos" id="local_turnos" maxlength="1" placeholder="CANTIDAD DE TURNOS"/> -->
                                <div class="input-group input-group-sm">
                                    <select name="local_turnos" id="local_turnos" class="select2 col-xs-12 requerido" title="Seleccione opción">
                                        <option value="" >-SELECCIONE-</option>
                                        <?php foreach($this->dataselect->turnos() as $item): ?>
                                            <option value="<?php show($item, 'id') ?>" <?php echo ($_local['turnos'] == $item['id']) ? 'selected' : '' ?>>
                                                <?php show($item, 'desc') ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="input-group-btn btn-flat">
                                        <!-- <a href="<?php echo base_url('asistente-supervisor/ie/aplicadores') ?>?aplicadores=3" rel="facebox" class="btn btn-info btn-flat" type="button"><i class="fa fa-user"></i></a> -->
                                    <?php if (isset($local['id_oficina'])): ?>
                                        <a href="<?php echo base_url('ajax/seguridad/editar'); ?>?id_oficina=<?php echo $local['id_oficina']; ?>&seguridad=<?php echo $local['turnos']; ?>" id="btn_reg_seguridad" rel='facebox' data-toggle='tooltip' title="Registrar personal de Seguridad" class="btn btn-flat btn-info"><i class="fa fa-male"></i></a>
                                    <?php else: ?>
                                        <a href="#" id="btn_reg_seguridad" rel='facebox' data-toggle='tooltip' title="Registrar personal de Seguridad" disabled class="btn  btn-flat btn-info"><i class="fa fa-male"></i></a>
                                    <?php endif; ?>
                                    </span>
                                </div>
                                <input type="hidden" name="local_seguridad_listado" value="" id="listado_seguridad">
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">
                                    PRESUPUESTO ASIGNADO POR TURNO (S/.)
                                </span></label>
                                <input type="text" class="input-sm form-control text-uppercase number onlyNumbers requerido" value="<?php show($local, 'turnos_presupuesto') ?>" name="local_turnos_presupuesto" id="local_turnos_presupuesto" maxlength="10" placeholder="500.00"/>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="row">
                    <fieldset class="col-xs-12 col-md-3">
                        <legend class="text-center text-aqua"><small>E) PRESUPUESTO</small></legend>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="local_costo_local" class="form-label col-xs-12 col-sm-6 no-padding">COSTO TOTAL DEL LOCAL</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" class="input-sm form-control number onlyNumbers requerido" value="<?php show($local, 'costos') ?>" maxlength="10" name="local_costo_local" id="local_costo_local" placeholder="3000.00"/>
                                    <span class="input-group-addon">S./</span>
                                </div>

                                    <!-- <div class="input-group input-group-sm">
                                        <input type="text" class="input-sm form-control number onlyNumbers requerido" value="<?php show($local, 'costos') ?>" maxlength="10" name="local_costo_local" id="local_costo_local" placeholder="3000.00"/>
                                        <span class="input-group-btn btn-flat">
                                            <i class="fa fa-laptop"></i>
                                        </span>
                                    </div> -->


                            </div>
                            <div id="costo_local_container" class="margin-left-10 col-xs-12" >
                                <div class="form-group">
                                    <label for="" class="col-xs-12 sinpadding">DETALLE DEL COSTO:</label>
                                    <div class="col-xs-12 sinpadding">
                                        <label for="detalle_costo[mantenimiento]" class="col-xs-12 cursor-pointer">
                                            <input type="checkbox" class="minimal" name="local_costo_detalle_mantenimiento" value="1" <?php echo (isset($local['costos_mantenimiento']) && $local['costos_mantenimiento'] !== '') ? 'checked' : ''; ?>>MANTENIMIENTO DEL LOCAL
                                        </label>
                                        <label for="detalle_costo[internet]" class="col-xs-12 cursor-pointer">
                                            <input type="checkbox" class="minimal" name="local_costo_detalle_internet" value="1" <?php echo isset($local['costos_internet']) && $local['costos_internet'] !== '' ? 'checked' : ''; ?>>SERVICIO DE INTERNET
                                        </label>
                                        <label for="detalle_costo[mobiliario]" class="col-xs-12 cursor-pointer">
                                            <input type="checkbox" class="minimal" name="local_costo_detalle_mobiliario" value="1" <?php echo isset($local['costos_mobiliario']) && $local['costos_mobiliario'] !== '' ? 'checked' : ''; ?>>ALQUILER DE MOBILIARIO
                                        </label>
                                        <label for="detalle_costo[local]" class="col-xs-12 cursor-pointer">
                                            <input type="checkbox" class="minimal" name="local_costo_detalle_local" value="1" <?php echo isset($local['costos_local'] ) && $local['costos_local'] !== ''? 'checked' : ''; ?>>ALQUILER DEL LOCAL
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label"><span class="pull-right">OBSERVACIONES</span></label>
                                <textarea class="input-sm form-control text-uppercase requerido" name="local_observacion_presupuesto" id="local_observacion_presupuesto" maxlength="250" cols="30" rows="5" placeholder="OBSERVACION DEL PRESUPUESTO"/><?php show($local, 'observacion_local') ?></textarea>
                                <!-- <a href="<?php echo base_url('locales/administrativo/enviar_oficio'); ?>?costo=4000&detalle=Bienvenido" rel='facebox' data-toggle='tooltip' title="Ver oficio a enviar" class="btn btn-flat btn-info margin-top-10"><i class="fa fa-book"></i> VER OFICIO A ENVIAR</a> -->
                            </div>
                        </div>
                    </fieldset>

                </div>
            </div>
        </div>
    <?php if(false): ?>
        <div class="tab-pane" id="tab_4">
            <div class="box-header page-header">
                <h3 class="box-title">IV. FOTOGRAFIAS DEL LOCAL ADMINISTRATIVO</h3>
            </div>
            <div class="box-body form-horizontal">
                <label for="" class="col-sm-3 form-label"><span class="pull-right">SUBIR FOTOS</span></label>
                <input type="input" name="name" value="">
                 <!-- CONTROL PANEL -->
                <div class="form-group">
                    <!-- {{ form_row(form.hash_archivo, {'value':hash_archivo}) }} -->
                    <div class="col-xs-12 col-md-10">
                        <span class="btn btn-success btn-file btn-small fileinput-button">
                            <i class="icon-plus icon-white"></i>
                            <span>Elegir fotos</span>
                            <input id="fileupload" type="file" name="files[]" multiple>
                        </span>
                        <button id="fileupload-submit" type="submit" class="btn btn-primary btn-small">
                            <i class="icon-upload icon-white"></i>
                            <span>Subir todas las fotos</span>
                        </button>
                        <button id="fileupload-cancel" class="btn btn-warning btn-small">
                            <i class="icon-remove icon-white"></i>
                            <span>Cancelar todas las subidas</span>
                        </button>
                    </div>
                </div>
                <!-- PROGRESS BAR -->
                <div class="row">
                    <div class="col-xs-12">
                        <br>
                        <div id="progress" class="progress progress-striped">
                            <div class="bar"></div>
                        </div>
                        <div id="error-msg" class="alert alert-error" style="display:none;">
                            <button type="button" class="close">&times;</button>
                            <strong></strong>
                        </div>
                    </div>
                </div>
                <!-- FOTOS CONTAINER -->
                <div class="row">
                    <div id="files" class="files"></div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    </div>
</div>
<script>
    $(function () {

        $.validator.messages['area-min'] = 'El área mínima en m2 es {0}m2.';

        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
           checkboxClass: 'icheckbox_minimal-blue',
           radioClass: 'iradio_minimal-blue'
       });

        $('a[rel*=facebox]').facebox();
        $('#facebox').draggable({handle: 'div.titulo'});
        // $('#div_sede_distrital').addClass('hide');
        var local_seguridad = $('#local_seguridad');

        local_seguridad.on('change', function(){
            var seg = local_seguridad.val();
            var _parent = local_seguridad.parent();

            if (seg === 'OTROS') {
                var input_seguridad = '<div id="local_seguridad_datos" class="margin-left-10 margin-top-20 col-xs-12" style="">'+
                    '<div class="form-group"><label for="local_seguridad" class="col-xs-5 form-label sinpadding"><span class="pull-right">SEGURIDAD</span></label>'+
                    '<div class="col-xs-7"><input name="local_seguridad" class="input-sm form-control text-uppercase onlyStrings requerido" maxlength="30" /></div></div></div>';
                _parent.append(input_seguridad);
            } else {
                $('#local_seguridad_datos').remove();
            }
        });
    });

    $(function(){
        // VARIABLES GLOBALES
        var geocoder;
        var marker;
        var latLng;
        var map;
        var form_georeferencia  = document.getElementById('form_locales');
        var map_canvas          = document.getElementById('map_canvas');
        var peru_lat = (+form_georeferencia.latitud_georeferencia.value || -12.0393205),
            peru_long = (+form_georeferencia.longitud_georeferencia.value || -76.9921875);

        $(map_canvas).on('click.first', function(){
            $('#map_marker').removeClass('infinite');
        });

        var tipo_internet = $('#internet_datos');
        $('#local_telefono_rpm').soloPermitir();

        $('#local_internet').on('change.internet', function(){
            var internet = this.value;

            if (internet == '1') {
                tipo_internet.show();
                return;
            } else {
                tipo_internet.hide();
                return;
            }
        });

        var ajaxAlmacenAreaMinimo = function(){
            var data = {cod_sede_operativa: document.getElementById('local_sede').value, cod_sede_prov: document.getElementById('local_sede_provincial').value, cod_sede_dist: document.getElementById('local_sede_distrital').value };

            $.ajax({
                type: 'POST',
                data: data,
                url: '<?php echo base_url("ajax/area_almacen") ?>',
                success: function(data){
                    if (data.success) {
                        document.getElementById('min_area_almacen').innerHTML = data.almacen;
                        $('#local_area_almacen').data('validateMin', data.almacen);
                    }
                },
                complete: function(data){

                },
            })
        };

        $('#local_sede_provincial').on('change.forAlmacen', ajaxAlmacenAreaMinimo);
        $('#local_sede_distrital').on('change.forAlmacen', ajaxAlmacenAreaMinimo);

        var local_area_almacen = $('#local_area_almacen');
        var area_almacen_msg   = $('#area_almacen_msg');

        local_area_almacen.on('keyup.area', function(){
            var area = local_area_almacen.val();
            var minimo = local_area_almacen.data('validateMin');

            if (area < minimo) {
                area_almacen_msg.show();
            } else {
                area_almacen_msg.hide();
            }
        });

        $('#local_pc').on('keyup.pc', function(){
            var reg_computadoras = document.getElementById('btn_reg_computadoras');
            <?php if (isset($local['id_oficina'])): ?>
                var ruta = '<?php echo base_url('ajax/computadora/editar'); ?>?id_oficina=<?php echo $local['id_oficina']; ?>&pcs='+this.value;
                reg_computadoras.href = ruta;
            <?php else: ?>
                var ruta = '<?php echo base_url('ajax/computadora/crear'); ?>?pcs='+this.value;
            <?php endif; ?>

            if (this.value < 1) {
                $(reg_computadoras).attr('disabled', true);

            } else {
                reg_computadoras.href = ruta;

                $(reg_computadoras).attr('disabled', false);
            }
        });

        $('#local_turnos').on('change.turnos', function(){
            var reg_seguridad = document.getElementById('btn_reg_seguridad');
            <?php if (isset($local['id_oficina'])): ?>
                var ruta = '<?php echo base_url('ajax/seguridad/editar'); ?>?id_oficina=<?php echo $local['id_oficina']; ?>&seguridad='+this.value;
            <?php else: ?>
                var ruta = '<?php echo base_url('ajax/seguridad/crear'); ?>?seguridad='+this.value;
            <?php endif; ?>

            if (this.value === '') {
                $(reg_seguridad).attr('disabled', true);
            } else {
                reg_seguridad.href = ruta;
                $(reg_seguridad).attr('disabled', false);
            }
        });

        initialize();

        // INICiALIZACIÓN DEL MAPA
        function initialize() {
            geocoder  = new google.maps.Geocoder();
            latLng    = new google.maps.LatLng(peru_lat, peru_long);
            var opciones ={
                zoom:5,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(map_canvas, opciones);
            // CREACION DEL MARCADOR
            marker = new google.maps.Marker({
                position: latLng,
                title: 'Arrastra el marcador si quieres moverlo',
                map: map,
                draggable: true
            });
            // Escucho el CLICK sobre el mama y si se produce actualizo la posicion del marcador
            google.maps.event.addListener(map, 'click', function(event) {
                updateMarker(event.latLng);
            });

            // Inicializo los datos del marcador

            // Permito los eventos drag/drop sobre el marcador
            // google.maps.event.addListener(marker, 'dragstart', function() {
            //     updateMarkerAddress('Arrastrando...');
            // });
            google.maps.event.addListener(marker, 'drag', function() {
                // updateMarkerAddress('Arrastrando...');
                updateMarkerPosition(marker.getPosition());
            });
            // google.maps.event.addListener(marker, 'dragend', function() {
            //     updateMarkerAddress('Arrastre finalizado');
            //     geocodePosition(marker.getPosition());
            // });
        }

        function geocodePosition(pos) {
            geocoder.geocode({latLng: pos}, function(responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('No puedo encontrar esta dirección.');
                }
            });
        }

        // RECUPERO LOS DATOS LON LAT Y DIRECCION Y LOS PONGO EN EL FORMULARIO
        function updateMarkerPosition (latLng) {
            form_georeferencia.longitud_georeferencia.value    = latLng.lng();
            form_georeferencia.latitud_georeferencia.value     = latLng.lat();
        }
        function updateMarkerAddress(str) {
            form_georeferencia.local_direccion.value = str;
        }
    // ACTUALIZO LA POSICION DEL MARCADOR
        function updateMarker(location) {
            marker.setPosition(location);
            updateMarkerPosition(location);
            //geocodePosition(location);
        }

});
</script>
