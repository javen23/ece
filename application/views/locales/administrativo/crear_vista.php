<?php $local = isset($local) ? $local : array(); ?>
<section class="content">
    <div class="box noBox">
        <div class="box-header text-center">
            <h3 class="box-title"><?php echo mb_strtoupper($titulo, 'UTF-8'); ?></h3>
        </div>
        <form id="form_locales" method="POST" action="<?php echo base_url('locales/jurisdiccion-provincial-distrital/crear') ?>" role="form-horizontal" autocomplete="off" >
            <input type="hidden" name="local_tipo_oficina" value="OF">
            <div class="box-body">
                <div class="row-fluid">
                    <?php $this->load->view('locales/administrativo/partials_vista'); ?>
                </div>
			</div>
            <div class="box-footer">
                <input name="hidden_name_local" type="hidden"  value="valorHiddenLocal"/>
                <button type="submit" class="btn btn-guardar btn-flat " name="crearLocal" id="btnCreateLocal">CREAR LOCAL</button>
                <button type="reset" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
            </div>
        </form>
    </div>
    <script>
        $(function () {
            $('.select2').select2();
            $('[data-mask]').inputmask();
            $('#form_locales').validate();

            $('[data-toggle="tab"]').tooltip({
				trigger: 'hover',
				placement: 'top',
				animate: true,
				delay: 100,
				container: 'body'
			});

            llamadasAjaxUbigeo();
        });
    </script>
</section>
