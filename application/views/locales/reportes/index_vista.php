<section class="content">
	<div class="row">
        <div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('locales/reportes/directorio') ?>" data-toggle="tooltip" title="REPORTE DE DIRECTORIO DE LOCALES">
				<div class="info-box">
					<span class="info-box-icon bg-navy"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">DIRECTORIO</span>
						<span class="info-box-number">Directorio de Locales</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('locales/reportes/mapa-regional') ?>" data-toggle="tooltip" title="MAPA DE JURISDICCIÓN REGIONAL">
				<div class="info-box">
					<span class="info-box-icon bg-maroon"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">MAPA DE JURISDICCIÓN REGIONAL</span>
						<span class="info-box-number">Indicador Regional de Locales</span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('locales/reportes/mapa-provincial-distrital') ?>" data-toggle="tooltip" title="MAPA DE JURISDICCIÓN PROVINCIAL / DISTRITAL">
				<div class="info-box">
					<span class="info-box-icon bg-purple"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">MAPA DE JURISDICCIÓN PROVINCIAL / DISTRITA</span>
						<span class="info-box-number">Indicador Provincial / Distrital de Locales</span>
					</div>
				</div>
			</a>
		</div>

		<!-- <div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('locales/reportes/preseleccion') ?>" data-toggle="tooltip" title="REPORTE DE LOCAL DE PRESELECCIÓN">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">PRESELECCIÓN</span>
						<span class="info-box-number"></span>
					</div>
				</div>
			</a>
		</div>

		<div class="col-md-4 col-sm-12">
			<a href="<?php echo base_url('locales/reportes/capacitacion') ?>" data-toggle="tooltip" title="REPORTE DE LOCAL DE CAPACITACIÓN">
				<div class="info-box">
					<span class="info-box-icon bg-blue"><i class="fa fa-university"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">CAPACITACIÓN</span>
						<span class="info-box-number"></span>
					</div>
				</div>
			</a>
		</div> -->
	</div>
</section>
