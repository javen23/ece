<section class="content">
    <div class="box noBox">
        <div class="box-header">
            <h3 class="box-title">
                EXPORTAR
                <span class="inline-block">
                    <form action="<?php echo base_url('ajax-exportar-tabla') ?>" method="POST" target="_blank" id="form_exportar_tabla">
                        <span class="pull-right cursor-pointer exportar_tabla_reporte" data-toggle="tooltip" title="Descargar <?php echo $titulo ?>"><i class="fa fa-cloud-download fa-2x text-green"></i></span>
                        <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
                        <input type="hidden" name="nombre_archivo" value="<?php echo url_amigable(notildes($titulo)) ?>">
                    </form>
                </span>
            </h3>
        </div>
        <div class="box-body table-responsive">
            <table width="100%" id="tablaListado" class="display table table-bordered table-striped table-hover" >
                <thead class="headTablaListado">
                    <tr class="text-uppercase">
                        <th colspan="10">DIRECTORIO DE LOCALES</th>
                    </tr>
                    <tr class="text-uppercase th-head-inputs">
                        <th rowspan="2">N°</th>
                        <th rowspan="2">SEDE OPERATIVA</th>
                        <th rowspan="2">SEDE PROVINCIAL</th>
                        <th rowspan="2">DISTRITO</th>
                        <th rowspan="2">TIPO DE LOCAL</th>
                        <th rowspan="2">DIRECCIÓN</th>
                        <th rowspan="2">REFERENCIA</th>
                        <th rowspan="2" class="border-th-header-right">NOMBRE DEL FUNCIONARIO</th>
                        <th colspan="2" >TELEFONO</th>
                    </tr>
                    <tr class="text-uppercase th-head-inputs">
                        <th>FIJO</th>
                        <th>CELULAR</th>
                    </tr>
                </thead>
                <tfoot class="footTablaListado">
                    <tr class="text-uppercase tfoot-placeholder">
                        <th>N°</th>
                        <th>SEDE OPERATIVA</th>
                        <th>SEDE PROVINCIAL</th>
                        <th>DISTRITO</th>
                        <th>TIPO DE LOCAL</th>
                        <th>DIRECCIÓN</th>
                        <th>REFERENCIA</th>
                        <th>NOMBRE DEL FUNCIONARIO</th>
                        <th>FIJO</th>
                        <th>CELULAR</th>
                    </tr>
                </tfoot>
                <?php $tipo_datos = array(
                    'si'=>array('LZ', 'Local de Jurisdicción Regional'),
                    'no'=>array('OF', 'Local de Jurisdicción Provincial/Distrital'),
                ); ?>
                <tbody class="bodyTablaListado">
                    <?php
                     $n=1;
                     foreach ($locales as $local): ?>
                        <tr>
                           <td class="text-center"><?php echo $n++; ?></td>
                           <td><?php show($local, 'sede_operativa') ?></td>
                           <td><?php show($local, 'sede_prov') ?></td>
                           <td><?php show($local, 'distrito') ?></td>
                           <td><?php echo si_no($local['tipo'], null, $tipo_datos) ?></td>
                           <td><?php show($local, 'direccion') ?></td>
                           <td><?php show($local, 'referencia') ?></td>
                           <td><?php echo mb_strtoupper($local['nombres'], 'UTF-8') ?></td>
                           <td><?php show($local, 'electricidad') ?></td>
                           <td><?php show($local, 'sshh') ?></td>
                       </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
    $(function () {
        // $('#facebox').draggable({handle: 'div.titulo'});
        // $('a[rel*=facebox]').facebox();

        tablaListadoDataTable();
    });
</script>
