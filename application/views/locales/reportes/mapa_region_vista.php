    <?php
        $categories         = $graph['consecucion_meta']['categories'];
        $consecucion_meta   = $graph['consecucion_meta']['consecucion'];
        unset($graph['consecucion_meta']);
    ?>
<section class="content">
    <div class="box noBox">
        <div class="box-body">
            <div class="row-fluid">
                <div class="col-xs-12 margin-bottom-10 text-center hidden-lg">
                    <a href="#indicadores" class="btn btn-flat btn-info">Ver indicadores de Local Regional</a>
                </div>
                <!-- MAPAS -->
                <div class="row-fluid">
                    <div class="col-xs-12 col-lg-6 sinpadding" >
                        <div id="map-canvas" style="width:100%;height:900px;"></div>
                        <!-- FILTRO MAPAS -->
                        <div class="filtro_map col-sm-4">
                            <div id="dv_sede" class="row-fluid">
                                <label class="form-label" for="depa">SEDE REGIONAL</label>
                				<div class="controls">
                					<select id="local_sede" class="select2 form-control input-sm sinpadding" name="local_sede">
                                        <option value="0">TODOS...</option>
                                        <?php foreach ($sedes as $key => $item): ?>
                                            <option value="<?php echo $item['cod_sede_operativa'] ?>"><?php echo $item['sede_operativa'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                				</div>
                			</div>
                			<div id="dv_dep" class="row-fluid">
                                <label class="form-label" for="depa">DEPARTAMENTO</label>
                				<div class="controls">
                					<select id="local_departamento" class="select2 form-control input-sm sinpadding" name="local_departamento">
                                        <option value="0">TODOS...</option>
                                    </select>
                				</div>
                			</div>
                		</div>
                    </div>
                    <div class="col-xs-12 col-lg-6 margin-top-10" id="indicadores">
                        <?php foreach ($graph as $title => $info): ?>
                            <div class="col-md-6">
                                <div class="box box-info waves-float">
                                    <div class="chart" id="hc_<?php echo $title; ?>" style="height: 300px; position: relative;"></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- INDICADORES -->
                <div class="row-fluid" class="consecucion-meta">
                    <div class="col-xs-12 margin-top-10">
                        <div class="box box-info waves-float">
                            <div class="chart consecucion-meta" id="hc_consecucion_meta" style="min-width: 600px; height: 400px; margin: 0 auto; /*height: 300px; min-width:600px;margin: 0 auto*/"></div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </div>
    <!-- LOCALES MODAL  -->
    <div class="modal fade" id="local_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa fa-spinner fa-spin fa-3x"></i>
                        <h3 class="text-navy">Cargando por favor espere...</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <script type="text/javascript" src="//www.google.com/jsapi"></script> -->
<!-- <script type="text/javascript" src="//geoxml3.googlecode.com/svn/branches/polys/geoxml3.js"></script> -->
<script type="text/javascript">
google.load('visualization', '1', {'packages':['corechart', 'table', 'geomap']});
$(document).ready(function(){
    //google.load('visualization', '1', {'packages':['corechart', 'table', 'geomap']});
    $('.select2').select2();
    llamadasAjaxUbigeo();

    var locales = <?php echo $locales ?>;

    var kmlArray = [];
    var maploaded = false;
    var map;
    var layer;
    var capaKml;
    var table_data = '1be4h6-mmQ8GdQEwGIjn2CE5vVJnuuRZzAKgMPRfa';
    var table_dpto = '1GpIA0mBHMTame6QFenQeQCazLW4NiLciy3lfLvSZ';
    var table_prov = '1tmpbIqHGt8ymHU_L_qTEOpzcMHTOh3i_zzvWB7ZQ';
    var table_dist = '1Qvu7A-6HA7TCPVTAJ6xgld_3J7UFBr2SIlbQBz4w';

    var _lat = -9.25, _long = -74.2;
    var minZoomLevel = 6;
    var mapStyle = [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}];

    function initialize() {
        var myOptions = {
            zoom: minZoomLevel,
            draggable:false,
            center: new google.maps.LatLng(_lat, _long),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: mapStyle,
            disableDefaultUI: true,
            zoomControl: false,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            streetViewControl: true,
            streetViewControlOptions:{
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            panControl: false,
            panControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            scaleControl: false,
            scaleControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                position: google.maps.ControlPosition.RIGHT_CENTER
            }
        }

        map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);

        capaKml = new google.maps.FusionTablesLayer({
            query: {
                select: " * ",
                from: table_dpto,
                where: ""
            },
            options: {
                styleId: 2,
                templateId: 2
            }
        });
        capaKml.setMap(map);

        map.addListener('zoom_changed', function () {
            if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
        });

        return map;
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    //initialize();

    google.maps.event.addDomListener(window, 'load', function() {

        for (var eq in locales) {

            //Si el local no tiene GPS pasar al siguiente
            if ( locales[eq].gps_latitud == '' || locales[eq].gps_latitud == 0 )  {
                continue;
            }

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locales[eq].gps_latitud, locales[eq].gps_longitud),
                title: 'Arrastra el marcador si quieres moverlo',
                map: map,
                icon: CI.base_url + 'assets/img/blu-circle-lv.png',
                draggable: false
            });

            marker.localRegional = locales[eq];

            // Agregar un listener por cada marker
            google.maps.event.addListener(marker, 'click', function(_marker) {
                return function() {
                    showModalLocal(_marker.localRegional, _marker.getPosition());
                };
            }(marker));
        }
    });


    // Metodo para hacer ZOOM al lugar que se ha consultado
    function zoomTo(response) {
    	if (!response) {
    		alert('no response');
    		return;
    	}
    	if (response.isError()) {
    		alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
    		return;
    	}
    	FTresponse = response;

    	var kml =  FTresponse.getDataTable().getValue(0,1);
    	// create a geoXml3 parser for the click handlers
    	var geoXml = new geoXML3.parser({
    		map: map,
    		zoom: false
    	});

    	geoXml.parseKmlString("<Placemark>"+kml+"</Placemark>");
    	geoXml.docs[0].gpolygons[0].setMap(null);
    	map.fitBounds(geoXml.docs[0].gpolygons[0].bounds);
    }

    function cargarCapaKML( tabla, code, reload, options ) {
        var options = options || { styleId: 2, templateId: 2};

        if ( capaKml != undefined && reload ) capaKml.setMap(null);

        capaKml = new google.maps.FusionTablesLayer({
            query: {
                select: "geometry",
                from: tabla,
                where: 'Ubigeo IN ('+ code +')',
            },
            options: options
        });
        capaKml.setMap(map);

        var queryText = "SELECT Ubigeo, geometry FROM " + tabla + " Where Ubigeo IN ("+ code +")";
        var encodedQuery = encodeURIComponent(queryText);
        // console.log(queryText);

        var query = new google.visualization.Query('http://www.google.com/fusiontables/gvizdata?tq=' + queryText);

        query.send(zoomTo);
    }


    var local_sede          = $('#local_sede');
    var local_departamento  = $('#local_departamento');
    var local_provincia     = $('#local_provincia');
    var local_distrito      = $('#local_distrito');

    local_sede.on('change.sede', function(ev){
        var interval = setInterval(function(){
                var arr     = local_departamento.find('option').toArray();

                if (arr.length > 1) {
                    clearInterval(interval);

                    var ubigeos = $.map(arr, function(num){ if (num.value) return num.value }).join(',');
                    // console.log(ubigeos);
                    cargarCapaKML( table_dpto, ubigeos, true );
                }
        }, 300);
    })

    local_departamento.on('change.departamento', function(ev){
        var ccdd = this.value;
        // var interval1 = setTimeout(function(){

                cargarCapaKML( table_dpto, ccdd, true );
        // }, 300);
    })

    // depa_region();

    // $('#region').change(function(event){
    //
	// 	if ( layer != undefined ) layer.setMap(null);
	// 	$('#cat').val(-1);
	// 	depa_region();
	// 	if ( $('#region option:selected').attr('id') == 3 ) load_ubigeo('PROV');
	// });

    //load_kml_ft( table_prov, '1560101' )
    var modalLocal      = $('#local_modal');
    var modalContent    = $('div.modal-content');
    $('.modal-dialog').draggable({handle: 'div.modal-header'});

    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
        modalContent.html('<div class="text-center">'+
            '<i class="fa fa-spinner fa-spin fa-3x"></i>'+
            '<h3 class="text-navy">Cargando por favor espere...</h3>'+
        '</div>');
    });

    var showModalLocal = function(local, position){
        console.dir(local);
        //$('.modal-content').empty();
        // modalLocal.removeData('modal');
        modalLocal.modal({
            show: true,
            backdrop: 'static',
            remote: CI.base_url+'ajax/reportes/mapa_region_modal?tipo_local=LZ&titulo_local=Regional&id_oficina='+local.id_oficina,
        });
    };
});
</script>
<script type="text/javascript">

    var highchartsConfig = {
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
        },
        title: {
          text: ''
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        exporting: {
          allowHTML: true,
          enabled: true,
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style: {
                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
              }
            }
          }
        },
        credits: {
          enabled: false
        },
        series: [{
          name: "Porcentaje",
          colorByPoint: true,
          data: []
        }]
    };

$(document).ready(function () {
    <?php
    foreach($graph as $title => $info): ?>
        var local_data = JSON.parse('<?php echo $info['data'] ?>');

        highchartsConfig.title.text = '<?php echo $info['title'] ?>';
        highchartsConfig.series = [{name:"Porcentaje", data: local_data}];
        $('#hc_<?php echo $title ?>').highcharts(highchartsConfig);
    <?php
    endforeach; ?>

    var categorias = <?php echo $categories; ?>;

    var columnsHighchartsConfig = new Highcharts.Chart({
        chart: {
            renderTo: 'hc_consecucion_meta',
            type: 'column'
        },
        title: {
            text: '<strong>Consecución de Locales Regionales</strong><br><br>'
        },
        credits: {
          enabled: false
        },
        colors: ['#0000FF', '#0066FF', '#00CCFF','#ff0000', '#ff6600', '#ffcc00'],
        xAxis: {
            min: 0,
            max: 8,
            categories: categorias
        },
        scrollbar: {
            enabled: true
        },
        yAxis: {
            min: 0,
            max: 60,
            title: {
                text: 'Consecución total de Locales'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: 1,
            verticalAlign: 'top',
            y: 22,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false,
        },
        tooltip: {
            headerFormat: '<strong>{point.x}</strong><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        exporting: {
            enabled: true,
            sourceWidth: 1200,
            sourceHeight: 600,
            chartOptions: {
                xAxis: [{
                    min: 0,                           // Added for fix
                    categories: categorias,
                    max: categorias.length - 1
                }],
                scrollbar: {
                    enabled: false
                }
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
            }
        },
        series: <?php echo $consecucion_meta; ?>
    });

    // columnsHighchartsConfig.series = <?php #echo $consecucion_meta; ?>;
    // $('#hc_consecucion_meta').highcharts(columnsHighchartsConfig);
});
</script>
