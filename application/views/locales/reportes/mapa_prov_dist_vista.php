<section class="content">
    <div class="box noBox">
        <div class="box-body">
            <div class="row-fluid">
                <!-- CANVA MAPAS -->
                <div class="col-xs-12 col-md-6 sinpadding" >
                    <div id="map-canvas" style="width:100%;height:900px;"></div>
                </div>
                <!-- FILTRO MAPAS -->
                <div class="filtro_map">
                    <div id="dv_sede" class="row-fluid">
                        <label class="form-label" for="depa">SEDE REGIONAL</label>
        				<div class="controls">
        					<select id="local_sede" class="select2 form-control input-sm sinpadding" name="local_sede">
                                <option value="0">TODOS...</option>
                                <?php foreach ($sedes as $key => $item): ?>
                                    <option value="<?php echo $item['cod_sede_operativa'] ?>"><?php echo $item['sede_operativa'] ?></option>
                                <?php endforeach; ?>
                            </select>
        				</div>
        			</div>

        			<div id="dv_dep" class="row-fluid">
                        <label class="form-label" for="depa">DEPARTAMENTO</label>
        				<div class="controls">
        					<select id="local_departamento" class="select2 form-control input-sm sinpadding" name="local_departamento">
                                <option value="0">TODOS...</option>
                            </select>
        				</div>
        			</div>

        			<div id="dv_prov" class="row-fluid">
                        <label class="form-label" for="prov">PROVINCIA</label>
        				<div class="controls">
        					<select id="local_provincia" class="select2 form-control input-sm sinpadding" name="local_provincia">
                                <option value="0">TODOS...</option>
                            </select>
        				</div>
        			</div>

        			<div id="dv_dist" class="row-fluid">
                        <label class="form-label" for="dist">DISTRITO</label>
        				<div class="controls">
        					<select id="local_distrito" class="select2 form-control input-sm sinpadding" name="local_distrito">
                                <option value="0">TODOS...</option>
                            </select>
        				</div>
        			</div>
        		</div>
                <!-- INDICADORES -->
                <div class="col-xs-12 col-md-6 sinpadding" >
                    <?php foreach ($graph as $title => $info): ?>
                        <div class="col-md-6">
                            <div class="box box-info waves-float">
                                <div class="chart" id="hc_<?php echo $title; ?>" style="height: 300px; position: relative;"></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
		</div>
    </div>
    <!-- LOCALES MODAL  -->
    <div class="modal fade" id="local_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa fa-spinner fa-spin fa-3x"></i>
                        <h3 class="text-navy">Cargando por favor espere...</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <script type="text/javascript" src="//www.google.com/jsapi"></script> -->
<!-- <script type="text/javascript" src="//geoxml3.googlecode.com/svn/branches/polys/geoxml3.js"></script> -->
<script type="text/javascript">
google.load('visualization', '1', {'packages':['corechart', 'table', 'geomap']});
$(document).ready(function(){

    $('.select2').select2();
    llamadasAjaxUbigeo();

    var locales = <?php echo $locales ?>;

    var kmlArray = [];
    var maploaded = false;
    var map;
    var layer;
    var capaKml;
    var table_data = '1be4h6-mmQ8GdQEwGIjn2CE5vVJnuuRZzAKgMPRfa';
    var table_dpto = '1GpIA0mBHMTame6QFenQeQCazLW4NiLciy3lfLvSZ';
    var table_prov = '1tmpbIqHGt8ymHU_L_qTEOpzcMHTOh3i_zzvWB7ZQ';
    var table_dist = '1Qvu7A-6HA7TCPVTAJ6xgld_3J7UFBr2SIlbQBz4w';

    var _lat = -9.25, _long = -74.2;

    function initialize() {
        var myOptions = {
            zoom: 6,
            center: new google.maps.LatLng(_lat, _long),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            streetViewControl: true,
            streetViewControlOptions:{
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            panControl: false,
            panControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            scaleControl: false,
            scaleControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                position: google.maps.ControlPosition.RIGHT_CENTER
            }
        }

        map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);

        capaKml = new google.maps.FusionTablesLayer({
            query: {
                select: " geometry ",
                from: table_dpto,
                //where: " Ubigeo IN (05,09,11) "
            },
            options: {
                styleId: 2,
                templateId: 2
            }
        });
        capaKml.setMap(map);

        return map;
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    //initialize();

    google.maps.event.addDomListener(window, 'load', function() {

        for (var eq in locales) {

            //Si el local no tiene GPS pasar al siguiente
            if ( locales[eq].gps_latitud == '' || locales[eq].gps_latitud == 0 )  {

                (function(_local){
                    $.get(CI.base_url+'locales/actualizar_gps?id_local='+_local.id_oficina+'&direccion='+_local.departamento +' '+ _local.provincia +' '+ _local.distrito +' '+_local.direccion , function(status){
                        console.dir(status);
                        console.log('Direccion: '+_local.departamento +' '+ _local.provincia +' '+ _local.distrito +' '+_local.direccion);
                    })
                })(locales[eq]);

                continue;
            }

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locales[eq].gps_latitud, locales[eq].gps_longitud),
                title: 'Sede Operativa: '+locales[eq].sede_operativa,
                map: map,
                icon: CI.base_url + 'assets/img/blu-circle-lv.png',
                draggable: false
            });

            marker.localRegional = locales[eq];

            // Agregar un listener por cada marker
            google.maps.event.addListener(marker, 'click', function(_marker) {
                return function() {
                    showModalLocal(_marker.localRegional, _marker.getPosition());
                };
            }(marker));
        }
    });

    // Metodo para hacer ZOOM al lugar que se ha consultado
    function zoomTo(response) {
    	if (!response) {
    		alert('no response');
    		return;
    	}
    	if (response.isError()) {
    		alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
    		return;
    	}
    	FTresponse = response;

    	var kml =  FTresponse.getDataTable().getValue(0,1);
    	// create a geoXml3 parser for the click handlers
    	var geoXml = new geoXML3.parser({
    		map: map,
    		zoom: false
    	});

    	geoXml.parseKmlString("<Placemark>"+kml+"</Placemark>");
    	geoXml.docs[0].gpolygons[0].setMap(null);
    	map.fitBounds(geoXml.docs[0].gpolygons[0].bounds);
    }

    function cargarCapaKML( tabla, code, reload, options ) {
        var options = options || { styleId: 2, templateId: 2};

        if ( capaKml != undefined && reload ) capaKml.setMap(null);

        capaKml = new google.maps.FusionTablesLayer({
            query: {
                select: "geometry",
                from: tabla,
                where: 'Ubigeo IN ('+ code +')',
            },
            options: options
        });
        capaKml.setMap(map);

        var queryText = "SELECT Ubigeo, geometry FROM " + tabla + " Where Ubigeo IN ("+ code +")";
        var encodedQuery = encodeURIComponent(queryText);
        // console.log(queryText);

        var query = new google.visualization.Query('http://www.google.com/fusiontables/gvizdata?tq=' + queryText);

        query.send(zoomTo);
    }

    // depa_region();

    var local_sede          = $('#local_sede');
    var local_departamento  = $('#local_departamento');
    var local_provincia     = $('#local_provincia');
    var local_distrito      = $('#local_distrito');

    local_sede.on('change.sede', function(ev){
        var interval = setInterval(function(){
                var arr     = local_departamento.find('option').toArray();

                if (arr.length > 1) {
                    clearInterval(interval);

                    var ubigeos = $.map(arr, function(num){ if (num.value) return num.value }).join(',');
                    // console.log(ubigeos);
                    cargarCapaKML( table_dpto, ubigeos, true );
                }
        }, 1000);
    })

    local_departamento.on('change.departamento', function(ev){
        var ccdd = this.value;
        var interval1 = setInterval(function(){
                var arr     = local_provincia.find('option').toArray();

                if (arr.length > 1) {
                    clearInterval(interval1);

                    var ubigeos = $.map(arr, function(num){ if (num.value) return ccdd+num.value }).join(',');
                    // console.log(ubigeos);
                    cargarCapaKML( table_prov, ubigeos, true );
                }
        }, 1000);
    })

    local_provincia.on('change.provincia', function(ev){
        var ccdd = local_departamento.val();
        var ccpp = this.value;
        var interval2 = setInterval(function(){
                var arr     = local_distrito.find('option').toArray();

                if (arr.length > 1) {
                    clearInterval(interval2);

                    var ubigeos = $.map(arr, function(num){ if (num.value) return ccdd+ccpp+num.value }).join(',');
                    // console.log(ubigeos);
                    cargarCapaKML( table_dist, ubigeos, true );
                }
        }, 1000);
    })

    local_distrito.on('change.distrito', function(ev){
        var ccdd = local_departamento.val();
        var ccpp = local_provincia.val();
        var ubigeos = ccdd + ccpp + this.value;

        cargarCapaKML( table_dist, ubigeos, true);
    })

    //load_kml_ft( table_prov, '1560101' )
    var modalLocal      = $('#local_modal');
    var modalContent    = $('div.modal-content');
    $('.modal-dialog').draggable({handle: 'div.modal-header'});

    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
        modalContent.html('<div class="text-center">'+
            '<i class="fa fa-spinner fa-spin fa-3x"></i>'+
            '<h3 class="text-navy">Cargando por favor espere...</h3>'+
        '</div>');
    });

    var showModalLocal = function(local, position){
        // console.dir(local);
        //$('.modal-content').empty();
        // modalLocal.removeData('modal');
        modalLocal.modal({
            show: true,
            backdrop: 'static',
            remote: CI.base_url+'ajax/reportes/mapa_region_modal?tipo_local=OF&titulo_local=Provincial/Distrital&id_oficina='+local.id_oficina,
        });
    };
});
</script>
<script type="text/javascript">

    var highchartsConfig = {
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
        },
        title: {
          text: ''
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style: {
                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
              }
            }
          }
        },
        credits: {
          enabled: false
        },
        series: [{
          name: "Porcentaje",
          colorByPoint: true,
          data: []
        }]
    };

$(document).ready(function () {
    <?php foreach($graph as $title => $info): ?>
        var local_data = JSON.parse('<?php echo $info['data'] ?>');

        highchartsConfig.title.text = '<?php echo $info['title'] ?>';
        highchartsConfig.series = [{name:"Porcentaje", data: local_data}];
        $('#hc_<?php echo $title ?>').highcharts(highchartsConfig);
    <?php endforeach; ?>
});
</script>
