<div class="modal-header" style="cursor:move;">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-university"></i> <?php echo mb_strtoupper($titulo, 'UTF-8') ?></h4>
</div>
<div class="modal-body" id="local_modal_body">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="seccion1 active"><a href="#tab_1" data-toggle="tab" title="I. Ubicación de la Sede Operativa">Sección I</a></li>
            <li class="seccion2"><a href="#tab_2" data-toggle="tab" title="II. Información del Responsable">Sección II</a></li>
            <li class="seccion3"><a href="#tab_3" data-toggle="tab" title="III. Información del Local Administrativo">Sección III</a></li>
            <!-- <li class="seccion4"><a href="#tab_4" data-toggle="tab" title="IV. Fotografías del Local Administrativo">Sección IV</a></li> -->
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1" >
                <div class="row-fluid">
                    <fieldset>
                        <legend><h3 class="">I. Sede Operativa <?php echo $local['sede_operativa'] ?></h3></legend>
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <span class="col-xs-3 sinpadding">Departamento:</span>
                                <span class="col-xs-9"><strong class="text-navy bold"><?php echo $local['departamento']  ?></strong></span>
                            </div>
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <span class="col-xs-3 sinpadding">Provincia:</span>
                                <span class="col-xs-9"><strong class="text-navy bold"><?php echo $local['provincia']  ?></strong></span>
                            </div>
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <span class="col-xs-3 sinpadding">Distrito:</span>
                                <span class="col-xs-9"><strong class="text-navy bold"><?php echo $local['distrito']  ?></strong></span>
                            </div>
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <span class="col-xs-3 sinpadding">Dirección:</span>
                                <span class="col-xs-9">
                                    <strong class="text-navy bold"><?php echo $local['direccion']  ?></strong>
                                    <?php if ($local['referencia'] !== ''): ?>
                                        <br><em class="text-navy">(<?php echo $local['referencia']  ?>)</em>
                                    <?php endif; ?>
                                </span>
                            </div>
                            <?php if ($local['observacion'] !== ''): ?>
                                <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                    Observaciones:<br/><pre class="text-navy bold"><?php echo $local['observacion']  ?></pre>
                                </div>
                            <?php endif; ?>
                    </fieldset>
                </div>
            </div>
            <div class="tab-pane" id="tab_2" >
                <div class="row-fluid">
                    <fieldset>
                        <legend><h3 class="">II. Información del Responsable</h3></legend>
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <span class="col-xs-3 sinpadding">Responsable:</span>
                                <span class="col-xs-9"><strong class="text-navy bold"><?php echo $local['nombres']  ?></strong></span>
                            </div>
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <span class="col-xs-3 sinpadding">Cargo:</span>
                                <span class="col-xs-9"><strong class="text-navy bold"><?php echo $local['cargo']  ?></strong></span>
                            </div>
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <span class="col-xs-3 sinpadding">Email:</span>
                                <span class="col-xs-9"><strong class="text-navy bold"><?php echo $local['email']  ?></strong></span>
                            </div>
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <span class="col-xs-3 sinpadding">Teléfonos:</span>
                                <span class="col-xs-9">
                                    <?php if ($local['telef_fijo'] !== ''): ?>
                                        <br>Fijo: <strong class="text-navy bold"><?php echo $local['telef_fijo']  ?></strong>
                                    <?php elseif ($local['telef_celular'] !== ''): ?>
                                        <br>Celular: <strong class="text-navy bold"><?php echo $local['telef_celular']  ?></strong>
                                    <?php elseif ($local['telef_rpm'] !== ''): ?>
                                        <br>RPM: <strong class="text-navy bold"><?php echo $local['telef_rpm']  ?></strong>
                                    <?php endif; ?>
                                </span>
                            </div>
                    </fieldset>
                </div>
            </div>
            <div class="tab-pane" id="tab_3" >
                <fieldset>
                    <legend><h3 class="">III. Información del Local Administrativo</h3></legend>
                        <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                            <span class="col-xs-5 sinpadding">Área Total en M2:</span>
                            <span class="col-xs-7"><strong class="text-navy bold"><?php echo $local['area']  ?></strong></span>
                        </div>
                        <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                            <span class="col-xs-5 sinpadding">N° de Ambientes:</span>
                            <span class="col-xs-7"><strong class="text-navy bold"><?php echo $local['nro_ambiente']  ?></strong></span>
                        </div>
                        <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                            <span class="col-xs-5 sinpadding">Área de Almacen en M2:</span>
                            <span class="col-xs-7"><strong class="text-navy bold"><?php echo $local['area_almacen']  ?></strong></span>
                        </div>
                        <h4 class="text-navy">Mobiliario</h4>
                        <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                            <span class="col-xs-5 sinpadding">Escritorios:</span>
                            <span class="col-xs-7"><strong class="text-navy bold"><?php echo $local['nro_escritorio']  ?></strong></span>
                        </div>
                        <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                            <span class="col-xs-5 sinpadding">Mesas:</span>
                            <span class="col-xs-7"><strong class="text-navy bold"><?php echo $local['nro_mesa']  ?></strong></span>
                        </div>
                        <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                            <span class="col-xs-5 sinpadding">Sillas:</span>
                            <span class="col-xs-7"><strong class="text-navy bold"><?php echo $local['nro_silla']  ?></strong></span>
                        </div>
                </fieldset>
                <!-- <pre>
                    <?php print_r($local); ?>
                </pre> -->
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary btn-flat" data-dismiss="modal">Cerrar</button>
</div>
<script type="text/javascript">
    $('[data-toggle="tab"]').tooltip({
        trigger: 'hover',
        placement: 'top',
        animate: true,
        delay: 100,
        container: 'body'
    });
</script>
