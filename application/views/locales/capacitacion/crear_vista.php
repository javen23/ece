<div class="titulo text-center">CREAR LOCAL DE CAPACITACIÓN</div>
<div class="cuerpo" style="width: 750px">
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_locales" method="POST" action="<?php echo base_url('locales-capacitacion/crear') ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-12 col-sm-4">
                    <fieldset>
                        <legend class="fieldset-legend-title">UBIGEO</legend>
                        <div class="col-xs-12 sinpadding margin-bottom-20">
                            <label for="">SEDE OPERATIVA</label>
                            <select name="local_sede" id="local_sede"  class="selec2 requerido col-xs-12 sinpadding" title="Seleccione sede">
                                <option value="">-SELECCIONE-</option>
                                <?php foreach($sedes as $sede): ?>
                                    <option value="<?php echo $sede['cod_sede_operativa'] ?>"><?php echo $sede['sede_operativa'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-xs-12 sinpadding margin-bottom-20">
                            <label for="">DEPARTAMENTO</label>
                            <select name="local_departamento" id="local_departamento" class="selec2 requerido col-xs-12 sinpadding" title="Seleccione departamento">
                                <option value="" >
                                    -SELECCIONE-
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-12 sinpadding margin-bottom-20">
                            <label for="">PROVINCIA</label>
                            <select name="local_provincia" id="local_provincia" class="selec2 requerido col-xs-12 sinpadding" title="Seleccione provincia">
                                <option value="" >
                                    -SELECCIONE-
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-12 sinpadding margin-bottom-20">
                            <label for="">DISTRITO</label>
                            <select name="local_distrito" id="local_distrito" class="selec2 requerido col-xs-12 sinpadding" title="Seleccione distrito">
                                <option value="" >
                                    -SELECCIONE-
                                </option>
                            </select>
                        </div>
                        <div id="div_sede_distrital" class="col-xs-12 sinpadding margin-bottom-20 hide">
                            <label for="" class="col-xs-12">SEDE DISTRITAL</label>
                            <select name="local_sede_distrital" id="local_sede_distrital" class="selec2 requerido col-xs-12 sinpadding" title="Seleccione Sede distrital">
                                <option value="" >
                                    -SELECCIONE-
                                </option>
                            </select>
                        </div>
                    </fieldset>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <fieldset>
                        <legend class="fieldset-legend-title">DATOS DEL LOCAL DE CAPACITACIÓN</legend>
                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">NOMBRE DEL LOCAL</label>
                            <input type="text" class="input-sm form-control text-uppercase requerido" value="" name="local_nombre" id="local_nombre" placeholder="Nombre"/>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">DIRECCIÓN</label>
                            <input type="text" class="input-sm form-control text-uppercase requerido" value="" name="local_direccion" id="local_direccion" placeholder="Dirección"/>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">REFERENCIA</label>
                            <input type="text" class="input-sm form-control text-uppercase requerido" value="" name="local_referencia" id="local_referencia" placeholder="Referencia"/>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <div class="row-fluid">
                                <div class="col-xs-3 sinpadding">
                                    <label for="">LUZ <span class="hidden-xs">ELÉCTRICA</span><br/>(SI = 1/NO = 0)</label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control text-uppercase requerido si-no" value="" name="local_luz" id="local_luz" data-inputmask="'mask':'9', 'placeholder':''" data-mask maxlength="1" placeholder="1/0"/>
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <label for="">SS.HH<br/> (SI = 1/NO = 0)</label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control text-uppercase requerido si-no" value="" name="local_sshh" id="local_sshh" data-inputmask="'mask':'9', 'placeholder':''" data-mask maxlength="1" placeholder="1/0"/>
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <label for="">
                                        <span class="visible-xs"><br>PRTES.</span>
                                        <span class="hidden-xs">N° DE<br> PARTICIPANTES</span>
                                    </label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control text-uppercase requerido digits min" value="" data-min="1" data-inputmask="'mask':'999999', 'placeholder':''" data-mask name="local_pea" id="local_pea" placeholder="PEA"/>
                                </div>
                                <div class="col-xs-3 no-padding-right">
                                    <label for="">
                                        <span class="visible-xs"><br>AULAS</span>
                                        <span class="hidden-xs">AULAS<br/>PROGRAMADAS</span>
                                    </label>
                                    <input type="text" class="col-xs-12 text-center input-sm form-control text-uppercase" value="" readonly="readonly" name="local_aulas" id="local_aulas" placeholder="AULAS"/>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div id="container_resto_pea" class="col-xs-12 sinpadding margin-top-10" style="display:none;">
                                    <div class="alert-message">
                                        <div class="alert alert-danger alert-dismissable">
                                            <strong>Tiene <span id="resto_pea">0</span> PEA sobrantes</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 margin-bottom-10">
                            <label for="">OBSERVACIONES</label>
                            <textarea name="local_observacion" id="local_observacion" class="input-sm no-requerido form-control text-uppercase" cols="30" rows="5" placeholder="OBSERVACIONES"></textarea>
                        </div>

                    </fieldset>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="hidden_name_local" type="hidden"  value="valorHiddenLocal"/>
                    <input type="submit" class="btn btn-guardar btn-flat" value="CREAR LOCAL" name="crearLocal" id="btnCreateLocal">
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                </div>

            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        //$('.maskDecimal').autoNumeric('init');
        $('.selec2').select2();
        $('[data-mask]').inputmask();
        $('#form_locales').validate();

        limitePeaLocales();
        llamadasAjaxUbigeo();
    });
</script>
