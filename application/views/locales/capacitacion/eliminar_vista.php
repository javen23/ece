<div class="titulo text-center">ELIMINAR LOCAL DE CAPACITACIÓN</div>
<div class="cuerpo" style="width: 450px">
    <div class="box noBox no-box-shadow">
        <div class="box-body">
            <form id="form_crear_" method="POST" action="<?php echo base_url('locales-capacitacion/eliminar') ?>" autocomplete="off" class="text-center padding-top-20">
                <div class="col-xs-12">
                    <fieldset>
                        <legend>¿ESTA SEGURO DE ELIMINAR ESTE LOCAL DE APLICACIÓN?</legend>
                        <p class="text-red">
                            Una vez eliminado este local no se podrá revertir los cambios, tenga en cuenta este mensaje.
                        </p>
                    </fieldset>
                </div>
                <div class="col-xs-12 margin-top-20">
                    <input name="local_id_local" type="hidden" value="<?php echo $local['id_local']; ?>"/>
                    <button type="button" class="btn bg-red btn-flat cerrar" id="btnCreateCancelar">CANCELAR</button>
                    <button type="submit" class="btn btn-guardar btn-flat " name="eliminarLocal" id="btnCreateLocal">ELIMINAR LOCAL</button>
                </div>

            </form>
        </div>
    </div>
</div>
