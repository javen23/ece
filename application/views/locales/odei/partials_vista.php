<script type="text/javascript">
    $(document).ready(function(){
        var geocoder;
        var marker;
        var latLng;
        var map;
        var form_georeferencia  = document.getElementById('form_locales');
        var map_canvas          = document.getElementById('map_canvas');
        var peru_lat = (+form_georeferencia.latitud_georeferencia.value || -12.0393205),
            peru_long = (+form_georeferencia.longitud_georeferencia.value || -76.9921875);

        $(map_canvas).on('click.first', function(){
            $('#map_marker').removeClass('infinite');
        });

        initialize();

        // INICiALIZACIÓN DEL MAPA
        function initialize() {
            geocoder  = new google.maps.Geocoder();
            latLng    = new google.maps.LatLng(peru_lat, peru_long);
            var opciones ={
                zoom:5,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(map_canvas, opciones);
            // CREACION DEL MARCADOR
            marker = new google.maps.Marker({
                position: latLng,
                title: 'Arrastra el marcador si quieres moverlo',
                map: map,
                draggable: true
            });
            // Escucho el CLICK sobre el mama y si se produce actualizo la posicion del marcador
            google.maps.event.addListener(map, 'click', function(event) {
                updateMarker(event.latLng);
            });

            // Inicializo los datos del marcador

            // Permito los eventos drag/drop sobre el marcador
            // google.maps.event.addListener(marker, 'dragstart', function() {
            //     updateMarkerAddress('Arrastrando...');
            // });
            google.maps.event.addListener(marker, 'drag', function() {
                // updateMarkerAddress('Arrastrando...');
                updateMarkerPosition(marker.getPosition());
            });
            // google.maps.event.addListener(marker, 'dragend', function() {
            //     updateMarkerAddress('Arrastre finalizado');
            //     geocodePosition(marker.getPosition());
            // });
        }

        function geocodePosition(pos) {
            geocoder.geocode({latLng: pos}, function(responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('No puedo encontrar esta dirección.');
                }
            });
        }

        // RECUPERO LOS DATOS LON LAT Y DIRECCION Y LOS PONGO EN EL FORMULARIO
        function updateMarkerPosition (latLng) {
            form_georeferencia.longitud_georeferencia.value    = latLng.lng();
            form_georeferencia.latitud_georeferencia.value     = latLng.lat();
        }
        function updateMarkerAddress(str) {
            form_georeferencia.local_direccion.value = str;
        }
    // ACTUALIZO LA POSICION DEL MARCADOR
        function updateMarker(location) {
            marker.setPosition(location);
            updateMarkerPosition(location);
            //geocodePosition(location);
        }
    });
</script>
