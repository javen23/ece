<section class="content">
    <div class="box noBox">
        <div class="box-header text-center">
            <h3 class="box-title"><?php echo mb_strtoupper($titulo, 'UTF-8'); ?></h3>
        </div>
        <form id="form_locales" method="POST" action="<?php echo base_url('locales/jurisdiccion-regional/editar') ?>?idLocal=<?php echo $local['id_oficina'] ?>" role="form-horizontal" autocomplete="off" >
            <input type="hidden" name="local_tipo_oficina" value="LZ">
            <div class="box-body">
                <div class="row-fluid">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="seccion1 active"><a href="#tab_1" data-toggle="tab" title="I. Ubicación de la Sede Operativa">Sección I</a></li>
                            <li class="seccion2"><a href="#tab_2" data-toggle="tab" title="II. Información del Responsable">Sección II</a></li>
                            <li class="seccion3"><a href="#tab_3" data-toggle="tab" title="III. Información del Local Administrativo">Sección III</a></li>
                            <!-- <li class="seccion4"><a href="#tab_4" data-toggle="tab" title="IV. Fotografías del Local Administrativo">Sección IV</a></li> -->
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1" >
                                <div class="box-header page-header">
                                    <h3 class="box-title">I. UBICACIÓN DE LA SEDE REGIONAL</h3>
                                </div>
                                <div class="box-body form-horizontal">
                                    <div class="col-sm-6 sinpadding">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 form-label"><span class="pull-right">SEDE REGIONAL</span></label>
                                            <div class="col-sm-5">
                                                <select name="local_sede" id="local_sede"  class="select2 col-xs-12 requerido sinpadding" title="Seleccione sede">
                                                    <option value="">-SELECCIONE-</option>
                                                    <?php foreach($sedes as $sede): ?>
                                                        <option
                                                            value="<?php show($sede, 'cod_sede_operativa') ?>"
                                                            <?php echo ($local['cod_sede_operativa'] == $sede['cod_sede_operativa']) ? 'selected' : '' ?>
                                                        >
                                                            <?php show($sede, 'sede_operativa') ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 form-label"><span class="pull-right">DEPARTAMENTO</span></label>
                                            <div class="col-sm-5">
                                                <select name="local_departamento" id="local_departamento" class="select2 col-xs-12 requerido sinpadding" value="bagua"  title="Seleccione departamento">
                                                    <option value="" >-SELECCIONE-</option>
                                                    <?php foreach($departamentos as $item): ?>
                                                        <option
                                                            value="<?php show($item, 'ccdd') ?>"
                                                            <?php echo ($local['ccdd'] == $item['ccdd']) ? 'selected' : '' ?>
                                                        >
                                                            <?php show($item, 'nombre') ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 form-label"><span class="pull-right">PROVINCIA</span></label>
                                            <div class="col-sm-5">
                                                <select name="local_provincia" id="local_provincia" class="select2 col-xs-12 requerido sinpadding" value="bagua"  title="Seleccione provincia">
                                                    <option value="" >-SELECCIONE-</option>
                                                    <?php foreach($provincias as $item): ?>
                                                        <option
                                                            value="<?php show($item, 'ccpp') ?>"
                                                            <?php echo ($local['ccpp']  == $item['ccpp']) ? 'selected' : '' ?>
                                                        >
                                                            <?php show($item, 'nombre') ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 form-label"><span class="pull-right">DISTRITO</span></label>
                                            <div class="col-sm-5">
                                                <select name="local_distrito" id="local_distrito" class="select2 col-xs-12 requerido sinpadding" value="distrito2"  title="Seleccione distrito">
                                                    <option value="" >-SELECCIONE-</option>
                                                    <?php foreach($distritos as $item): ?>
                                                        <option
                                                            value="<?php show($item, 'ccdi') ?>"
                                                            <?php echo ($local['ccdi']  == $item['ccdi']) ? 'selected' : '' ?>
                                                        >
                                                            <?php show($item, 'nombre') ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 form-label"><span class="pull-right">DIRECCIÓN</span></label>
                                            <div class="col-sm-5">
                                                <input type="text" class="input-sm form-control requerido text-uppercase" value="<?php show($local, 'direccion') ?>" name="local_direccion" id="local_direccion" placeholder="Dirección de la oficina"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 form-label"><span class="pull-right">REFERENCIA</span></label>
                                            <div class="col-sm-5">
                                                <input type="text" class="input-sm form-control requerido text-uppercase" value="<?php show($local, 'referencia') ?>" name="local_referencia" id="local_referencia" placeholder="Referencia"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 form-label"><span class="pull-right">OBSERVACIONES</span></label>
                                            <div class="col-sm-5">
                                                <textarea name="local_observacion" id="local_observacion" class="input-sm form-control text-uppercase" maxlength="700" cols="30" rows="5" placeholder="OBSERVACIONES"><?php show($local, 'observacion') ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 sinpadding">
                                        <div class="form-group">
                                            <label for="ubicacion_georeferencia" class="col-xs-12 form-label">UBICACIÓN: <span id="map_marker" class="animated bounce infinite"><i class="fa fa-map-marker text-red fa-2x"></i></span><small class="text-blue">ARRASTRE EL MARCADOR A LA UBICACIÓN DEL LOCAL</small></label>
                                            <div class="col-xs-12" >
                                                <div id="map_canvas" style="width:100%;height:400px;"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lat_long" class="col-xs-12 form-label">LATITUD Y LONGITUD DEL LOCAL</label>
                                            <div class="col-xs-6">
                                                <input type="text" name="latitud_georeferencia" id="latitud_georeferencia" class="form-control input-sm" value="<?php show($local, 'gps_latitud') ?>" readonly="readonly" placeholder="Latitud"/>
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="text" name="longitud_georeferencia" id="longitud_georeferencia" class="form-control input-sm" value="<?php show($local, 'gps_longitud') ?>" readonly="readonly" placeholder="Longitud"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_2">
                                <div class="box-header page-header">
                                    <h3 class="box-title">II. INFORMACIÓN DEL RESPONSABLE</h3>
                                </div>
                                <div class="box-body form-horizontal">
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 form-label"><span class="pull-right">NOMBRES Y APELLIDOS</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" class="input-sm form-control requerido onlyStrings text-uppercase" value="<?php show($local, 'nombres') ?>" name="local_nombre_funcionario" id="local_nombre_funcionario" placeholder="Nombre del Funcionario"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 form-label"><span class="pull-right">CARGO</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" class="input-sm form-control requerido onlyStrings text-uppercase" value="<?php show($local, 'cargo') ?>" name="local_cargo_responsable" id="local_cargo_responsable" placeholder="Cargo del Responsable"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 form-label"><span class="pull-right">EMAIL</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" class="input-sm form-control requerido email text-uppercase" value="<?php show($local, 'email') ?>" name="local_email" id="local_email" placeholder="Email"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 form-label"><span class="pull-right">TELÉFONO</span></label>
                                        <div class="col-xs-12 col-sm-4">
                                            <div class="row">
                                                <div class="col-xs-4 ">
                                                    <input type="text" class="form-control text-center al-menos-uno telefono telefono_fijo input-sm text-uppercase" value="<?php show($local, 'telef_fijo') ?>" data-inputmask="'mask':'(999) 999 999[9]'" data-mask data-identity="telefono" name="local_telefono_fijo" id="local_telefono_fijo" placeholder="Fijo"/>
                                                </div>
                                                <div class="col-xs-4 ">
                                                    <input type="text" class="form-control text-center al-menos-uno telefono telefono_celular input-sm text-uppercase" value="<?php show($local, 'telef_celular') ?>" data-inputmask="'mask':'999 999 999'" data-mask data-identity="telefono" name="local_telefono_celular" id="local_telefono_celular" placeholder="Celular"/>
                                                </div>
                                                <div class="col-xs-4 ">
                                                    <input type="text" class="form-control text-center al-menos-uno telefono telefono_rpm soloPermitir input-sm text-uppercase" value="<?php show($local, 'telef_rpm') ?>" data-identity="telefono" data-permitido="*#123456789" maxlength="10" name="local_telefono_rpm" id="local_telefono_rpm" placeholder="RPM"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_3">
                                <div class="box-header page-header">
                                    <h3 class="box-title">III. INFORMACIÓN DEL LOCAL</h3>
                                </div>
                                <div class="box-body form-horizontal">
                                    <div class="row">
                                        <fieldset class="col-xs-12 col-md-6">
                                            <legend class="text-center text-light-blue"><small>A) DATOS</small></legend>
                                            <div class="col-md-offset-2 col-md-8">
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">ÁREA TOTAL M2<br/> DEL LOCAL</span></label>
                                                    <input type="text" class="input-sm form-control text-uppercase no-requerido number onlyNumbers min" value="<?php show($local, 'area') ?>" name="local_area" id="local_area" data-validate-min="120" maxlength="8" placeholder="ÁREA M2"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">N° DE AMBIENTES</span></label>
                                                    <input type="text" class="input-sm form-control text-uppercase requerido onlyNumbers digits min" value="<?php show($local, 'nro_ambiente') ?>" name="local_ambientes" id="local_ambientes" data-validate-min="2" maxlength="5" placeholder="N° DE AMBIENTES"/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label pull-left">ÁREA M2<br/> DEL ALMACÉN</label>
                                                    <!-- <label class="form-label pull-right">MINIMO: <br/><span id="min_area_almacen">10</span>M2</label> -->
                                                    <input type="text" class="input-sm form-control text-uppercase number onlyNumbers no-requerido" value="<?php show($local, 'area_almacen') ?>" name="local_area_almacen" id="local_area_almacen" data-validate-min="10" maxlength="8" placeholder="ÁREA M2"/>
                                                    <!-- <p id="area_almacen_msg" class="text-danger" style="display:none;">Área mínima del almacén es: <span id="min_area_almacen">10</span>m2.</p> -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">TIPO DE CONSTRUCCIÓN</span></label>
                                                    <input type="text" class="input-sm form-control text-uppercase requerido" value="<?php show($local, 'tipo_construc') ?>" name="local_tipo" id="local_tipo" placeholder="TIPO DE CONSTRUCCIÓN"/>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-xs-12 col-md-6">
                                            <legend class="text-center text-red"><small>B) MOBILIARIO</small></legend>
                                            <div class="col-md-offset-2 col-md-8">
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">N° DE ESCRITORIOS</span></label>
                                                    <input type="text" class="input-sm form-control text-uppercase onlyNumbers requerido digits" value="<?php show($local, 'nro_escritorio') ?>" name="local_escritorios" id="local_escritorios" maxlength="5" placeholder="N° DE ESCRITORIOS"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">N° DE MESAS</span></label>
                                                    <input type="text" class="input-sm form-control text-uppercase onlyNumbers requerido digits" value="<?php show($local, 'nro_mesa') ?>" name="local_mesas" id="local_mesas" maxlength="5" placeholder="N° DE MESAS"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">N° DE SILLAS</span></label>
                                                    <input type="text" class="input-sm form-control text-uppercase onlyNumbers requerido digits" value="<?php show($local, 'nro_silla') ?>" name="local_sillas" id="local_sillas" maxlength="5" placeholder="N° DE SILLAS"/>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="row">
                                        <fieldset class="col-xs-12 col-md-6">
                                            <legend class="text-center text-yellow"><small>C) SERVICIOS</small></legend>
                                            <div class="col-md-offset-2 col-md-8">
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">N° DE COMPUTADORAS</span></label>
                                                    <input type="text" class="input-sm form-control text-uppercase requerido onlyNumbers" value="<?php show($local, 'pc') ?>" name="local_pc" id="local_pc" maxlength="2" placeholder="N° DE COMPUTADORAS"/>
                                                    <!-- <a href="#" id="btn_reg_computadoras" rel='facebox' data-toggle='tooltip' title="Registrar computadoras" disabled class="btn btn-flat form-input btn-info margin-top-10"><i class="fa fa-laptop"></i> COMPUTADORAS</a> -->
                                                    <a href="<?php echo base_url('ajax/computadora/editar'); ?>?id_oficina=<?php echo $local['id_oficina']; ?>&pcs=<?php echo $local['pc']; ?>" id="btn_reg_computadoras" rel='facebox' data-toggle='tooltip' title="Registrar computadoras" class="btn form-input btn-flat btn-info margin-top-10"><i class="fa fa-laptop"></i> REGISTRAR COMPUTADORAS</a>
                                                    <input type="hidden" name="local_pc_listado" value="" id="listado_computadoras">
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">INTERNET</span></label>
                                                    <select name="local_internet" id="local_internet" class="select2 col-xs-12 requerido" title="Seleccione opción">
                                                        <option value="" >-SELECCIONE-</option>
                                                        <?php foreach($this->dataselect->si_no() as $item): ?>
                                                            <option
                                                                value="<?php show($item, 'id') ?>"
                                                                <?php echo ($local['internet'] == $item['id']) ? 'selected' : '' ?>
                                                            >
                                                                <?php show($item, 'desc') ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div id="internet_datos" class="margin-left-10 col-xs-12" style="<?php echo ($local['internet'] == '1') ? '' : 'display:none;'; ?>">
                                                    <div class="form-group">
                                                        <label for="" class="col-xs-5 form-label"><span class="pull-right">TIPO DE INTERNET</span></label>
                                                        <div class="col-xs-7 sinpadding">
                                                            <select name="local_internet_tipo" id="local_internet_tipo" class="select2 col-xs-12" title="Seleccione opción">
                                                                <option value="" >-SELECCIONE-</option>
                                                                <?php foreach($this->dataselect->tipo_internet() as $item): ?>
                                                                    <option
                                                                        value="<?php show($item, 'id') ?>"
                                                                        <?php echo ($local['internet_tipo'] == $item['id']) ? 'selected' : '' ?>
                                                                    >
                                                                        <?php show($item, 'desc') ?>
                                                                    </option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-xs-5 form-label"><span class="pull-right">VELOCIDAD (MB/s)</span></label>
                                                        <div class="col-xs-7 sinpadding">
                                                            <input type="text" class="input-sm form-control text-uppercase requerido onlyNumbers" value="<?php show($local, 'internet_velocidad') ?>" name="local_internet_velocidad" id="local_internet_velocidad" maxlength="2" placeholder="N° DE COMPUTADORAS"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">LUZ ELECTRICA</span></label>
                                                    <select name="local_electricidad" id="local_electricidad" class="select2 col-xs-12 requerido" title="Seleccione opción">
                                                        <option value="" >-SELECCIONE-</option>
                                                        <?php foreach($this->dataselect->si_no() as $item): ?>
                                                            <option
                                                                value="<?php show($item, 'id') ?>"
                                                                <?php echo ($local['electricidad'] == $item['id']) ? 'selected' : '' ?>
                                                            >
                                                                <?php show($item, 'desc') ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">SS.HH</span></label>
                                                    <select name="local_sshh" id="local_sshh" class="select2 col-xs-12 requerido" title="Seleccione opción">
                                                        <option value="" >-SELECCIONE-</option>
                                                        <?php foreach($this->dataselect->si_no() as $item): ?>
                                                            <option
                                                                value="<?php show($item, 'id') ?>"
                                                                <?php echo ($local['sshh'] == $item['id']) ? 'selected' : '' ?>
                                                            >
                                                                <?php show($item, 'desc') ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-xs-12 col-md-6">
                                            <legend class="text-center text-green"><small>D) SEGURIDAD</small></legend>
                                            <div class="col-md-offset-2 col-md-8">
                                                <div class="form-group">
                                                    <?php if ( in_array($local['seguridad'], array_column($this->dataselect->tipo_seguridad(), 'id')) ): ?>
                                                        <?php $otra_seguridad = false;?>
                                                    <?php else: ?>
                                                        <?php   $otra_seguridad = $local['seguridad'];
                                                                $local['seguridad'] = 'OTROS';
                                                            ?>
                                                    <?php endif; ?>
                                                    <label for="" class="form-label"><span class="pull-right">TIPO DE SEGURIDAD</span></label>
                                                    <select name="local_seguridad" id="local_seguridad" class="select2 col-xs-12 requerido" title="Seleccione opción">
                                                        <option value="" >-SELECCIONE-</option>
                                                        <?php foreach($this->dataselect->tipo_seguridad() as $item): ?>
                                                            <option
                                                                value="<?php show($item, 'id') ?>"
                                                                <?php echo ($local['seguridad'] == $item['id']) ? 'selected' : '' ?>
                                                            >
                                                                <?php show($item, 'desc') ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <?php if ($otra_seguridad): ?>
                                                    <div id="local_seguridad_datos" class="margin-left-10 margin-top-20 col-xs-12" style="">
                                                        <div class="form-group">
                                                            <label for="local_seguridad" class="col-xs-5 form-label sinpadding"><span class="pull-right">SEGURIDAD</span></label>
                                                            <div class="col-xs-7">
                                                                <input name="local_seguridad" class="input-sm form-control text-uppercase onlyStrings requerido" value="<?php echo $otra_seguridad ?>" maxlength="30" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">CANTIDAD DE TURNOS</span></label>
                                                    <select name="local_turnos" id="local_turnos" class="select2 col-xs-12 requerido" title="Seleccione opción">
                                                        <option value="" >-SELECCIONE-</option>
                                                        <?php foreach($this->dataselect->turnos() as $item): ?>
                                                            <option
                                                                value="<?php show($item, 'id') ?>"
                                                                <?php echo ($local['turnos'] == $item['id']) ? 'selected' : '' ?>
                                                            >
                                                                <?php show($item, 'desc') ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <a href="<?php echo base_url('ajax/seguridad/editar'); ?>?id_oficina=<?php echo $local['id_oficina']; ?>&seguridad=<?php echo $local['turnos']; ?>" id="btn_reg_seguridad" rel='facebox' data-toggle='tooltip' title="Registrar personal de Seguridad" class="btn form-input btn-flat btn-info margin-top-10"><i class="fa fa-male"></i> REGISTRAR PERSONAL DE SEGURIDAD</a>
                                                    <input type="hidden" name="local_seguridad_listado" value="" id="listado_seguridad">
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">
                                                        PRESUPUESTO ASIGNADO POR TURNO (S/.)
                                                    </span></label>
                                                    <input type="text" class="input-sm form-control text-uppercase number onlyNumbers requerido" value="<?php show($local, 'turnos_presupuesto') ?>" name="local_turnos_presupuesto" id="local_turnos_presupuesto" maxlength="10" placeholder="500.00"/>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="row">
                                        <fieldset class="col-xs-12 col-md-6">
                                            <legend class="text-center text-aqua"><small>E) PRESUPUESTO</small></legend>
                                            <div class="col-md-offset-2 col-md-8">
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">COSTO TOTAL DEL LOCAL (S/.)</span></label>
                                                    <input type="text" class="input-sm form-control text-uppercase number onlyNumbers requerido" value="<?php show($local, 'costos') ?>" maxlength="10" name="local_costo_local" id="local_costo_local" placeholder="3000.00"/>
                                                </div>
                                                <div id="costo_local_container" class="margin-left-10 col-xs-12" >
                                                    <div class="form-group">
                                                        <label for="" class="col-xs-12 sinpadding">DETALLE DEL COSTO:</label>
                                                        <div class="col-xs-12 sinpadding">
                                                            <label for="detalle_costo[mantenimiento]" class="col-xs-12 cursor-pointer">
                                                                <input type="checkbox" class="minimal" name="local_costo_detalle_mantenimiento" value="1" <?php echo (isset($local['costos_mantenimiento']) && $local['costos_mantenimiento'] !== '') ? 'checked' : ''; ?>>MANTENIMIENTO DEL LOCAL
                                                            </label>
                                                            <label for="detalle_costo[internet]" class="col-xs-12 cursor-pointer">
                                                                <input type="checkbox" class="minimal" name="local_costo_detalle_internet" value="1" <?php echo isset($local['costos_internet']) && $local['costos_internet'] !== '' ? 'checked' : ''; ?>>SERVICIO DE INTERNET
                                                            </label>
                                                            <label for="detalle_costo[mobiliario]" class="col-xs-12 cursor-pointer">
                                                                <input type="checkbox" class="minimal" name="local_costo_detalle_mobiliario" value="1" <?php echo isset($local['costos_mobiliario']) && $local['costos_mobiliario'] !== '' ? 'checked' : ''; ?>>ALQUILER DE MOBILIARIO
                                                            </label>
                                                            <label for="detalle_costo[local]" class="col-xs-12 cursor-pointer">
                                                                <input type="checkbox" class="minimal" name="local_costo_detalle_local" value="1" <?php echo isset($local['costos_local'] ) && $local['costos_local'] !== ''? 'checked' : ''; ?>>ALQUILER DEL LOCAL
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="form-label"><span class="pull-right">OBSERVACIONES</span></label>
                                                    <textarea class="input-sm form-control text-uppercase requerido" name="local_observacion_presupuesto" id="local_observacion_presupuesto" maxlength="250" cols="30" rows="5" placeholder="OBSERVACION DEL PRESUPUESTO"/><?php show($local, 'observacion_local') ?></textarea>
                                                    <!-- <a href="<?php echo base_url('locales/administrativo/enviar_oficio'); ?>?costo=4000&detalle=Bienvenido" rel='facebox' data-toggle='tooltip' title="Ver oficio a enviar" class="btn btn-flat btn-info margin-top-10"><i class="fa fa-book"></i> VER OFICIO A ENVIAR</a> -->
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <input name="local_id_local" type="hidden" value="<?php echo $local['id_oficina']; ?>"/>
                <button type="submit" class="btn btn-guardar btn-flat " name="editarLocal" id="btnEditarLocal">GUARDAR EDICIÓN</button>
                <button type="reset" class="btn bg-red btn-flat cerrar" onclick="window.history.go(-1); return false;" id="btnCreateCancelar">CANCELAR</button>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        //$('.maskDecimal').autoNumeric('init');
        $('.select2').select2();
        $('[data-mask]').inputmask();
        $('#form_locales').validate();

        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
           checkboxClass: 'icheckbox_minimal-blue',
           radioClass: 'iradio_minimal-blue'
       });

        $('a[rel*=facebox]').facebox();
        $('#facebox').draggable({handle: 'div.titulo'});

        $('#local_telefono_rpm').soloPermitir();

        $('[data-toggle="tab"]').tooltip({
            trigger: 'hover',
            placement: 'top',
            animate: true,
            delay: 100,
            container: 'body'
        });

        llamadasAjaxUbigeo();
    });

    (function($){
        // $('#div_sede_distrital').addClass('hide');
        var local_seguridad = $('#local_seguridad');

        local_seguridad.on('change', function(){
            var seg = local_seguridad.val();

            if (seg === 'OTROS') {
                var input_seguridad = '<div id="local_seguridad_datos" class="margin-left-10 margin-top-20 col-xs-12" style="">'+
                    '<div class="form-group"><label for="local_seguridad" class="col-xs-5 form-label sinpadding"><span class="pull-right">SEGURIDAD</span></label>'+
                    '<div class="col-xs-7"><input name="local_seguridad" class="input-sm form-control text-uppercase onlyStrings requerido" maxlength="30" /></div></div></div>';
                local_seguridad.parent().append(input_seguridad);
            } else {
                $('#local_seguridad_datos').remove();
            }
        });
    })(jQuery);

    (function($){
        // VARIABLES GLOBALES

        var tipo_internet = $('#internet_datos');

        $('#local_internet').on('change.internet', function(){
            var internet = this.value;

            if (internet == '1') {
                tipo_internet.show();
                return;
            } else {
                tipo_internet.hide();
                return;
            }
        });

        $('#local_pc').on('keyup.pc', function(){
            var reg_computadoras = document.getElementById('btn_reg_computadoras');
            <?php if (isset($local['id_oficina'])): ?>
                var ruta = '<?php echo base_url('ajax/computadora/editar'); ?>?id_oficina=<?php echo $local['id_oficina']; ?>&pcs='+this.value;
                reg_computadoras.href = ruta;
            <?php else: ?>
                var ruta = '<?php echo base_url('ajax/computadora/crear'); ?>?pcs='+this.value;
            <?php endif; ?>

            if (this.value < 1) {
                $(reg_computadoras).attr('disabled', true);

            } else {
                reg_computadoras.href = ruta;

                $(reg_computadoras).attr('disabled', false);
            }
        });

        $('#local_turnos').on('change.turnos', function(){
            var reg_seguridad = document.getElementById('btn_reg_seguridad');
            <?php if (isset($local['id_oficina'])): ?>
                var ruta = '<?php echo base_url('ajax/seguridad/editar'); ?>?id_oficina=<?php echo $local['id_oficina']; ?>&seguridad='+this.value;
            <?php else: ?>
                var ruta = '<?php echo base_url('ajax/seguridad/crear'); ?>?seguridad='+this.value;
            <?php endif; ?>

            if (this.value === '') {
                $(reg_seguridad).attr('disabled', true);
            } else {
                reg_seguridad.href = ruta;
                $(reg_seguridad).attr('disabled', false);
            }
        });
    })(jQuery);
</script>
<?php $this->load->view('locales/odei/partials_vista'); ?>
