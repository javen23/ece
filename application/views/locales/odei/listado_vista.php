<section class="content">
    <div class="box noBox">
        <?php if ($msg = $this->session->flashdata('mensaje_flash')): ?>
            <div class="alert-message">
                <div class="alert alert-<?php echo $msg['tipo'] ?> alert-dismissable">
                    <button type="button" class="close text-warning" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><?php echo $msg['cuerpo'] ?></strong>
                </div>
            </div>
        <?php endif; ?>
        <div class="box-header">
            <h3 class="box-title pull-right">
                <a href="<?php echo base_url('locales/jurisdiccion-regional/crear'); ?>" class="btn btn-success btn-flat awhite">
                    <span class="fa fa-plus-circle"></span> CREAR LOCAL
                </a>
            </h3>

            <h3 class="box-title">
                EXPORTAR LISTADO DE LOCALES
                <span class="inline-block">
                    <form action="<?php echo base_url('ajax-exportar-tabla') ?>" method="POST" target="_blank" id="form_exportar_tabla">
                        <span class="pull-right cursor-pointer exportar_tabla_reporte" data-toggle="tooltip" title="Descargar <?php echo $titulo ?>"><i class="fa fa-cloud-download fa-2x text-green"></i></span>
                        <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
                        <input type="hidden" name="nombre_archivo" value="<?php echo url_amigable(notildes($titulo)) ?>">
                    </form>
                </span>
            </h3>
        </div>
        <div class="box-body table-responsive">
            <table width="100%" id="tablaListado" class="display table table-bordered table-striped table-hover" >
                <thead class="headTablaListado">
                    <tr class="text-uppercase">
                        <th colspan="8" class="seccion1">I. UBICACIÓN DE LA SEDE OPERATIVA</th>
                        <th colspan="6" class="seccion2 border-th-header-left border-th-header-right">II. INFORMACIÓN DEL RESPONSABLE</th>
                        <th colspan="16" class="seccion3 border-th-header-right">III. INFORMACIÓN DEL LOCAL</th>
                        <th colspan="2">ACCIONES</th>
                    </tr>
                    <tr class="text-uppercase">
                        <th rowspan="2">N°</th>
                        <th rowspan="2">SEDE OPERATIVA</th>
                        <th rowspan="2">DEPARTAMENTO</th>
                        <th rowspan="2">PROVINCIA</th>
                        <th rowspan="2">DISTRITO</th>
                        <th rowspan="2">DIRECCIÓN DE LA OFICINA</th>
                        <th rowspan="2">REFERENCIA</th>
                        <th rowspan="2">OBSERVACIÓN</th>
                        <th rowspan="2" class="border-th-header-left">CARGO DEL RESPONSABLE</th>
                        <th rowspan="2">NOMBRE DEL FUNCIONARIO</th>
                        <th colspan="3" class="border-th-header-left border-th-header-right">TELEFONOS</th>
                        <th rowspan="2" class="border-th-header-right">EMAIL DEL FUNCIONARIO</th>
                        <th rowspan="2">AREA TOTAL M2 <br/>DEL LOCAL</th>
                        <th rowspan="2">NÚMERO DE <br/>AMBIENTES</th>
                        <th rowspan="2">AREA M2 <br/>DEL ALMACEN</th>
                        <th colspan="3" class="border-th-header-left border-th-header-right">TOTAL MOBILIARIO</th>
                        <th colspan="4" class="border-th-header-right">SERVICIOS CON LOS QUE CUENTA</th>
                        <th rowspan="2">TIPO DE <br/>CONSTRUCCIÓN</th>
                        <th colspan="3" class="border-th-header-left border-th-header-right">SEGURIDAD</th>
                        <th colspan="2" class="border-th-header-right">REQUERIMIENTO PRESUPUESTAL</th>
                        <th rowspan="2">EDITAR</th>
                        <th rowspan="2">ELIMINAR</th>
                    </tr>
                    <tr class="text-uppercase">
                        <th class="border-th-header-left">FIJO</th>
                        <th>CELULAR</th>
                        <th class="border-th-header-right">RPM</th>
                        <th class="border-th-header-left">ESCRITORIOS</th>
                        <th>MESAS</th>
                        <th class="border-th-header-right">SILLAS</th>
                        <th>PC</th>
                        <th>INTERNET <br>WIFI</th>
                        <th>LUZ <br>ELECTRICA</th>
                        <th class="border-th-header-right">SERVICIOS <br>HIGIÉNICOS</th>
                        <th class="border-th-header-left">TIPO DE SEGURIDAD</th>
                        <th>TURNOS</th>
                        <th class="border-th-header-right">PRESUPUESTO <br/> POR TURNO</th>
                        <th>COSTO DE ALQUILER<br/> DEL LOCAL</th>
                        <th class="border-th-header-right">DETALLE</th>
                    </tr>
                </thead>
                <tfoot class="footTablaListado">
                    <tr class="text-uppercase tfoot-placeholder">
                        <th>N°</th>
                        <th>SEDE OPERATIVA</th>
                        <th>DEPARTAMENTO</th>
                        <th>PROVINCIA</th>
                        <th>DISTRITO</th>
                        <th>DIRECCIÓN DE LA OFICINA</th>
                        <th>REFERENCIA</th>
                        <th>OBSERVACIONES</th>
                        <th>CARGO DEL RESPONSABLE</th>
                        <th>NOMBRE DEL FUNCIONARIO</th>
                        <th>FIJO</th>
                        <th>CELULAR</th>
                        <th>RPM</th>
                        <th>EMAIL DEL FUNCIONARIO</th>
                        <th>AREA TOTAL M2 <br/>DEL LOCAL</th>
                        <th>NÚMERO DE <br/>AMBIENTES</th>
                        <th>AREA M2 <br/>DEL ALMACEN</th>
                        <th>ESCRITORIOS</th>
                        <th>MESAS</th>
                        <th>SILLAS</th>
                        <th>PC</th>
                        <th>INTERNET</th>
                        <th>LUZ</th>
                        <th>SS.HH</th>
                        <th>TIPO DE <br/>CONSTRUCCIÓN</th>
                        <th>TIPO DE SEGURIDAD</th>
                        <th>TURNOS</th>
                        <th>PRESUPUESTO <br/> POR TURNO</th>
                        <th>COSTO DE ALQUILER<br/> DEL LOCAL</th>
                        <th>DETALLE</th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
                <tbody class="bodyTablaListado">
                    <?php $n=1; ?>
                    <?php foreach ($locales as $local): ?>
                        <tr>
                           <td class="text-center"><?php echo $n++; ?></td>
                           <td><?php show($local, 'sede_operativa') ?></td>
                           <td><?php show($local, 'departamento') ?></td>
                           <td><?php show($local, 'provincia') ?></td>
                           <td><?php show($local, 'distrito') ?></td>
                           <td><?php show($local, 'direccion') ?></td>
                           <td><?php show($local, 'referencia') ?></td>
                           <td><?php show($local, 'observacion') ?></td>
                           <td><?php show($local, 'cargo') ?></td>
                           <td><?php show($local, 'nombres') ?></td>
                           <td class="text-center"><?php show($local, 'telef_fijo') ?></td>
                           <td class="text-center"><?php show($local, 'telef_celular') ?></td>
                           <td class="text-center"><?php show($local, 'telef_rpm') ?></td>
                           <td><?php show($local, 'email') ?></td>
                           <td class="text-center"><?php show($local, 'area') ?></td>
                           <td class="text-center"><?php show($local, 'nro_ambiente') ?></td>
                           <td class="text-center"><?php show($local, 'area_almacen') ?></td>
                           <td class="text-center"><?php show($local, 'nro_escritorio') ?></td>
                           <td class="text-center"><?php show($local, 'nro_mesa') ?></td>
                           <td class="text-center"><?php show($local, 'nro_silla') ?></td>
                           <td class="text-center"><?php show($local, 'pc') ?></td>
                        <!--?php if($local['internet'] === '1'): ?-->
                           <!--td>
                               <!--?php foreach ($this->dataselect->tipo_internet() as $item) {
                                   if ($item['id'] === $local['internet_tipo']) {
                                       echo $item['desc'];
                                   }
                               } ?>
                           </td-->
                        <!--?php else: ?-->
                           <td><!--?php echo si_no($local['internet']) ?-->Si</td>
                        <!--?php endif; ?-->
                           <td><!--?php echo si_no($local['electricidad']) ?-->No</td>
                           <td><!--?php echo si_no($local['sshh']) ?-->No</td>
                           <td><?php show($local, 'tipo_construc') ?></td>
                           <td><?php show($local, 'seguridad') ?></td>
                           <td><?php show($local, 'turnos') ?></td>
                           <td><?php show($local, 'turnos_presupuesto') ?></td>
                           <td class="text-center"><?php show($local, 'costos') ?></td>
                           <td class="text-center">
                               <a tabindex="0" role="button" class="btn btn-sm btn-success" data-placement="left" data-toggle="popover" data-trigger="focus" title="Observaciones" data-content="<?php show($local, 'observacion_local') ?>"><i class="fa fa-eye"></i> Ver detalle</a>
                           </td>
                           <td class="text-center">
                               <!--a href="<php echo base_url('locales/jurisdiccion-regional/editar'); ?>?idLocal=<php echo $local['id_oficina'] ?>" data-toggle='tooltip' title="EDITAR" class="btn btn-flat btn-primary"><i class="icon ion-edit"></i></a-->
                               <a href="<?php echo base_url('locales/jurisdiccion-regional/editar'); ?>?idLocal=1" data-toggle='tooltip' title="EDITAR" class="btn btn-sm btn-primary"><i class="icon ion-edit"></i></a>
                           </td>
                           <td class="text-center">
                               <!--a href="<php echo base_url('locales/jurisdiccion-regional/eliminar'); ?>?idLocal=<php echo $local['id_oficina'] ?>" rel="facebox" data-toggle='tooltip' title="ELIMINAR" class="btn btn-flat btn-danger"><i class="icon ion-trash-a"></i></a-->
                               <a href="<?php echo base_url('locales/jurisdiccion-regional/eliminar'); ?>?idLocal=1" rel="facebox" data-toggle='tooltip' title="ELIMINAR" class="btn btn-sm btn-danger"><i class="icon ion-trash-a"></i></a>
                           </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
    $(function () {
        $('a[rel*=facebox]').facebox();
        $('#facebox').draggable({handle: 'div.titulo'});

        tablaListadoDataTable();
    });
</script>
