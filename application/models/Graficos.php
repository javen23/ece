<?php

/**
 *
 */
class Model_Graficos extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function seguridad($tipo = 'OF', $params = array())
    {
        $sql  = "SELECT
            seguridad
            ,COUNT(id_oficina) AS total
            ,seguridad+' ['+CAST(COUNT(id_oficina) AS VARCHAR)+']' AS titulo ";

        return $this->graph_query($sql, $tipo, $params, 'o.seguridad');
    }

    public function condicion_local($tipo = 'OF', $params = array())
    {
        $sql  = "SELECT
            o.costos
            ,CASE
                WHEN o.costos = 0 THEN 'Prestamo' ELSE 'Alquilado'
            END AS local_estado ";

        return $this->graph_query($sql, $tipo, $params, ' o.id_oficina, o.costos');
    }

    public function area_local($tipo = 'OF', $params = array())
    {
        $sql  = "SELECT
            o.area
            ,CASE
                WHEN o.area < 50 THEN 'Menos de 50m2'
                WHEN o.area <= 120 THEN 'Entre 50m2 y 120m2'
                WHEN o.area <= 200 THEN 'Entre 120m2 y 200m2'
                WHEN o.area <= 500 THEN 'Entre 200m2 y 500m2'
                ELSE 'Mas de 500m2'
            END AS local_area ";

        return $this->graph_query($sql, $tipo, $params, 'o.id_oficina, o.area');
    }

    public function nro_ambiente($tipo = 'OF', $params = array())
    {
        $sql  = "SELECT
            o.nro_ambiente
        	,COUNT(o.id_oficina) AS total
            ,CASE
        		WHEN o.nro_ambiente = 1 THEN CAST(o.nro_ambiente AS VARCHAR)+' ambiente ['+CAST(COUNT(id_oficina) AS VARCHAR)+']'
        		ELSE
        			CAST(o.nro_ambiente AS VARCHAR)+' ambientes ['+CAST(COUNT(id_oficina) AS VARCHAR)+']'
             END AS titulo ";

        return $this->graph_query($sql, $tipo, $params, 'o.nro_ambiente');
    }

    public function consecucion_regional($tipo = 'OF', $params = array())
    {
        $sql  = "SELECT
                so.cod_sede_operativa
                ,so.sede_operativa AS titulo
                ,(SELECT COUNT(o.cod_sede_operativa) FROM oficina o WHERE o.tipo = 'LZ' AND o.estado = 1 AND o.cod_sede_operativa = so.cod_sede_operativa) AS local ";
        $sql .= " FROM sede_operativa so WHERE so.cod_sede_operativa != '999' ";
        $sql .= " ORDER BY so.sede_operativa ASC ";

        $query = $this->db->query($sql, array($tipo));

        if ($query && $query->num_rows() > 0) {
            return $this->convert_utf8->convert_result($query);
        }
        return array();
    }

    public function consecucion_provincial($tipo = 'OF', $params = array())
    {
        $sql  = "SELECT
                so.cod_sede_operativa
                ,sp.cod_sede_prov
                ,sp.sede_prov
                ,(SELECT COUNT(o.cod_sede_operativa) FROM oficina o WHERE o.tipo = 'OF' AND o.estado = 1 AND o.cod_sede_operativa = sp.cod_sede_operativa  AND o.cod_sede_prov = sp.cod_sede_prov AND o.cod_sede_dist IS NULL) AS local ";
        $sql .= " FROM sede_prov sp ";
        $sql .= " LEFT JOIN sede_operativa so ON so.cod_sede_operativa = sp.cod_sede_operativa ";
        $sql .= " ORDER BY so.sede_operativa ASC ";

        $query = $this->db->query($sql, array($tipo));

        if ($query && $query->num_rows() > 0) {
            return $this->convert_utf8->convert_result($query);
        }
        return array();
    }

    public function consecucion_distrital($tipo = 'OF', $params = array())
    {
        $sql  = "SELECT
                so.cod_sede_operativa
                ,sd.cod_sede_prov
                ,sd.cod_sede_dist
                ,sd.sede_dist
                ,(SELECT COUNT(o.cod_sede_prov) FROM oficina o WHERE o.tipo = 'OF' AND o.estado = 1 AND o.cod_sede_operativa = sd.cod_sede_operativa  AND o.cod_sede_prov = sd.cod_sede_prov AND o.cod_sede_dist = sd.cod_sede_dist AND o.cod_sede_dist IS NOT NULL) AS local ";
        $sql .= " FROM sede_dist sd ";
        $sql .= " LEFT JOIN sede_operativa so ON so.cod_sede_operativa = sd.cod_sede_operativa ";
        $sql .= " ORDER BY so.sede_operativa ASC ";

        $query = $this->db->query($sql, array($tipo));

        if ($query && $query->num_rows() > 0) {
            return $this->convert_utf8->convert_result($query);
        }
        return array();
    }

    private function graph_query($sql, $tipo, array $params, $groupBy)
    {
        $sql .= " FROM oficina o ";
        $sql .= " WHERE o.tipo = ? AND o.estado = 1 ";

        if (!empty($params)) {
            foreach ($params as $param) {
                $sql .= " AND o.{$param['campo']} = '{$param['valor']}'";
            }
        }
        $sql .= " GROUP BY {$groupBy} ";

        $query = $this->db->query($sql, array($tipo));

        if ($query && $query->num_rows() > 0) {
            return $this->convert_utf8->convert_result($query);
        }
        return null;
    }
}
