<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Usuario_model extends CI_Model {

   public function __construct() {
      parent::__construct();
      $this->idLogin = $this->session->userdata('idUserLogin');
      $this->nameUsuario = $this->session->userdata('userNameUsuario');
   }

   public function todos_los_usuarios(){
      $sql="SELECT
               usu.idUsu AS id,
               usu.usuario,
               usu.estado,
               usu.usuarioCrea AS creado_por,
               convert(varchar, usu.fechaCrea, 120) AS fecha_creado,
               usu.usuarioModifica AS modificado_por,
               convert(varchar, usu.fechaModifica, 120) AS fecha_modificado,
               rol.idRol AS id_rol,
               rol.rol AS nombre_rol,
               rol.descripcion
            FROM
            	usuario AS usu
            	LEFT JOIN rol ON usu.idRol=rol.idRol";
      $query=$this->db->query($sql);
      return $this->convert_utf8->convert_result($query);
   }

   public function todos_los_roles() {
      //$query = $this->db->select('idRol,rol,descripcion')
      //           ->get('rol');
      $sql = "SELECT idRol, rol, descripcion FROM rol";
      $query = $this->db->query($sql);
      
      if($query->num_rows()>0){
         return $this->convert_utf8->convert_result($query);
      }
      return NULL;

   }

   public function seleccionar_rol($idRol){
      $sql = "SELECT * FROM rol WHERE idRol='".$idRol."'";
      $query = $this->db->query($sql);
      if($query->num_rows()==1){
         return $this->convert_utf8->convert_row($query);
      }
      return NULL;
   }

   public function actualizar_estado($params)
   {
      ($params['estado']==1)? $estado= '0': $estado= '1';
      $datos=array(
         'estado'           => $estado,
         'lastIP'           => $params['lastIP'],
         'navegador'        => $params['navegador'],
         'usuarioModifica'  => $this->nameUsuario,
         'fechaModifica'    => date("Y-m-d H:i:s")
      );
      $this->db->where('idUsu',$params['idUsuario']);
      return $this->db->update('usuario',$datos);
   }

   public function seleccionar_usuario($idUsuario){
      $sql="SELECT
               idUsu AS id,usuario,rol.idRol AS id_rol,rol.rol AS nombre_rol,dni,nombres,apellidos
            FROM
            	usuario AS usu
            	LEFT JOIN rol ON usu.idRol=rol.idRol
            WHERE
               idUsu='".$idUsuario."'";
      $query=$this->db->query($sql);
      if($query->num_rows()==1){
         return $this->convert_utf8->convert_row($query);
      }
      return NULL;
   }

   public function insertar_usuario($params){
      $datos = array(
          'idRol'       => $params['rol_crear'],
          'usuario'     => $params['user_crear'],
          'clave'       => $params['pass_crear'],
          'dni'         => $params['dni_crear'],
          'nombres'     => $params['nombres_crear'],
          'apellidos'   => $params['apellidos_crear'],
          'estado'      => '1',
          'lastIP'      => $params['lastIP'],
          'navegador'   => $params['navegador'],
          'usuarioCrea' => $this->nameUsuario,
          'fechaCrea'   => date("Y-m-d H:i:s")
      );
      return $this->db->insert('usuario',$datos);
   }

   public function actualizar_usuario($params){
      $datos = array(
          'idRol'           => $params['rol_editar'],
          'usuario'         => $params['user_editar'],
          'dni'             => $params['dni_editar'],
          'nombres'         => $params['nombres_editar'],
          'apellidos'       => $params['apellidos_editar'],
          'lastIP'          => $params['lastIP'],
          'navegador'       => $params['navegador'],
          'usuarioModifica' => $this->nameUsuario,
          'fechaModifica'   => date("Y-m-d H:i:s")
      );
      if($params['pass_editar'] != ''){
         $this->db->set('clave',$params['pass_editar']);
      }
      $this->db->where('idUsu',$params['hidden_id_usuario_editar']);
      return $this->db->update('usuario',$datos);
   }

    public function verificar_nombre_usuario($params) {
        if(isset($_REQUEST['dni_crear'])){
            //$query = $this->db->where('dni',$params['dni_crear'])->select('dni')->get('usuario');
            $sql = "SELECT dni FROM usuario WHERE dni='".$params['dni_crear']."'";
            $query = $this->db->query($sql);
            
        } elseif (isset($_REQUEST['user_crear'])){
            //$query = $this->db->where('usuario',$params['user_crear'])->select('usuario')->get('usuario');
            $sql = "SELECT usuario FROM usuario WHERE usuario='".$params['user_crear']."'";
            $query = $this->db->query($sql);
            
        }
        if($query->num_rows()==0){
            return TRUE;
        }
        return FALSE;
   }
}
