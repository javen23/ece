<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->idLogin      = $this->session->userdata('idUserLogin');
        $this->nameUsuario  = $this->session->userdata('userNameUsuario');
    }

    public function todas_las_sedes($sede_operativa = null) {

        if ($sede_operativa) {
            $data   = array('cod_sede_operativa'=>$sede_operativa);

            $query  = $this->db->get_where('sede_operativa', $data);
        }
        else {            
            //$this->db->order_by('sede_operativa','asc');
            //$query  = $this->db->get('sede_operativa');
            $sql = "SELECT * FROM sede_operativa ORDER BY sede_operativa asc";
            $query = $this->db->query($sql);
            
        }
        
        return $this->convert_utf8->convert_result($query);
    }

    public function todos_los_departamentos($sede_operativa = null) {

        if ($sede_operativa) {
            $data   = array('cod_sede_operativa'=>$sede_operativa);

            $query  = $this->db->get_where('dpto', $data);
        }
        else {
            $this->db->select('ccdd, nombre');
            $this->db->order_by('nombre','asc');
            $this->db->group_by('ccdd, nombre');
            $query = $this->db->get('dpto');
        }

        return $this->convert_utf8->convert_result($query);
    }

    public function todas_las_provincias($departamento = null) {

        $this->db->select('ccpp, nombre');
        $this->db->order_by('nombre','asc');
        $this->db->group_by('ccpp, nombre');
        if ($departamento) {
            $data   = array('ccdd'=>$departamento);

            $query  = $this->db->get_where('prov', $data);
        }
        else {
            $query = $this->db->get('prov');
        }

        return $this->convert_utf8->convert_result($query);
    }

    public function todos_los_distritos($departamento = null, $provincia = null) {

        $this->db->select('ccdi, nombre');
        $this->db->order_by('nombre','asc');
        $this->db->group_by('ccdi, nombre');
        if ($departamento && $provincia) {
            $data   = array(
                'ccdd'=>$departamento,
                'ccpp'=>$provincia
            );

            $query  = $this->db->get_where('dist', $data);
        }
        else {
            $query = $this->db->get('dist');
        }

        return $this->convert_utf8->convert_result($query);
    }

    public function obtener_sede($id) {

        $data   = array('cod_sede_operativa' => $id);
        $query  = $this->db->get_where('sede_operativa', $data);

        if( $query->num_rows() == 1 ) {

            return $this->convert_utf8->convert_row($query);
        }

        return array();
    }

    public function seguridad($Seguridad) {

        if ( $Seguridad->getId() == '' ) {
            $status  = $this->db->insert('seguridad', $Seguridad);

            if ($status) {
                return $this->db->insert_id();
            }
        }
        elseif ($Seguridad->getId() !== '') {
            unset($Seguridad->usuarioCrea);
            unset($Seguridad->fechaCrea);

            $this->db->where('id_seguridad', $Seguridad->getId());
            $status  = $this->db->update('seguridad', $Seguridad);

            if ($status) {
                return $Seguridad->getId();
            }
        }

        return $status;
    }

    public function obtener_seguridad($id_oficina) {

        if ( $id_oficina !== '' ) {
            $data['id_oficina'] = $id_oficina;

            $query  = $this->db->get_where('seguridad', $data);

            if( $query->num_rows() > 0 ) {

                return $this->convert_utf8->convert_result($query);
            }
        }

        return array();
    }

    public function computadora($Computadora) {

        if ( $Computadora->getId() == '' ) {
            $status  = $this->db->insert('oficina_pc', $Computadora);

            if ($status) {
                return $this->db->insert_id();
            }
        }
        elseif ($Computadora->getId() !== '') {
            unset($Computadora->usuarioCrea);
            unset($Computadora->fechaCrea);

            $this->db->where('id_pc', $Computadora->getId());
            $status  = $this->db->update('oficina_pc', $Computadora);

            if ($status) {
                return $Computadora->getId();
            }
        }

        return $status;
    }

    public function obtener_computadora($id_oficina) {

        if ( $id_oficina !== '' ) {
            $data['id_oficina'] = $id_oficina;

            $query  = $this->db->get_where('oficina_pc', $data);

            if( $query->num_rows() > 0 ) {

                return $this->convert_utf8->convert_result($query);
            }
        }

        return array();
    }

    public function obtener_departamentos_por_sede($id) {

        $data   = array(
            'cod_sede_operativa' => (string)$id
        );

        $query  = $this->db->select('dpto.ccdd, dpto.nombre')
                            ->group_by(array('dpto.ccdd', 'dpto.nombre'))
                            ->get_where('dpto', $data);

        if( $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function obtener_provincias_por_departamento($ccdd, $cod_sede_operativa) {

        $data   = array(
            'prov.ccdd' => $ccdd,
            'prov.cod_sede_operativa' => $cod_sede_operativa
        );

        $query  = $this->db->select('prov.ccpp, prov.nombre')
                            ->group_by(array('prov.ccpp', 'prov.nombre'))
                            ->get_where('prov', $data);

        if( $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function obtener_sede_provincial($input) {


        $data   = array();

        if ( isset($input['sede_operativa']) ) {
            $data['cod_sede_operativa'] = $input['sede_operativa'];
        }

        $query  = $this->db->get_where('sede_prov', $data);

        if( $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function obtener_sede_provincial_multiple($params) {

        $separar_sedes  = implode("','",explode(',', $params['sedes']));
        $params_sedes   = $separar_sedes;
        $sql= "SELECT
                    sprov.cod_sede_operativa,sprov.cod_sede_prov,sope.sede_operativa,sprov.sede_prov
                FROM
                    sede_prov AS sprov
                    LEFT JOIN sede_operativa AS sope ON sprov.cod_sede_operativa=sope.cod_sede_operativa
                WHERE
                    1=1 AND sprov.cod_sede_operativa IN ('{$params_sedes}')";

        $query= $this->db->query($sql);
        if( $query->num_rows() > 0 ) {
            return $this->convert_utf8->convert_result($query);
        }
        return array();

    }

    public function obtener_distritos_por_provincia($ccpp, $ccdd, $cod_sede_operativa) {

        $data   = array(
            'ccpp' => $ccpp,
            'ccdd' => $ccdd,
            'cod_sede_operativa' => $cod_sede_operativa
        );

        $query  = $this->db->select('dist.ccdi, dist.nombre')
                            ->group_by(array('dist.ccdi', 'dist.nombre'))
                            ->get_where('dist', $data);

        if( $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function obtener_distritos($input) {

        $data = array();

        if ( isset($input['provincia']) ) {
            $data   = array(
                'ccdi' => $input['distrito'],
                'ccpp' => $input['provincia'],
                'ccdd' => $input['departamento'],
                'cod_sede_operativa' => $input['sede_operativa']
            );
        } else if ( isset($input['sede_prov']) ) {
            $data = array(
                'cod_sede_prov'         => $input['sede_prov'],
                'cod_sede_operativa'    => $input['sede_operativa']
            );
        }

        $query  = $this->db->select('dist.ccdi, dist.nombre, dist.cod_sede_prov')
                            ->group_by(array('dist.ccdi', 'dist.nombre', 'dist.cod_sede_prov'))
                            ->get_where('dist', $data);

        if( $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function obtener_distrito($ccdi, $ccpp, $ccdd, $cod_sede_operativa) {
        $data   = array(
            'ccdi' => $ccdi,
            'ccpp' => $ccpp,
            'ccdd' => $ccdd,
            'cod_sede_operativa' => $cod_sede_operativa
        );

        $query  = $this->db->select('dist.ccdi, dist.nombre, dist.cod_sede_dist, dist.cod_sede_prov')
                            ->group_by(array('dist.ccdi', 'dist.nombre', 'dist.cod_sede_dist', 'dist.cod_sede_prov'))
                            ->get_where('dist', $data);

        if( $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function obtener_lectura_barra($codigo){
        $query=$this->db->query("SELECT cod_barra FROM material_imprenta WHERE cod_barra='{$codigo}'");
        if($query->num_rows==1):
            $query_leido=$this->db->query("SELECT mimp.*, sope.sede_operativa AS nombre_sede,'0' AS reciente
                                            FROM material_imprenta AS mimp
                                            LEFT JOIN sede_operativa AS sope ON mimp.sede_operativa=sope.cod_sede_operativa WHERE mimp.cod_barra='{$codigo}' AND mimp.estado=1");
            if($query_leido->num_rows==1):
                return $this->convert_utf8->convert_row($query_leido);
            else:
                $datos = array('estado' => '1','fecha_registro' => date("Y-m-d H:i:s"));
                $this->db->where('cod_barra',$codigo);
                $actualizado=$this->db->update('material_imprenta',$datos);
                if($actualizado):
                    $query_actualizado=$this->db->query("SELECT mimp.*, sope.sede_operativa AS nombre_sede,'1' AS reciente
                                            FROM material_imprenta AS mimp
                                            LEFT JOIN sede_operativa AS sope ON mimp.sede_operativa=sope.cod_sede_operativa WHERE mimp.cod_barra='{$codigo}'");
                    return $this->convert_utf8->convert_row($query_actualizado);
                endif;
            endif;
        else:
            return NULL;
        endif;

    }

    public function area_almacen($params)
    {
        if (isset($params['cod_sede_dist']) && $params['cod_sede_dist'] !== '') {
            $result = $this->db->select('area_minima_almacen')->get_where('sede_dist', $params)->row_array();
        }
        else {
            unset($params['cod_sede_dist']);
            $result = $this->db->select('area_minima_almacen')->get_where('sede_prov', $params)->row_array();
        }

        return is_null($result['area_minima_almacen']) ? 10 : $result['area_minima_almacen'];
    }

    public function obtener_sede_distrital_por_distrito($ccdi, $ccpp, $ccdd, $cod_sede_operativa) {

        $data   = array(
            'cod_sede_operativa' => $cod_sede_operativa
        );

        $distritos = $this->obtener_distrito($ccdi, $ccpp, $ccdd, $cod_sede_operativa);

        function sede_distrital_cod_dist($item) {
            if (isset($item['cod_sede_dist'])) {
                return $item['cod_sede_dist'];
            }
        };

        // function sede_distrital_cod_prov($item) {
        //     if (isset($item['cod_sede_prov'])) {
        //         return $item['cod_sede_prov'];
        //     }
        // };

        $cod_sede_dists = array_map('sede_distrital_cod_dist', $distritos);
        // $cod_sede_provs = array_map('sede_distrital_cod_prov', $distritos);

        $query  = $this->db->select()
                            ->where($data);

        if (!empty($cod_sede_dists)) {
            $query->where_in('cod_sede_dist', $cod_sede_dists);
        }

        $query  = $query->get('sede_dist');

        if( !$query ) {
            return array();
        }

        return $this->convert_utf8->convert_result($query);
    }

}
