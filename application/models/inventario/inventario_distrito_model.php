<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventario_distrito_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->idLogin = $this->session->userdata('idUserLogin');
        $this->nameUsuario = $this->session->userdata('userNameUsuario');
    }

    public function lista_material_distrito(){
        $query=$this->db->query("SELECT
                                    mimp.correlativo,
                                    mimp.sede_operativa,
                                    mimp.cod_sede_prov,
                                    mimp.cod_barra,
                                    mimp.cod_minedu,
                                    mimp.cod_modular,
                                    mimp.ie_temp,
                                    mimp.estado,
                                    CONVERT(VARCHAR,mimp.fecha_registro,120) AS fecha_registro,
                                    sope.sede_operativa AS nombre_sede
                                FROM
                                    material_imprenta AS mimp
                                    LEFT JOIN sede_operativa AS sope ON mimp.sede_operativa=sope.cod_sede_operativa");
        return $this->convert_utf8->convert_result($query);
    }

}
