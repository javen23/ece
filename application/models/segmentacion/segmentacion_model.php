<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Segmentacion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->idLogin      = $this->session->userdata('idUserLogin');
        $this->nameUsuario  = $this->session->userdata('userNameUsuario');

        $this->campos = array(
            // 'cod_sede_region',
            'sed_region' => 'ie_sede_region',
            // 'cod_sede_provincia',
            'sed_prov' => 'ie_sede_prov',
            // 'cod_sede_distrital',
            'sed_dist' => 'ie_sede_dist',
            'cod_ugel' => 'ie_cod_ugel',
            'ugel' => 'ie_ugel',
            // 'cod_geo',
            'ccdd' => 'ie_ccdd',
            'ccpp' => 'ie_ccpp',
            'ccdi' => 'ie_ccdi',
            'cp' => 'ie_cp',
            'cod_local' => 'ie_cod_local',
            'cod_modular' => 'ie_cod_modular',
            'ie' => 'ie_ie',
            'direccion' => 'ie_direccion',
            'area' => 'ie_area',
            'nivel' => 'ie_nivel',
            'cod_eva15' => 'ie_cod_eva15',
            'sec_prog' => 'ie_sec_prog',
            'aplica_lider' => 'ie_aplica_lider',
            'aplica' => 'ie_aplica',
            'aplica_total' => 'ie_aplica_total',
            'cod_asist_sup' => 'ie_cod_asist_sup',
            'turno' => 'ie_turno',
            'dias_aplica' => 'ie_dias_aplica',
            'dias_viaje' => 'ie_dias_viaje',
            'pasaje' => 'ie_pasaje',
            'movilidad' => 'ie_movilidad',
            'gastos_operativos' => 'ie_sede_region',
            'total_pasaje',
            'total_movilidad',
            'total_bonificacion',
            'total_ie',
            'estado_ie',
            'observacion',
            'cierre',
        );
    }

    public function total_resultado_sede_operativa($params=NULL){
        if($params ==null){
            $params_nivel=" ";
            $params_sedes=" ";
        }else{
            ($params['nivel']=='0')? $params_nivel=" ": $params_nivel=" AND ie.nivel IN ('{$params['nivel']}') ";
            if($params['sedes']=='0'){
                $params_sedes=" ";
            }else{
                $separar_sedes = implode("','",explode(',', $params['sedes']));
                $params_sedes=" AND ie.sed_region IN ('".$separar_sedes."') ";
            }
        }

        $sql="SELECT
                ie.sed_region AS codigo_sede,
                sope.sede_operativa AS nombre_sede,
                COUNT(ie.cod_modular) AS total_instituciones,
                SUM(ie.sec_prog) AS total_secciones,
                SUM(ie.aplica_total) AS total_aplicadores,
                SUM(CASE WHEN ie.gasto_operativa IS NULL THEN 0 ELSE ie.gasto_operativa END) AS total_gastos_operativos,
                SUM(CASE WHEN ie.total_pasaje IS NULL THEN 0 ELSE ie.total_pasaje END) AS total_pasaje,
                SUM(CASE WHEN ie.total_movilidad IS NULL THEN 0 ELSE ie.total_movilidad END) AS total_movilidad,
                SUM(CASE WHEN ie.total_bonificacion IS NULL THEN 0 ELSE ie.total_bonificacion END) AS total_bonificacion,
                (SUM(CASE WHEN ie.gasto_operativa IS NULL THEN 0 ELSE ie.gasto_operativa END)+
                SUM(CASE WHEN ie.total_pasaje IS NULL THEN 0 ELSE ie.total_pasaje END)+
                SUM(CASE WHEN ie.total_movilidad IS NULL THEN 0 ELSE ie.total_movilidad END)+
                SUM(CASE WHEN ie.total_bonificacion IS NULL THEN 0 ELSE ie.total_bonificacion END)) AS total
            FROM
                ie
                LEFT JOIN sede_operativa AS sope ON ie.sed_region=sope.cod_sede_operativa
            WHERE
                1=1 {$params_nivel} {$params_sedes}
                GROUP BY ie.sed_region,sope.sede_operativa ORDER BY sope.sede_operativa";

        //log_message('INFO', "Segmentacion_reporte: {$sql}");

        $query = $this->db->query($sql);

        return $this->convert_utf8->convert_result($query);
    }

    public function total_resultado_provincia_operativa($params=NULL){
        if($params ==null){
            $params_nivel=" ";
            $params_sedes=" ";
        }else{
            ($params['nivel']=='0')? $params_nivel=" ": $params_nivel=" AND ie.nivel IN ('{$params['nivel']}') ";
            if($params['sedes']=='0'){
                $params_sedes=" ";
            }else{
                $separar_sedes = implode("','",explode(',', $params['sedes']));
                $params_sedes=" AND ie.sed_region IN ('".$separar_sedes."') ";
            }
        }

        $sql="SELECT
                ie.sed_region AS codigo_sede,
                sope.sede_operativa AS nombre_sede,
                COUNT(ie.cod_modular) AS total_instituciones,
                SUM(ie.sec_prog) AS total_secciones,
                SUM(ie.aplica_total) AS total_aplicadores,
                SUM(CASE WHEN ie.gasto_operativa IS NULL THEN 0 ELSE ie.gasto_operativa END) AS total_gastos_operativos,
                SUM(CASE WHEN ie.total_pasaje IS NULL THEN 0 ELSE ie.total_pasaje END) AS total_pasaje,
                SUM(CASE WHEN ie.total_movilidad IS NULL THEN 0 ELSE ie.total_movilidad END) AS total_movilidad,
                SUM(CASE WHEN ie.total_bonificacion IS NULL THEN 0 ELSE ie.total_bonificacion END) AS total_bonificacion,
                (SUM(CASE WHEN ie.gasto_operativa IS NULL THEN 0 ELSE ie.gasto_operativa END)+
                SUM(CASE WHEN ie.total_pasaje IS NULL THEN 0 ELSE ie.total_pasaje END)+
                SUM(CASE WHEN ie.total_movilidad IS NULL THEN 0 ELSE ie.total_movilidad END)+
                SUM(CASE WHEN ie.total_bonificacion IS NULL THEN 0 ELSE ie.total_bonificacion END)) AS total
            FROM
                ie
                LEFT JOIN sede_operativa AS sope ON ie.sed_region=sope.cod_sede_operativa
            WHERE
                1=1 {$params_nivel} {$params_sedes}
                GROUP BY ie.sed_region,sope.sede_operativa ORDER BY sope.sede_operativa";

        //log_message('INFO', "Segmentacion_reporte: {$sql}");

        $query = $this->db->query($sql);

        return $this->convert_utf8->convert_result($query);

    }

    public function total_resultado_distrito_operativa($params=NULL){

        if($params ==null){
            $params_nivel=" ";
            $params_sedes=" ";
        }else{
            ($params['nivel']=='0')? $params_nivel=" ": $params_nivel=" AND ie.nivel IN ('{$params['nivel']}') ";
            if($params['sedes']=='0'){
                $params_sedes=" ";
            }else{
                $separar_sedes = implode("','",explode(',', $params['sedes']));
                $params_sedes=" AND ie.sed_region IN ('".$separar_sedes."') ";
            }
        }

        $sql="SELECT
                ie.sed_region AS codigo_sede,
                sope.sede_operativa AS nombre_sede,
                COUNT(ie.cod_modular) AS total_instituciones,
                SUM(ie.sec_prog) AS total_secciones,
                SUM(ie.aplica_total) AS total_aplicadores,
                SUM(CASE WHEN ie.gasto_operativa IS NULL THEN 0 ELSE ie.gasto_operativa END) AS total_gastos_operativos,
                SUM(CASE WHEN ie.total_pasaje IS NULL THEN 0 ELSE ie.total_pasaje END) AS total_pasaje,
                SUM(CASE WHEN ie.total_movilidad IS NULL THEN 0 ELSE ie.total_movilidad END) AS total_movilidad,
                SUM(CASE WHEN ie.total_bonificacion IS NULL THEN 0 ELSE ie.total_bonificacion END) AS total_bonificacion,
                (SUM(CASE WHEN ie.gasto_operativa IS NULL THEN 0 ELSE ie.gasto_operativa END)+
                SUM(CASE WHEN ie.total_pasaje IS NULL THEN 0 ELSE ie.total_pasaje END)+
                SUM(CASE WHEN ie.total_movilidad IS NULL THEN 0 ELSE ie.total_movilidad END)+
                SUM(CASE WHEN ie.total_bonificacion IS NULL THEN 0 ELSE ie.total_bonificacion END)) AS total
            FROM
                ie
                LEFT JOIN sede_operativa AS sope ON ie.sed_region=sope.cod_sede_operativa
            WHERE
                1=1 {$params_nivel} {$params_sedes}
                GROUP BY ie.sed_region,sope.sede_operativa ORDER BY sope.sede_operativa";

        //log_message('INFO', "Segmentacion_reporte: {$sql}");

        $query = $this->db->query($sql);

        return $this->convert_utf8->convert_result($query);
    }

    public function total_resultado_asistente_operativa($params=NULL){

        if($params ==null){
            $params_nivel=" ";
            $params_sedes=" ";
        }else{
            ($params['nivel']=='0')? $params_nivel=" ": $params_nivel=" AND ie.nivel IN ('{$params['nivel']}') ";
            if($params['sedes']=='0'){
                $params_sedes=" ";
            }else{
                $separar_sedes = implode("','",explode(',', $params['sedes']));
                $params_sedes=" AND ie.sed_region IN ('".$separar_sedes."') ";
            }
        }

        $sql="SELECT
                ie.sed_region AS codigo_sede,
                sope.sede_operativa AS nombre_sede,
                COUNT(ie.cod_modular) AS total_instituciones,
                SUM(ie.sec_prog) AS total_secciones,
                SUM(ie.aplica_total) AS total_aplicadores,
                SUM(CASE WHEN ie.gasto_operativa IS NULL THEN 0 ELSE ie.gasto_operativa END) AS total_gastos_operativos,
                SUM(CASE WHEN ie.total_pasaje IS NULL THEN 0 ELSE ie.total_pasaje END) AS total_pasaje,
                SUM(CASE WHEN ie.total_movilidad IS NULL THEN 0 ELSE ie.total_movilidad END) AS total_movilidad,
                SUM(CASE WHEN ie.total_bonificacion IS NULL THEN 0 ELSE ie.total_bonificacion END) AS total_bonificacion,
                (SUM(CASE WHEN ie.gasto_operativa IS NULL THEN 0 ELSE ie.gasto_operativa END)+
                SUM(CASE WHEN ie.total_pasaje IS NULL THEN 0 ELSE ie.total_pasaje END)+
                SUM(CASE WHEN ie.total_movilidad IS NULL THEN 0 ELSE ie.total_movilidad END)+
                SUM(CASE WHEN ie.total_bonificacion IS NULL THEN 0 ELSE ie.total_bonificacion END)) AS total
            FROM
                ie
                LEFT JOIN sede_operativa AS sope ON ie.sed_region=sope.cod_sede_operativa
            WHERE
                1=1 {$params_nivel} {$params_sedes}
                GROUP BY ie.sed_region,sope.sede_operativa ORDER BY sope.sede_operativa";

        //log_message('INFO', "Segmentacion_reporte: {$sql}");

        $query = $this->db->query($sql);

        return $this->convert_utf8->convert_result($query);
    }

    public function todos_los_locales($sede_operativa = null) {

        $sql  = $this->selects();
        $sql .= $this->joins();
        $sql .= "WHERE ie.cod_modular IS NOT NULL ";

        //log_message('INFO', "Segmentacion Obtener: {$sql}");

        $query = $this->db->query($sql);

        return $this->convert_utf8->convert_result($query);
    }

    public function total_locales_filtrados($filtro = array()) {

        $sql = "SELECT COUNT(ie.cod_modular) AS total ";
        $sql .= $this->joins();
        $sql .= "WHERE ie.cod_modular IS NOT NULL ";

        $sql .= $this->filtros($filtro);

        log_message('INFO', "Segmentacion Total Filtrado: {$sql}");

        $query = $this->db->query($sql);

        return $this->convert_utf8->convert_row($query);
    }

    public function obtener_locales($campos, $start, $end) {

        $sql = " WITH OrderedOrders AS ( ";

            $sql .= $this->selects();
            $sql .= $this->joins();
            $sql .= "WHERE ie.cod_modular IS NOT NULL ";
            $sql .= $this->filtros($campos);

        $sql .= " )
            SELECT *
            FROM OrderedOrders
            WHERE orden BETWEEN {$start} AND {$end}; ";

        $query = $this->db->query($sql);

        log_message('INFO', "Segmentacion Obtener: {$sql}");

        if( $query && $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function actualizar_campo($id, $campo_actualizado, $campos) {

        $data   = array();

        foreach ($campos as $campo => $valor) {
            $data[$campo] = $valor;
        }

        if ( 'pasaje' === $campo_actualizado ) {
            $data['movilidad']          = '0';
            $data['total_movilidad']    = '0';
        }
        else if ( 'movilidad' === $campo_actualizado ) {
            $data['pasaje']         = '0';
            $data['total_pasaje']   = '0';
        }

        $status = $this->db->where('cod_modular', $id)
                ->update('ie', $data);

        // log_message('INFO', "Segmentacion Update: {$status}");
        return $status;
    }

    private function filtros($campos)
    {
        $sql = '';

        foreach ($campos as $campo => $valor) {

            if ($campo === 'cod_modular' && $valor !== '') {
                $sql .= "AND ie.{$campo} LIKE '%{$valor}%' ";
            } else {
                $sql .= $valor !== '' ? "AND  ie.{$campo} = '{$valor}' " : "";
            }

        }

        return $sql;
    }

    private function selects()
    {
        return "SELECT
                    ROW_NUMBER() OVER (ORDER BY so.sede_operativa, ie.ie ) AS orden,
                    ie.sed_region,
                    so.sede_operativa AS nom_sede_region,
                    ie.sed_prov,
                    sp.sede_prov  AS nom_sede_prov,
                    ie.sed_dist,
                    sd.sede_dist  AS nom_sede_dist,
                    ie.cod_ugel,
                    ie.ugel,
                    dep.nombre AS departamento,
                    pro.nombre AS provincia,
                    dis.nombre AS distrito,
                    ie.cp,
                    ie.cod_local,
                    ie.cod_modular,
                    ie.ie,
                    ie.direccion,
                    ie.area,
                    ie.nivel,
                    ie.cod_eva15,
                    ie.sec_prog,
                    ie.aplica_lider,
                    ie.aplica,
                    ie.aplica_total,
                    ie.cod_asist_sup,
                    ie.turno,
                    ie.dias_aplica,
                    ie.dias_viaje,
                    ie.pasaje,
                    ie.movilidad,
                    ie.gasto_operativa,
                    ie.total_pasaje,
                    ie.total_movilidad,
                    ie.total_bonificacion,
                    ie.total_ie,
                    ie.estado_ie,
                    ie.observa,
                    ie.cierre
                    ";
    }

    private function joins()
    {
        return "FROM
                ie ie
                LEFT JOIN dpto AS dep
                    ON dep.ccdd = ie.ccdd
                    AND dep.cod_sede_operativa = ie.sed_region
                    AND dep.cod_sede_prov = ie.sed_prov
                    AND dep.cod_sede_dist = ie.sed_dist
                LEFT JOIN prov AS pro
                    ON pro.ccpp = ie.ccpp
                    AND pro.ccdd = ie.ccdd
                    AND pro.cod_sede_operativa = ie.sed_region
                    AND pro.cod_sede_prov = ie.sed_prov
                    AND pro.cod_sede_dist = ie.sed_dist
                LEFT JOIN dist AS dis
                    ON dis.ccdi = ie.ccdi
                    AND dis.ccpp = ie.ccpp
                    AND dis.ccdd = ie.ccdd
                    AND dis.cod_sede_operativa = ie.sed_region
                    AND dis.cod_sede_prov = ie.sed_prov
                    AND dis.cod_sede_dist = ie.sed_dist
                LEFT JOIN sede_operativa AS so
                    ON so.cod_sede_operativa = ie.sed_region
                LEFT JOIN sede_prov AS sp
                    ON sp.cod_sede_operativa = ie.sed_region
                    AND sp.cod_sede_prov = ie.sed_prov
                LEFT JOIN sede_dist AS sd
                    ON sd.cod_sede_operativa = ie.sed_region
                    AND sd.cod_sede_prov = ie.sed_prov
                    AND sd.cod_sede_dist = ie.sed_dist ";
    }
}
