<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Programacion_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->idLogin      = $this->session->userdata('idUserLogin');
        $this->nameUsuario  = $this->session->userdata('userNameUsuario');

        $this->campos = array(
            // 'cod_sede_region',
            'sed_region' => 'ie_sede_region',
            // 'cod_sede_provincia',
            'sed_prov' => 'ie_sede_prov',
            // 'cod_sede_distrital',
            'sed_dist' => 'ie_sede_dist',
            'cod_ugel' => 'ie_cod_ugel',
            'ugel' => 'ie_ugel',
            // 'cod_geo',
            'ccdd' => 'ie_ccdd',
            'ccpp' => 'ie_ccpp',
            'ccdi' => 'ie_ccdi',
            'cp' => 'ie_cp',
            'cod_local' => 'ie_cod_local',
            'cod_modular' => 'ie_cod_modular',
            'ie' => 'ie_ie',
            'direccion' => 'ie_direccion',
            'area' => 'ie_area',
            'nivel' => 'ie_nivel',
            'cod_eva15' => 'ie_cod_eva15',
            'sec_prog' => 'ie_sec_prog',
            'aplica_lider' => 'ie_aplica_lider',
            'aplica' => 'ie_aplica',
            'aplica_total' => 'ie_aplica_total',
            'cod_asist_sup' => 'ie_cod_asist_sup',
            'turno' => 'ie_turno',
            'dias_aplica' => 'ie_dias_aplica',
            'dias_viaje' => 'ie_dias_viaje',
            'pasaje' => 'ie_pasaje',
            'movilidad' => 'ie_movilidad',
            'gastos_operativos' => 'ie_sede_region',
            'total_pasaje',
            'total_movilidad',
            'total_bonificacion',
            'total_ie',
            'estado_ie',
            'observacion',
            'cierre',
        );
    }

    public function todos_los_locales($sede_operativa = null) {

        $sql  = $this->selects();
        $sql .= $this->joins();
        $sql .= "WHERE sd.cod_sede_operativa IS NOT NULL ";

        log_message('INFO', "Programacion Obtener: {$sql}");

        $query = $this->db->query($sql, array('2015-09-07'));

        return $this->convert_utf8->convert_result($query);
    }

    public function total_locales_filtrados($filtro = array()) {

        $sql = "SELECT COUNT(ie.cod_modular) AS total ";
        $sql .= $this->joins();
        $sql .= "WHERE ie.cod_modular IS NOT NULL ";

        $sql .= $this->filtros($filtro);

        log_message('INFO', "Segmentacion Total Filtrado: {$sql}");

        $query = $this->db->query($sql);

        return $this->convert_utf8->convert_row($query);
    }

    public function obtener_locales($campos, $start, $end) {

        $sql = " WITH OrderedOrders AS ( ";

            $sql .= $this->selects();
            $sql .= $this->joins();
            $sql .= "WHERE ie.cod_modular IS NOT NULL ";
            $sql .= $this->filtros($campos);

        $sql .= " )
            SELECT *
            FROM OrderedOrders
            WHERE orden BETWEEN {$start} AND {$end}; ";

        $query = $this->db->query($sql);

        log_message('INFO', "Segmentacion Obtener: {$sql}");

        if( $query && $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function actualizar_campo($id, $campo_actualizado, $campos) {

        $data   = array();

        foreach ($campos as $campo => $valor) {
            $data[$campo] = $valor;
        }

        if ( 'pasaje' === $campo_actualizado ) {
            $data['movilidad']          = '0';
            $data['total_movilidad']    = '0';
        }
        else if ( 'movilidad' === $campo_actualizado ) {
            $data['pasaje']         = '0';
            $data['total_pasaje']   = '0';
        }

        $status = $this->db->where('cod_modular', $id)
                ->update('ie', $data);

        // log_message('INFO', "Segmentacion Update: {$status}");
        return $status;
    }

    private function filtros($campos)
    {
        $sql = '';

        foreach ($campos as $campo => $valor) {

            if ($campo === 'cod_modular' && $valor !== '') {
                $sql .= "AND ie.{$campo} LIKE '%{$valor}%' ";
            } else {
                $sql .= $valor !== '' ? "AND  ie.{$campo} = '{$valor}' " : "";
            }

        }

        return $sql;
    }

    private function selects()
    {
        return "SELECT
                sd.id_sede
            	,sd.sede_region
            	,sd.sede_prov
            	,sd.sede_dist

            	,sp.cant_primaria AS primaria_programadas
            	,sp.cant_secundaria AS secundaria_programadas
            	,sp.cant_total AS total_programadas

            	,sa.cant_primaria AS primaria_avance
            	,sa.cant_secundaria AS secundaria_avance
            	,sa.cant_primaria + sa.cant_secundaria AS total_avance
                    ";
    }

    private function joins()
    {
        return "FROM
                SEDE sd
                LEFT JOIN segmenta_programa sp
                    ON sd.id_sede = sp.id_sede
                LEFT JOIN segmenta_avance sa
                    ON sd.id_sede = sa.id_sede AND sa.fecha = ?
                 ";
    }
}
