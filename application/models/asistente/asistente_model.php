<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Asistente_model extends CI_Model {

    public function __construct() { 
        parent::__construct();
        $this->idLogin          = $this->session->userdata('idUserLogin');
        $this->nameUsuario      = $this->session->userdata('userNameUsuario');
        $this->userIdRol        = $this->session->userdata('userIdRol');
        $this->userNombreRol    = $this->session->userdata('userNombreRol');

        $this->data = array(
            'cod_sede_operativa' => 'local_sede',
            'cod_sede_prov'      => 'local_sede_provincial',
            'cod_sede_dist'      => 'local_sede_distrital',
            'dni'                => 'dni',
            'ape_paterno'        => 'ap_paterno',
            'ape_materno'        => 'ap_materno',
            'nombres'            => 'nombre',
            //'ccdd'             => 'local_departamento',
            //'ccpp'             => 'local_provincia',
            //'ccdi'             => 'local_distrito',
            'id_ie'              => 'local_ie',
            'cantidad_aplica'    => 'local_cant_aplicadores',
            'duracion_viaje'     => 'cod_duracion_viaje',
            'fecha_salida'       => 'fecha_salida'
        );
    }

    public function todas_las_ie() {

       $query = $this->db->query("SELECT TOP 100 * FROM ie");

        //log_message('INFO', "Asistente Sip: {$query}");

        if( $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }
        
        return array();
    }

    public function todos_los_asistentes($sede_operativa = null){

        $query = $this->db->query("SELECT
            _as.dni                                 AS dni,
            _as.ape_paterno                         AS apellido_paterno,
            _as.ape_materno                         AS apellido_materno,
            _as.nombres                             AS nombres,
            _as.duracion_viaje                      AS viaje,
            CONVERT(VARCHAR,_as.fecha_salida,120)   AS salida,
            _as.cantidad_aplica                     AS aplicadores,
            _as.cod_asistente                       AS cod_asistente,
            i.cod_modular                           AS cod_modular,
            i.ie                                    AS nombre_ie,
            so.sede_operativa                       AS sede_operativa,
            sp.sede_prov                            AS sede_provincial,
            sd.sede_dist                            AS sede_distrital
            FROM asistente_sup _as
            LEFT JOIN ie i ON i.id_ie = _as.id_ie
            LEFT JOIN sede_operativa so ON so.cod_sede_operativa = _as.cod_sede_operativa
            LEFT JOIN sede_prov sp ON sp.cod_sede_operativa = _as.cod_sede_operativa AND sp.cod_sede_prov = _as.cod_sede_prov
            LEFT JOIN sede_dist sd ON sd.cod_sede_operativa = _as.cod_sede_operativa AND sd.cod_sede_prov = _as.cod_sede_prov AND sd.cod_sede_dist = _as.cod_sede_dist");

        if( $query && $query->num_rows() > 0){

            return $this->convert_utf8->convert_result($query);

        }

        return array();
    }

    public function insertar_ie($params)
    {
        $cod_asistente = '000' . rand(10, 99);

        $params = limpia_datos($params);

        foreach ($this->data as $key => $value) {
            if ($key == 'fecha_salida') {
                 $params[$value] = convertir_fecha($params[$value]);
            }
            
            $parametros[$key] = $params[$value];
        }
        
        $parametros['cod_asistente'] = $cod_asistente; //seteamos

        log_message('INFO', "Insertar IE Parametros: ".json_encode($parametros));

        $status = $this->db->insert('asistente_sup', $parametros);

        return $status;
    }


    public function obtener_asistente($cod_asistente) {

        $sql    = $this->selects();
        $sql   .= $this->joins();
        $sql   .= " WHERE _as.cod_asistente = '{$cod_asistente}' ";
        $sql   .= $this->groupBy();

        $query  = $this->db->query($sql);

        if( $query && $query->num_rows() == 1 ){

            return $this->convert_utf8->convert_row($query);
        }

        return array();
    }


    public function editar_asistente($params, $cod_asistente) {

        $params = limpia_datos($params);

        $sql  = "UPDATE _as SET ";

        $sql .= $this->ordenar_parametros('editar', $params);

        $sql .= "FROM asistente_sup _as ";
        $sql .= "WHERE _as.cod_asistente = {$cod_asistente}";

        log_message('INFO', "Editar Asistente: ". $sql);

        $result = $this->db->query($sql);

        //$this->guardar_relaciones($cod_asistente, $params);

        return $result;
    }

    public function eliminar_asistente($cod_asistente) {

        $data   = array('estado' => 0);

        $this->db->where('cod_asistente', $cod_asistente);
        //echo print_r($this->db);
        //return;
        return $this->db->update('asistente_sup', $data);
    }


    private function selects()
    {
        return "SELECT
                _as.dni,
                _as.ape_paterno     AS apellido_paterno,
                _as.ape_materno     AS apellido_materno,
                _as.nombres         AS nombres,
                _as.duracion_viaje  AS viaje,
                _as.fecha_salida    AS salida,
                _as.cantidad_aplica AS aplicadores,
                _as.cod_asistente   AS cod_asistente,
                _as.cod_sede_operativa AS cod_sede_operativa,
                _as.cod_sede_prov   AS cod_sede_prov,
                _as.cod_sede_dist   AS cod_sede_dist,
                _as.id_ie           AS ie,
                  i.cod_modular       AS cod_modular,
                  i.id_ie             AS nombre_ie,
                 so.sede_operativa   AS sede_operativa,
                 sp.sede_prov        AS sede_provincial,
                 sd.sede_dist        AS sede_distrital
                ";
    }

    private function groupBy()
    {
        return "GROUP BY
                _as.dni,
                _as.ape_paterno,
                _as.ape_materno,
                _as.nombres,
                _as.duracion_viaje,
                _as.fecha_salida,
                _as.cantidad_aplica,
                _as.cod_asistente,
                _as.cod_sede_operativa,
                _as.cod_sede_prov,
                _as.cod_sede_dist,
                _as.id_ie,
                  i.cod_modular,
                  i.id_ie,
                 so.sede_operativa,
                 sp.sede_prov,
                 sd.sede_dist
                 ";
    }

    protected function joins()
    {
        return "FROM
                asistente_sup AS _as
                LEFT JOIN ie AS i ON i.id_ie = _as.id_ie
                LEFT JOIN sede_operativa AS so ON so.cod_sede_operativa = _as.cod_sede_operativa
                LEFT JOIN sede_prov AS sp ON sp.cod_sede_operativa = _as.cod_sede_operativa AND sp.cod_sede_prov = _as.cod_sede_prov
                LEFT JOIN sede_dist AS sd ON sd.cod_sede_operativa =_as.cod_sede_operativa AND sd.cod_sede_prov = _as.cod_sede_prov AND sd.cod_sede_dist = _as.cod_sede_dist
                 ";
    }

    protected function ordenar_parametros($tipo, $params) {
        if ($tipo === 'insertar') {
            $parametros = array();
            // Armamos las columnas de la tabla segun el array data
            $data = implode(', ', array_keys($this->data));
            $sql = "({$data}, estado) ";

            // Recorremos todos los parametros del formulario
            foreach ($this->data as $key => $value) {
                $parametros[] = isset($params[$value]) ? $params[$value] : null;
            }

            $valores = implode("','", array_values($parametros));
            $valores = str_replace("''", "NULL", $valores);

            return $sql .= "VALUES ('{$valores}', 1) ";
        }
        else if ($tipo === 'editar') {
            $sql = '';
            $contador = 0;

            $total_elems = count($this->data);
            foreach ($this->data as $key => $value) {
                if ($key == 'fecha_salida') {
                    $params[$value] = convertir_fecha($params[$value]);
                }
                $contador++;
                $valor = (isset($params[$value]) && $params[$value] !== '') ? "'{$params[$value]}'" : "NULL" ;

                $sql .= "_as.{$key} = {$valor} ";

                if ($contador < $total_elems) {
                    $sql .= ", ";
                }              
            }

            return $sql;
        }

        return false;
    }

    public function verificar_dni_asistente($dni, $cod_asistente = NULL) {
        $sql = "SELECT dni FROM asistente_sup WHERE dni='".$dni."'";
        //if ($cod_asistente != NULL)
           // $sql .= " AND cod_asistente != is NOT $cod_asistente";
        //log_message("INFO","<<<<<Verificando asistente dni ".$sql);
        $query = $this->db->query($sql);
        // $query = $this->db->where('dni',$dni)->select('dni')->get('asistente_sup');
        if( $query && $query->num_rows()==0){
            return TRUE;

        }else{
            return FALSE;
        }
    }

}
