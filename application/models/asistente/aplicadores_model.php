<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aplicadores_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->idLogin          = $this->session->userdata('idUserLogin');
	    $this->nameUsuario      = $this->session->userdata('userNameUsuario');
	    $this->userIdRol        = $this->session->userdata('userIdRol');
	    $this->userNombreRol    = $this->session->userdata('userNombreRol');

	    $this->data = array(
	    	'DNI'			=> 'DNI',
	    	'apellidos'		=> 'apellidos',
	    	'nombres'		=> 'nombres'	
    );
}

public function todos_los_aplicadores(){

	$query = $this->db->query("SELECT TOP 100 * FROM aplicador");

        //log_message('INFO', "Asistente Sip: {$query}");

        if( $query && $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
}

public function insertar_dni_aplicadores($params)
    {
        //$dni = '000' . rand(10, 99);

        $params = limpia_datos($params);

        foreach ($this->data as $key => $value) {
			$parametros[$key] = $params[$value];
		}

        $parametros['dni'] = $dni; //seteamos

        log_message('INFO', "Insertar IE Parametros: ".json_encode($parametros));

        $status = $this->db->insert('asistente_sup_aplicador', $parametros);

        return $status;
    }

	public function obtener_aplicador_dni($dni) {

		//$sql 	 = "SELECT _ap.DNI AS nombre ";
		$sql 	 = "SELECT _ap.nombres +' '+_ap.apellidos+' - '+_ap.DNI AS nombre ";
		$sql    .= $this->joins();
		$sql	.= " WHERE _ap.dni LIKE '%{$dni}%' ";
		$sql	.= $this->groupby();
		
		$query	= $this->db->query($sql);
		
		log_message('INFO', "Aplicadores por DNI: ".$sql);

		if ( $query && $query->num_rows() > 1) {	
			
			return $this->convert_utf8->convert_result($query);
		}

		return array();

	}

	private function selects() {
	return "SELECT 
			_ap.DNI 			AS DNI,
			_ap.apellidos 		AS apellidos,
			_ap.nombres 		AS nombres
			";
	}

	private function groupBy() {

			return "GROUP BY 
				_ap.DNI,
				_ap.apellidos,
				_ap.nombres
			";
	}

	private function joins() {

			return "FROM 
					aplicador AS _ap
					LEFT JOIN asistente_sup_aplicador AS _asa ON _asa.DNI = _ap.DNI 
					";
	}

	public function verificar_dni_aplicador($dni, $cod_asistente = NULL) {
        
        $sql = "SELECT DNI FROM aplicador WHERE DNI = '".$dni."'";

        $query = $this->db->query($sql);

        if ( $query && $query->num_rows()==0){
        	return TRUE;
        }else{
        	return FALSE;
        }
    }

}

/* End of file aplicadores_model.php */
/* Location: ./application/models/asistente/aplicadores_model.php */