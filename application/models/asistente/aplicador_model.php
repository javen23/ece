<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aplicador_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->idLogin          = $this->session->userdata('idUserLogin');
        $this->nameUsuario      = $this->session->userdata('userNameUsuario');
        $this->userIdRol        = $this->session->userdata('userIdRol');
        $this->userNombreRol    = $this->session->userdata('userNombreRol');

        $this->data = array(
        	'cod_asistente'	=> 'codigo_asistente',
        	'DNI'			=> 'DNI'	

        );
	}

	public function todos_los_aplicadores() {

		$sql    = $this->selects();
        $sql   .= $this->joins();
        //$sql   .= " WHERE _asa.cod_asistente = '{$cod_asistente}' ";
        $sql   .= $this->groupBy();
		//$query = $this->db->query("SELECT TOP 50 * FROM asistente_sup_aplicador");

        $query  = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			
			return $this->convert_utf8->convert_result($query);
		}
		return array();
	}

	public function insertar_aplicadores($params, $cod_asistente) {

		//$cod_asistente = '00034';

		$params = limpia_datos($params);

		foreach ($this->data as $key => $value) {

			$parametros[$key] = $params[$value];
		}

		$parametros['cod_asistente'] = $cod_asistente;

		$status = $this->db->insert('asistente_sup_aplicador', $parametros);

		return $status;
	}

public function obtener_aplicadores($cod_asistente) {

		$query = $this->db->query("SELECT 
				_asa.cod_asistente 		AS cod_asistente,
				_ap.DNI 				AS DNI,
				_ap.nombres 			AS nombres,
				_ap.apellidos 			AS apellidos

				FROM asistente_sup_aplicador AS _asa
				LEFT JOIN aplicador _ap ON _ap.DNI = _asa.DNI
				LEFT JOIN asistente_sup _as ON _as.cod_asistente = _asa.cod_asistente

				WHERE _asa.cod_asistente = ".$cod_asistente."; 

				");

		//$sql = "SELECT * FROM asistente_sup_aplicador WHERE cod_asistente = '".$cod_asistente."' ";
		//$query	= $this->db->query($sql);

		if ( $query && $query->num_rows() > 0) {
			return $this->convert_utf8->convert_result($query);			
			//return $result->result_array();
		}

		return array();
	}

	public function editar_aplicadores($params, $cod_asistente) {

		$params = limpia_datos($params);

        $sql  = "UPDATE _asa SET ";

        $sql .= $this->ordenar_parametros('editar', $params);

        $sql .= "FROM asistente_sup_aplicador _asa ";
        $sql .= "WHERE _asa.cod_asistente = {$cod_asistente}";

        //log_message('INFO', "Editar ASP: ". $sql);

        $result = $this->db->query($sql);

        return $result;

	}

	public function eliminar_aplicadores($cod_asistente) {

		$sql  = "DELETE FROM asistente_sup_aplicador WHERE cod_asistente like '".$cod_asistente."' ";
        $result = $this->db->query($sql);
        return $result;
        
        /* FORMA ACTUAL - COMENTADA TEMPORALMENTE POR FALTA DE CAMPO ESTADO
        $data = array('estado' => 0);

		$this->db->where('cod_asistente', $cod_asistente);
        */
	}

	private function selects() {
		return "SELECT 
				_asa.cod_asistente 		AS cod_asistente,
				_ap.DNI 				AS DNI,
				_ap.nombres 			AS nombres,
				_ap.apellidos 			AS apellidos
				
				";
	}

	private function groupBy() {

			return "GROUP BY 
				_asa.cod_asistente,
				_ap.DNI,
				 _ap.nombres,
				 _ap.apellidos
			";
	}

	private function joins() {

			return "FROM 
					asistente_sup_aplicador AS _asa
					LEFT JOIN asistente_sup AS _as ON _as.cod_asistente = _asa.cod_asistente
					LEFT JOIN aplicador AS _ap ON _ap.DNI = _asa.DNI 
					";
	}


	protected function ordenar_parametros($tipo, $params) {
        if ($tipo === 'insertar') {
            $parametros = array();
            // Armamos las columnas de la tabla segun el array data
            $data = implode(', ', array_keys($this->data));
            $sql = "({$data}, estado) ";

            // Recorremos todos los parametros del formulario
            foreach ($this->data as $key => $value) {
                $parametros[] = isset($params[$value]) ? $params[$value] : null;
            }

            $valores = implode("','", array_values($parametros));
            $valores = str_replace("''", "NULL", $valores);

            return $sql .= "VALUES ('{$valores}', 1) ";
        }
        else if ($tipo === 'editar') {
            $sql = '';
            $contador = 0;

            $total_elems = count($this->data);
            foreach ($this->data as $key => $value) {
                $contador++;
                $valor = (isset($params[$value]) && $params[$value] !== '') ? "'{$params[$value]}'" : "NULL" ;

                $sql .= "_asa.{$key} = {$valor} ";

                if ($contador < $total_elems) {
                    $sql .= ", ";
                }              
            }

            return $sql;
        }

        return false;
    }

}

/* End of file asistenteap_model.php */
/* Location: ./application/models/asistente/asistenteap_model.php */