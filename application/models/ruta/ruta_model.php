<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ruta_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->idLogin          = $this->session->userdata('idUserLogin');
        $this->nameUsuario      = $this->session->userdata('userNameUsuario');
        $this->userIdRol        = $this->session->userdata('userIdRol');
        $this->userNombreRol    = $this->session->userdata('userNombreRol');
    }

    public function todos_los_rol_rutas() {
        $sql  = $this->selects();
        $sql .= $this->joins();

        $query  = $this->db->query($sql);

        if( $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function todas_las_rutas() {
        $sql  = "SELECT ru.idRuta, ru.ruta ";
        $sql .= "FROM ruta ru ";
        $sql .= "GROUP BY ru.idRuta, ru.ruta ";

        $query  = $this->db->query($sql);

        if( $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function asignar_ruta($params)
    {
        if (isset($params['todas_las_rutas'])) {
            unset($params['ruta']);
            foreach ($this->todas_las_rutas() as $key => $item) {
                $params['ruta'][] = $item['idRuta'];
            }
        }

        $data   = array();
        foreach ($params['ruta'] as $ruta) {
            $data[] = array('idRol'=>$params['idRol'], 'idRuta'=>$ruta);
        }

        return $this->db->insert_batch('rol_ruta', $data);
    }

    public function insertar_ruta($params)
    {
        $params = limpia_datos($params);
        $rutas  = limpia_datos(explode(',', $params['ruta']), 'utf8_minusculas');
        $data   = array();

        foreach ($rutas as $ruta) {
            $data[] = array('ruta'=>$ruta);
        }

        return $this->db->insert_batch('ruta', $data);
    }

    public function editar_ruta($params) {
        $params = limpia_datos($params, 'utf8_minusculas');

        log_message('INFO', "Datos: {$params}");

        return $this->db->where('idRuta', $params['idRuta'])
                        ->update('ruta', array('ruta' => $params['ruta']));
    }

    public function obtener_ruta($idRuta) {

        $sql    = "SELECT ru.idRuta, ru.ruta ";
        $sql    .= "FROM ruta ru ";
        $sql    .= "WHERE ru.idRuta = '{$idRuta}' ";

        $query  = $this->db->query($sql);

        if( $query->num_rows() == 1 ) {

            return $this->convert_utf8->convert_row($query);
        }

        return array();
    }

    public function obtener_rol_ruta($idRuta, $idRol) {

        $sql    = $this->selects();
        $sql    .= $this->joins();
        $sql    .= "WHERE ru.idRuta = '{$idRuta}' AND ro.idRol = '{$idRol}'";

        $query  = $this->db->query($sql);

        if( $query->num_rows() == 1 ) {

            return $this->convert_utf8->convert_row($query);
        }

        return array();
    }

    public function eliminar_ruta($idRuta, $idRol) {

        return $this->db->delete('rol_ruta', array('idRuta' => $idRuta, 'idRol' => $idRol));
    }


    private function selects()
    {
        return "SELECT
                ro.idRol    AS rol_id,
                ro.rol      AS rol_nombre,
                ru.ruta     AS ruta,
                ru.idRuta   AS ruta_id ";
    }

    private function joins()
    {
        return "FROM
                rol_ruta rr
                JOIN rol AS ro
                    ON ro.idRol = rr.idRol
                JOIN ruta AS ru
                    ON ru.idRuta = rr.idRuta ";
    }
}
