<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Acceso_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function logearme($params) {
        $sql = "SELECT
                    usu.*,
                    (usu.apellidos+' '+usu.nombres) AS nombres_apellidos,
                    us.cod_sede_operativa AS sede_operativa,
                    rol.rol
                FROM
                    usuario AS usu
                    LEFT JOIN rol ON usu.idRol = rol.idRol
                    LEFT JOIN usuario_sede_operativa us ON usu.idUsu = us.idUsu
                WHERE
                    usu.usuario='" . $params['user'] . "'
                    AND usu.clave='" . $params['pass'] . "'
                    AND usu.estado='1'";

        $query = $this->db->query($sql);
        if ($query->num_rows() == 1) {
            return $query->row_array();
        }

        return NULL;
    }

}
