<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Portada_model extends CI_Model {

   public function __construct() {
      parent::__construct();
      $this->idLogin = $this->session->userdata('idUserLogin');
      $this->nameUsuario = $this->session->userdata('userNameUsuario');
   }

   public function actualizar_password($params){

      $datos = array(
         'lastIP'    => $params['lastIP'],
         'navegador' => $params['navegador']
      );
      if($params['pass_cambiar'] != ''){
         $this->db->set('clave',$params['pass_cambiar']);
      }
      $this->db->where('idUsu',$params['id_hidden_usuario_cambiar']);
      return $this->db->update('usuario',$datos);
   }

}
