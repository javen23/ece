<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locales_Administrativo_model extends CI_Model {

    private $graficos;

    public function __construct() {
        parent::__construct();
        $this->idLogin      = $this->session->userdata('idUserLogin');
        $this->nameUsuario  = $this->session->userdata('userNameUsuario');

        $this->campos = array(
            'cod_sede_operativa' => 'local_sede',
            'cod_sede_prov' => 'local_sede_provincial',
            'cod_sede_dist' => 'local_sede_distrital',
            'ccdd' => 'local_departamento',
            'ccpp' => 'local_provincia',
            'ccdi' => 'local_distrito',
            'direccion' => 'local_direccion',
            'referencia' => 'local_referencia',
            'observacion' => 'local_observacion',
            'nombres' => 'local_nombre_funcionario',
            'cargo' => 'local_cargo_responsable',
            'telef_fijo' => 'local_telefono_fijo',
            'telef_celular' => 'local_telefono_celular',
            'telef_rpm' => 'local_telefono_rpm',
            'email' => 'local_email',
            'area' => 'local_area',
            'nro_ambiente' => 'local_ambientes',
            'area_almacen' => 'local_area_almacen',
            'nro_escritorio' => 'local_escritorios',
            'nro_mesa' => 'local_mesas',
            'nro_silla' => 'local_sillas',
            'pc' => 'local_pc',
            'tipo' => 'local_tipo_oficina',
            'internet' => 'local_internet',
            'internet_tipo' => 'local_internet_tipo',
            'internet_velocidad' => 'local_internet_velocidad',
            'electricidad' => 'local_electricidad',
            'sshh' => 'local_sshh',
            'tipo_construc' => 'local_tipo',
            'seguridad' => 'local_seguridad',
            'turnos' => 'local_turnos',
            'turnos_presupuesto' => 'local_turnos_presupuesto',
            'costos' => 'local_costo_local',
            'costos_mantenimiento' => 'local_costo_detalle_mantenimiento',
            'costos_internet' => 'local_costo_detalle_internet',
            'costos_local' => 'local_costo_detalle_local',
            'costos_mobiliario' => 'local_costo_detalle_mobiliario',
            'observacion_local' => 'local_observacion_presupuesto',
            'tipo_construc' => 'local_tipo',
            'gps_latitud' => 'latitud_georeferencia',
            'gps_longitud' => 'longitud_georeferencia',
        );
    }

    public function todos_los_locales($sede_operativa = null, $tipo = false) {
        $sql  = $this->selects();
        $sql .= $this->joins();
        $sql .= "WHERE loc.estado = 1 ";

        if ($tipo) {
            $sql .= "AND loc.tipo = '{$tipo}' ";
        }

        if ($sede_operativa) {
            $sql .= " AND loc.cod_sede_operativa = '{$sede_operativa}' ";
        }

        $sql .= $this->groupBy();

        $query = $this->db->query($sql);

        if($query && $query->num_rows() > 0 ) {

            return $this->convert_utf8->convert_result($query);
        }

        return array();
    }

    public function obtener_local($idLocal) {

        $sql     = $this->selects();
        $sql    .= $this->joins();
        $sql    .= " WHERE loc.id_oficina = '{$idLocal}' AND loc.tipo = 'OF' ";
        $sql    .= $this->groupBy();

        $query  = $this->db->query($sql);

        if( $query->num_rows() == 1 ) {

            return $this->convert_utf8->convert_row($query);
        }

        return array();
    }

    public function obtener_local_tipo($idLocal, $tipo = false) {

        $sql     = $this->selects();
        $sql    .= $this->joins();
        $sql    .= " WHERE loc.id_oficina = '{$idLocal}' ";
        if ($tipo) {
            $sql .= " AND loc.tipo = '{$tipo}' ";
        }
        $sql    .= $this->groupBy();

        $query  = $this->db->query($sql);

        if( $query->num_rows() == 1 ) {

            return $this->convert_utf8->convert_row($query);
        }

        return array();
    }

    public function insertar_local($params) {

        $params = limpia_datos($params);

        $sql  = "INSERT INTO oficina ";

        $sql .= $this->ordenar_parametros('insertar', $params);

        $result = $this->db->query($sql);
        $id     = $this->db->insert_id();

        $this->guardar_relaciones($id, $params);

        return $result;
    }

    public function editar_local($params, $idLocal) {
        $params = limpia_datos($params);

        $sql  = "UPDATE loc SET ";

        $sql .= $this->ordenar_parametros('editar', $params);

        $sql .= "FROM oficina loc ";
        $sql .= "WHERE loc.id_oficina = {$idLocal}";

        $result = $this->db->query($sql);

        $this->guardar_relaciones($idLocal, $params);

        return $result;
    }

    public function eliminar_local($idLocal)
    {
        $data   = array('estado' => 0);

        $this->db->where('id_oficina', $idLocal);
        return $this->db->update('oficina', $data);
    }

    public function guardar_relaciones($idLocal, $params)
    {
        if (isset($params['local_seguridad_listado']) && $params['local_seguridad_listado'] !== '' ) {
            $this->db->where_in('id_seguridad', explode(',', $params['local_seguridad_listado']));
            $this->db->update('seguridad', array('id_oficina'=>$idLocal));
        }

        if (isset($params['local_pc_listado']) && $params['local_pc_listado'] !== '' ) {
            $this->db->where_in('id_pc', explode(',', $params['local_pc_listado']));
            $this->db->update('oficina_pc', array('id_oficina'=>$idLocal));
        }
    }

    /**
     * Retorna la clase que genera los graficos e indicadores
     */
    public function graph()
    {
        if (!$this->graficos) {
            $this->graficos = new Model_Graficos();
        }
        return $this->graficos;
    }
    
    public function actualizar_gps($id, array $coordenadas)
    {
        if (empty($coordenadas)) {
            return false;
        }

        $gps = array('gps_latitud'=>$coordenadas['lat'], 'gps_longitud'=>$coordenadas['lng']);

        $sql = "UPDATE oficina SET gps_latitud = ".$gps['gps_latitud'].", gps_longitud = ".$gps['gps_longitud']." WHERE id_oficina = ".$id;
        $status = $this->db->query($sql);

        // log_message('INFO', "ID: {$id} | Estado: {$status} | SQL: {$sql} | ".json_encode($gps));
        return $status;
    }

    protected function selects()
    {
        return "SELECT
                so.sede_operativa  AS sede_operativa,
                sp.sede_prov  AS sede_prov,
                sd.sede_dist  AS sede_dist,
                dep.nombre AS departamento,
                pro.nombre AS provincia,
                dis.nombre AS distrito,
                loc.id_oficina,
                loc.cod_sede_operativa,
                loc.cod_sede_prov,
                loc.cod_sede_dist,
                loc.ccdd,
                loc.ccpp,
                loc.ccdi,
                loc.tipo,
                loc.direccion,
                loc.referencia,
                loc.observacion,
                loc.nombres,
                loc.cargo,
                loc.telef_fijo,
                loc.telef_celular,
                loc.telef_rpm,
                loc.email,
                loc.area,
                loc.nro_ambiente,
                loc.area_almacen,
                loc.nro_escritorio,
                loc.nro_mesa,
                loc.nro_silla,
                loc.pc,
                loc.internet,
                loc.internet_tipo,
                loc.internet_velocidad,
                loc.electricidad,
                loc.sshh,
                loc.tipo_construc,
                loc.seguridad,
                loc.turnos,
                loc.turnos_presupuesto,
                loc.costos,
                loc.costos_mantenimiento,
                loc.costos_mobiliario,
                loc.costos_internet,
                loc.costos_local,
                loc.gps_latitud,
                loc.gps_longitud,
                loc.observacion_local ";
    }

    protected function groupBy()
    {
        return "GROUP BY
            so.sede_operativa
            ,sp.sede_prov
            ,sd.sede_dist
            ,dep.nombre
            ,pro.nombre
            ,dis.nombre
            ,loc.id_oficina
            ,loc.cod_sede_operativa
            ,loc.cod_sede_prov
            ,loc.cod_sede_dist
            ,loc.ccdd
            ,loc.ccpp
            ,loc.ccdi
            ,loc.tipo
            ,loc.direccion
            ,loc.referencia
            ,loc.observacion
            ,loc.nombres
            ,loc.cargo
            ,loc.telef_fijo
            ,loc.telef_celular
            ,loc.telef_rpm
            ,loc.email
            ,loc.area
            ,loc.nro_ambiente
            ,loc.area_almacen
            ,loc.nro_escritorio
            ,loc.nro_mesa
            ,loc.nro_silla
            ,loc.pc
            ,loc.internet
            ,loc.internet_tipo
            ,loc.internet_velocidad
            ,loc.electricidad
            ,loc.sshh
            ,loc.tipo_construc
            ,loc.seguridad
            ,loc.turnos
            ,loc.turnos_presupuesto
            ,loc.costos
            ,loc.costos_mantenimiento
            ,loc.costos_mobiliario
            ,loc.costos_internet
            ,loc.costos_local
            ,loc.gps_latitud
            ,loc.gps_longitud
            ,loc.observacion_local
            ,loc.estado
            ,loc.lastIP
            ,loc.navegador
            ,loc.usuarioCrea
            ,loc.fechaCrea
            ,loc.usuarioModifica
            ,loc.fechaModifica ";
    }

    protected function joins()
    {
        return "FROM
                oficina AS loc
                LEFT JOIN dist AS dis
                    ON dis.ccdi = loc.ccdi
                    AND dis.ccpp = loc.ccpp
                    AND dis.ccdd = loc.ccdd
                LEFT JOIN prov AS pro
                    ON pro.ccpp = loc.ccpp
                    AND pro.ccdd = loc.ccdd
                    AND pro.cod_sede_operativa = loc.cod_sede_operativa
                    -- AND pro.cod_sede_prov = dis.cod_sede_prov
                    -- AND pro.cod_sede_dist = dis.cod_sede_dist
                LEFT JOIN dpto AS dep
                    ON dep.ccdd = loc.ccdd
                    AND dep.cod_sede_operativa = loc.cod_sede_operativa
                    -- AND dep.cod_sede_prov = dis.cod_sede_prov
                    -- AND dep.cod_sede_dist = dis.cod_sede_dist
                LEFT JOIN sede_operativa AS so
                    ON so.cod_sede_operativa = loc.cod_sede_operativa
                LEFT JOIN sede_prov AS sp
                    ON sp.cod_sede_operativa = loc.cod_sede_operativa
                    AND sp.cod_sede_prov = loc.cod_sede_prov
                LEFT JOIN sede_dist AS sd
                    ON sd.cod_sede_operativa = loc.cod_sede_operativa
                    AND sd.cod_sede_prov = loc.cod_sede_prov
                    AND sd.cod_sede_dist = loc.cod_sede_dist
                 ";
    }

    protected function ordenar_parametros($tipo, $params) {
        if ($tipo === 'insertar') {
            $parametros = array();
            // Armamos las columnas de la tabla segun el array campos
            $campos = implode(', ', array_keys($this->campos));
            $sql = "({$campos}, estado) ";

            // Recorremos todos los parametros del formulario
            foreach ($this->campos as $key => $value) {
                $parametros[] = isset($params[$value]) ? $params[$value] : null;
            }

            $valores = implode("','", array_values($parametros));
            $valores = str_replace("''", "NULL", $valores);

            return $sql .= "VALUES ('{$valores}', 1) ";
        }
        else if ($tipo === 'editar') {
            $sql = '';

            foreach ($this->campos as $key => $value) {
                $valor = (isset($params[$value]) && $params[$value] !== '') ? "'{$params[$value]}'" : "NULL" ;

                $sql .= "loc.{$key} = {$valor}, ";
            }
            return $sql .= "loc.estado = 1 ";
        }

        return false;
    }
}
