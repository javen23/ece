<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locales_Preseleccion_model extends Locales_model {

    public function __construct() {
        parent::__construct();
    }

    public function todos_los_locales($sede_operativa = null) {
        $sql  = $this->selects();
        $sql .= $this->joins();
        $sql .= "WHERE loc.estado = 1 ";
        $sql .= "AND loc.hora_inicio IS NOT NULL AND loc.hora_fin IS NOT NULL ";

        if ($sede_operativa) {
            $sql .= "AND loc.cod_sede_operativa = '{$sede_operativa}' ";
        }

        $sql .= $this->groupBy();

        $query = $this->db->query($sql);

        log_message('INFO', "Local Preseleccion SQL :{$sql}");

        return $this->convert_utf8->convert_result($query);
    }

    public function obtener_local($idLocal) {

        $sql     = $this->selects();
        $sql    .= $this->joins();
        $sql    .= " WHERE loc.id_local = '{$idLocal}' ";
        $sql .= $this->groupBy();

        $query  = $this->db->query($sql);

        if( $query->num_rows() == 1 ) {

            return $this->convert_utf8->convert_row($query);
        }

        return NULL;
    }

    protected function selects()
    {
        return "SELECT
                dep.nombre AS departamento,
                pro.nombre AS provincia,
                dis.nombre AS distrito,
                loc.cod_sede_operativa,
                loc.id_local,
                loc.cod_sede_prov,
                loc.cod_sede_dist,
                loc.ccdd,
                loc.ccpp,
                loc.ccdi,
                loc.nombre_local,
                loc.direccion,
                loc.referencia,
                loc.electricidad,
                loc.sshh,
                loc.pea_programa,
                loc.aulas_program,
                loc.observacion,
                loc.tipo,
                loc.estado,
                so.sede_operativa  AS sede_operativa,
                CAST(loc.hora_inicio AS TIME(0)) AS hora_inicio,
                CAST(loc.hora_fin AS TIME(0)) AS hora_fin ";
    }

    public function insertar_local($params) {

        $params = limpia_datos($params);

        $params['local_luz']    = (int)$params['local_luz'];
        $params['local_sshh']   = (int)$params['local_sshh'];

        //'{$params['local_sede_distrital']}',
        $sql  = "INSERT INTO local ";
        $sql .= "( cod_sede_operativa, ccdd, ccpp, ccdi, nombre_local, direccion, referencia, electricidad, sshh, pea_programa, aulas_program, hora_inicio, hora_fin, observacion, estado) ";
        $sql .= "VALUES (
                    '{$params['local_sede']}',
                    '{$params['local_departamento']}',
                    '{$params['local_provincia']}',
                    '{$params['local_distrito']}',
                    '{$params['local_nombre']}',
                    '{$params['local_direccion']}',
                    '{$params['local_referencia']}',
                    {$params['local_luz']},
                    {$params['local_sshh']},
                    '{$params['local_pea']}',
                    '{$params['local_aulas']}',
                    '{$params['local_hora_inicio']}',
                    '{$params['local_hora_fin']}',
                    '{$params['local_observacion']}',
                    1
                )";

        return $this->db->query($sql);
    }

    public function editar_local($params, $idLocal) {
        $params = limpia_datos($params);

        $params['local_luz']    = (int)$params['local_luz'];
        $params['local_sshh']   = (int)$params['local_sshh'];

        // loc.cod_sede_dist = '{$params['local_sede_distrital']}',
        $sql  = "UPDATE loc SET ";
        $sql .= "
                    loc.cod_sede_operativa = '{$params['local_sede']}',
                    loc.ccdd = '{$params['local_departamento']}',
                    loc.ccpp = '{$params['local_provincia']}',
                    loc.ccdi = '{$params['local_distrito']}',
                    loc.nombre_local = '{$params['local_nombre']}',
                    loc.direccion = '{$params['local_direccion']}',
                    loc.referencia = '{$params['local_referencia']}',
                    loc.electricidad = '{$params['local_luz']}',
                    loc.sshh = '{$params['local_sshh']}',
                    loc.pea_programa = '{$params['local_pea']}',
                    loc.aulas_program = '{$params['local_aulas']}',
                    loc.hora_inicio = '{$params['local_hora_inicio']}',
                    loc.hora_fin = '{$params['local_hora_fin']}',
                    loc.observacion = '{$params['local_observacion']}'
                ";
        $sql .= "FROM local loc ";
        $sql .= "WHERE loc.id_local = {$idLocal}";

        return $this->db->query($sql);
    }
}
