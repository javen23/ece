<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locales_Odei_model extends Locales_Administrativo_model {

    public function __construct() {
        parent::__construct();
    }

    public function obtener_local($idLocal) {

        $sql     = $this->selects();
        $sql    .= $this->joins();
        $sql    .= " WHERE loc.id_oficina = '{$idLocal}' AND loc.tipo = 'LZ' ";
        $sql    .= $this->groupBy();

        $query  = $this->db->query($sql);

        if( $query->num_rows() == 1 ) {

            return $this->convert_utf8->convert_row($query);
        }

        return array();
    }
}
