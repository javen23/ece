<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locales_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->idLogin      = $this->session->userdata('idUserLogin');
        $this->nameUsuario  = $this->session->userdata('userNameUsuario');
    }

    public function eliminar_local($idLocal) {

        $data   = array('estado' => 0);

        $this->db->where('id_local', $idLocal);
        return $this->db->update('local', $data);

    }

    protected function selects()
    {
        return "SELECT
                dep.nombre AS departamento,
                pro.nombre AS provincia,
                dis.nombre AS distrito,
                loc.cod_sede_operativa,
                loc.id_local,
                loc.cod_sede_prov,
                loc.cod_sede_dist,
                loc.ccdd,
                loc.ccpp,
                loc.ccdi,
                loc.nombre_local,
                loc.direccion,
                loc.referencia,
                loc.electricidad,
                loc.sshh,
                loc.pea_programa,
                loc.aulas_program,
                loc.observacion,
                loc.tipo,
                loc.estado,
                loc.hora_inicio,
                loc.hora_fin,
                so.sede_operativa  AS sede_operativa ";
    }

    protected function groupBy()
    {
        return "GROUP BY loc.cod_sede_operativa
          ,dep.nombre
          ,pro.nombre
          ,dis.nombre
          ,loc.id_local
          ,loc.cod_sede_prov
          ,loc.cod_sede_dist
          ,loc.ccdd
          ,loc.ccpp
          ,loc.ccdi
          ,loc.nombre_local
          ,loc.direccion
          ,loc.referencia
          ,loc.electricidad
          ,loc.sshh
          ,loc.pea_programa
          ,loc.aulas_program
          ,loc.observacion
          ,loc.tipo
          ,loc.estado
          ,loc.hora_inicio
          ,loc.hora_fin
          ,so.sede_operativa ";
    }

    protected function joins()
    {
        // CASE WHEN dis.cod_sede_dist IS NOT NULL
        //     THEN (LEFT JOIN dist AS dis ON dis.cod_sede_dist = loc.cod_sede_dist)
        //     ELSE null
        // END
        return "FROM
                local AS loc
                LEFT JOIN dist AS dis
                    ON dis.ccdi = loc.ccdi
                    AND dis.ccpp = loc.ccpp
                    AND dis.ccdd = loc.ccdd
                    AND dis.cod_sede_operativa = loc.cod_sede_operativa
                    -- AND dis.cod_sede_dist = loc.cod_sede_dist
                LEFT JOIN prov AS pro
                    ON pro.ccpp = loc.ccpp
                    AND pro.ccdd = loc.ccdd
                    AND pro.cod_sede_operativa = loc.cod_sede_operativa
                    -- AND pro.cod_sede_prov = dis.cod_sede_prov
                    -- AND pro.cod_sede_dist = dis.cod_sede_dist
                LEFT JOIN dpto AS dep
                    ON dep.ccdd = loc.ccdd
                    AND dep.cod_sede_operativa = loc.cod_sede_operativa
                    -- AND dep.cod_sede_prov = dis.cod_sede_prov
                    -- AND dep.cod_sede_dist = dis.cod_sede_dist
                LEFT JOIN sede_operativa AS so
                    ON so.cod_sede_operativa = loc.cod_sede_operativa
                ";
    }
}
