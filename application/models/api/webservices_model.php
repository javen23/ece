<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Webservices_model extends CI_Model {

   public function __construct() {
      parent::__construct();
   }

   public function logeo_acceso($pass,$user) {
      $query = $this->db->query("SELECT usu.idUsu,usu.idRol,usu.usuario,usu.estado,rol.rol,rol.descripcion,uloc.id_local
                              FROM
                                 usuario AS usu
                                 LEFT JOIN rol ON usu.idRol=rol.idRol
                                 LEFT JOIN usuario_local AS uloc ON usu.idUsu=uloc.idUsu
                              WHERE
                                 usu.usuario='".$user."'
                                 AND usu.clave='".$pass."'");
      //log_message('INFO', "<<<<<<LOGEO ".$query);
      if($query->num_rows()==1){
         return $query->row_array();
      }
      return NULL;
   }

   public function get_version(){
      $query = $this->db->query('SELECT
                  TOP 1
                  nro_version AS numero_de_version,
                  usuarioCrea,
                  CONVERT(VARCHAR,fechaCrea,120) AS fechaCrea
               FROM
                  version ORDER BY numero_de_version DESC');

      //log_message('INFO', "<<<<<<VERSION {$query}");
      if($query->num_rows()==1){
         return $query->row_array();
      }
      return NULL;
   }

   public function save_postulante($postulante) {
      $array_postulante = array();
      $array_postula = array();

      if (isset($postulante['asistencias'])) {
         foreach ($postulante['asistencias'] as $doc) {
             $data_insertar=array(
               'postulante_id'=>$doc['postulante_id'],
               'version_turno_id'=>$doc['version_turno_id'],
               'marcacion_id'=>$doc['marcacion_id'],
               'asistencia'=>2,
               'fecha'=>$doc['fecha'],
            );
            $array_postula = $data_insertar;

            $sql_asitencia="SELECT
                              *
                           FROM
                              postulante_asistencia
                           WHERE
                              postulante_id='".$doc['postulante_id']."'
                              AND marcacion_id='".$doc['marcacion_id']."'
                              AND version_turno_id='".$doc['version_turno_id']."'
                              AND CAST(fecha as date) ='".$doc['fecha']."'";
            //log_message('INFO', "<<<<<<ASISTENCIA  {$sql_asitencia}");
            $query_asistencia=$this->db->query($sql_asitencia);

            if($query_asistencia->num_rows()==0) {
               $pos_ingresado =$this->db->insert('postulante_asistencia',$array_postula);
               if ($pos_ingresado) {
                  array_push($array_postulante,$array_postula);
               }
            }else{
               array_push($array_postulante,$array_postula);
            }
         }

         $padron['asistencias'] = $array_postulante;
         $datosTotales = array();
         array_push($datosTotales, $padron);
         return $datosTotales;
      }
      return NULL;
   }

   public function obtener_padron($id_local) {

      $sql_local="SELECT * FROM local AS loc WHERE loc.id_local='".$id_local."'";
      $query_local = $this->db->query($sql_local);

      if ($query_local->num_rows() > 0) {

         //--LOCAL
         $array_local = array();
         $array_local = $this->convert_utf8->convert_resultado($query_local);

         //--VERSION
         $array_version = array();
         $sql_version = "SELECT
                           TOP 1
                           nro_version AS numero_de_version,
                           usuarioCrea,
                           CONVERT(VARCHAR,fechaCrea,120) AS fechaCrea
                        FROM
                           version ORDER BY numero_de_version DESC";
         //log_message('INFO', "<<<<<<OBTENER VERSION  {$sql_version}");
         $query_version = $this->db->query($sql_version);
         $array_version = $query_version->row_array();

         //--POSTULANTES
         $array_postulante=array();

         $sql_postulante   = "SELECT * FROM postulante WHERE local_id='".$id_local."' AND nro_version='".$array_version['numero_de_version']."'";
         //log_message('INFO', "<<<<<<OBTENER POSTULANTE {$sql_postulante}");
         $query_postulante = $this->db->query($sql_postulante);
         $array_postulante = $this->convert_utf8->convert_resultado($query_postulante);

         //--ROL
         $array_rol = array();
         $sql_rol = "SELECT * FROM rol";
         //log_message('INFO', "<<<<<<OBTENER ROL {$sql_rol}");
         $query_rol = $this->db->query($sql_rol);
         $array_rol = $this->convert_utf8->convert_resultado($query_rol);

         //--CARGO
         $array_cargo=array();
         $sql_cargo="SELECT * FROM cargo";
         $query_cargo=$this->db->query($sql_cargo);
         $array_cargo = $this->convert_utf8->convert_resultado($query_cargo);

         //--SEDE OPERATIVA
         $array_sede=array();
         $sql_sede="SELECT * FROM sede_operativa";
         $query_sede=$this->db->query($sql_sede);
         $array_sede = $this->convert_utf8->convert_resultado($query_sede);

         //--TIPO CAPACITACION
         $array_capacitacion=array();
         $sql_capacitacion="SELECT * FROM tipo_capacitacion";
         $query_capacitacion=$this->db->query($sql_capacitacion);
         $array_capacitacion = $this->convert_utf8->convert_resultado($query_capacitacion);

         //--MARCACION
         $array_marcacion=array();
         $sql_marcacion="SELECT * FROM marcacion";
         $query_marcacion=$this->db->query($sql_marcacion);
         $array_marcacion = $this->convert_utf8->convert_resultado($query_marcacion);

         //--VERSION TURNO
         $array_turno=array();
         $sql_turno="SELECT * FROM version_turno";
         $query_turno=$this->db->query($sql_turno);
         $array_turno = $this->convert_utf8->convert_resultado($query_turno);

         //--HORARIO
         $array_horario=array();
         $sql_horario="SELECT id,version_turno_id,tipo_capacitacion_id,marcacion_id,fecha,CONVERT(VARCHAR,hora_inicio,120) AS hora_inicio,CONVERT(VARCHAR,hora_fin,120) AS hora_fin FROM horario";
         $query_horario=$this->db->query($sql_horario);
         $array_horario = $this->convert_utf8->convert_resultado($query_horario);

         $padron['locales'] = $array_local;
         $padron['postulantes'] = $array_postulante;
         $padron['roles'] = $array_rol;
         $padron['version'] = $array_version;
         $padron['cargos'] = $array_cargo;
         $padron['sedes_operativas'] = $array_sede;
         $padron['tipos_capacitacion'] = $array_capacitacion;
         $padron['marcaciones'] = $array_marcacion;
         $padron['horarios'] = $array_horario;
         $padron['versiones_turnos'] = $array_turno;


         $datosTotales = array();
         array_push($datosTotales, $padron);
         return $datosTotales[0];

      }
      return NULL;

   }

}
