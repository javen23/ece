<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Supervision_model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }   

    public function capitulo1($capitulo)
    {        
        if(!(isset($capitulo['Capitulo1'])) or !(isset($capitulo['Capitulo1']['id_sede'],$capitulo['Capitulo1']['usuarioCrea'])))
        {
            $salida['estado'] = 0;
            $salida['mensaje'] = "formato incorrecto.";
            return $salida;
        }

        $capitulo = $capitulo['Capitulo1'];

        $query = $this->db->query("select count(*) from CAP1 
            where id_sede = ".$capitulo['id_sede']." and usuarioCrea = '".$capitulo['usuarioCrea']."'; ");
        if($query->num_rows()>0)
            $query = $this->db->query("delete from CAP1 where id_sede = ".$capitulo['id_sede']." and usuarioCrea = '".$capitulo['usuarioCrea']."'; ");

        $salida = array();

        $capitulo['navegador'] = $this->agent();
        $capitulo['lastIP']  = $this->input->ip_address();

        $query = $this->db->insert('CAP1', $capitulo);
        if($query == false)
            $estado = 0;
        else
            $estado = 1;       

        $salida['id_sede'] = $capitulo['id_sede'];
        $salida['usuarioCrea'] = $capitulo['usuarioCrea'];
        $salida['fechaCrea'] = $capitulo['fechaCrea'];
        $salida['estado'] =  $estado;

        return $salida;
    }

    public function capitulo2a($capitulo)
    {        
        if(!(isset($capitulo['Capitulo2A'])) or !(isset($capitulo['Capitulo2A']['id_sede_operativa'],$capitulo['Capitulo2A']['usuarioCrea'])))
        {
            $salida['estado'] = 0;
            $salida['mensaje'] = "formato incorrecto.";
            return $salida;
        }

        $capitulo = $capitulo['Capitulo2A'];

        $query = $this->db->query("select count(*) from CAP2_A 
            where id_sede_operativa = ".$capitulo['id_sede_operativa']." and usuarioCrea = '".$capitulo['usuarioCrea']."'; ");
        if($query->num_rows()>0)
            $query = $this->db->query("delete from CAP2_A where id_sede_operativa = ".$capitulo['id_sede_operativa']." and usuarioCrea = '".$capitulo['usuarioCrea']."'; ");

        $salida = array();

        $capitulo['navegador'] = $this->agent();
        $capitulo['lastIP']  = $this->input->ip_address();
        
        $query = $this->db->insert('CAP2_A', $capitulo);
        if($query == false)
            $estado = 0;
        else
            $estado = 1;

        $salida['id_sede'] = $capitulo['id_sede_operativa'];
        $salida['usuarioCrea'] = $capitulo['usuarioCrea'];
        $salida['fechaCrea'] = $capitulo['fechaCrea'];
        $salida['estado'] =  $estado;

        return $salida;
    }

    public function capitulo2b($capitulo)
    {
        if(!(isset($capitulo['Capitulo2B'])) or !(isset($capitulo['Capitulo2B']['id_sede'],$capitulo['Capitulo2B']['usuarioCrea'])))
        {
            $salida['estado'] = 0;
            $salida['mensaje'] = "formato incorrecto.";
            return $salida;
        }

        $capitulo = $capitulo['Capitulo2B'];

        $query = $this->db->query("select count(*) from CAP2_B 
            where id_sede = ".$capitulo['id_sede']." and usuarioCrea = '".$capitulo['usuarioCrea']."'; ");
        if($query->num_rows()>0)
            $query = $this->db->query("delete from CAP2_B where id_sede = ".$capitulo['id_sede']." and usuarioCrea = '".$capitulo['usuarioCrea']."'; ");

        $salida = array();
       
        $capitulo['navegador'] = $this->agent();
        $capitulo['lastIP']  = $this->input->ip_address();

        $query = $this->db->insert('CAP2_B', $capitulo);
        if($query == false)
            $estado = 0;
        else
            $estado = 1;

        $salida['id_sede'] = $capitulo['id_sede'];
        $salida['usuarioCrea'] = $capitulo['usuarioCrea'];
        $salida['fechaCrea'] = $capitulo['fechaCrea'];
        $salida['estado'] =  $estado;

        return $salida;
    }

    private function agent()
    {
        $this->load->library('user_agent');
        $agent = "";

        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }

        return $agent;
    }

}

/* End of file supervision_model.php */
/* Location: ./application/models/api/supervision_model.php */
