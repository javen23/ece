<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Director_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function listado() {
        $sql = "EXEC PA_DETALLE_CAPACITACION ";
        $query = $this->db->query($sql);
        if( $query->num_rows() > 0 ) {
            return $this->convert_utf8->convert_result($query);
        }
        return array();
    }

    public function mostrar_capacitacion(){
        $sql = "EXEC PA_REPORTE_CAPACITACION ";
        $query = $this->db->query($sql);
        if( $query->num_rows() == 1 ) {
          return $this->convert_utf8->convert_row($query);
        }
        return array();
    }

    protected function selects(){
        return "SELECT
                    loc.nombre_local AS local,
                    c.cargo AS cargo,
                    p.* ";
    }



    protected function joins(){
        return " FROM
                postulante AS p
                LEFT JOIN cargo AS c
                  ON c.id_cargo = c.id_cargo
                LEFT JOIN local AS loc
                  ON loc.id_local = p.id_local
                ";
    }
}
