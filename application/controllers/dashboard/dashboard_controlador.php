<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_controlador extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('portada/portada_model', 'modeloPortada');
        $this->isRol = $this->session->userdata('userIdRol');
    }

    public function index() {
        $datos['titulo'] = "DASHBOARD";
        $datos['contenido'] = 'dashboard/listado_dashboard_vista';
        $this->load->view('plantilla', $datos);
    }

    public function cambiar_password() {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post('valorPassword') == 'valorCambiarPassword') {
                $this->ip_navegador();
                $_REQUEST['pass_cambiar'] = $this->security->xss_clean(strip_tags(notildes($this->input->post('pass_cambiar'))));
                // print_r($_REQUEST);
                // exit;
                $status = $this->modeloPortada->actualizar_password($_REQUEST);
                $this->verificar_estado($status,'resul');
            } else {
                if ($this->input->is_ajax_request()) {
                    $this->load->view('usuario/cambiar_password_usuario_vista');
                }
            }
        }
    }

    private function ip_navegador(){
        $_REQUEST['lastIP'] = $this->input->ip_address();
        $_REQUEST['navegador'] = $this->input->user_agent();
   }

   private function verificar_estado($estado,$accion=null){

        if(is_null($accion)) :
            $this->output
                ->set_content_type('application/json;charset=utf-8')
                ->set_status_header('200')
                ->set_output(json_encode($estado));
        else:
            $this->output
                ->set_content_type('application/json;charset=utf-8')
                ->set_status_header('200')
                ->set_output(json_encode(array($accion => $estado)));
        endif;

   }
}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */