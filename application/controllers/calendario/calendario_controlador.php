<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calendario_controlador extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->isRol = $this->session->userdata('userIdRol');
    }

    public function index() {
        $datos['titulo'] = "Calendario de eventos";
        $datos['contenido'] = 'calendario/index_calendario_vista';
        $this->load->view('plantilla', $datos);
    }

}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */