<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Exportar_segmentacion_controlador extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('segmentacion/segmentacion_model', 'modeloSegmentacion');
        $this->load->library('PHPExcel');
        ini_set("memory_limit", -1);
        ini_set('max_execution_time', -1);
    }

    public function index(){
        echo "";
    }

    public function sede_reporte_exportar() {
        $phpExcel = new PHPExcel();
        $prestasi = $phpExcel->setActiveSheetIndex(0);
        //Estilo para el border
        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '7f8c8d')
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el titulo
        $titulo = array(
            'font' => array(
                'bold' => true,
                'size' => 28
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el subtitulo
        $subtitulo = array(
            'font' => array(
                'bold' => true,
                'size' => 24
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el fondo de la celda
        $backFondo = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap' => true
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => 'DDDDDD')
                ),
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '3498db')
            ),
        );
        //fusionando titulo
        $tabTitulo = 1;
        $phpExcel->getActiveSheet()->mergeCells('A' . $tabTitulo . ':K' . $tabTitulo);
        $phpExcel->getActiveSheet()->getStyle('A' . $tabTitulo . ':K' . $tabTitulo)->applyFromArray($titulo);
        //fusionanado subtitulo
        $tabSubtitulo = 2;
        $phpExcel->getActiveSheet()->mergeCells('A' . $tabSubtitulo . ':K' . $tabSubtitulo);
        $phpExcel->getActiveSheet()->getStyle('A' . $tabSubtitulo . ':K' . $tabSubtitulo)->applyFromArray($subtitulo);

        //fusionando Tab2
        $tab2 = 4;
        $phpExcel->getActiveSheet()->getStyle('A' . $tab2 . ':K' . $tab2)->applyFromArray($backFondo);
        //altura en la fila
        $phpExcel->getActiveSheet()->getRowDimension($tabTitulo)->setRowHeight(46);
        $phpExcel->getActiveSheet()->getRowDimension($tabSubtitulo)->setRowHeight(33);
        $phpExcel->getActiveSheet()->getRowDimension($tab2)->setRowHeight(38);
        //Titulo de nombres pie excel
        $prestasi->setCellValue('A' . $tabTitulo, 'REPORTE DE PRESUPUESTO POR SEDE OPERATIVA');
        $prestasi->setCellValue('A' . $tabSubtitulo, 'REPORTE DETALLADO');
        //border
        $phpExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

        $numerNombre = 4;
        $prestasi->setCellValue('A' . $numerNombre, 'N°');
        $prestasi->setCellValue('B' . $numerNombre, 'COD. SEDE OPERATIVA');
        $prestasi->setCellValue('C' . $numerNombre, 'SEDE OPERATIVA');
        $prestasi->setCellValue('D' . $numerNombre, 'TOTAL INSTITUCIONES');
        $prestasi->setCellValue('E' . $numerNombre, 'TOTAL SECCIONES');
        $prestasi->setCellValue('F' . $numerNombre, 'TOTAL APLICADORES');
        $prestasi->setCellValue('G' . $numerNombre, 'TOTAL GASTOS OPERATIVOS');
        $prestasi->setCellValue('H' . $numerNombre, 'TOTAL PASAJE');
        $prestasi->setCellValue('I' . $numerNombre, 'TOTAL MOVILIDAD');
        $prestasi->setCellValue('J' . $numerNombre, 'TOTAL BONIFICACIÓN');
        $prestasi->setCellValue('K' . $numerNombre, 'TOTAL');

        $data = $this->modeloSegmentacion->total_resultado_sede_operativa($_REQUEST);

        $rowexcel = 4;
        $numero = 0;
        foreach ($data as $row) {
            $rowexcel++;
            $numero++;
            $prestasi->getStyle('A' . $rowexcel . ':K' . $rowexcel)->applyFromArray($borders);
            $prestasi->setCellValue('A' . $rowexcel, $numero)->getStyle('A' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('B' . $rowexcel, $row['codigo_sede'])->setCellValueExplicit('B' . $rowexcel, $row['codigo_sede'], PHPExcel_Cell_DataType::TYPE_STRING);;
            $prestasi->setCellValue('C' . $rowexcel, $row['nombre_sede'])->getStyle('C' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('D' . $rowexcel, $row['total_instituciones']);
            $prestasi->setCellValue('E' . $rowexcel, $row['total_secciones']);
            $prestasi->setCellValue('F' . $rowexcel, $row['total_aplicadores']);
            $prestasi->setCellValue('G' . $rowexcel, $row['total_gastos_operativos'])->setCellValueExplicit('G' . $rowexcel, $row['total_gastos_operativos'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('H' . $rowexcel, $row['total_pasaje'])->setCellValueExplicit('H' . $rowexcel, $row['total_pasaje'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('I' . $rowexcel, $row['total_movilidad'])->setCellValueExplicit('I' . $rowexcel, $row['total_movilidad'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('J' . $rowexcel, $row['total_bonificacion'])->setCellValueExplicit('J' . $rowexcel, $row['total_bonificacion'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('K' . $rowexcel, $row['total'])->setCellValueExplicit('K' . $rowexcel, $row['total'], PHPExcel_Cell_DataType::TYPE_STRING);
        }

        $prestasi->setTitle('Reporte de presupuesto sede');
        header("Content-Type: application/vnd.ms-excel");
        $nameFile = "reporte_de_presupuesto_sede_" . date("d-m-Y_his");
        header("Content-Disposition: attachment; filename=\"$nameFile.xls\"");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel5");
        $objWriter->save("php://output");

        echo '1';
        exit;

    }

    public function exportar_detallado() {
        $phpExcel = new PHPExcel();
        $prestasi = $phpExcel->setActiveSheetIndex(0);
        //Estilo para el border
        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '7f8c8d')
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el titulo
        $titulo = array(
            'font' => array(
                'bold' => true,
                'size' => 28
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el subtitulo
        $subtitulo = array(
            'font' => array(
                'bold' => true,
                'size' => 24
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el fondo de la celda
        $backFondo = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap' => true
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => 'DDDDDD')
                ),
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '3498db')
            ),
        );
        //fusionando titulo
        $tabTitulo = 1;
        $phpExcel->getActiveSheet()->mergeCells('A' . $tabTitulo . ':I' . $tabTitulo);
        $phpExcel->getActiveSheet()->getStyle('A' . $tabTitulo . ':I' . $tabTitulo)->applyFromArray($titulo);
        //fusionanado subtitulo
        $tabSubtitulo = 2;
        $phpExcel->getActiveSheet()->mergeCells('A' . $tabSubtitulo . ':I' . $tabSubtitulo);
        $phpExcel->getActiveSheet()->getStyle('A' . $tabSubtitulo . ':I' . $tabSubtitulo)->applyFromArray($subtitulo);

        //fusionando Tab2
        $tab2 = 4;
        $phpExcel->getActiveSheet()->getStyle('A' . $tab2 . ':I' . $tab2)->applyFromArray($backFondo);
        //altura en la fila
        $phpExcel->getActiveSheet()->getRowDimension($tabTitulo)->setRowHeight(46);
        $phpExcel->getActiveSheet()->getRowDimension($tabSubtitulo)->setRowHeight(33);
        $phpExcel->getActiveSheet()->getRowDimension($tab2)->setRowHeight(38);
        //Titulo de nombres pie excel
        $prestasi->setCellValue('A' . $tabTitulo, 'SISTEMA DE REGISTRO DE ASISTENCIA');
        $prestasi->setCellValue('A' . $tabSubtitulo, 'REPORTE DE ASISTENCIA DETALLADO');
        //border
        $phpExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

        $numerNombre = 4;
        $prestasi->setCellValue('A' . $numerNombre, 'N°');
        $prestasi->setCellValue('B' . $numerNombre, 'SEDE');
        $prestasi->setCellValue('C' . $numerNombre, 'LOCAL');
        $prestasi->setCellValue('D' . $numerNombre, 'CARGO');
        $prestasi->setCellValue('E' . $numerNombre, 'DNI');
        $prestasi->setCellValue('F' . $numerNombre, 'APELLIDOS');
        $prestasi->setCellValue('G' . $numerNombre, 'NOMBRES');
        $prestasi->setCellValue('H' . $numerNombre, 'AULA');
        $prestasi->setCellValue('I' . $numerNombre, 'ASISTENCIA');

        $data = $this->modeloSegmentacion->getDetalleRegistro();

        $rowexcel = 4;
        $numero = 0;
        foreach ($data as $row) {
            $rowexcel++;
            $numero++;
            $prestasi->getStyle('A' . $rowexcel . ':I' . $rowexcel)->applyFromArray($borders);
            $prestasi->setCellValue('A' . $rowexcel, $numero)->getStyle('A' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('B' . $rowexcel, $row['SEDE'])->getStyle('B' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('C' . $rowexcel, $row['LOCAL'])->getStyle('C' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('D' . $rowexcel, $row['CARGO'])->getStyle('D' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('E' . $rowexcel, $row['DNI'])->setCellValueExplicit('E' . $rowexcel, $row['DNI'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('F' . $rowexcel, $row['APELLIDOS'])->getStyle('F' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('G' . $rowexcel, $row['NOMBRES'])->getStyle('G' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('H' . $rowexcel, $row['AULA']);
            $prestasi->setCellValue('I' . $rowexcel, $row['ASISTENCIA']);
        }
        $prestasi->setTitle('Reporte de Asistencia');
        header("Content-Type: application/vnd.ms-excel");
        $nameFile = "reporte_de_cobertura_" . date("d-m-Y_his");
        header("Content-Disposition: attachment; filename=\"$nameFile.xls\"");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel5");
        $objWriter->save("php://output");
    }

}
