<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Segmentacion_controlador extends CI_Controller {

    public $mensaje;

    protected $region_totalizados;
    protected $provincia_totalizados;
    protected $distrito_totalizados;
    protected $asistente_totalizados;

    const BONO_APLICADOR    = 230;
    const BONO_APLICADOR_LIDER_MC_PRIMARIA      = 250;
    const BONO_APLICADOR_LIDER_MC_SECUNDARIA    = 330;
    const GASTO_OPERATIVO   = 50;
    const MOVILIDAD         = 15;

    public function __construct() {
        parent::__construct();

        $this->region_totalizados       = array('total_gastos_operativos', 'total_pasaje', 'total_movilidad', 'total_bonificacion', 'total');
        $this->provincia_totalizados    = array_merge($this->region_totalizados, array('total_instituciones','total_secciones','total_aplicadores'));
        $this->distrito_totalizados     = array_merge($this->region_totalizados, array('total_instituciones', 'total_secciones', 'total_aplicadores'));
        $this->asistente_totalizados    = array_merge($this->region_totalizados, array('total_instituciones', 'total_secciones', 'total_aplicadores'));

        $this->load->model('segmentacion/segmentacion_model', 'modelSegmentacion');
        $this->load->model('asistente/asistente_model', 'modelAsistente');
        $this->load->model('ajax/ajax_model', 'modeloAjax');
        $this->load->model('segmentacion/programacion_model', 'modelProgramacion');

        $this->isRol    = $this->session->userdata('userIdRol');
        $this->sedeOperativa    = $this->session->userdata('sedeOperativa');
        $this->mensaje  = array('tipo'=>'danger', 'cuerpo'=>'No se pudo guardar la información enviada');
        //$this->output->enable_profiler(true);
    }

    public function index() {
        $datos['titulo']    = "Segmentación";
        $datos['contenido'] = 'segmentacion/inicio_vista';

        $this->load->view('plantilla', $datos);
    }

    public function reportes(){
        $datos['titulo']    = "REPORTES";
        $datos['contenido'] = 'segmentacion/reporte/index_vista';

        $this->load->view('plantilla', $datos);
    }

    public function reporte_sede(){
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array('plugins/datatables/jquery.dataTables.min','plugins/datatables/dataTables.bootstrap.min'));

        $datos['total_sedes']       = $this->modeloAjax->todas_las_sedes();
        $datos['resultado_reporte'] = $this->modelSegmentacion->total_resultado_sede_operativa();
        $datos['total_reporte']     =  $this->sumatoria($datos['resultado_reporte'], $this->region_totalizados );
        $datos['titulo']            = "Segmentación reporte por sede";
        $datos['contenido']         = 'segmentacion/reporte/sede/sede_operativa_vista';

        $this->load->view('plantilla', $datos);
    }

    public function listado_reporte_sede() {
        $datos['resultado_reporte'] = $this->modelSegmentacion->total_resultado_sede_operativa($_REQUEST);
        $datos['total_reporte']     = $this->sumatoria($datos['resultado_reporte'],  $this->region_totalizados);

        $this->load->view('segmentacion/reporte/sede/sede_operativa_vista_ajax', $datos);
    }

    private function sumatoria($array, $campos)
    {
        $result = array();
        foreach ($campos as $campo) {
            $result[$campo] = array_suma_index($array, $campo);
        }
        return $result;
    }

    public function reporte_provincia(){

        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array('plugins/datatables/jquery.dataTables.min','plugins/datatables/dataTables.bootstrap.min'));

        $datos['total_sedes']           = $this->modeloAjax->todas_las_sedes();
        $datos['resultado_reporte']     = $this->modelSegmentacion->total_resultado_sede_operativa();
        $datos['total_reporte']         = $this->sumatoria($datos['resultado_reporte'],  $this->provincia_totalizados);
        $datos['titulo']                = "Segmentación reporte por provincia";
        $datos['contenido']             = 'segmentacion/reporte/provincia/provincia_operativa_vista';

        $this->load->view('plantilla', $datos);
    }

    public function listado_reporte_provincia() {
        $datos['resultado_reporte'] = $this->modelSegmentacion->total_resultado_sede_operativa($_REQUEST);
        $datos['total_reporte']     = $this->sumatoria($datos['resultado_reporte'],  $this->provincia_totalizados);
        $this->load->view('segmentacion/reporte/sede/sede_operativa_vista_ajax', $datos);
    }

    public function reporte_distrito(){
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array('plugins/datatables/jquery.dataTables.min','plugins/datatables/dataTables.bootstrap.min'));

        $datos['total_sedes']     = $this->modeloAjax->todas_las_sedes();
        $datos['reporte_distrito'] = $this->modelSegmentacion->total_resultado_distrito_operativa();
        $datos['total_reporte']     =  $this->sumatoria($datos['reporte_distrito'], $this->distrito_totalizados);
        $datos['titulo']    = "Segmentación reporte por distrito";
        $datos['contenido'] = 'segmentacion/reporte/distrito/distrito_operativa_vista';

        $this->load->view('plantilla', $datos);
    }


    public function reporte_asistente(){
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array('plugins/datatables/jquery.dataTables.min','plugins/datatables/dataTables.bootstrap.min'));

        $datos['total_sedes']       = $this->modeloAjax->todas_las_sedes();
        $datos['reporte_asistente'] = $this->modelSegmentacion->total_resultado_asistente_operativa();
        $datos['total_reporte']     = $this->sumatoria($datos['reporte_asistente'], $this->asistente_totalizados);
        $datos['ie']                = $this->modelAsistente->todas_las_ie();
        $datos['titulo']            = "Segmentación reporte por asistente";
        $datos['contenido']         = 'segmentacion/reporte/asistente/asistente_operativa_vista';

        $this->load->view('plantilla', $datos);
    }

    public function listado(){
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array('plugins/datatables/jquery.dataTables.min','plugins/datatables/dataTables.bootstrap.min'));

        $datos['BONO_APLICADOR']    = self::BONO_APLICADOR;
        $datos['BONO_APLICADOR_LIDER_MC_PRIMARIA']      = self::BONO_APLICADOR_LIDER_MC_PRIMARIA;
        $datos['BONO_APLICADOR_LIDER_MC_SECUNDARIA']    = self::BONO_APLICADOR_LIDER_MC_SECUNDARIA;
        $datos['GASTO_OPERATIVO']   = self::GASTO_OPERATIVO;
        $datos['MOVILIDAD']         = self::MOVILIDAD;

        $datos['sedes']     = $this->modeloAjax->todas_las_sedes();
        $datos['locales']   = array();//$this->modelSegmentacion->todos_los_locales();
        $datos['titulo']    = "Segmentación listado";
        $datos['contenido'] = 'segmentacion/listado/listado_vista';

        $this->load->view('plantilla', $datos);
    }

    public function programacion(){
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'plugins/vue/vue.min',
        ));

        
        $datos['total']     = array(
            'primaria'      => array('programadas'=>25102, 'avance'=>12302,'porcentaje'=>11.29),
            'secundaria'    => array('programadas'=>12102, 'avance'=>12302,'porcentaje'=>21.29),
            'total'         => array('programadas'=>35102, 'avance'=>12302,'porcentaje'=>31.29),
        );

        $datos['titulo']    = "Avance de Programación";
        $datos['locales']   = $this->modelProgramacion->todos_los_locales($this->sedeOperativa);
        $datos['contenido'] = 'segmentacion/programacion/avance_vista';

        $this->load->view('plantilla', $datos);
    }

    public function actualizar() {
        $input = $this->input->post();

        $id     = $input['hidden_cod_mod'];
        $campo  = $input['campo_actualizado'];
        $campos = $input['ie'];

        $status = $this->modelSegmentacion->actualizar_campo($id, $campo, $campos);

        if ($status) {
            $this->respuestaJson( array('response'=>$input, 'mensaje'=>array(
    			'type'=>'success',
    			'title'=>'Segmentación',
    			'message'=>'Se guardó la información correctamente',
    		)) );
        } else {
            $this->respuestaJson( array('error'=>true, 'mensaje'=>array(
    			'type'=>'warning',
    			'title'=>'Segmentación',
    			'message'=>'Ocurrió un error con la información',
    		)) );
        }
    }

    public function filtrar() {

        $input = $this->input->post();
        $input['filtro']['cod_modular']    = $input['search']['value'];

        $start          = (int)$input['start'];
        $end            = (int)($input['start'] + $input['length']);

        $data['draw']   = (int)$input['draw'];
        $data['data']   = array_map('array_values', $this->modelSegmentacion->obtener_locales($input['filtro'], $start, $end));

        $data['recordsTotal']       = (int)$input['length'];

        $filtrado                   = $this->modelSegmentacion->total_locales_filtrados($input['filtro']);
        $data['recordsFiltered']    = $filtrado['total'];



        $this->respuestaJson( $data );
    }

    private function respuestaJson($data) {

        $this->output
            ->set_content_type('application/json;charset=utf-8')
            ->set_output(json_encode($data));
    }
}
