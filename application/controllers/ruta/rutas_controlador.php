<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rutas_controlador extends CI_Controller {

    public $mensaje;

    public function __construct() {
        parent::__construct();

        $this->load->model('ruta/ruta_model', 'modelRuta');
        $this->load->model('usuario/usuario_model', 'modelUsuario');
        $this->load->model('ajax/ajax_model', 'modeloAjax');

        // Cache configuration for APC its fallback to FILE
        // $cache['adapter'] = 'file';
        // if ( function_exists('apc_cache_info') ) {
        //     $cache['adapter'] = 'apc';
        // }
        // $this->load->driver('cache', $cache);

        $this->isRol    = $this->session->userdata('userIdRol');
        $this->mensaje  = array('tipo'=>'success', 'cuerpo'=>'No se pudo guardar la información enviada');
        //$this->output->enable_profiler(true);
    }

    public function index() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'facebox/src/facebox',
            'jqueryvalidation/dist/jquery.validate.min',
            'jqueryvalidation/dist/additional-methods.min',
            'inei.validation',
            'jquery-ui.min'));

        $datos['rutas']     = $this->modelRuta->todos_los_rol_rutas();
        $datos['titulo']    = "Roles";
        $datos['contenido'] = 'ruta/listado_vista';

        $this->load->view('plantilla', $datos);
    }

    public function crear() {

        if(isset($_REQUEST['crearLocal']) && ($this->input->post('hidden_name_local') == "valorHiddenLocal")){

            $status = $this->modelRuta->insertar_ruta($this->input->post(NULL, TRUE));

            $this->compruebaStatus($status, 'creado');
        } else {
            if ($this->input->is_ajax_request()) {

                $datos['roles']     = $this->modelUsuario->todos_los_roles();
                $this->load->view('ruta/crear_vista', $datos);
            }
        }
    }

    public function asignar() {

        if(isset($_REQUEST['crearLocal']) && ($this->input->post('hidden_name_local') == "valorHiddenLocal")){
            // print_r($_REQUEST);
            // exit;
            $status = $this->modelRuta->asignar_ruta($this->input->post(NULL, TRUE));

            $this->compruebaStatus($status, 'creado');
        } else {
            if ($this->input->is_ajax_request()) {

                $datos['roles']     = $this->modelUsuario->todos_los_roles();
                $datos['rutas']     = $this->modelRuta->todas_las_rutas();
                $this->load->view('ruta/asignar_vista', $datos);
            }
        }
    }

    public function eliminar() {

        if(isset($_REQUEST['eliminarLocal']) && ($this->input->post('idRuta') !== '')){

            $idRuta     = $this->input->post('idRuta');
            $idRol      = $this->input->post('idRol');
            $status     = $this->modelRuta->eliminar_ruta($idRuta, $idRol);

            $this->compruebaStatus($status, 'eliminado');
        } else {
            if ($this->input->is_ajax_request()) {

                $idRuta         = $this->input->get('idRuta');
                $idRol          = $this->input->get('idRol');
                $datos['ruta']  = $this->modelRuta->obtener_rol_ruta($idRuta, $idRol);

                // log_message('INFO', "PRUEBAS : ".implode(', ', array("('ab', 'ac', 'ad')", "('bb', 'bc', 'bd')", "('cb', 'cc', 'cd')")) );

                $this->load->view('ruta/eliminar_vista', $datos);
            }
        }
    }

    public function editar() {

        if(isset($_REQUEST['editarLocal']) && ($this->input->post('idRuta') !== '')) {

            log_message('INFO', "Datos: {$this->input->post()}");

            $status     = $this->modelRuta->editar_ruta($this->input->post(null, true));

            $this->compruebaStatus($status, 'editado');
        } else {
            if ($this->input->is_ajax_request()) {

                $idRuta         = $this->input->get('idRuta');
                $datos['ruta']  = $this->modelRuta->obtener_ruta($idRuta);

                $this->load->view('ruta/editar_vista', $datos);
            }
        }
    }

    private function compruebaStatus($status, $accion) {
        if ($status) {
            // $this->cache->delete('roles');

            $this->mensaje['cuerpo'] = "La ruta ha sido {$accion}";
        } else {
            $this->mensaje['tipo']   = 'warning';
            $this->mensaje['cuerpo'] = "La ruta NO ha sido {$accion}";

            log_message('ERROR', $status . "Ruta no pudo ser {$accion}");
        }

        $this->session->set_flashdata('mensaje_flash', $this->mensaje);

        redirect('rutas');
    }
}
