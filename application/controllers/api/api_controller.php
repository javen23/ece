<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_controller extends CI_Controller {

	public function __constructor(){
		parent::__constructor();
		$this->load->model('api/api_model','modelApi');

	}

	public function index(){
		echo "aqui no hay nada...";
	}

	public function login(){
		//echo "ENTRANDO A LOGIN";
		$array = json_decode(trim(file_get_contents('php://input')), true);
      	if(is_array($array)){
			$estado = $this->modelApi->logeo_acceso($array['password'],$array['username']);
			$this->verificar_estado($estado,'usuario');
  		}
	}

	private function verificar_estado($estado,$accion=null){
		if (!is_null($estado)) {
			if(is_null($accion)) {
				$this->output
					->set_content_type('application/json;charset=utf-8')
					->set_status_header('200')
					->set_output(json_encode($estado));
			}else{
				$this->output
					->set_content_type('application/json;charset=utf-8')
					->set_status_header('200')
					->set_output(json_encode(array($accion => $estado)));
			}
		}else{
			$this->output
				->set_content_type('application/json;charset=utf-8')
				->set_status_header('400')
				->set_output(json_encode(array("error"=>"No hay nada")));
		}
   	}

   	public function version() {
		$estado = $this->modelApi->get_version();
		$this->verificar_estado($estado,'version');
   	}

}

/* End of file ws_controller.php */
/* Location: ./application/controllers/ws/ws_controller.php */