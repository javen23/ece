<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supervision_controller extends CI_Controller {

	public function __constructor(){
		parent::__constructor();
		$this->load->model("api/supervision_model","supervisionModel");
	}

	public function index()
	{
		echo "demo";
	}

	private function verificar_estado($estado,$accion=null){
		if (!is_null($estado)) {
			if(is_null($accion)) {
				$this->output
					->set_content_type('application/json;charset=utf-8')
					->set_status_header('200')
					->set_output(json_encode($estado));
			}else{
				$this->output
					->set_content_type('application/json;charset=utf-8')
					->set_status_header('200')
					->set_output(json_encode(array($accion => $estado)));
			}
		}else{
			$this->output
				->set_content_type('application/json;charset=utf-8')
				->set_status_header('400')
				->set_output(json_encode(array("error"=>"No hay nada")));
		}
   	}

   	public function capitulo_uno()
   	{
   		$array = json_decode(trim(file_get_contents('php://input')), true);   		
   		$estado = "formato incorrecto";
      	if(is_array($array)){
      		$estado = $this->supervisionModel->capitulo1($array);    			
      		$this->verificar_estado($estado,'capitulo1');
      	}
         else
            echo $estado;
   	}

   	public function capitulo_dos_a()
   	{
   		$array = json_decode(trim(file_get_contents('php://input')), true);
   		$estado = "formato incorrecto";
      	if(is_array($array)){      		
            $estado = $this->supervisionModel->capitulo2a($array);    			
      		$this->verificar_estado($estado,'Capitulo2A');
      	}
         else            
      	  echo $estado;
   	}

   	//http://www.codeigniter.com/userguide2/libraries/user_agent.html

   	public function capitulo_dos_b()
   	{
   		$array = json_decode(trim(file_get_contents('php://input')), true);
   		$estado = "formato incorrecto";
      	if(is_array($array)){
            $estado = $this->supervisionModel->capitulo2b($array);
      		$this->verificar_estado($estado,'Capitulo2B');
      	}
         else            
      	  echo $estado;
   	}

}

/* End of file supervision_controller.php */
/* Location: ./application/controllers/api/supervision_controller.php */