<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax_controlador extends CI_Controller {



    public function __construct() {
        parent::__construct();

        $this->load->model('ajax/ajax_model', 'modeloAjax');

        $this->isRol    = $this->session->userdata('userIdRol');
        //$this->output->enable_profiler(true);
    }

    public function sede_operativa() {

        $lista = $this->modeloAjax->todas_las_sedes();

        if ($lista) {
            $data = array("sede_operativa"=>$lista);
        } else {
            $data = array("error"=>"No hay registro de Sede Operativa");
        }

        $this->respuestaJson($data);
    }

    public function exportar_tabla(){
        $nombre_archivo=$_POST['nombre_archivo']."_".date('d-m-Y_h:i:s');

        header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Content-Disposition: filename={$nombre_archivo}.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo utf8_decode($_POST['datos_a_enviar']);
    }

    public function sede_operativa_mostrar($id) {

        $mostrarExtras = $this->input->get('extras');

        $lista = $this->modeloAjax->obtener_sede($id);

        if ($lista) {
            $data = array("sede_operativa"=>$lista);
        }
        else {
            $data = array("error"=>"No hay registro de Sede Operativa #{$id}");
        }

        $this->respuestaJson($data);
    }

    public function departamentos() {

        $cod_sede_operativa = $this->input->get('sede_operativa');

        if ($cod_sede_operativa) {
            $lista = $this->modeloAjax->obtener_departamentos_por_sede($cod_sede_operativa);
        } else {
            $lista = $this->modeloAjax->todos_los_departamentos();
        }

        if ($lista) {
            $data = array("departamentos"=>$lista);
        } else {
            $data = array("error"=>"No hay registro de Departamentos");
        }

        $this->respuestaJson($data);
    }

    public function provincias() {

        $departamento = $this->input->get('departamento');
        $sede_operativa = $this->input->get('sede_operativa');

        if ($sede_operativa && $sede_operativa != 'undefined') {
            $lista = $this->modeloAjax->obtener_provincias_por_departamento($departamento, $sede_operativa);
        } else {
            $lista = $this->modeloAjax->todas_las_provincias($departamento);
        }

        if ($lista) {
            $data = array("provincias"=>$lista);
        } else {
            $data = array("error"=>"No hay registro de Provincias");
        }

        $this->respuestaJson($data);
    }

    public function sede_provincial() {

        $lista = $this->modeloAjax->obtener_sede_provincial($this->input->get());

        if ($lista) {
            $data = array("sede_provincial"=>$lista);
        } else {
            $data = array("error"=>"No hay registro de Sede Provincial");
        }

        $this->respuestaJson($data);
    }

     public function sede_provincial_multiple() {

        $lista = $this->modeloAjax->obtener_sede_provincial_multiple($this->input->get());

        if (!is_null($lista)) {
            $data = array("sede_provincial_multiple"=>$lista);
        } else {
            $data = array("error"=>"No hay registro de Sede Provincial");
        }

        $this->respuestaJson($data);
    }

    /**
     * Muestra todos los distritos
     *
     * Undocumented function long description
     *
     * @return JSON $data Distritos
     **/
    public function distritos() {

        $ccpp = $this->input->get('provincia');
        $ccdd = $this->input->get('departamento');
        $sede_operativa = $this->input->get('sede_operativa');

        if ($sede_operativa && $sede_operativa !== 'undefined') {
            $lista = $this->modeloAjax->obtener_distritos_por_provincia($ccpp, $ccdd, $sede_operativa);

        } else {
            $lista = $this->modeloAjax->todos_los_distritos($ccdd, $ccpp);
        }

        if ($lista) {
            $data = array("distritos"=>$lista);
        } else {
            $data = array("error"=>"No hay registro de Distritos");
        }

        $this->respuestaJson($data);
    }

    public function sede_distrital() {

        // $lista = $this->modeloAjax->obtener_sede_distrital_por_distrito($this->input->get());
        $ccdi = $this->input->get('distrito');
        $ccpp = $this->input->get('provincia');
        $ccdd = $this->input->get('departamento');
        $sede_operativa = $this->input->get('sede_operativa');

        $lista = $this->modeloAjax->obtener_sede_distrital_por_distrito($ccdi, $ccpp, $ccdd, $sede_operativa);

        if ($lista) {
            $data = array("sede_distrital"=>$lista);
        } else {
            $data = array("sede_distrital"=>null, "error"=>"No hay registro de Sede distrital");
        }

        $this->respuestaJson($data);
    }

    public function lector_barra(){

        if(isset($_REQUEST['codigo'])):
            $codigo = $this->input->post('codigo');
            //$codigo = $this->security->xss_clean(strip_tags(strtolower(trim(notildes($this->input->post('codigo'))))));
            $lista = $this->modeloAjax->obtener_lectura_barra($codigo);
            if (is_null($lista)) :
                $data = array("resultado"=>"NO");
            else:
                $data=array("resultado"=>$lista);
            endif;
            //$data=array("codigo_response"=>"CORRESPON DE A ESTÁ UBICACIÓN CENSAL");
        else:
            $data=array("error_codigo"=>"NO SE ENVIÓ CÓDIGO");
        endif;
        $this->respuestaJson($data);
    }

    // public function crear() {

    //     $lista = $this->modeloTablet->lista_padron_ubicacion();
    //     if (!is_null($lista)){
    //          $this->output
    //                ->set_content_type('application/json;charset=utf-8')
    //                ->set_output(json_encode(array("padron_ubicacion"=>$lista)));
    //     }else{
    //      $this->output
    //            ->set_content_type('application/json;charset=utf-8')
    //            ->set_output(json_encode(array("error_padron"=>"No hay registro de padron ubicación")));
    //     }
    // }

    public function crear_computadora() {

        if( $this->input->post('hidden_key') == "valorHiddenLocal" ){

            $data = $this->input->post();

            $total_registros = count($data);

            foreach ($data['item'] as $item) {
                if (!empty($item)) {
                    $Computadora = new Computadora($item);

                    $pcs_list[] = $this->modeloAjax->computadora($Computadora);
                }
            }

            $this->respuestaJson( array('success'=>true, 'computadoras'=>implode(',', $pcs_list)) );
        } else {
            if ($this->input->is_ajax_request()) {
                $total_pcs = $this->input->get('pcs');

                for ($i=0; $i < $total_pcs; $i++) {
                    $pcs_list[$i + 1] = new Computadora(array());
                }

                $this->load->library('DataSelect');

                $datos['titulo']        = 'Registro de Computadoras';
                $datos['elementos']     = $pcs_list;
                $datos['total']         = $total_pcs;
                $datos['form_ruta']     = base_url('ajax/computadora/crear');
                $this->load->view('computadora/crear_vista', $datos);
            }
        }

    }

    public function editar_computadora() {

        if($this->input->post('hidden_key') == "valorHiddenLocal"){

            $data = $this->input->post();

            foreach ($data['item'] as $item) {
                if (!empty($item) && $item['serie'] !== '') {
                    $Computadora = new Computadora($item);
                    $pcs_list[] = $this->modeloAjax->computadora($Computadora);
                }
            }

            $this->respuestaJson(  array('success'=>true, 'computadoras'=>implode(',', $pcs_list)) );
        } else {
            if ($this->input->is_ajax_request()) {
                $id_oficina     = $this->input->get('id_oficina');
                $total_pcs      = $this->input->get('pcs');
                $pcs_data       = $this->modeloAjax->obtener_computadora($id_oficina);

                for ($i=0; $i < $total_pcs; $i++) {
                    $pcs          = isset($pcs_data[$i]) ? $pcs_data[$i] : array();
                    $pcs_list[$i + 1] = new Computadora($pcs);
                }

                $this->load->library('DataSelect');



                $datos['titulo']        = 'Editar Computadora';
                $datos['elementos']     = $pcs_list;
                $datos['total']         = $total_pcs;
                $datos['form_ruta']     = base_url('ajax/computadora/editar').'?id_oficina='.$id_oficina;
                $this->load->view('computadora/crear_vista', $datos);
            }
        }
    }

    public function crear_seguridad() {

        if($this->input->post('hidden_key') == "valorHiddenLocal"){
            $data = $this->input->post();

            foreach ($data['item'] as $item) {
                if (!empty($item) || $item['dni'] !== '') {
                    $Seguridad = new Seguridad($item);
                    $seguridad_list[] = $this->modeloAjax->seguridad($Seguridad, 'create');
                }
            }

            $this->respuestaJson( array('success'=>true, 'seguridad'=>implode(',', $seguridad_list)) );
        } else {
            if ($this->input->is_ajax_request()) {
                $total_personal = $this->input->get('seguridad');

                for ($i=0; $i < $total_personal; $i++) {
                    $seguridad_list[$i + 1] = new Seguridad(array());
                }

                $this->load->library('DataSelect');

                $datos['titulo']        = 'Registro de Personal de Seguridad';
                $datos['elementos']     = $seguridad_list;
                $datos['total']         = $total_personal;
                $datos['form_ruta']     = base_url('ajax/seguridad/crear');
                $this->load->view('seguridad/crear_vista', $datos);
            }
        }

    }

    public function editar_seguridad() {

        if($this->input->post('hidden_key') == "valorHiddenLocal"){
            $data = $this->input->post();

            foreach ($data['item'] as $item) {
                if (!empty($item) || $item['dni'] !== '') {
                    $Seguridad = new Seguridad($item);
                    $seguridad_list[] = $this->modeloAjax->seguridad($Seguridad, 'create');
                }
            }

            $this->respuestaJson( array('success'=>true, 'seguridad'=>implode(',', $seguridad_list)) );
        } else {
            if ($this->input->is_ajax_request()) {
                $id_oficina     = $this->input->get('id_oficina');
                $total_personal = $this->input->get('seguridad');
                $seguridad_data = $this->modeloAjax->obtener_seguridad($id_oficina);

                for ($i=0; $i < $total_personal; $i++) {
                    $seguridad          = isset($seguridad_data[$i]) ? $seguridad_data[$i] : array();
                    $seguridad_list[$i + 1] = new Seguridad($seguridad);
                }

                $this->load->library('DataSelect');

                $datos['titulo']        = 'Editar Personal de Seguridad';
                $datos['elementos']     = $seguridad_list;
                $datos['total']         = $total_personal;
                $datos['form_ruta']     = base_url('ajax/seguridad/editar').'?id_oficina='.$id_oficina;
                $this->load->view('seguridad/crear_vista', $datos);
            }
        }
    }

    public function reporte_mapa_region() {

        // if ($this->input->is_ajax_request()) {
            $id_oficina     = $this->input->get('id_oficina');
            $tipo_local     = $this->input->get('tipo_local');
            $titulo_local   = $this->input->get('titulo_local');
            // $seguridad_data = $this->modeloAjax->obtener_seguridad($id_oficina);
            //
            $this->load->model('locales/locales_administrativo_model', 'modeloOficina');

            $datos['titulo']        = 'Local de Jurisdicción '.$titulo_local;
            $datos['local']         = $this->modeloOficina->obtener_local_tipo($id_oficina, $tipo_local);
            // $datos['total']         = $total_personal;
            $this->load->view('locales/reportes/mapa_region_modal', $datos);
        // }
    }

    public function area_almacen()
    {
        $params = $this->input->post();

        $data['success'] = true;
        $data['almacen'] = $this->modeloAjax->area_almacen($params);
        $data['data'] = $params;

        $this->respuestaJson($data);
    }

    private function respuestaJson($data) {

        $this->output
            ->set_content_type('application/json;charset=utf-8')
            ->set_output(json_encode($data));
    }
}
