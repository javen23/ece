<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exportar_asistente_sup_controlador extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('segmentacion/segmentacion_model', 'modeloSegmentacion');
        $this->load->library('PHPExcel');
        ini_set("memory_limit", -1);
        ini_set('max_execution_time', -1);
    }

	public function index()
	{
		
	}

	public function asistente_sup_reporte() {

		$phpExcel = new PHPExcel();
        $prestasi = $phpExcel->setActiveSheetIndex(0);
        //Estilo para el border
        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '7f8c8d')
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el titulo
        $titulo = array(
            'font' => array(
                'bold' => true,
                'size' => 28
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el subtitulo
        $subtitulo = array(
            'font' => array(
                'bold' => true,
                'size' => 24
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el fondo de la celda
        $backFondo = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap' => true
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => 'DDDDDD')
                ),
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '3498db')
            ),
        );
        //fusionando titulo
        $tabTitulo = 1;
        $phpExcel->getActiveSheet()->mergeCells('A' . $tabTitulo . ':N' . $tabTitulo);
        $phpExcel->getActiveSheet()->getStyle('A' . $tabTitulo . ':N' . $tabTitulo)->applyFromArray($titulo);
        //fusionanado subtitulo
        $tabSubtitulo = 2;
        $phpExcel->getActiveSheet()->mergeCells('A' . $tabSubtitulo . ':N' . $tabSubtitulo);
        $phpExcel->getActiveSheet()->getStyle('A' . $tabSubtitulo . ':N' . $tabSubtitulo)->applyFromArray($subtitulo);

        //fusionando Tab2
        $tab2 = 4;
        $phpExcel->getActiveSheet()->getStyle('A' . $tab2 . ':N' . $tab2)->applyFromArray($backFondo);
        //altura en la fila
        $phpExcel->getActiveSheet()->getRowDimension($tabTitulo)->setRowHeight(46);
        $phpExcel->getActiveSheet()->getRowDimension($tabSubtitulo)->setRowHeight(33);
        $phpExcel->getActiveSheet()->getRowDimension($tab2)->setRowHeight(38);
        //Titulo de nombres pie excel
        $prestasi->setCellValue('A' . $tabTitulo, 'REPORTE DE ASISTENTES SUPERVISORES');
        $prestasi->setCellValue('A' . $tabSubtitulo, 'REPORTE DETALLADO');
        //border
        $phpExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

        $numerNombre = 4;
        $prestasi->setCellValue('A' . $numerNombre, 'N°');
        $prestasi->setCellValue('B' . $numerNombre, 'SEDE REGIONAL');
        $prestasi->setCellValue('C' . $numerNombre, 'SEDE PROVINCIAL');
        $prestasi->setCellValue('D' . $numerNombre, 'SEDE DISTRITAL');
        $prestasi->setCellValue('E' . $numerNombre, 'DEPARTAMENTO PROVINCIA');
        $prestasi->setCellValue('F' . $numerNombre, 'DISTRITO');
        $prestasi->setCellValue('G' . $numerNombre, 'COD. MODULAR');
        $prestasi->setCellValue('H' . $numerNombre, 'INSITUCIÓN EDUCATIVA');
        $prestasi->setCellValue('I' . $numerNombre, 'NOMBRES');
        $prestasi->setCellValue('J' . $numerNombre, 'APELLIDOS');
        $prestasi->setCellValue('K' . $numerNombre, 'DNI');
        $prestasi->setCellValue('L' . $numerNombre, 'CANTIDAD DE APLICACIONES');
        $prestasi->setCellValue('M' . $numerNombre, 'DURACION TOTAL DEL VIAJE SEDE A II.EE');
        $prestasi->setCellValue('N' . $numerNombre, 'FECHA DE SALIDA');

        $data = $this->modeloSegmentacion->total_resultado_sede_operativa($_REQUEST);

        $rowexcel = 4;
        $numero = 0;
        foreach ($data as $row) {
            $rowexcel++;
            $numero++;
            $prestasi->getStyle('A' . $rowexcel . ':N' . $rowexcel)->applyFromArray($borders);
            $prestasi->setCellValue('A' . $rowexcel, $numero)->getStyle('A' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('B' . $rowexcel, $row['cod_sede_operativa'])->setCellValueExplicit('B' . $rowexcel, $row['codigo_sede_oeprativa'], PHPExcel_Cell_DataType::TYPE_STRING);;
            $prestasi->setCellValue('C' . $rowexcel, $row['local_sede_operativa'])->getStyle('C' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('D' . $rowexcel, $row['local_sede_distrital']);
            $prestasi->setCellValue('E' . $rowexcel, $row['dni']);
            $prestasi->setCellValue('F' . $rowexcel, $row['ap_paterno']);
            $prestasi->setCellValue('G' . $rowexcel, $row['ap_materno'])->setCellValueExplicit('G' . $rowexcel, $row['total_gastos_operativos'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('H' . $rowexcel, $row['nombre'])->setCellValueExplicit('H' . $rowexcel, $row['total_pasaje'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('I' . $rowexcel, $row['local_departamento'])->setCellValueExplicit('I' . $rowexcel, $row['total_movilidad'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('J' . $rowexcel, $row['local_provincia'])->setCellValueExplicit('J' . $rowexcel, $row['total_bonificacion'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('K' . $rowexcel, $row['local_distrito'])->setCellValueExplicit('K' . $rowexcel, $row['total'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('L' . $rowexcel, $row['local_ie'])->setCellValueExplicit('L' . $rowexcel, $row['total'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('M' . $rowexcel, $row['local_cant_aplicadores'])->setCellValueExplicit('M' . $rowexcel, $row['total'], PHPExcel_Cell_DataType::TYPE_STRING);
            $prestasi->setCellValue('N' . $rowexcel, $row['cod_duracion_'])->setCellValueExplicit('N' . $rowexcel, $row['total'], PHPExcel_Cell_DataType::TYPE_STRING);

        }

        $prestasi->setTitle('Reporte de las Instituciones Educativas');
        header("Content-Type: application/vnd.ms-excel");
        $nameFile = "reporte_de_presupuesto_sede_" . date("d-m-Y_his");
        header("Content-Disposition: attachment; filename=\"$nameFile.xls\"");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel5");
        $objWriter->save("php://output");

        echo '1';
        exit;

	}
}

/* End of file exportar_asistente_sup_controlador.php */
/* Location: ./application/controllers/asistente/exportar_asistente_sup_controlador.php */