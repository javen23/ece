<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Asistente_controlador extends CI_Controller {

    public $mensaje;
    protected $titulo = 'Asistentes';

    public function __construct() {
        parent::__construct();

        $this->load->model('asistente/aplicadores_model', 'modelAplicadores');
        $this->load->model('asistente/aplicador_model', 'modelAplicador');
        $this->load->model('asistente/asistente_model', 'modelAsistente');

        $this->isRol            = $this->session->userdata('userIdRol');
        $this->sedeOperativa    = $this->session->userdata('sedeOperativa');
        $this->mensaje  = array('tipo'=>'success', 'cuerpo'=>'Registro guardado exitosamente');

        $this->load->library('DataSelect');
        //$this->output->enable_profiler(true);

    }

    /*public function index(){
        $datos['titulo']    = "MÓDULO ASISTENTE SUPERVISOR";
        $datos['contenido'] = 'asistente/index_asistente_vista';
        $this->load->view('plantilla', $datos);
    }*/

    public function ie() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'facebox/src/facebox',
            'jquery-ui.min'));

        $datos['asistentes']     = $this->modelAsistente->todos_los_asistentes($this->sedeOperativa);
        $datos['titulo']        = 'Cronograma de Salida por '.$this->titulo;
        $datos['title']         = $this->titulo;
        $datos['contenido']     = 'asistente/ie/listado_vista';
        $this->load->view('plantilla', $datos);
    }

    public function ie_crear() {

        if(isset($_REQUEST['crearAsistente']) && ($this->input->post('hidden_name_local') == "valorHiddenLocal")){
            $_REQUEST['fecha_salida'] = convertir_fecha($_REQUEST['fecha_salida']);
            //print_r(convertir_fecha($_REQUEST['fecha_salida']));
            //echo "<pre>";
            //print_r($_REQUEST);
            //print_r(convertir_Fecha($_REQUEST['fecha_salida']));
            //print_r (explode(" ", $fechahora));
            //$data_fecha = (explode(" ", $fechahora));
            //print_r($dateTime);
            //echo $data_fecha;
            //print_r (explode("-", $data_fecha[1]));
            //print_r (explode(":", $data_fecha[1]));
            //echo "</pre>";
            //exit;
            $status = $this->modelAsistente->insertar_ie($this->input->post(NULL, TRUE));
            $this->compruebaStatus($status, 'creado');
        } else {
            add_css(array('datetimepicker/bootstrap-datetimepicker.min'));
            add_js(array(
                'plugins/moment/min/moment.min',
                'plugins/moment/locale/es',
                'plugins/datetimepicker/bootstrap-datetimepicker.min',
                'plugins/typeahead/typeahead.bundle.min',
                'facebox/src/facebox',
                'jquery-ui.min',
                'plugins/input-mask/jquery.inputmask',
                'plugins/input-mask/jquery.inputmask.extensions',
                'jqueryvalidation/dist/jquery.validate.min',
                'jqueryvalidation/dist/additional-methods.min',
                'inei.validation'));
            $this->load->model('ajax/ajax_model', 'modeloAjax');
            $datos['cant_aplicadores']  = $this->input->get('aplicadores');
            $datos['sedes']         = $this->modeloAjax->todas_las_sedes($this->sedeOperativa);
            $datos['elementos']     = $this->modelAsistente->todas_las_ie($this->sedeOperativa);
            $datos['titulo']        = 'Cronograma de Salida por '.$this->titulo;
            $datos['title']         = $this->titulo;
            $datos['contenido']     = 'asistente/ie/crear_vista';
            $this->load->view('plantilla', $datos);
        }
    }

       public function ie_editar() {
        add_css(array('datetimepicker/bootstrap-datetimepicker.min'));
        add_js(array(
            'plugins/moment/min/moment.min',
            'plugins/moment/locale/es',
            'plugins/datetimepicker/bootstrap-datetimepicker.min',
            'plugins/typeahead/typeahead.bundle.min',
            'facebox/src/facebox',
            'jquery-ui.min',
            'plugins/input-mask/jquery.inputmask',
            'plugins/input-mask/jquery.inputmask.extensions',
            'jqueryvalidation/dist/jquery.validate.min',
            'jqueryvalidation/dist/additional-methods.min',
            'inei.validation'));

        $cod_asistente    = $this->input->get('id');

        if(isset($_REQUEST['editarAsistente']) && ($this->input->post('asistente_cod_asistente') !== '')){
            $datos = $this->input->post(NULL, TRUE);
            
            //separar datos del asistente de la lista de aplicadores*
            $aplicadores = array();
            $aplicadores['datos_nombres'] = $this->input->post('datos_nombres');
            $aplicadores['datos_apellidos'] = $this->input->post('datos_apellidos');
            $aplicadores['datos_dni'] = $this->input->post('datos_dni');
            $aplicadores['datos_telefono'] = $this->input->post('datos_telefono');
            unset($datos['datos_nombres']);
            unset($datos['datos_apellidos']);
            unset($datos['datos_dni']);
            unset($datos['datos_telefono']);
            //actualizar asistente
            $status = $this->modelAsistente->editar_asistente($datos, $cod_asistente);

            //eliminar aplicadores* (PENDIENTE. falta campo estado)
            $this->modelAplicador->eliminar_aplicadores($cod_asistente);            

            //print_r($this->input->post('datos_dni'));

            foreach ($this->input->post('datos_dni') as $aplicador) {
                
                $dni = preg_replace( '/[^0-9]/', '', $aplicador);

                $params['DNI'] = $dni;
                $params['codigo_asistente'] = $cod_asistente;

                $this->modelAplicador->insertar_aplicadores($params, $cod_asistente);

            }


            $this->compruebaStatus($status, 'editado');

        } else {

            $this->load->model('ajax/ajax_model', 'modeloAjax');
            //$datos['cant_aplicadores']  = $this->input->get('aplicadores');
            $datos['ies'] = $this->modelAsistente->todas_las_ie($this->sedeOperativa);
            $datos['asistentes'] = $this->modelAsistente->obtener_asistente($cod_asistente);
            $datos['sedes']         = $this->modeloAjax->todas_las_sedes($this->sedeOperativa);
            $datos['sede_provinciales'] = $this->modeloAjax->obtener_sede_provincial(array('sede_operativa'=>$datos['asistentes']['cod_sede_operativa']));
            //$datos['sede_distritales'] = $this->modeloAjax->obtener_sede_distrital_por_distrito($datos['asistentes']['ccdi'], $datos['asistentes']['ccpp'], $datos['asistentes']['ccdd'], $datos['asistentes']['cod_sede_operativa']);
            //
            // $datos['departamentos'] = $this->modeloAjax->obtener_departamentos_por_sede($datos['asistentes']['cod_sede_operativa']);
            // $datos['provincias']    = $this->modeloAjax->obtener_provincias_por_departamento($datos['asistentes']['ccdd'], $datos['asistentes']['cod_sede_operativa']);
            // $datos['distritos']     = $this->modeloAjax->obtener_distritos_por_provincia($datos['asistentes']['ccpp'], $datos['asistentes']['ccdd'], $datos['asistentes']['cod_sede_operativa']);

            $datos['titulo']    = "Cronograma de ".$this->titulo;
            $datos['title']     = $this->titulo;
            $datos['contenido'] = 'asistente/ie/editar_vista';
         
            $asistentes = $this->modelAsistente->obtener_asistente($cod_asistente);
            $aplicadores = $this->modelAplicador->obtener_aplicadores($cod_asistente);
            $datos['aplicadores'] = $aplicadores;
            $datos['aplicadores_cantidad'] = count($aplicadores);
            
           /* echo "<pre>";
            print_r($cod_asistente);
            print_r($asistentes);
            print_r($aplicadores);
            print_r(count($aplicadores));
            echo "</pre>";
            exit;*/
            
            $this->load->view('plantilla', $datos);
        }

    }


    public function ie_eliminar() {

        if(isset($_REQUEST['eliminarAsistente']) && ($this->input->post('asistente_cod_asistente') !== '')){

            $cod_asistente    = $this->input->post('asistente_cod_asistente');
            $status     = $this->modelAsistente->eliminar_asistente($cod_asistente);

            $this->compruebaStatus($status, 'eliminado');
        } else {
            if ($this->input->is_ajax_request()) {

                $cod_asistente        = $this->input->get('id');
                $datos['asistentes'] = $this->modelAsistente->obtener_asistente($cod_asistente);

                $this->load->view('asistente/ie/eliminar_vista', $datos);
            }
        }
    }

    public function ie_formato(){

        $datos['titulo']    = "MÓDULO ASISTENTE SUPERVISOR";
        $datos['contenido'] = 'asistente/ie/formato_vista';
        $this->load->view('plantilla', $datos);
    }

    public function ie_aplicadores() {
        add_js(array(
            'plugins/moment/min/moment.min',
            'plugins/moment/locale/es',
            'plugins/datetimepicker/bootstrap-datetimepicker.min',
            'plugins/typeahead/typeahead.bundle.min',
            'facebox/src/facebox',
            'jquery-ui.min',
            'plugins/input-mask/jquery.inputmask',
            'plugins/input-mask/jquery.inputmask.extensions',
            'jqueryvalidation/dist/jquery.validate.min',
            'jqueryvalidation/dist/additional-methods.min',
            'inei.validation'));

        $this->load->model('ajax/ajax_model', 'modeloAjax');
        $datos['cant_aplicadores']  = $this->input->get('aplicadores');
        $datos['aplicadores_dni']   = $this->modelAplicadores->todos_los_aplicadores();
        $datos['asistentes']        = $this->modelAsistente->todos_los_asistentes();
        $datos['elementos']         = $this->modelAsistente->todas_las_ie($this->sedeOperativa);
        $datos['titulo']            = 'Registro de Aplicadores';
        $this->load->view('asistente/ie/aplicadores_vista', $datos);
    }


  public function verificar_dni() {
      $dni = $this->input->post('dni');
      $verificadni = $this->modelAsistente->verificar_dni_asistente($dni);
      if ($verificadni) {
         echo "true";
      } else {
         echo "false";
      }
   }


    private function compruebaStatus($status, $accion) {
        if ($status) {
            $this->mensaje['cuerpo'] = "El Asistente ha sido {$accion}";
        } else {
            $this->mensaje['tipo']   = 'warning';
            $this->mensaje['cuerpo'] = "El Asistente NO ha sido {$accion}";

            log_message('ERROR', $status . "Asistente no pudo ser {$accion}");
        }

        $this->session->set_flashdata('mensaje_flash', $this->mensaje);

        redirect('asistente-supervisor/ie');
    }

}
