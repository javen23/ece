<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aplicadores_controller extends CI_Controller {

	public 		$mensaje;
	protected 	$titulo = 'Aplicadores';

	public function __construct() {
			parent::__construct();

			$this->load->model('asistente/aplicadores_model', 'modelAplicadores');
            $this->load->model('asistente/asistente_model', 'modelAsistente');
			$this->load->model('ajax/ajax_model', 'modeloAjax');

       		$this->isRol    = $this->session->userdata('userIdRol');
	}


	public function aplicadores() {

		$dni 	= $this->input->get('dni');

		$lista = $this->modelAplicadores->obtener_aplicador_dni($dni);

		if ($lista) {
            $data = array_column($lista, 'nombre');
        } else {
            $data = array("[{$dni}] - No existe Aplicadores...");
        }

        log_message('INFO', "DNI APLICADOR  {$dni}");

        $this->respuestaJson($data);

	}

	private function respuestaJson($data) {

        $this->output
            ->set_content_type('application/json;charset=utf-8')
            ->set_output(json_encode($data));
    }

    public function verificar_dni() {
        $dni = $this->input->post('datos_dni');
        $verificadniap = $this->modelAplicadores->verificar_dni_aplicador($dni);
        if ($verificadniap) {
            echo "true";
        }else {
            echo "false";
        }
   }

}

/* End of file aplicadores_controller.php */
/* Location: ./application/controllers/asistente/aplicadores_controller.php */