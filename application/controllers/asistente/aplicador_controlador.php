<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aplicador_controlador extends CI_Controller {

	public 		$mensaje;
	protected 	$titulo = 'Aplicadores';

	public function __construct() {
			parent::__construct();

			$this->load->model('asistente/aplicador_model', 'modelAplicador');
            $this->load->model('asistente/asistente_model', 'modelAsistente');

			$this->isRol            = $this->session->userdata('userIdRol');
        	$this->sedeOperativa    = $this->session->userdata('sedeOperativa');
        	$this->mensaje  = array('tipo'=>'success', 'cuerpo'=>'Registro guardado exitosamente');
	}

	public function aplicador()	{
		add_css(array('datatables/dataTables.bootstrap'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'facebox/src/facebox',
            'jquery-ui.min'));

        $datos['aplicadores']		= $this->modelAplicador->todos_los_aplicadores($this->modelAsistente);
        $datos['titulo']        = 'Listado de  '.$this->titulo;
        $datos['title']         = $this->titulo;
        $datos['contenido']		= 'asistente/aplicador/listado_vista';
        $this->load->view('plantilla', $datos);

	}


	public function ap_crear() {
		if(isset($_REQUEST['crearAplicador']) && ($this->input->post('hidden_name_aplicador') == "ValorHiddenAplicador")){
			$status = $this->modelAplicador->insertar_aplicadores($this->input->post(NULL, TRUE));
		} else {
			add_css(array('datetimepicker/bootstrap-datetimepicker.min'));
            add_js(array(
                'plugins/moment/min/moment.min',
                'plugins/moment/locale/es',
                'plugins/datetimepicker/bootstrap-datetimepicker.min',
                'facebox/src/facebox',
                'jquery-ui.min',
                'plugins/input-mask/jquery.inputmask',
                'plugins/input-mask/jquery.inputmask.extensions',
                'jqueryvalidation/dist/jquery.validate.min',
                'jqueryvalidation/dist/additional-methods.min',
                'inei.validation'));

            $datos['aplicadores']       = $this->modelAplicador->todos_los_aplicadores($this->modelAsistente);
            $datos['cant_aplicadores']  = $this->input->get('aplicadores');
            $datos['titulo']            = 'Registro de Aplicadores';
            $datos['contenido']			= 'asistente/aplicador/crear_vista';
            $this->load->view('asistente/aplicador/crear_vista', $datos);
		}
	}

	public function ap_editar() {
		add_css(array('datetimepicker/bootstrap-datetimepicker.min'));
        add_js(array(
            'plugins/moment/min/moment.min',
            'plugins/moment/locale/es',
            'plugins/datetimepicker/bootstrap-datetimepicker.min',
            'facebox/src/facebox',
            'jquery-ui.min',
            'plugins/input-mask/jquery.inputmask',
            'plugins/input-mask/jquery.inputmask.extensions',
            'jqueryvalidation/dist/jquery.validate.min',
            'jqueryvalidation/dist/additional-methods.min',
            'inei.validation'));

        $cod_asistente    = $this->input->get('id');

        if(isset($_REQUEST['editarAplicador']) && ($this->input->post('aplicador_cod_asistente') !== '')){
            $status = $this->modelAplicador->editar_aplicadores($this->input->post(NULL, TRUE), $cod_asistente);
            $this->compruebaStatus($status, 'editado');
        } else {

            $this->load->model('ajax/ajax_model', 'modeloAjax');

            $datos['asistentes'] = $this->modelAsistente->obtener_asistente($cod_asistente);
            $datos['aplicadores'] = $this->modelAplicador->obtener_aplicadores($cod_asistente);
            $datos['cant_aplicadores']  = $this->input->get('aplicadores');
            $datos['titulo']    = "Editar ".$this->titulo;
            $datos['title']     = $this->titulo;
            $datos['contenido'] = 'asistente/aplicador/editar_vista';
            $this->load->view('asistente/aplicador/editar_vista', $datos);
        }
	}

	public function ap_eliminar() {

        if(isset($_REQUEST['eliminarAplicador']) && ($this->input->post('aplicador_cod_asistente') !== '')){

            $cod_asistente    = $this->input->post('aplicador_cod_asistente');
            $status     = $this->modelAplicador->eliminar_aplicador($cod_asistente);

            $this->compruebaStatus($status, 'eliminado');
        } else {
            if ($this->input->is_ajax_request()) {

                $cod_asistente        = $this->input->get('id');
                $datos['asistentes'] = $this->modelAsistente->obtener_asistente($cod_asistente);
                $datos['aplicadores'] = $this->modelAplicador->obtener_aplicadores($cod_asistente);

                $this->load->view('asistente/aplicador/eliminar_vista', $datos);
            }
        }
    }

}

/* End of file asistenteap_controlador.php */
/* Location: ./application/controllers/asistente/asistenteap_controlador.php */