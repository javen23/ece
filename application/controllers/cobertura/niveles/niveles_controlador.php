<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Niveles_controlador extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $datos['titulo']        = "Niveles";
        $datos['contenido']     = "cobertura/niveles/index_niveles_vista";
        $this->load->view('plantilla', $datos);
    }

    public function nivel_uno() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array('plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min'));
        $datos['titulo']        = "Capacitación de nivel I";
        $datos['contenido']     = "cobertura/niveles/nivel_uno/index_nivel_uno_vista";
        $this->load->view('plantilla', $datos);
    }

    public function nivel_dos() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array('plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min'));
        $datos['titulo']        = "Capacitación de nivel II";
        $datos['contenido']     = "cobertura/niveles/nivel_dos/index_nivel_dos_vista";
        $this->load->view('plantilla', $datos);
    }

    public function nivel_tres() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array('plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min'));
        $datos['titulo']        = "Capacitación de nivel III";
        $datos['contenido']     = "cobertura/niveles/nivel_tres/index_nivel_tres_vista";
        $this->load->view('plantilla', $datos);
    }

}
