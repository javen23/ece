<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cobertura_controlador extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $datos['titulo']    = "Módulo de Cobertura";
        $datos['contenido'] = "cobertura/index_cobertura_vista";
        $this->load->view('plantilla', $datos);
    }

}