<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Instructor_controlador extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('cobertura/instructor/instructor_model', 'modelInstructor');
    }

    public function index() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array('plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min'));
        $datos['titulo']        = "Capacitación de Instructores";
        $datos['contenido']     = "cobertura/instructor/index_instructor_vista";
        $this->load->view('plantilla', $datos);
    }
}
