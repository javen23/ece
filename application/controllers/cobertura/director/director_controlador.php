<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Director_controlador extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('cobertura/director/director_model', 'modelDirector');
    }

    public function index() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array('plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min'));
        $datos['titulo']        = "Capacitación de Directores";
        $datos['contenido']     = "cobertura/director/index_director_vista";
        $this->load->view('plantilla', $datos);
    }
}
