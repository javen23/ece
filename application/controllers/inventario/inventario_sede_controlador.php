<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventario_sede_controlador extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('inventario/inventario_sede_model', 'modeloInventarioSede');
        $this->isRol = $this->session->userdata('userIdRol');
    }

    public function index() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'jqueryvalidation/dist/jquery.validate',
            'jqueryvalidation/dist/additional-methods.min',
            'facebox/src/facebox',
            'jquery-ui.min'));

        $datos['titulo'] = "Inventario sede";
        $datos['contenido'] = 'inventario/sede/listado_inventario_sede_vista';
        $datos['material_sede'] = $this->modeloInventarioSede->lista_material_sede();
        $this->load->view('plantilla', $datos);
    }

    public function lector(){

        if ($this->input->is_ajax_request()) {
            $this->load->view('inventario/sede/lector_inventario_sede_vista');
        }

    }

}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */