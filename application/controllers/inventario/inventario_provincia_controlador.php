<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventario_provincia_controlador extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('inventario/inventario_provincia_model', 'modeloInventarioProvincia');
        $this->isRol = $this->session->userdata('userIdRol');
    }

    public function index() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'jqueryvalidation/dist/jquery.validate',
            'jqueryvalidation/dist/additional-methods.min',
            'facebox/src/facebox',
            'jquery-ui.min'));

        $datos['titulo'] = "Inventario provincia";
        $datos['contenido'] = 'inventario/provincia/listado_inventario_provincia_vista';
        $datos['material_provincia'] = $this->modeloInventarioProvincia->lista_material_provincia();
        $this->load->view('plantilla', $datos);
    }

    public function lector(){

        if ($this->input->is_ajax_request()) {
            $this->load->view('inventario/provincia/lector_inventario_provincia_vista');
        }

    }

}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */