<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventario_imprenta_controlador extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('inventario/inventario_imprenta_model', 'modeloInventarioImprenta');
        $this->isRol = $this->session->userdata('userIdRol');
    }

    //cobertura
    public function index() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'jqueryvalidation/dist/jquery.validate',
            'jqueryvalidation/dist/additional-methods.min',
            'facebox/src/facebox',
            'jquery-ui.min'));

        $datos['titulo'] = "Inventario imprenta";
        $datos['contenido'] = 'inventario/imprenta/listado_inventario_imprenta_vista';
        $datos['material_imprenta'] = $this->modeloInventarioImprenta->lista_material_imprenta();
        $this->load->view('plantilla', $datos);
    }

    public function lector(){

        if(isset($_REQUEST['crearLector']) && ($this->input->post('hidden_name_lector') == "valorHiddenLector")){
            $_REQUEST['lastIP'] = $this->input->ip_address();
            $_REQUEST['navegador'] = $this->input->user_agent();
            print_r($_REQUEST);
            exit;
            redirect('inventario','refresh');
        }else{
            if ($this->input->is_ajax_request()) {
                $this->load->view('inventario/imprenta/lector_inventario_imprenta_vista');
            }
        }

    }

    public function acta_envio(){
        if ($this->input->is_ajax_request()){
            $datos['sedes']=$this->modeloInventarioImprenta->traer_todas_las_sedes();
            $this->load->view('inventario/imprenta/acta_envio_inventario_imprenta_vista',$datos);
        }

    }

}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */