<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Usuario_controlador extends CI_Controller {

   public $mensaje;

   public function __construct() {
      parent::__construct();
      $this->load->model('usuario/usuario_model', 'modeloUsuario');
      $this->is_rol = $this->session->userdata('userIdRol');
      $this->mensaje  = array('tipo'=>'success', 'cuerpo'=>'creado exitosamente');
      if ($this->is_rol != 1) {
         redirect("", "refresh");
      }
   }

   public function index() {
      add_css(array('datatables/dataTables.bootstrap','iCheck/all'));
      add_js(array('plugins/datatables/jquery.dataTables.min',
         'plugins/datatables/dataTables.bootstrap.min',
         'jqueryvalidation/dist/jquery.validate',
         'jqueryvalidation/dist/additional-methods.min',
         'facebox/src/facebox',
         'jquery-ui.min'));
      add_js_(array('plugins/moment/min/moment.min',
         'plugins/moment/locale/es',));
         //$datos['totalRol'] = $this->modeloUsuario->todos_los_roles();
         $datos['totalUsuarios'] = $this->modeloUsuario->todos_los_usuarios();
         $datos['titulo'] = "Lista de usuarios";
         $datos['contenido'] = 'usuario/listado_usuario_vista';
         $this->load->view('plantilla', $datos);
   }

   public function crear() {
      if (isset($_REQUEST['crearUsuario']) && $this->input->post('valorUsuario') == 'valorCrearUsuario') {
         $this->ip_navegador();
         $_REQUEST['user_crear']        = $this->security->xss_clean(strip_tags(strtolower(trim(notildes($this->input->post('user_crear'))))));
         $_REQUEST['nombres_crear']     = $this->security->xss_clean(strip_tags(strtoupper(trim(notildes($this->input->post('nombres_crear', TRUE))))));
         $_REQUEST['apellidos_crear']   = $this->security->xss_clean(strip_tags(strtoupper(trim(notildes($this->input->post('apellidos_crear', TRUE))))));
         $status                        = $this->modeloUsuario->insertar_usuario($_REQUEST);
         $this->estado_accion($status, 'creado');
      } else {
         if ($this->input->is_ajax_request()) {
            $datos['totalRol'] = $this->modeloUsuario->todos_los_roles();
            $this->load->view('usuario/crear_usuario_vista', $datos);
         }
      }
   }

   public function editar() {
      if (isset($_REQUEST['editarUsuario']) and $this->input->post('valorUsuario') == 'valorEditarUsuario') {
         $this->ip_navegador();
         $_REQUEST['user_editar']       = $this->security->xss_clean(strip_tags(strtolower(trim(notildes($this->input->post('user_editar'))))));
         $_REQUEST['nombres_editar']    = $this->security->xss_clean(strip_tags(strtoupper(trim(notildes($this->input->post('nombres_editar', TRUE))))));
         $_REQUEST['apellidos_editar']  = $this->security->xss_clean(strip_tags(strtoupper(trim(notildes($this->input->post('apellidos_editar', TRUE))))));
         $status                        = $this->modeloUsuario->actualizar_usuario($_REQUEST);
         $this->estado_accion($status, 'editado');
      } else {
         if ($this->input->is_ajax_request()) {
            $id_usuario         = $this->input->get('idUsuario');
            $datos['totalRol']  = $this->modeloUsuario->todos_los_roles();
            $datos['usuario']   = $this->modeloUsuario->seleccionar_usuario($id_usuario);
            $this->load->view('usuario/editar_usuario_vista', $datos);
         }
      }
   }

   private function ip_navegador(){
      $_REQUEST['lastIP']       = $this->input->ip_address();
      $_REQUEST['navegador']    = $this->input->user_agent();
   }

   private function estado_accion($status,$accion){
      if ($status) {
         $this->mensaje['cuerpo'] = "El usuario ha sido {$accion}";
      } else {
         $this->mensaje['tipo']   = 'warning';
         $this->mensaje['cuerpo'] = "El usuario NO ha sido {$accion}";
         //log_message('ERROR', $status . "Usuario no pudo ser {$accion}");
      }
      $this->session->set_flashdata('mensaje_flash', $this->mensaje);
      redirect('usuarios','refresh');
   }

    public function verificar() {

        if(isset($_REQUEST['dni_crear'])){
            $_REQUEST['dni_crear']  = $this->input->post('dni_crear');
        }elseif (isset($_REQUEST['user_crear'])){
            $_REQUEST['user_crear'] = $this->input->post('user_crear');
        }
        $verificauser = $this->modeloUsuario->verificar_nombre_usuario($_REQUEST);
        if ($verificauser) {
         echo "true";
        }else{
         echo "false";
        }
    }

   public function estado() {
      $this->ip_navegador();
      $status = $this->modeloUsuario->actualizar_estado($_REQUEST);
      $this->estado_accion($status, 'cambiado de estado');
   }

}

   /* End of file Usuario_controlador.php */
   /* Location: ./application/controllers/Usuario_controlador.php */
