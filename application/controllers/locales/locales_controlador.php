<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locales_controlador extends CI_Controller {

    public $mensaje;
    //public $EmailSender;

    public function __construct() {
        parent::__construct();
        // $this->load->library('EmailSender.php');
        $this->isRol            = $this->session->userdata('userIdRol');
        $this->sedeOperativa    = $this->session->userdata('sedeOperativa');
        $this->mensaje  = array('tipo'=>'success', 'cuerpo'=>'Local creado exitosamente');
    }

    public function index() {
        $datos['titulo']    = "LOCALES";
        $datos['contenido'] = 'locales/index_locales_vista';
        $this->load->view('plantilla', $datos);
    }

    public function reportes() {
        $datos['titulo']    = "Reportes de Consecución de Locales";
        $datos['contenido'] = 'locales/reportes/index_vista';
        $this->load->view('plantilla', $datos);
    }

    public function reporte_directorio() {
        add_css(array('datatables/dataTables.bootstrap','iCheck/all'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
        ));
        $this->load->model('locales/locales_administrativo_model', 'modeloLocal');
        $datos['locales'] =  $this->modeloLocal->todos_los_locales($this->sedeOperativa);
        $datos['titulo']    = "Directorio de Locales";
        $datos['contenido'] = 'locales/reportes/directorio_vista';
        $this->load->view('plantilla', $datos);
    }

    public function reporte_mapa_regional() {
        add_js_(array(
            '//maps.google.com/maps/api/js?sensor=false&foo=bar',
            '//www.google.com/jsapi?foooo=bar',
            '//geoxml3.googlecode.com/svn/branches/polys/geoxml3.js?foooo=bar',
            'jquery-ui.min',
            // 'plugins/highcharts/highcharts',
            'plugins/highcharts/highstock',
            'plugins/highcharts/export/exporting',
        ));

        $this->load->library('Graph_Generator');

        $this->load->model('ajax/ajax_model', 'modeloAjax');
        $this->load->model('locales/locales_administrativo_model', 'modeloLocal');
/*
        $locales = $this->modeloLocal->todos_los_locales($this->sedeOperativa, 'LZ');
        $condicion_local_lista  = $this->modeloLocal->graph()->condicion_local('LZ');
        $area_local_lista       = $this->modeloLocal->graph()->area_local('LZ');
        $nro_ambiente_lista     = $this->modeloLocal->graph()->nro_ambiente('LZ');

        $datos_graficos = array(
            'nro_ambientes'  => $nro_ambiente_lista,
            'condicion_local' => $condicion_local_lista,
            'area_local'    => $area_local_lista,
            'consecucion_meta' => array(
                'regional' => $this->modeloLocal->graph()->consecucion_regional(),
                'provincial' => $this->modeloLocal->graph()->consecucion_provincial(),
                'distrital'  => $this->modeloLocal->graph()->consecucion_distrital(),
            ),
        );
*/        
        $datos_graficos = array(
            'nro_ambientes'  => "1",
            'condicion_local' => "Prestamo",
            'area_local'    => "Entre 50m2 y 120m2",
            'consecucion_meta' => array(
                'regional' => array("cod_sede_operativa" =>"1","titulo"=>"AMAZONAS","local"=>"2"),
                'provincial' => array("cod_sede_operativa" =>"1","cod_sede_prov" =>"1","sede_prov"=>"BAGUA GRANDE","local"=>"2"),
                'distrital'  => array("cod_sede_operativa" =>"1","cod_sede_prov" =>"1","cod_sede_dist" =>"1","sede_prov"=>"GRANDE","local"=>"2"),
            ),
        );

        //$datos['locales']   = json_encode($locales);//json_encode(array('latitud'=>array_column($locales, 'gps_latitud'), 'longitud'=>array_column($locales, 'gps_longitud')));
        $datos['graph']     = $this->graph_generator->generar($datos_graficos, 'Regional');
        //$datos['sedes']     = $this->modeloAjax->todas_las_sedes();
        $datos['titulo']      = "Indicador Regional de Locales";
        $datos['contenido']   = 'locales/reportes/mapa_region_vista';
        $this->load->view('plantilla', $datos);
    }

    public function reporte_mapa_provincial()
    {
        add_js_(array(
            '//maps.google.com/maps/api/js?sensor=false&foooo=bar',
            '//www.google.com/jsapi?foooo=bar',
            '//geoxml3.googlecode.com/svn/branches/polys/geoxml3.js?foooo=bar',
            'plugins/highcharts/highcharts',
        ));
        add_js_(array(
            'jquery-ui.min',
        ));

        $this->load->library('Graph_Generator');
        $this->load->model('locales/locales_administrativo_model', 'modeloLocal');
        $this->load->model('ajax/ajax_model', 'modeloAjax');

        $locales                = $this->modeloLocal->todos_los_locales($this->sedeOperativa, 'OF');
        $seguridad_lista        = $this->modeloLocal->graph()->seguridad('OF');
        $condicion_local_lista  = $this->modeloLocal->graph()->condicion_local('OF');
        $area_local_lista       = $this->modeloLocal->graph()->area_local('OF');
        $nro_ambiente_lista     = $this->modeloLocal->graph()->nro_ambiente('OF');

        $datos_graficos = array(
            'nro_ambientes'  => $nro_ambiente_lista,
            'area_local' => $area_local_lista,
            'seguridad' => $seguridad_lista,
            'condicion_local' => $condicion_local_lista,
        );

        $datos['locales']       = json_encode($locales);
        $datos['graph']         = $this->graph_generator->generar($datos_graficos, 'Provincial/Distrital');
        $datos['sedes']         = $this->modeloAjax->todas_las_sedes();
        $datos['titulo']        = "Indicador Provincial / Distrital de Locales";
        $datos['contenido']     = 'locales/reportes/mapa_prov_dist_vista';

        $this->load->view('plantilla', $datos);
    }

    public function actualizar_gps_local() {
        $this->load->model('locales/locales_administrativo_model', 'modeloLocal');

        $id_local   = $this->input->get('id_local');
        $direccion  = $this->input->get('direccion');

        $ruta   = 'http://maps.google.com/maps/api/geocode/json?sensor=false&address='.urlencode($direccion);

        $res    = json_decode(file_get_contents($ruta), true);

        if ($res['status'] !== 'OK') {
            return $this->respuestaJson(array('success'=>false));
        }

        $gps    = array('lat'=>$res['results'][0]['geometry']['location']['lat'], 'lng'=>$res['results'][0]['geometry']['location']['lng']);
        $status = $this->modeloLocal->actualizar_gps($id_local, $gps);

        return $this->respuestaJson(array('success'=>$status,'id'=>$id_local));
    }

    private function respuestaJson($data) {
        $this->output
            ->set_content_type('application/json;charset=utf-8')
            ->set_output(json_encode($data));
    }
}
