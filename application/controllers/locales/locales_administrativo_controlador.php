<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locales_Administrativo_controlador extends CI_Controller {

    public $mensaje;
    protected $titulo = 'Jurisdicción Provincial/Distrital';

    public function __construct() {
        parent::__construct();

        $this->load->model('locales/locales_administrativo_model', 'modeloLocalAdministrativo');

        $this->isRol            = $this->session->userdata('userIdRol');
        $this->sedeOperativa    = $this->session->userdata('sedeOperativa');
        $this->mensaje  = array('tipo'=>'success', 'cuerpo'=>'Local creado exitosamente');

        $this->load->library('DataSelect');
        //$this->output->enable_profiler(true);
    }

    public function index() {
        add_css(array('datatables/dataTables.bootstrap','iCheck/all'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'jqueryvalidation/dist/jquery.validate',
            'jqueryvalidation/dist/additional-methods.min',
            'plugins/input-mask/jquery.inputmask',
            'plugins/input-mask/jquery.inputmask.extensions',
            'inei.validation',
            'facebox/src/facebox',
            'jquery-ui.min'));

        $datos['locales']   = $this->modeloLocalAdministrativo->todos_los_locales($this->sedeOperativa, $tipo = 'OF');
        $datos['titulo']    = "Local de ".$this->titulo;
        $datos['title']     = $this->titulo;
        $datos['contenido'] = 'locales/administrativo/listado_vista';

        $this->load->view('plantilla', $datos);
    }

    public function crear() {
        add_css(array('iCheck/all'));
        add_js_('//maps.google.com/maps/api/js?sensor=false&foo=bar');
        add_js(array(
            // '//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min',
            // '//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min',
            // 'plugins/fileupload/js/jquery.iframe-transport',
            // 'plugins/fileupload/js/jquery.fileupload',
            // 'plugins/fileupload/js/jquery.fileupload-process',
            // 'plugins/fileupload/js/jquery.fileupload-image',
            // 'plugins/fileupload/js/jquery.fileupload-validate',
            // 'inei.fileupload',
            'plugins/iCheck/icheck.min',
            'jqueryvalidation/dist/jquery.validate',
            'jqueryvalidation/dist/additional-methods.min',
            'plugins/input-mask/jquery.inputmask',
            'plugins/input-mask/jquery.inputmask.extensions',
            'inei.validation',
            'facebox/src/facebox',
            'jquery-ui.min',
        ));

        if(isset($_REQUEST['crearLocal']) && ($this->input->post('hidden_name_local') == "valorHiddenLocal")){

            $status = $this->modeloLocalAdministrativo->insertar_local($this->input->post(NULL, TRUE));

            $this->compruebaStatus($status, 'creado');
        } else {

            $this->load->model('ajax/ajax_model', 'modeloAjax');

            $datos['sedes']     = $this->modeloAjax->todas_las_sedes($this->sedeOperativa);
            $datos['sede_provinciales'] = array();
            $datos['sede_distritales'] = array();

            $datos['departamentos'] = array();
            $datos['provincias'] = array();
            $datos['distritos'] = array();

            $datos['titulo']    = "Crear Local de ".$this->titulo;
            $datos['title']     = $this->titulo;
            $datos['contenido'] = 'locales/administrativo/crear_vista';

            $this->load->view('plantilla', $datos);
        }

    }

    public function editar() {
        add_css(array('iCheck/all'));
        add_js_('//maps.google.com/maps/api/js?sensor=false&foo=bar');
        add_js(array(
            // '//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min',
            // '//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min',
            // 'plugins/fileupload/js/jquery.iframe-transport',
            // 'plugins/fileupload/js/jquery.fileupload',
            // 'plugins/fileupload/js/jquery.fileupload-process',
            // 'plugins/fileupload/js/jquery.fileupload-image',
            // 'plugins/fileupload/js/jquery.fileupload-validate',
            // 'inei.fileupload',
            'plugins/iCheck/icheck.min',
            'jqueryvalidation/dist/jquery.validate',
            'jqueryvalidation/dist/additional-methods.min',
            'plugins/input-mask/jquery.inputmask',
            'plugins/input-mask/jquery.inputmask.extensions',
            'inei.validation',
            'facebox/src/facebox',
            'jquery-ui.min',
        ));

        if(isset($_REQUEST['editarLocal']) && ($this->input->post('local_id_local') !== '')){

            $idLocal    = $this->input->post('local_id_local');
            $status     = $this->modeloLocalAdministrativo->editar_local($this->input->post(NULL, TRUE), $idLocal);

            $this->compruebaStatus($status, 'editado');
        } else {

            $this->load->model('ajax/ajax_model', 'modeloAjax');

            $idLocal        = $this->input->get('idLocal');

            $datos['local'] = $this->modeloLocalAdministrativo->obtener_local($idLocal);

            $datos['sedes']         = $this->modeloAjax->todas_las_sedes($this->sedeOperativa);
            $datos['sede_provinciales'] = $this->modeloAjax->obtener_sede_provincial(array('sede_operativa'=>$datos['local']['cod_sede_operativa']));
            $datos['sede_distritales'] = $this->modeloAjax->obtener_sede_distrital_por_distrito($datos['local']['ccdi'], $datos['local']['ccpp'], $datos['local']['ccdd'], $datos['local']['cod_sede_operativa']);

            $datos['departamentos'] = $this->modeloAjax->obtener_departamentos_por_sede($datos['local']['cod_sede_operativa']);
            $datos['provincias']    = $this->modeloAjax->obtener_provincias_por_departamento($datos['local']['ccdd'], $datos['local']['cod_sede_operativa']);
            $datos['distritos']     = $this->modeloAjax->obtener_distritos_por_provincia($datos['local']['ccpp'], $datos['local']['ccdd'], $datos['local']['cod_sede_operativa']);

            $datos['titulo']    = "Editar Local de ".$this->titulo;
            $datos['title']     = $this->titulo;
            $datos['contenido'] = 'locales/administrativo/editar_vista';

            $this->load->view('plantilla', $datos);
        }

    }

    public function eliminar() {

        if(isset($_REQUEST['eliminarLocal']) && ($this->input->post('local_id_local') !== '')){

            $idLocal    = $this->input->post('local_id_local');
            $status     = $this->modeloLocalAdministrativo->eliminar_local($idLocal);

            $this->compruebaStatus($status, 'eliminado');
        } else {
            if ($this->input->is_ajax_request()) {

                $idLocal        = $this->input->get('idLocal');
                $datos['local'] = $this->modeloLocalAdministrativo->obtener_local($idLocal);

                $this->load->view('locales/administrativo/eliminar_vista', $datos);
            }
        }
    }

    public function enviar_oficio() {

        // if(isset($_REQUEST['eliminarLocal']) && ($this->input->post('local_id_local') !== '')){

        //     $idLocal    = $this->input->post('local_id_local');
        //     $status     = $this->modeloLocalAdministrativo->eliminar_local($idLocal);

        //     $this->compruebaStatus($status, 'eliminado');
        // } else {
            if ($this->input->is_ajax_request()) {
                $datos['destinatarios'] = "<cfrasinetti@inei.gob.pe>,<lcastro@inei.gob.pe>,<gmoran@inei.gob.pe>";
                $datos['oficio_cuerpo'] = "Estimados Señores,

                    Por la presente hago constancia del costo total de S/. {$this->input->get('costo')} del Local de Jurisdicción Provincial/Distrital
                    según el siguiente detalle:
                    {$this->input->get('detalle')}

                    Atentamente.

                    Nombre de Usuario
                    Cargo";

                $this->load->view('locales/administrativo/enviar_oficio_vista', $datos);
            }
        // }
    }

    private function compruebaStatus($status, $accion, $ruta = 'locales/jurisdiccion-provincial-distrital') {
        if ($status) {
            $this->mensaje['cuerpo'] = "El local ha sido {$accion}";
        } else {
            $this->mensaje['tipo']   = 'warning';
            $this->mensaje['cuerpo'] = "El local NO ha sido {$accion}";

            log_message('ERROR', $status . "Local no pudo ser {$accion}");
        }

        $this->session->set_flashdata('mensaje_flash', $this->mensaje);

        redirect($ruta);
    }

    public function exportar()
    {
        $this->load->library('PHPExcel');

        $phpExcel = new PHPExcel();
        $prestasi = $phpExcel->setActiveSheetIndex(0);
        //Estilo para el border
        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '7f8c8d')
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el titulo
        $titulo = array(
            'font' => array(
                'bold' => true,
                'size' => 28
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el subtitulo
        $subtitulo = array(
            'font' => array(
                'bold' => true,
                'size' => 24
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        //Estilo para el fondo de la celda
        $backFondo = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap' => true
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => 'DDDDDD')
                ),
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '3498db')
            ),
        );
        //fusionando titulo
        $tabTitulo = 1;
        $phpExcel->getActiveSheet()->mergeCells('A' . $tabTitulo . ':H' . $tabTitulo);
        $phpExcel->getActiveSheet()->getStyle('A' . $tabTitulo . ':H' . $tabTitulo)->applyFromArray($titulo);
        //fusionanado subtitulo
        $tabSubtitulo = 2;
        $phpExcel->getActiveSheet()->mergeCells('A' . $tabSubtitulo . ':H' . $tabSubtitulo);
        $phpExcel->getActiveSheet()->getStyle('A' . $tabSubtitulo . ':H' . $tabSubtitulo)->applyFromArray($subtitulo);

        //fusionando Tab2
        $tab2 = 4;
        $phpExcel->getActiveSheet()->getStyle('A' . $tab2 . ':H' . $tab2)->applyFromArray($backFondo);
        //altura en la fila
        $phpExcel->getActiveSheet()->getRowDimension($tabTitulo)->setRowHeight(46);
        $phpExcel->getActiveSheet()->getRowDimension($tabSubtitulo)->setRowHeight(33);
        $phpExcel->getActiveSheet()->getRowDimension($tab2)->setRowHeight(38);
        //Titulo de nombres pie excel
        $prestasi->setCellValue('A' . $tabTitulo, 'EVALUACIÓN CENSAL DE ESTUDIANTES - 2015');
        $prestasi->setCellValue('A' . $tabSubtitulo, 'LISTADO DE LOCALES DE '.mb_strtoupper($this->titulo, 'UTF-8'));
        //border
        $phpExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
//        $phpExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

        $numerNombre = 4;
        $prestasi->setCellValue('A' . $numerNombre, 'N°');
        $prestasi->setCellValue('B' . $numerNombre, 'SEDE OPERATIVA');
        $prestasi->setCellValue('C' . $numerNombre, 'SEDE PROVINCIAL');
        $prestasi->setCellValue('D' . $numerNombre, 'SEDE DISTRITAL');
        $prestasi->setCellValue('E' . $numerNombre, 'DEPARTAMENTO');
        $prestasi->setCellValue('F' . $numerNombre, 'PROVINCIA');
        $prestasi->setCellValue('G' . $numerNombre, 'DISTRITO');
        $prestasi->setCellValue('H' . $numerNombre, 'DIRECCIÓN DE LA OFICINA');
        $prestasi->setCellValue('I' . $numerNombre, 'REFERENCIA');


//        ($this->input->get('local') != "") ? $local = $this->input->get('local') : $local = 0;
        $data = $this->modeloLocalAdministrativo->todos_los_locales($this->sedeOperativa);

        $rowexcel = 4;
        $numero = 0;
        foreach ($data as $row) {
            $rowexcel++;
            $numero++;
            $prestasi->getStyle('A' . $rowexcel . ':H' . $rowexcel)->applyFromArray($borders);
            $prestasi->setCellValue('A' . $rowexcel, $numero)->getStyle('A' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('B' . $rowexcel, $row['sede_operativa'])->getStyle('B' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('C' . $rowexcel, $row['sede_prov'])->getStyle('C' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('D' . $rowexcel, $row['sede_dist'])->getStyle('D' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('E' . $rowexcel, $row['departamento'])->setCellValueExplicit('E' . $rowexcel, $row['departamento'], PHPExcel_Cell_DataType::TYPE_STRING);
//            $prestasi->setCellValue('F' . $rowexcel, $row['APELLIDOS'])->getStyle('F' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('F' . $rowexcel, $row['provincia'])->getStyle('F' . $rowexcel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $prestasi->setCellValue('G' . $rowexcel, $row['distrito']);
            $prestasi->setCellValue('H' . $rowexcel, $row['direccion']);
        }

        $prestasi->setTitle("Local de ".$this->titulo);
        header("Content-Type: application/vnd.ms-excel");
        $nameFile = "local_de_jurisdiccion_regional_provincial_" . date("d-m-Y_his");
        header("Content-Disposition: attachment; filename=\"$nameFile.xls\"");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel5");
        $objWriter->save("php://output");
    }

}
