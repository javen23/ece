<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locales_capacitacion_controlador extends CI_Controller {

    public $mensaje;
    //public $EmailSender;

    public function __construct() {
        parent::__construct();

        $this->load->model('locales/locales_capacitacion_model', 'modeloLocal');
        $this->load->library('EmailSender.php');

        $this->isRol            = $this->session->userdata('userIdRol');
        $this->sedeOperativa    = $this->session->userdata('sedeOperativa');
        $this->mensaje  = array('tipo'=>'success', 'cuerpo'=>'Local creado exitosamente');
        //$this->output->enable_profiler(true);
    }

    public function index() {
        add_css(array('datatables/dataTables.bootstrap','iCheck/all'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'jqueryvalidation/dist/jquery.validate.min',
            'jqueryvalidation/dist/additional-methods.min',
            'plugins/input-mask/jquery.inputmask',
            'plugins/input-mask/jquery.inputmask.extensions',
            'inei.validation',
            'facebox/src/facebox',
            'jquery-ui.min'));

        $datos['locales']   = $this->modeloLocal->todos_los_locales($this->sedeOperativa);
        $datos['titulo']    = "Consecución de Locales";
        $datos['contenido'] = 'locales/capacitacion/listado_vista';

        $this->load->view('plantilla', $datos);
    }

    public function crear() {

        if(isset($_REQUEST['crearLocal']) && ($this->input->post('hidden_name_local') == "valorHiddenLocal")){

            $status = $this->modeloLocal->insertar_local($this->input->post(NULL, TRUE));

            $this->compruebaStatus($status, 'creado');
        } else {
            if ($this->input->is_ajax_request()) {

                $this->load->model('ajax/ajax_model', 'modeloAjax');

                $datos['sedes']     = $this->modeloAjax->todas_las_sedes($this->sedeOperativa);

                $datos['departamentos'] = array();
                $datos['provincias'] = array();
                $datos['distritos'] = array();
                $this->load->view('locales/capacitacion/crear_vista', $datos);
            }
        }

    }

    public function editar() {

        if(isset($_REQUEST['editarLocal']) && ($this->input->post('local_id_local') !== '')){

            $idLocal    = $this->input->post('local_id_local');
            $status     = $this->modeloLocal->editar_local($this->input->post(NULL, TRUE), $idLocal);

            $this->compruebaStatus($status, 'editado');
        } else {
            if ($this->input->is_ajax_request()) {

                $this->load->model('ajax/ajax_model', 'modeloAjax');
                $idLocal        = $this->input->get('idLocal');

                $datos['local'] = $this->modeloLocal->obtener_local($idLocal);

                $datos['sedes'] = $this->modeloAjax->todas_las_sedes($this->sedeOperativa);
                $datos['departamentos'] = $this->modeloAjax->obtener_departamentos_por_sede($datos['local']['cod_sede_operativa']);
                $datos['provincias'] = $this->modeloAjax->obtener_provincias_por_departamento($datos['local']['ccdd'], $datos['local']['cod_sede_operativa']);
                $datos['distritos'] = $this->modeloAjax->obtener_distritos_por_provincia($datos['local']['ccpp'], $datos['local']['ccdd'], $datos['local']['cod_sede_operativa']);

                $this->load->view('locales/capacitacion/editar_vista', $datos);
            }
        }

    }

    public function eliminar() {

        if(isset($_REQUEST['eliminarLocal']) && ($this->input->post('local_id_local') !== '')){

            $idLocal    = $this->input->post('local_id_local');
            $status     = $this->modeloLocal->eliminar_local($idLocal);

            $this->compruebaStatus($status, 'eliminado');
        } else {
            if ($this->input->is_ajax_request()) {

                $idLocal        = $this->input->get('idLocal');
                $datos['local'] = $this->modeloLocal->obtener_local($idLocal);

                $this->load->view('locales/capacitacion/eliminar_vista', $datos);
            }
        }
    }

    private function compruebaStatus($status, $accion) {
        if ($status) {
            $this->mensaje['cuerpo'] = "El local ha sido {$accion}";
        } else {
            $this->mensaje['tipo']   = 'warning';
            $this->mensaje['cuerpo'] = "El local NO ha sido {$accion}";

            log_message('ERROR', $status . "Local no pudo ser {$accion}");
        }

        $this->session->set_flashdata('mensaje_flash', $this->mensaje);

        redirect('locales-capacitacion');
    }

}
