<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locales_Odei_controlador extends CI_Controller {

    public $mensaje;
    protected $titulo = 'Jurisdicción regional';

    public function __construct() {
        parent::__construct();
        $this->load->model('locales/locales_odei_model', 'modeloLocalOdei');
        $this->load->library('DataSelect');
        $this->isRol            = $this->session->userdata('userIdRol');
        $this->sedeOperativa    = $this->session->userdata('sedeOperativa');
        $this->mensaje  = array('tipo'=>'success', 'cuerpo'=>'Local creado exitosamente');
    }

    public function index() {
        add_css(array('datatables/dataTables.bootstrap'));
        add_js(array(
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'facebox/src/facebox',
            'jquery-ui.min'
        ));
/*        $datos['locales']   = $this->modeloLocalOdei->todos_los_locales($this->sedeOperativa, $tipo = 'LZ');*/

        $datos['locales']   = array("sede_operativa" => "lala", 
"sede_prov" => "lala",
"sede_dist" => "lala",
"departamento" => "lala",
"provincia" => "lala",
"distrito" => "lala",
"id_oficina" => "1",
"cod_sede_operativa" => "lala",
"cod_sede_prov" => "lala",
"cod_sede_dist" => "lala",
"ccdd" => "lala",
"ccpp" => "lala",
"ccdi" => "lala",
"tipo" => "lala",
"direccion" => "lala",
"referencia" => "lala",
"observacion" => "lala",
"nombres" => "lala",
"cargo" => "lala",
"telef_fijo" => "lala",
"telef_celular" => "lala",
"telef_rpm" => "lala",
"email" => "lala",
"area" => "lala",
"nro_ambiente" => "lala",
"area_almacen" => "lala",
"nro_escritorio" => "lala",
"nro_mesa" => "lala",
"nro_silla" => "lala",
"pc" => "lala",
"internet" => "1",
"internet_tipo" => "2",
"internet_velocidad" => "100",
"electricidad" => "lala",
"tipo_construc" => "lala",
"seguridad" => "lala",
"turnos" => "lala",
"turnos_presupuesto" => "lala",
"costos" => "lala",
"costos_mantenimiento" => "lala",
"costos_mobiliario" => "lala",
"costos_internet" => "lala",
"costos_local" => "lala",
"gps_latitud" => "lala",
"gps_longitud" => "lala",
"observacion_local" => "lala");


        $datos['titulo']    = "Local de ".$this->titulo;
        $datos['contenido'] = 'locales/odei/listado_vista';
        $this->load->view('plantilla', $datos);
    }

    public function crear() {
        if(isset($_REQUEST['crearLocal']) && ($this->input->post('hidden_name_local') == "valorHiddenLocal")){
            $status = $this->modeloLocalOdei->insertar_local($this->input->post(NULL, TRUE));
            //print_r($_REQUEST);
            $this->compruebaStatus($status, 'creado');
        } else {
            add_css(array('iCheck/all'));
            add_js_('//maps.google.com/maps/api/js?sensor=false&foo=bar');
            add_js(array(
                'jquery-ui.min',
                '//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min',
            '//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min',
            'plugins/fileupload/js/jquery.iframe-transport',
            'plugins/fileupload/js/jquery.fileupload',
            'plugins/fileupload/js/jquery.fileupload-process',
            'plugins/fileupload/js/jquery.fileupload-image',
            'plugins/fileupload/js/jquery.fileupload-validate',
            'inei.fileupload',
                'plugins/iCheck/icheck.min',
                'jqueryvalidation/dist/jquery.validate',
                'jqueryvalidation/dist/additional-methods.min',
                'plugins/input-mask/jquery.inputmask',
                'plugins/input-mask/jquery.inputmask.extensions',
                'inei.validation',
                'facebox/src/facebox',

            ));
            $this->load->model('ajax/ajax_model', 'modeloAjax');
            //$datos['sedes']     = $this->modeloAjax->todas_las_sedes($this->sedeOperativa);
            $datos['titulo']    = "Crear Local de ".$this->titulo;
            $datos['title']     = $this->titulo;
            $datos['contenido'] = 'locales/odei/crear_vista';
            $this->load->view('plantilla', $datos);
        }
    }

    public function editar() {

        $idLocal    = $this->input->get('idLocal');
        if(isset($_REQUEST['editarLocal']) && ($this->input->post('local_id_local') !== '')){
            //print_r($_REQUEST);
            $idLocal = $this->input->post('local_id_local');
            $status = $this->modeloLocalOdei->editar_local($this->input->post(NULL, TRUE), $idLocal);
            $this->compruebaStatus($status, 'creado');

        } else {
            add_css(array('iCheck/all'));
            add_js_('//maps.google.com/maps/api/js?sensor=false&foo=bar');
            add_js(array(
                'plugins/iCheck/icheck.min',
                'jqueryvalidation/dist/jquery.validate',
                'jqueryvalidation/dist/additional-methods.min',
                'plugins/input-mask/jquery.inputmask',
                'plugins/input-mask/jquery.inputmask.extensions',
                'inei.validation',
                'facebox/src/facebox',
                'jquery-ui.min',
            ));
            $this->load->model('ajax/ajax_model', 'modeloAjax');
            $datos['local'] = $this->modeloLocalOdei->obtener_local($idLocal);
            $datos['sedes'] = $this->modeloAjax->todas_las_sedes($this->sedeOperativa);
            $datos['departamentos'] = $this->modeloAjax->obtener_departamentos_por_sede($datos['local']['cod_sede_operativa']);
            $datos['provincias'] = $this->modeloAjax->obtener_provincias_por_departamento($datos['local']['ccdd'], $datos['local']['cod_sede_operativa']);
            $datos['distritos'] = $this->modeloAjax->obtener_distritos_por_provincia($datos['local']['ccpp'],$datos['local']['ccdd'],$datos['local']['cod_sede_operativa']);
            $datos['titulo']    = "Editar Local de ".$this->titulo;
            $datos['title']     = $this->titulo;
            $datos['contenido'] = 'locales/odei/editar_vista';

            $this->load->view('plantilla', $datos);
        }
    }

    public function eliminar() {

        if(isset($_REQUEST['eliminarLocal']) && ($this->input->post('local_id_local') !== '')){
            $idLocal    = $this->input->post('local_id_local');

            //print_r($_REQUEST);

            $status     = $this->modeloLocalOdei->eliminar_local($idLocal);
            $this->compruebaStatus($status, 'eliminado');
        } else {
            if ($this->input->is_ajax_request()) {
                $idLocal        = $this->input->get('idLocal');
                $datos['local'] = $this->modeloLocalOdei->obtener_local($idLocal);
                $this->load->view('locales/odei/eliminar_vista', $datos);
            }
        }
    }

    private function compruebaStatus($status, $accion) {
        if ($status) {
            $this->mensaje['cuerpo'] = "El local ha sido {$accion}";
        } else {
            $this->mensaje['tipo']   = 'warning';
            $this->mensaje['cuerpo'] = "El local NO ha sido {$accion}";
            log_message('ERROR', $status . "Local no pudo ser {$accion}");
        }
        $this->session->set_flashdata('mensaje_flash', $this->mensaje);
        redirect('locales/jurisdiccion-regional');
    }

}
