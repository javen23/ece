<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Acceso extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('acceso_model', 'modeloAcceso');
    }

    public function login() {
        if (!$this->session->userdata('idUserLogin')) {
            $_REQUEST['user'] = $this->input->post('sendUser');
            $_REQUEST['pass'] = $this->input->post('sendPass');

            if ($_REQUEST['user'] && $_REQUEST['pass']) {
                //$check = $this->modeloAcceso->logearme( $_REQUEST );
                $check = array("idUsu"=>"1","usuario"=>"javen","idRol"=>"1","sede_operativa"=>"1","dni"=>"12345678","nombres_apellidos"=>"Javier Venero");
                if ($check != NULL) {
                    $datoUser = array(
                        'idUserLogin'           => $check['idUsu'],
                        'userNameUsuario'       => $check['usuario'],
                        'userIdRol'             => $check['idRol'],
                        'userNombreRol'         => $check['rol'],
                        'sedeOperativa'         => isset($check['sede_operativa']) ? $check['sede_operativa'] : null,
                        'userDni'               => isset($check['dni']) ? $check['dni'] : null,
                        'userNombreApellidos'   => isset($check['nombres_apellidos']) ? $check['nombres_apellidos'] : null
                    );
                    $this->session->set_userdata($datoUser);

                    //log_message('INFO', "<======================Usuario {$check['usuario']} ha INGRESADO al SISTEMA=======================>");

                    redirect('', 'refresh');
                } else {
                    $this->flash_mensaje('Datos ingresados incorrectos...', 'Login', 'danger');
                    redirect('login', 'refresh');
                }
            } else {
                $data['titulo']     = "Acceso al sistema";
                $data['contenido']  = 'acceso/login_acceso';
                $data['sidebar']    = FALSE;
                $this->load->view('plantilla', $data);
            }
        } else {
            redirect('', 'refresh');
        }
    }

    public function logout() {
        $this->session->unset_userdata('idUserLogin');
        $this->session->sess_destroy();
        redirect('login', 'refresh');
    }
}

/* End of file Home.php */
    /* Location: ./application/controllers/Home.php */
