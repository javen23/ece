<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

$route['default_controller'] 										= "dashboard/dashboard_controlador";
$route['login'] 													= "acceso/login";
$route['logout'] 													= "acceso/logout";

$route['cambiar-password']											= "dashboard/dashboard_controlador/cambiar_password";

/*** MODULO LOCALES ***/
// Locales
$route['locales'] 													= "locales/locales_controlador";

$route['locales/actualizar_gps'] 									= "locales/locales_controlador/actualizar_gps_local";

$route['locales/reportes'] 											= "locales/locales_controlador/reportes";
$route['locales/reportes/directorio']								= "locales/locales_controlador/reporte_directorio";
$route['locales/reportes/mapa-regional']							= "locales/locales_controlador/reporte_mapa_regional";
$route['locales/reportes/mapa-provincial-distrital']				= "locales/locales_controlador/reporte_mapa_provincial";
$route['locales/reportes/preseleccion']								= "locales/locales_controlador/reporte_preseleccion";
$route['locales/reportes/capacitacion']								= "locales/locales_controlador/reporte_capacitacion";

//Locales ODEI
$route['locales/jurisdiccion-regional'] 							= "locales/locales_odei_controlador";
$route['locales/jurisdiccion-regional/crear'] 						= "locales/locales_odei_controlador/crear";
$route['locales/jurisdiccion-regional/editar'] 						= "locales/locales_odei_controlador/editar";
$route['locales/jurisdiccion-regional/eliminar'] 					= "locales/locales_odei_controlador/eliminar";

//Locales Administrativo
$route['locales/jurisdiccion-provincial-distrital'] 				= "locales/locales_administrativo_controlador";
$route['locales/jurisdiccion-provincial-distrital/crear'] 			= "locales/locales_administrativo_controlador/crear";
$route['locales/jurisdiccion-provincial-distrital/editar'] 			= "locales/locales_administrativo_controlador/editar";
$route['locales/jurisdiccion-provincial-distrital/eliminar'] 		= "locales/locales_administrativo_controlador/eliminar";
$route['locales/jurisdiccion-provincial-distrital/exportable'] 		= "locales/locales_administrativo_controlador/exportar";
$route['locales/jurisdiccion-provincial-distrital/enviar-oficio']	= "locales/locales_administrativo_controlador/enviar_oficio";

// Locales Capacitacion
$route['locales-capacitacion'] 										= "locales/locales_capacitacion_controlador";
$route['locales-capacitacion/crear'] 								= "locales/locales_capacitacion_controlador/crear";
$route['locales-capacitacion/editar'] 								= "locales/locales_capacitacion_controlador/editar";
$route['locales-capacitacion/eliminar']								= "locales/locales_capacitacion_controlador/eliminar";

//Locales Pre Seleccion
$route['locales-preseleccion'] 										= "locales/locales_preseleccion_controlador";
$route['locales-preseleccion/crear'] 								= "locales/locales_preseleccion_controlador/crear";
$route['locales-preseleccion/editar'] 								= "locales/locales_preseleccion_controlador/editar";
$route['locales-preseleccion/eliminar']								= "locales/locales_preseleccion_controlador/eliminar";

/***********/

//--USUARIOS
$route['usuarios'] 													= "usuario/usuario_controlador";
$route['usuarios/crear'] 											= "usuario/usuario_controlador/crear";
$route['usuarios/estado'] 											= "usuario/usuario_controlador/estado";
$route['usuarios/editar'] 											= "usuario/usuario_controlador/editar";
$route['usuarios/verificar']										= "usuario/usuario_controlador/verificar";

//--RUTA
$route['rutas']         											= "ruta/rutas_controlador/index";
$route['rutas/crear']   											= "ruta/rutas_controlador/crear";
$route['rutas/editar']   											= "ruta/rutas_controlador/editar";
$route['rutas/asignar']   											= "ruta/rutas_controlador/asignar";
$route['rutas/eliminar']											= "ruta/rutas_controlador/eliminar";

//--CALENDARIO
$route['calendario'] 												= "calendario/calendario_controlador";

//------------INVENTARIO--------------
//-----ASISTENCIA - IMPRENTA
$route['inventario/imprenta'] 										= "inventario/inventario_imprenta_controlador";
$route['inventario/lector-inventario-imprenta'] 					= "inventario/inventario_imprenta_controlador/lector";
$route['inventario/acta-envio-inventario-imprenta'] 				= "inventario/inventario_imprenta_controlador/acta_envio";

//-----IMPRENTA
$route['inventario/imprenta'] 										= "inventario/inventario_imprenta_controlador";
$route['inventario/lector-inventario-imprenta'] 					= "inventario/inventario_imprenta_controlador/lector";
$route['inventario/acta-envio-inventario-imprenta'] 				= "inventario/inventario_imprenta_controlador/acta_envio";

//--SEDE REGIONAL
$route['inventario/sede-regional'] 									= "inventario/inventario_sede_controlador";
$route['inventario/lector-inventario-sede'] 						= "inventario/inventario_sede_controlador/lector";

//--SEDE PROVINCIAL
$route['inventario/sede-provincial'] 								= "inventario/inventario_provincia_controlador";
$route['inventario/lector-inventario-provincia'] 					= "inventario/inventario_provincia_controlador/lector";

//--SEDE DISTRITAL
$route['inventario/sede-distrital'] 								= "inventario/inventario_distrito_controlador";
$route['inventario/lector-inventario-distrito'] 					= "inventario/inventario_distrito_controlador/lector";

//--SEGMENTACION
$route['segmentacion'] 												= "segmentacion/segmentacion_controlador";
$route['segmentacion/listado-ingresos'] 							= "segmentacion/segmentacion_controlador/listado";
$route['segmentacion/avance-programacion'] 							= "segmentacion/segmentacion_controlador/programacion";
$route['segmentacion/reportes'] 									= "segmentacion/segmentacion_controlador/reportes";
$route['segmentacion/reportes/sede-operativa'] 						= "segmentacion/segmentacion_controlador/reporte_sede";
$route['segmentacion/reportes/provincia-operativa'] 				= "segmentacion/segmentacion_controlador/reporte_provincia";
$route['segmentacion/reportes/distrito-operativa'] 					= "segmentacion/segmentacion_controlador/reporte_distrito";
$route['segmentacion/reportes/asistente'] 							= "segmentacion/segmentacion_controlador/reporte_asistente";
$route['segmentacion/actualizar'] 									= "segmentacion/segmentacion_controlador/actualizar";
$route['segmentacion/filtrar'] 										= "segmentacion/segmentacion_controlador/filtrar";

//--REPORTE
$route['segmentacion/listado-reporte-sede'] 		= "segmentacion/segmentacion_controlador/listado_reporte_sede";
$route['segmentacion/exportar-reporte-sede'] 		= "segmentacion/exportar_segmentacion_controlador/sede_reporte_exportar";
$route['segmentacion/listado-reporte-provincia'] 	= "segmentacion/segmentacion_controlador/listado_reporte_provincia";
$route['segmentacion/exportar-reporte-provincia'] 	= "segmentacion/exportar_segmentacion_controlador/provincia_reporte_exportar";

//--COBERTURA
$route['cobertura'] 								= "cobertura/cobertura_controlador";
//----------/CAPACITACION
$route['cobertura'] 					= "cobertura/cobertura_controlador";
$route['cobertura/directores'] 			= "cobertura/director/director_controlador";
$route['cobertura/instructores'] 		= "cobertura/instructor/instructor_controlador";
$route['cobertura/niveles'] 			= "cobertura/niveles/niveles_controlador";
$route['cobertura/niveles/nivel-uno'] 	= "cobertura/niveles/niveles_controlador/nivel_uno";
$route['cobertura/niveles/nivel-dos'] 	= "cobertura/niveles/niveles_controlador/nivel_dos";
$route['cobertura/niveles/nivel-tres'] 	= "cobertura/niveles/niveles_controlador/nivel_tres";

//--ASISTENTE SUPERVISOR
$route['asistente-supervisor'] 						= "asistente/asistente_controlador";

//----------/IE
$route['asistente-supervisor/ie'] 							= "asistente/asistente_controlador/ie";
$route['asistente-supervisor/ie/crear'] 					= "asistente/asistente_controlador/ie_crear";
$route['asistente-supervisor/ie/editar'] 					= "asistente/asistente_controlador/ie_editar";
$route['asistente-supervisor/ie/eliminar'] 					= "asistente/asistente_controlador/ie_eliminar";
$route['asistente-supervisor/ie/formato-accesibilidad'] 	= "asistente/asistente_controlador/ie_formato";
$route['asistente-supervisor/ie/aplicadores'] 				= "asistente/asistente_controlador/ie_aplicadores";
$route['asistente-supervisor/ie/exportar-ie'] 				= "asistente/aplicador_controlador/asistente_sup_reporte";

//--APLICADORES
//
$route['ajax/aplicadores/check_dni']						= "asistente/aplicadores_controller/aplicadores";
$route['asistente-supervisor/aplicador/aplicadores_dni'] 	= "asistente/aplicadores_controller/insertar_dni_aplicadores";
//--APLICADORES
//
$route['asistente-supervisor/aplicador'] 				= "asistente/aplicador_controlador/aplicador";
$route['asistente-supervisor/aplicador/crear'] 			= "asistente/aplicador_controlador/ap_crear";
$route['asistente-supervisor/aplicador/editar'] 		= "asistente/aplicador_controlador/ap_editar";
$route['asistente-supervisor/aplicador/eliminar'] 		= "asistente/aplicador_controlador/ap_eliminar";


//--Ajax
//------/Exportar-tabla
$route['ajax-exportar-tabla'] 						= "ajax/ajax_controlador/exportar_tabla";

//------/Sede_Operativa
$route['ajax-sede-operativa'] 						= "ajax/ajax_controlador/sede_operativa";
$route['ajax-sede-operativa/(:num)'] 				= "ajax/ajax_controlador/sede_operativa_mostrar/$1";

//------/Lector_Barra

$route['ajax-lector-barra'] 						= "ajax/ajax_controlador/lector_barra";
// $route['ajax-lector-barra/(:num)'] = "ajax/ajax_controlador/lector_barra/$1";

//------/Departamentos
$route['ajax-departamentos'] 						= "ajax/ajax_controlador/departamentos";

//------/Provincias
$route['ajax-provincias'] 							= "ajax/ajax_controlador/provincias";
$route['ajax-sede-provincial'] 						= "ajax/ajax_controlador/sede_provincial";
$route['ajax-sede-provincial-multiple'] 			= "ajax/ajax_controlador/sede_provincial_multiple";
//------/Distritos
$route['ajax-distritos'] 							= "ajax/ajax_controlador/distritos";
//------/Sede distrital
$route['ajax-sede-distrital'] 						= "ajax/ajax_controlador/sede_distrital";
//------/Computadora
$route['ajax/computadora/crear'] 				    = "ajax/ajax_controlador/crear_computadora";
$route['ajax/computadora/editar'] 				    = "ajax/ajax_controlador/editar_computadora";
//------/Seguridad
$route['ajax/seguridad/crear'] 				        = "ajax/ajax_controlador/crear_seguridad";
$route['ajax/seguridad/editar'] 				    = "ajax/ajax_controlador/editar_seguridad";
//-----/Almacen
$route['ajax/area_almacen'] 		                = "ajax/ajax_controlador/area_almacen";

//----/Reportes/Modal
$route['ajax/reportes/mapa_region_modal']           = "ajax/ajax_controlador/reporte_mapa_region";
$route['verificar-dni-asistente']					= "asistente/asistente_controlador/verificar_dni";
$route['verificar-dni-aplicador']					= "asistente/aplicadores_controller/verificar_dni";

//------Web-services
$route['api']          								= "api/api_controller";
$route['api/login']          						= "api/api_controller/login";
$route['api/version']          						= "api/api_controller/version";
//-----------Supervision
$route['api/supervision']          					= "api/supervision_controller";
$route['api/supervision/capitulo1']					= "api/supervision_controller/capitulo_uno";
$route['api/supervision/capitulo2a']				= "api/supervision_controller/capitulo_dos_a";
$route['api/supervision/capitulo2b']				= "api/supervision_controller/capitulo_dos_b";


/* End of file routes.php */
/* Location: ./application/config/routes.php */
