<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Add Css and JS
  |--------------------------------------------------------------------------
  |
  | Añadir Css y Js al iniciar la Aplicacioón.
  |
 */


$config['header_css'] = array('bootstrap.min',
    'animate',
    'font-awesome.min',
    'ionicons.min',
    'style.min',
    'skins/_all-skins.min',
    'select2.min');

$config['header_js'] = array('bootstrap.min',
    'plugins/slimScroll/jquery.slimscroll.min',
    'app.min',
    'select2.min',
    'i18n/es');

$config['header_js_'] = array();

$config['compiled_header_css'] = array();
$config['compiled_header_js'] = array();
$config['compiled_footer_js'] = array();
