/*
     * Bootstrap Growl - Notifications popups
     */
    // function notify(message, type){
    //     $.growl({
    //         message: message
    //     },{
    //         type: type,
    //         allow_dismiss: false,
    //         label: 'Cancel',
    //         className: 'btn-xs btn-inverse',
    //         placement: {
    //             from: 'top',
    //             align: 'right'
    //         },
    //         delay: 2500,
    //         animate: {
    //                 enter: 'animated bounceIn',
    //                 exit: 'animated bounceOut'
    //         },
    //         offset: {
    //             x: 20,
    //             y: 85
    //         }
    //     });
    // };


(function($){
    var soloPermitir = function (a){
        // console.dir($(this).data('permitido'));
        // console.dir(a.data);
        var permitido = ($(this).data('permitido') || a.data.permitido).toString();
        var c=a.which,
            d=a.keyCode,
            e=String.fromCharCode(c).toLowerCase(),
            f=permitido;

            (-1!=f.indexOf(e)||9==d||37!=c&&37==d||39==d&&39!=c||8==d||46==d&&46!=c)&&161!=c||a.preventDefault();
    };

    $.fn.soloPermitir = function(b){
        $(this).on('keypress', {permitido: b}, soloPermitir);
        return $(this);
    };
})(jQuery);

(function(){
         Waves.attach('a.btn');
         Waves.attach('.btn:not(.btn-icon)');
         Waves.attach('.btn-icon', ['waves-circle', 'waves-float']);
        Waves.init();
    })();

/*
     * Clear Notification
     */
    $('body').on('click', '[data-clear="notification"]', function(e){
      e.preventDefault();

      var x = $(this).closest('.listview');
      var y = x.find('.lv-item');
      var z = y.size();

      $(this).parent().fadeOut();

      x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
      x.find('.grid-loading').fadeIn(1500);


      var w = 0;
      y.each(function(){
          var z = $(this);
          setTimeout(function(){
          z.addClass('animated fadeOutRightBig').delay(1000).queue(function(){
              z.remove();
          });
          }, w+=150);
      })

    //Popup empty message
    setTimeout(function(){
        $('#notifications').addClass('empty');
    }, (z*150)+200);
    });


function validarNumero(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla === 8)
        return true;
    if (tecla === 48)
        return true;
    if (tecla === 49)
        return true;
    if (tecla === 50)
        return true;
    if (tecla === 51)
        return true;
    if (tecla === 52)
        return true;
    if (tecla === 53)
        return true;
    if (tecla === 54)
        return true;
    if (tecla === 55)
        return true;
    if (tecla === 56)
        return true;
    if (tecla === 57)
        return true;
    patron = /1/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}
function validarLetras(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla === 8)
        return true; // backspace
    if (tecla === 32)
        return true; // espacio
    if (e.ctrlKey && tecla === 86) {
        return true;
    } //Ctrl v
    if (e.ctrlKey && tecla === 67) {
        return true;
    } //Ctrl c
    if (e.ctrlKey && tecla === 88) {
        return true;
    } //Ctrl x
    patron = /[a-zA-Z]/; //patron
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

$(".onlyNumbers").keypress(function (e) {
    return validarNumero(e);
});

$(".onlyStrings").keypress(function (e) {
    return validarLetras(e);
});

// CARGAR ELEMENTOS GENERALES
$(function(){    
    $('[data-toggle="popover"]').popover();
});


$("#btn-exportar-reporte-sede").on('click', function () {
    // var padre_opcion_reporte_sede = $(".reporte-opciones");
    // var obtener_sede=padre_opcion_reporte_sede.find('select[name="sede_reporte"]');
    // var obtener_nivel=padre_opcion_reporte_sede.find('select[name="nivel_reporte"]');
    // // console.log(obtener_sede.val());
    // // console.log(obtener_nivel.val());
    // var sedes=obtener_sede.val();
    // if(sedes==null){
    //   sedes=0;
    // };
    // var nivel=obtener_nivel.val();
    opciones_reporte();

    var url = CI.base_url + "segmentacion/exportar-reporte-sede?nivel="+ nivel+"&sedes="+sedes;
    //console.log(url);

    $(this).attr('href', url);
    setTimeout(function () {
        $(this).click();
    }, 2000);
});

function opciones_reporte(sede_id){
    sede=sede_id || "";
    padre_opcion_reporte_sede = $(".reporte-opciones");
    obtener_sede=padre_opcion_reporte_sede.find('select[name="sede_reporte"]');
    obtener_nivel=padre_opcion_reporte_sede.find('select[name="nivel_reporte"]');
    // console.log(obtener_sede.val());
    // console.log(obtener_nivel.val());

    sedes=obtener_sede.val();
    if(sedes==null){
        sedes=0;
    };
    nivel=obtener_nivel.val();
    console.log(sede);
}

$("#sede_reporte_presupuesto").on('change',function(){
    var valor_sede = $(this).val();
    //console.log(valor_sede);
    if(valor_sede !=0){
        //console.log("no hay cero");
        var url = CI.base_url + "ajax-sede-provincial-multiple?sedes="+valor_sede;
        // console.log(url);
        $.ajax({
            url: url,
            type:'GET',
            dataType:'json',
            beforeSend:function(){

            },
            success:function(response){
                if(!response.error){
                    console.log("lista de sedes: ",response);
                    var options = "";

                    $.each(response.sede_provincial_multiple, function(i, item){

                        options +='<option value="'+item.cod_sede_prov+'">'+item.sede_operativa+' - '+item.sede_prov+'</option>'
                    });
                    $("#provincia_reporte_presupuesto").html(options);
                }else{
                    //console.log("aqui si hay un error");
                    $("#provincia_reporte_presupuesto").html('<option value="0">TODOS</option>');
                }
            },
            error:function(err){
                console.log(err);
            }
        });
    }else{
        $("#provincia_reporte_presupuesto").html('<option value="0">TODOS</option>');
    }
});

$("#btn-filtrar-reporte-sede").on('click',function(){
    // var padre_opcion_reporte_sede = $(".reporte-opciones");
    // var obtener_sede=padre_opcion_reporte_sede.find('select[name="sede_reporte"]');
    // var obtener_nivel=padre_opcion_reporte_sede.find('select[name="nivel_reporte"]');
    // // console.log(obtener_sede.val());
    // // console.log(obtener_nivel.val());
    // var sedes=obtener_sede.val();
    // if(sedes==null){
    //   sedes=0;
    // };
    // var nivel=obtener_nivel.val();
    opciones_reporte();
    var url = CI.base_url + "segmentacion/listado-reporte-sede?nivel=" + nivel+"&sedes="+sedes;
    // console.log(url);
    $('#resultado_filtro').slideUp('high', function () {
        $('#resultado_filtro').load(url, function () {
            tablaListadoDataTable();
            $('#resultado_filtro').slideDown('slow');
        });
    });
});


$(document).on('click',".resetear_formulario",function(){
    $("#form_cambiar_password")[0].reset();
    $(".hide-verifica").removeClass('overlay-blanco').addClass('hide').html('<i class="fa fa-spinner fa-pulse"></i>');
});

$(document).on('click','#btnCambiarPassword',function(){
    var pass = $('input[name="pass_cambiar"]').val();
    if(pass.length==0){
        $('input[name="pass_cambiar"]').focus();
        return false;
    }else{
        $.ajax({
            url: CI.base_url+'cambiar-password',
            type: 'POST',
            data: $("#form_cambiar_password").serialize(),
            dataType: 'json',
            beforeSend: function(){
                $(".hide-verifica").removeClass('hide');
            },
            success: function(response){
                console.log(response);
                if(response.resul===true){
                    setTimeout(function () {
                        $(".hide-verifica").addClass('overlay-blanco').html('<span class="resultado-mensaje-exito"><span class="fa fa-check text-success"></span> Contraseña cambiada</span>')
                    }, 2000);

                }
            }
        });
    }
});


$(document).on('click', '#btnLogeoIngresar', function () {
    var user = $('#userLogeo').val();
    var pass = $('#userPass').val();
    if (user.length > 0 && pass.length > 0) {
        $("#form_login_usuario").submit();
    } else {
        $(".alert-message-error").empty();
        $(".alert-message").html('<div class="alert alert-danger alert-dismissable">' +
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                '<h4><i class="icon fa fa-ban"></i> Atención...!</h4>' +
                '<strong>Porfavor ingrese los datos</strong>' +
                '</div> ');
    }
    return false;
});


$(".exportar_tabla_reporte").on('click',function(event) {
    var cant_registro = $('#tablaListado_length select')
    cant_registro.val(-1);
    cant_registro.trigger('change');

    var tabla_limpia= $("#tablaListado").clone();

    tabla_limpia.find('tfoot').remove();

    tabla_limpia.find('thead tr th').css({
      'border':'1px solid #09486D',
      'color': '#fff',
      'font-size': '15px',
      'background' : '#3498db',
      'width': '100%',
      'height':'auto',
    });

    tabla_limpia.find('tbody tr td').css('border','1px solid #3498db');
    $("#datos_a_enviar").val($("<div>").append(tabla_limpia).html());
    $("#form_exportar_tabla").submit();

    cant_registro.val(10);
    cant_registro.trigger('change');
});

function cerrar_facebox() {
    jQuery(document).trigger('close.facebox');
}

function tablaListadoDataTable() {
    $('#tablaListado').dataTable({
        "oLanguage": {
            "sSearch": "<i class='ion ion-ios-search'></i> Buscar: ",
            "oPaginate": {
                "sFirst": "&lt;&lt;",
                "sLast": "&gt;&gt;",
                "sNext": "&gt;",
                "sPrevious": "&lt;"
            },
            "sInfoEmpty": "0 registros que mostrar",
            "sInfoFiltered": " ",
            "sZeroRecords": "<div class='text-center'><i class='ion ion-podium fa-5x'></i><h3>No hay registro que mostrar</h3></div>",
            "sLoadingRecords": "Por favor espere - cargando...",
            "sLengthMenu": 'Mostrando <select class="form-control input-sm">' +
                    '<option value="10">10</option>' +
                    '<option value="20">20</option>' +
                    '<option value="30">30</option>' +
                    '<option value="40">40</option>' +
                    '<option value="50">50</option>' +
                    '<option value="-1">Todos</option>' +
                    '</select> registros',
            "sInfo": " _TOTAL_ registros encontrados, mostrando _START_ de _END_ registros"
        }
    });
    var table = $("#tablaListado").DataTable();
    $('#tablaListado tfoot th').each(function () {
        var title = $('#tablaListado thead tr.th-head-inputs th').eq($(this).index()).text();
        $(this).html('<input type="text" placeholder="' + title + '" />');
    });
    table.columns().eq(0).each(function (colIdx) {
        $('input', table.column(colIdx).footer()).on('keyup change', function () {
            table
                    .column(colIdx)
                    .search(this.value)
                    .draw();
        });
    });
}

function limitePeaLocales() {
    var local_pea   = $('#local_pea');
    var local_aulas = $('#local_aulas');
    var resto_pea   = $('#resto_pea');
    var contenedor_pea = $('#container_resto_pea');
    var btn_guardar    = $('#btnCreateLocal');

    local_pea.on('keyup', function(){
        var pea     = local_pea.val();

        var resto   = pea % CI.limite_pea;

        local_aulas.val( Math.floor(pea / CI.limite_pea) || 1 );

        if (pea > CI.limite_pea && resto > 0) {
            resto_pea.html(resto);
            contenedor_pea.show();
            btn_guardar.prop('disabled', true);
        } else {
            contenedor_pea.hide();
            btn_guardar.prop('disabled', false);
        }
    });
    //
    // local_pea.on('blur', function(){
    //     contenedor_pea.hide();
    //     btn_guardar.prop('disabled', false);
    // });
}

function llamadasAjaxUbigeo() {
    var local_sede          = $("#local_sede");
    var local_sede_prov     = $("#local_sede_provincial");

    var local_departamento  = $("#local_departamento");
    var local_provincia     = $("#local_provincia");
    var local_distrito      = $("#local_distrito");

    var div_sede_distrital      = $("#div_sede_distrital");
    var local_sede_distrital      = $("#local_sede_distrital");

    var s2id_local_departamento = $('#select2-local_departamento-container');
    var s2id_local_provincia    = $('#select2-local_provincia-container');
    var s2id_local_distrito     = $('#select2-local_distrito-container');
    var s2id_local_sede_prov    = $('#select2-local_sede_provincial-container');
    var s2id_local_sede_distrital     = $('#select2-local_sede_distrital-container');

    var icon_load       = '<i class="fa fa-spinner fa-pulse fa-2x"></i>';
    var elemento_vacio  = '<option value="" class="text-center">-Seleccione-</option>';

    //Ajax para traer DEPARTAMENTOS
    local_sede.on('change',function(){
        var id = local_sede.val();

        // Ocultamos las Sedes distritales
        div_sede_distrital.addClass('hide');
        local_sede_distrital.html(elemento_vacio);
        s2id_local_sede_distrital.text('-SELECCIONE-');

        if(id !== ""){
            var ajaxCall = {
                url: CI.base_url+'ajax-departamentos?sede_operativa='+id,
                type:'GET',
                data:{},
                dataType: 'json',
                beforeSend: function() {
                    s2id_local_departamento.html(icon_load);
                    s2id_local_sede_prov.html(icon_load);
                    s2id_local_provincia.text('-SELECCIONE-');
                    s2id_local_distrito.text('-SELECCIONE-');

                    local_departamento.html(elemento_vacio);
                    local_sede_prov.html(elemento_vacio);
                    local_provincia.html(elemento_vacio);
                    local_distrito.html(elemento_vacio);
                },
                success: function (data) {
                    s2id_local_departamento.text('-SELECCIONE-');
                    local_departamento.append(llenar_selects(data.departamentos, 'ccdd', 'nombre'));
                },
                error: function (error) {
                    console.log(error);
                }
            };

            $.ajax(ajaxCall);

            if (local_sede_prov.length) {
                ajaxCall.url = CI.base_url+'ajax-sede-provincial?sede_operativa='+id;
                ajaxCall.success = function (data) {
                    s2id_local_sede_prov.text('-SELECCIONE-');
                    local_sede_prov.append(llenar_selects(data.sede_provincial, 'cod_sede_prov', 'sede_prov'));

                };

                $.ajax(ajaxCall);
            }

        }
        else{
            s2id_local_departamento.text('-SELECCIONE-');
            s2id_local_sede_prov.text('-SELECCIONE-');
            s2id_local_provincia.text('-SELECCIONE-');
            s2id_local_distrito.text('-SELECCIONE-');

            local_departamento.html(elemento_vacio);
            local_sede_prov.html(elemento_vacio);
            local_provincia.html(elemento_vacio);
            local_distrito.html(elemento_vacio);
        }
    });

    //Ajax para traer PROVINCIAS
    local_departamento.on('change',function(){
        var id = local_departamento.val();

        // Ocultamos las Sedes distritales
        div_sede_distrital.addClass('hide');
        local_sede_distrital.html(elemento_vacio);
        s2id_local_sede_distrital.text('-SELECCIONE-');

        if(id !== ""){
            $.ajax({
                url: CI.base_url+'ajax-provincias?departamento='+id+'&sede_operativa='+local_sede.val(),
                type:'GET',
                data:{},
                dataType: 'json',
                beforeSend: function() {
                    s2id_local_provincia.html(icon_load);
                    s2id_local_distrito.text('-SELECCIONE-');

                    local_provincia.html(elemento_vacio);
                    local_distrito.html(elemento_vacio);
                },
                success: function (data) {
                    // console.log(data);
                    s2id_local_provincia.text('-SELECCIONE-');
                    local_provincia.append(llenar_selects(data.provincias, 'ccpp', 'nombre'));

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
        else{
            s2id_local_provincia.text('-SELECCIONE-');
            s2id_local_distrito.text('-SELECCIONE-');

            local_provincia.html(elemento_vacio);
            local_distrito.html(elemento_vacio);
        }
    });

    // local_sede_prov.on('change',function(){
        // var id = $(this).val();

        // // Ocultamos las Sedes distritales
        // div_sede_distrital.addClass('hide');
        // local_sede_distrital.html(elemento_vacio);
        // s2id_local_sede_distrital.text('-SELECCIONE-');
        //
        // if(id !== ""){
        //     $.ajax({
        //         url: CI.base_url+'ajax-distritos?sede_prov='+id+'&sede_operativa='+local_sede.val(),
        //         type:'GET',
        //         data:{},
        //         dataType: 'json',
        //         beforeSend: function() {
        //             s2id_local_distrito.html(icon_load);
        //             local_distrito.html(elemento_vacio);
        //         },
        //         success: function (data) {
        //             // console.log(data);
        //             s2id_local_distrito.text('-SELECCIONE-');
        //
        //             local_distrito.append(llenar_selects(data.distritos, 'ccdi', 'nombre'));
        //         },
        //         error: function (error) {
        //             console.log(error);
        //         }
        //     });
        // }
        // else{
        //     s2id_local_distrito.text('-SELECCIONE-');
        //     local_distrito.html(elemento_vacio);
        // }
    // });

    //Ajax para traer DISTRITOS
    local_provincia.on('change',function(){
        var id = local_provincia.val();

        // Ocultamos las Sedes distritales
        div_sede_distrital.addClass('hide');
        local_sede_distrital.html(elemento_vacio);
        s2id_local_sede_distrital.text('-SELECCIONE-');

        if(id !== ""){
            $.ajax({
                url: CI.base_url+'ajax-distritos?provincia='+id+'&departamento='+local_departamento.val()+'&sede_operativa='+local_sede.val(),
                type:'GET',
                data:{},
                dataType: 'json',
                beforeSend: function() {
                    s2id_local_distrito.html(icon_load);
                    local_distrito.html(elemento_vacio);
                },
                success: function (data) {
                    // console.log(data);
                    s2id_local_distrito.text('-SELECCIONE-');

                    local_distrito.append(llenar_selects(data.distritos, 'ccdi', 'nombre'));
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
        else{
            s2id_local_distrito.text('-SELECCIONE-');
            local_distrito.html(elemento_vacio);
        }
    });

    //Ajax para traer SEDE DISTRITALES
    local_distrito.on('change',function(){
        var id = local_distrito.val();

        if(id !== ""){
            $.ajax({
                url: CI.base_url+'ajax-sede-distrital?distrito='+id+'&provincia='+local_provincia.val()+'&departamento='+local_departamento.val()+'&sede_operativa='+local_sede.val(),
                type:'GET',
                data:{},
                dataType: 'json',
                beforeSend: function() {
                    s2id_local_sede_distrital.html(icon_load);

                    local_sede_distrital.html(elemento_vacio);
                },
                success: function (data) {
                    console.dir(data);
                    s2id_local_sede_distrital.text('-SELECCIONE-');

                    if (data.sede_distrital) {
                        div_sede_distrital.removeClass('hide');
                        local_sede_distrital.append(llenar_selects(data.sede_distrital, 'cod_sede_dist', 'sede_dist'));
                        div_sede_distrital.children('span').css('width', '100%');

                    } else {
                        div_sede_distrital.addClass('hide');
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
        else{
            s2id_local_sede_distrital.text('-SELECCIONE-');

            local_sede_distrital.html(elemento_vacio);
        }
    });


    var llenar_selects = function(elementos, key, label) {
        var options = "";

        $.each(elementos, function(i, item){
            options +='<option value="'+item[key]+'">'+item[label]+'</option>'
        });

        return options;
    }
}

(function($){
    $.fn.notify = function(data, from, align){
        var from    = from || 'top';
        var align   = align || 'center';

        var data = data || {};
        var icon    = data.type === 'success' ? 'check':'ban';
        var type    = data.type || 'success';
        var title   = data.title || 'Atención';
        var message = data.message || 'La información ha sido guardada';
        var url     = data.url || '';

        $.growl({
                icon: icon,
                title: '<strong class="margin-left-5">'+title+'</strong>',
                message: '<span class="margin-left-10">'+message+'</span>',
                url: url
            },{
                element: 'body',
                type: type,
                allow_dismiss: true,
                placement: {
                    from: from,
                    align: align
                },
                offset: {
                    x: 20,
                    y: 85
                },
                spacing: 10,
                z_index: 1031,
                delay: 2500,
                timer: 2500,
                url_target: '_blank',
                mouse_over: false,

                icon_type: 'class',
                template: '<div data-growl="container" class="alert centrar-bloque-growl" role="alert">' +
                                '<button type="button" class="close" data-growl="dismiss">' +
                                    '<span aria-hidden="true">&times;</span>' +
                                    '<span class="sr-only">Close</span>' +
                                '</button>' +
                                '<span data-growl="icon"><i class="fa fa-'+icon+'"></i></span>' +
                                '<span data-growl="title"></span>' +
                                '<span data-growl="message"></span>' +
                                '<a href="#" data-growl="url"></a>' +
                            '</div>'
        });
    };
})(jQuery);

//Agregado por calevano
//-----------Inicializar el dateTimePicker-----------
if ($('#date_time_picker').length) {
    $('#date_time_picker').datetimepicker();
}
