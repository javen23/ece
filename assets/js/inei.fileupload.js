$(document).ready(function (event) {

  var subir_todos     = $('#fileupload-submit');
  var cancelar_todos  = $('#fileupload-cancel');
  var error_msg       = $("#error-msg");

  var indice          = 0;
  var label_error     = '<span class="badge badge-important"/>';
  var label_exito     = '<span class="badge badge-success"/>';
  var ruta_fileupload = "http://sigma2.intimedia-dev.net/app_dev.php";


  $("#error-msg > .close").click(function(){
      error_msg.hide();
  });

  // Iniciar ocultando el boton cancelar
  cancelar_todos.hide();

  subir_todos.on('click', function(event){
      var contenedor_p    = $('#files > div.archivos-preview > p.text-center');
      var contador        = 0;
      var todos_textarea  = contenedor_p.find('textarea');

      todos_textarea.each(function(){
        var textarea = $(this);
        if ( textarea.val() === "" ) {
          console.log('Desde condicional cont:'+contador);
          contador++;
          error_msg.fadeOut('fast').fadeIn('normal');
          error_msg.attr('style', '')
            .removeClass('alert-success').addClass('alert-error')
            .find('strong').text("Debe ingresar una descripción para cada foto");

          textarea.focus();
          return false;
        }
      });

      if ( contador === 0 ){
        console.log('Desde cont:'+contador);
        contenedor_p.find('button.btn-primary').each(function(){
          var button = $(this);
          button.trigger('click');
          subir_todos.hide();
          cancelar_todos.show();
        });
      }

      return false;
  });

  cancelar_todos.on('click', function(){
    var contenedor_p = $('#files > div.archivos-preview > p.text-center');
    cancelar_todos.hide();
    subir_todos.show();

    contenedor_p.find('button.btn-warning').each(function(){
      var button = $(this);
      button.trigger('click');
    });

    return false;
  });

  function formatoPesoArchivo(peso){
    var nuevo_peso;

    if (peso < 1048576) {
      nuevo_peso = (peso/1024).toFixed(2)+'KB';
    } else {
      nuevo_peso = ((peso/1024)/1024).toFixed(2)+'MB';
    }
    return nuevo_peso;
  }

  var uploadButton = $('<button/>')
    .addClass('btn btn-primary btn-small')
    .prop('disabled', true)
    .text('Procesando...')
    .on('click', function (event) {
        var $this     = $(this);
        var  data     = $this.data();
        var textarea  = $(data.context[0]).find('textarea');

        if (textarea.val() === "") {
          textarea.focus();
          return false;
        }

        $this
            .off('click')
            .removeClass('btn-primary').addClass('btn-warning') //Agregado
            .text('Cancelar').on('click', function () {
                $this.remove();
                data.abort();
            });
        data.submit().always(function () {
            $this.remove();
        });

        return false;
    });

  $('#fileupload').fileupload({
      url:                    ruta_fileupload+'/_uploader/gallery/upload',
      dataType:               'json',
      limitMultiFileUploads:  4,
      maxNumberOfFiles:       5,
      sequentialUploads:      true,
      autoUpload:             false,
      acceptFileTypes:        /(\.|\/)(gif|jpe?g|png|bmp)$/i,
      maxFileSize:            4000000, // 4 MB
      //Desactivar Resize para Android y Opera
      disableImageResize:     /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
      previewMaxWidth:        200,
      previewMaxHeight:       200,
      previewCrop:            true,

  }).on('fileuploadadd', function (event, data) {
    console.log(this);
      // Agregar el div.archivos-preview contenedor de cada Foto
      data.context = $('<div class="archivos-preview span3" />').appendTo('#files');

      // Iterar sobre cada Foto agregandole un nodo "p" con canvas, titulo, textarea, etc.
      $.each(data.files, function (index, file) {
          // indice para crear Ids secuenciales
          indice++;
          // Crear un nodo contenedor "p" para cada Foto
          var nodo_principal    = $('<p class="text-center" />');
          var nodo_nombre       = $('<span />').text(file.name+' - '+formatoPesoArchivo(file.size));
          var nodo_textarea     = '<textarea name="descr[]" style="margin-top:10px;" placeholder="Ingrese una descripción." />';
          var nodo_boton        = uploadButton.clone(true).data(data);

          nodo_principal.append( nodo_nombre );
          nodo_principal.append( nodo_textarea );
          ////////////////////////////////////////
          var icono_ocultar     = $(label_error).attr( 'id', 'ocultar-foto'+indice )
                                  .addClass('pull-right').append('<i class="icon-remove" style="color:white;" />');
          var nodo_secundario   = $('<a href="#"/>').append(icono_ocultar);

          // Agregar al inicio del contenedor "div.archivos-preview" el icono "ocultar".
          if (!index) {
              nodo_principal.append('<br>').append( nodo_boton );
              data.context.prepend( nodo_secundario );
          }
          // Finalmente agregar el nodo "p" al contenedor "div.archivos-preview"
          nodo_principal.appendTo(data.context);
      });

      ocultarFoto(indice);

  }).on('fileuploadsubmit', function (event, data) {
      //console.dir(data);
      //Agregar informacion al objeto "formData" antes de enviarlo.
      var textarea  = $(data.context[0]).find('textarea');

      data.formData = {
        usuario:      100,// Obtener el Id del Usuario actual
        modulo:       111, // Obtener el Id del Usuario actual
        descripcion:  textarea.val(),
      };

  }).on('fileuploadprocessalways', function (event, data) {
      console.log('Foto: '+data.files[0].name+'| MimeType:'+data.files[0].type);
      console.dir(data);
      var index           = data.index;
      var file            = data.files[index];
      var nodo_principal  = $(data.context.children()[index+1]);

      if (file.preview) {
        // Anteponer el "file.preview" que es un thumbnail de tipo "canvas" de la imagen.
        nodo_principal.prepend('<br>').prepend(file.preview);
      }
      if (file.error) {
        error_msg.fadeOut('fast').fadeIn('normal');
        // Al "error_msg" quitarle el style "display:none", buscar su tag "strong" pasarle como texto el "file.error"
        error_msg.attr('style', '')
            .removeClass('alert-success').addClass('alert-error')
            .find('strong').text(file.error);
        // Remover el "parent"(div.archivos-preview) del nodo "p"
        nodo_principal.parent().remove();
      }
      if (index + 1 === data.files.length) {
        data.context.find('button').text('Subir Foto').prop('disabled', !!data.files.error);
      }

  }).on('fileuploadprogressall', function (event, data) {
      var progress_bar = $('#progress .bar');
      var progress;

      progress_bar.css( 'width', '0%' ).text('Subiendo Foto: 0%');
      progress = parseInt(data.loaded / data.total * 100, 10);
      progress_bar.css( 'width', progress + '%' ).text('Subiendo Foto: '+progress+'%');

      if (progress >= 100){
        progress_bar.text('Subida de foto completada');
      }

  }).on('fileuploaddone', function (event, data) {

      $.each(data.result.files, function (index, file) {
          // ID de cada Foto (enviada desde su Entity).
          console.dir(file);
          var id_foto           = file.id;
          var nodo_principal  = $(data.context.children()[index+1]);
          var nombre_foto       = nodo_principal.find('span');
          // Agregarle el "badge-success" al nombre de la Foto confirmando la subida.
          nombre_foto.append('&nbsp;<i class="icon-ok"></i>').wrap(label_exito);
          //////////////////
          var nodo_secundario = $(data.context.children()[index]);
          var icono_eliminar    = $(label_error).attr('id', 'eliminar-foto'+file.id).addClass('pull-right')
                                .append('<i class="icon-trash" style="color:white;" />');
          //eliminar el icono_ocultar.
          nodo_secundario.find('span:first').remove();
          // Agregar el nuevo icono eliminar a su envoltorio
          nodo_secundario.append( icono_eliminar );

          // Mostrar el mensaje de subida correcta.
          error_msg.fadeOut('fast').fadeIn('normal');
          error_msg.attr('style', '')
                .removeClass('alert-error').addClass('alert-success')
                .find('strong').text('Foto subida correctamente');

          //Llama a la funcion que elimina por AJAX la foto subida.
          eliminarFoto(icono_eliminar, id_foto);
      });

  }).on('fileuploadfail', function (event, data) {

      $.each(data.files, function (index, file) {
          // Mensaje de error enviado por el plugin
          var mensaje_error   = data._response.errorThrown;
          var nodo_principal  = $(data.context.children()[index+1]);
          var etiqueta_error  = $(label_error).text('Error: '+mensaje_error);

          // Agregar al contenedor de la Foto el mensaje de error.
          nodo_principal.append( etiqueta_error );
      });

  }).prop('disabled', !$.support.fileInput)
      .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
// FIN DE DOCUMENT READY

function ocultarFoto(indice)
{
    // Recibe el indice del "icono_ocultar" y lo convierte en un objeto jQuery
    var boton_preupload  = $("span#ocultar-foto"+indice+"");
    var contenedor_total = boton_preupload.parent().parent();

    boton_preupload.on('click', function() {
        contenedor_total.fadeOut('normal', function(){
            contenedor_total.remove();
        });
    });
}

function eliminarFoto(boton, id)
{
    // Recibe el "icono_eliminar" como objeto jQuery
    var boton_postupload  = boton;
    var contenedor_total  = boton_postupload.parent().parent();
    var nombre_foto       = contenedor_total.find('span');
    var error_msg         = $('#error-msg');

    boton_postupload.on('click', function(event) {
      //TODO: Cambiar este confirm nativo de JS por un modal de Bootstrap.
      respuesta = confirm('¿Desea eliminar esta imagen?');
      if (respuesta) {
        nombre_foto.removeClass('badge-success').addClass('badge-important');
        var request_ajax = $.ajax({
          type: "POST",
          url:  ruta_fileupload+"/panel/archivo/"+id+"/delete",
          //data: { name: "John", location: "Boston" }
        });

        request_ajax.done(function( msg ) {
          //console.log( "Foto eliminada del Servidor: " );
          contenedor_total.slideUp('normal', function(){
              error_msg.fadeOut('fast').fadeIn('normal');
              error_msg.attr('style', '')
                  .removeClass('alert-error').addClass('alert-success')
                  .find('strong').text('Foto eliminada correctamente');

              contenedor_total.remove();
          });
        });

        request_ajax.fail(function( xhr, textStatus, error) {
          //console.log( "Error en el Serivdor:"+xhr.statusText );
        });
      }
    });
}

function potenciarBotonesActualizarEliminar(){
    var contenedor = $('#files');

    //Icono Eliminar para cada Foto.
    contenedor.find("span.badge-important").each(function(event){
      eliminar  = $(this).attr('id');
      id_foto   = eliminar.replace('eliminar_foto', '');
      obj_boton = $('#'+eliminar);

      //funcion desde visitadocente-fileupload.js
      eliminarFoto(obj_boton, id_foto);
    });

    // Boton Actualizar descripcion para cada Foto
    contenedor.find("a.btn-primary").each(function(event){
      var btn_actualizar  = $(this);
      var error_msg       = $('#error-msg');

      btn_actualizar.on('click', function(){
        identificador = $(this).attr('id');
        id_foto       = identificador.replace('actualizar_foto', '');
        textarea      = $(this).parent().find("textarea");

          // Mostrar Alert si textarea esta vacio
          if ( textarea.val() === "" ) {
            textarea.focus();
            error_msg.fadeOut('fast').fadeIn('normal');
            error_msg.attr('style', '')
                    .removeClass('alert-success').addClass('alert-error')
                    .find('strong').text('Ingrese una descripción');
            return false;
          }

          // Enviar por Ajax la descripcion de la Foto.
          var actualizar = $.ajax({
            type:   "POST",
            url:    ruta_fileupload+"/panel/archivo/"+id_foto+"/update",
            data:   { descripcion: textarea.val() },
          });

          actualizar.done( function( msg ) {
            error_msg.fadeOut('fast').fadeIn('normal');
            error_msg.attr('style', '')
                    .removeClass('alert-error').addClass('alert-success')
                    .find('strong').text('Foto actualizada correctamente');
            // textarea.css('border', '1px green dashed');
          });

          actualizar.fail( function( xhr, textStatus, error) {
            error_msg.fadeOut('fast').fadeIn('normal');
            error_msg.attr('style', '')
                    .removeClass('alert-succes').addClass('alert-error')
                    .find('strong').text('Error al actualizar Foto');
            // textarea.css('border', '1px red dashed');
            //console.log( "Error en el Serivdor:"+xhr.statusText );
          });
      })

    });
  }

(function(){var b,d,c;b=jQuery;c=(function(){function b(){this.fadeDuration=500;this.fitImagesInViewport=true;this.resizeDuration=700;this.showImageNumberLabel=true;this.wrapAround=false}b.prototype.albumLabel=function(b,c){return"Imagen "+b+" de "+c};return b})();d=(function(){function c(b){this.options=b;this.album=[];this.currentImageIndex=void 0;this.init()}c.prototype.init=function(){this.enable();return this.build()};c.prototype.enable=function(){var c=this;return b('body').on('click','a[rel^=lightbox], area[rel^=lightbox], a[data-lightbox], area[data-lightbox]',function(d){c.start(b(d.currentTarget));return false})};c.prototype.build=function(){var c=this;b("<div id='lightboxOverlay' class='lightboxOverlay'></div><div id='lightbox' class='lightbox'><div class='lb-outerContainer'><div class='lb-container'><img class='lb-image' src='' /><div class='lb-nav'><a class='lb-prev' href='' ></a><a class='lb-next' href='' ></a></div><div class='lb-loader'><a class='lb-cancel'></a></div></div></div><div class='lb-dataContainer'><div class='lb-data'><div class='lb-details'><span class='lb-caption'></span><span class='lb-number'></span></div><div class='lb-closeContainer'><a class='lb-close'></a></div></div></div></div>").appendTo(b('body'));this.$lightbox=b('#lightbox');this.$overlay=b('#lightboxOverlay');this.$outerContainer=this.$lightbox.find('.lb-outerContainer');this.$container=this.$lightbox.find('.lb-container');this.containerTopPadding=parseInt(this.$container.css('padding-top'),10);this.containerRightPadding=parseInt(this.$container.css('padding-right'),10);this.containerBottomPadding=parseInt(this.$container.css('padding-bottom'),10);this.containerLeftPadding=parseInt(this.$container.css('padding-left'),10);this.$overlay.hide().on('click',function(){c.end();return false});this.$lightbox.hide().on('click',function(d){if(b(d.target).attr('id')==='lightbox'){c.end()}return false});this.$outerContainer.on('click',function(d){if(b(d.target).attr('id')==='lightbox'){c.end()}return false});this.$lightbox.find('.lb-prev').on('click',function(){if(c.currentImageIndex===0){c.changeImage(c.album.length-1)}else{c.changeImage(c.currentImageIndex-1)}return false});this.$lightbox.find('.lb-next').on('click',function(){if(c.currentImageIndex===c.album.length-1){c.changeImage(0)}else{c.changeImage(c.currentImageIndex+1)}return false});return this.$lightbox.find('.lb-loader, .lb-close').on('click',function(){c.end();return false})};c.prototype.start=function(c){var f,e,j,d,g,n,o,k,l,m,p,h,i;b(window).on("resize",this.sizeOverlay);b('select, object, embed').css({visibility:"hidden"});this.$overlay.width(b(document).width()).height(b(document).height()).fadeIn(this.options.fadeDuration);this.album=[];g=0;j=c.attr('data-lightbox');if(j){h=b(c.prop("tagName")+'[data-lightbox="'+j+'"]');for(d=k=0,m=h.length;k<m;d=++k){e=h[d];this.album.push({link:b(e).attr('href'),title:b(e).attr('title')});if(b(e).attr('href')===c.attr('href')){g=d}}}else{if(c.attr('rel')==='lightbox'){this.album.push({link:c.attr('href'),title:c.attr('title')})}else{i=b(c.prop("tagName")+'[rel="'+c.attr('rel')+'"]');for(d=l=0,p=i.length;l<p;d=++l){e=i[d];this.album.push({link:b(e).attr('href'),title:b(e).attr('title')});if(b(e).attr('href')===c.attr('href')){g=d}}}}f=b(window);o=f.scrollTop()+f.height()/10;n=f.scrollLeft();this.$lightbox.css({top:o+'px',left:n+'px'}).fadeIn(this.options.fadeDuration);this.changeImage(g)};c.prototype.changeImage=function(f){var d,c,e=this;this.disableKeyboardNav();d=this.$lightbox.find('.lb-image');this.sizeOverlay();this.$overlay.fadeIn(this.options.fadeDuration);b('.lb-loader').fadeIn('slow');this.$lightbox.find('.lb-image, .lb-nav, .lb-prev, .lb-next, .lb-dataContainer, .lb-numbers, .lb-caption').hide();this.$outerContainer.addClass('animating');c=new Image();c.onload=function(){var m,g,h,i,j,k,l;d.attr('src',e.album[f].link);m=b(c);d.width(c.width);d.height(c.height);if(e.options.fitImagesInViewport){l=b(window).width();k=b(window).height();j=l-e.containerLeftPadding-e.containerRightPadding-20;i=k-e.containerTopPadding-e.containerBottomPadding-110;if((c.width>j)||(c.height>i)){if((c.width/j)>(c.height/i)){h=j;g=parseInt(c.height/(c.width/h),10);d.width(h);d.height(g)}else{g=i;h=parseInt(c.width/(c.height/g),10);d.width(h);d.height(g)}}}return e.sizeContainer(d.width(),d.height())};c.src=this.album[f].link;this.currentImageIndex=f};c.prototype.sizeOverlay=function(){return b('#lightboxOverlay').width(b(document).width()).height(b(document).height())};c.prototype.sizeContainer=function(f,g){var b,d,e,h,c=this;h=this.$outerContainer.outerWidth();e=this.$outerContainer.outerHeight();d=f+this.containerLeftPadding+this.containerRightPadding;b=g+this.containerTopPadding+this.containerBottomPadding;this.$outerContainer.animate({width:d,height:b},this.options.resizeDuration,'swing');setTimeout(function(){c.$lightbox.find('.lb-dataContainer').width(d);c.$lightbox.find('.lb-prevLink').height(b);c.$lightbox.find('.lb-nextLink').height(b);c.showImage()},this.options.resizeDuration)};c.prototype.showImage=function(){this.$lightbox.find('.lb-loader').hide();this.$lightbox.find('.lb-image').fadeIn('slow');this.updateNav();this.updateDetails();this.preloadNeighboringImages();this.enableKeyboardNav()};c.prototype.updateNav=function(){this.$lightbox.find('.lb-nav').show();if(this.album.length>1){if(this.options.wrapAround){this.$lightbox.find('.lb-prev, .lb-next').show()}else{if(this.currentImageIndex>0){this.$lightbox.find('.lb-prev').show()}if(this.currentImageIndex<this.album.length-1){this.$lightbox.find('.lb-next').show()}}}};c.prototype.updateDetails=function(){var b=this;if(typeof this.album[this.currentImageIndex].title!=='undefined'&&this.album[this.currentImageIndex].title!==""){this.$lightbox.find('.lb-caption').html(this.album[this.currentImageIndex].title).fadeIn('fast')}if(this.album.length>1&&this.options.showImageNumberLabel){this.$lightbox.find('.lb-number').text(this.options.albumLabel(this.currentImageIndex+1,this.album.length)).fadeIn('fast')}else{this.$lightbox.find('.lb-number').hide()}this.$outerContainer.removeClass('animating');this.$lightbox.find('.lb-dataContainer').fadeIn(this.resizeDuration,function(){return b.sizeOverlay()})};c.prototype.preloadNeighboringImages=function(){var c,b;if(this.album.length>this.currentImageIndex+1){c=new Image();c.src=this.album[this.currentImageIndex+1].link}if(this.currentImageIndex>0){b=new Image();b.src=this.album[this.currentImageIndex-1].link}};c.prototype.enableKeyboardNav=function(){b(document).on('keyup.keyboard',b.proxy(this.keyboardAction,this))};c.prototype.disableKeyboardNav=function(){b(document).off('.keyboard')};c.prototype.keyboardAction=function(g){var d,e,f,c,b;d=27;e=37;f=39;b=g.keyCode;c=String.fromCharCode(b).toLowerCase();if(b===d||c.match(/x|o|c/)){this.end()}else if(c==='p'||b===e){if(this.currentImageIndex!==0){this.changeImage(this.currentImageIndex-1)}}else if(c==='n'||b===f){if(this.currentImageIndex!==this.album.length-1){this.changeImage(this.currentImageIndex+1)}}};c.prototype.end=function(){this.disableKeyboardNav();b(window).off("resize",this.sizeOverlay);this.$lightbox.fadeOut(this.options.fadeDuration);this.$overlay.fadeOut(this.options.fadeDuration);return b('select, object, embed').css({visibility:"visible"})};return c})();b(function(){var e,b;b=new c();return e=new d(b)})}).call(this);
