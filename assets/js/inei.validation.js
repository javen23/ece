$(document).ready(function(){

    $.validator.addClassRules({
        'requerido': {
            required: true,
        },
        'number': {
            number: true,
        },
        'no-requerido': {
            required: false,
        },
        'email': {
            email: true,
        },
        'alfanumerico': {
            alphanumeric: true,
        },
        'telefono_fijo': {
            telefono_fijo: true,
        },
        'telefono_celular': {
            telefono_celular: true,
        },
        'telefono_rpm': {
            telefono_rpm: true,
        },
        'min': {
            min: function(element) {
                return $(element).data('validateMin') || 1;
            },
        },
        'max': {
            max: function(element) {
                return $(element).data('validateMax') || 1;
            },
        },
        'partida': {
            numbers: true,
            maxlength: 10,
        },
        'si-no': {
            digits: true,
            maxlength: 1,
            min: 0,
            max: 1,
        },
        'al-menos-uno': {
            alMenosUno: '.al-menos-uno'
        },
    });

});

/** AGREGAR METODOS DE VALIDACION **/
    $.validator.addMethod('alMenosUno', function(value, element, params){
        // console.log($(element));
        var valido   = 0;
        var identity = $(element).data('identity') || false;

        if (identity) {
            params = params + '.' + identity;
        }
        // console.log('EACH');
        $.each($(params), function(key, item){
            // console.log($(item));
            if (item.value !== '') valido++;
        })

        return valido > 0;
    });
    // $.validator.addMethod("number",  function( value, element ) {
    //     return this.optional( element ) || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)?(?:,\d+)?$/.test( value );
    // });
    $.validator.addMethod("telefono_rpm", function (value, element) {
        value = value.replace('_', '');
        return this.optional(element) || /^[#|*][0-9]{9}$/i.test(value);
    });
    $.validator.addMethod("telefono_fijo", function (value, element) {
        value = value.replace('_', '');
        return this.optional(element) || /^[(][0-9]{3}[)][\s][0-9]{3}[\s]([0-9]{3,4})$/i.test(value);
    });
    $.validator.addMethod("telefono_celular", function (value, element) {
        value = value.replace('_', '');
        return this.optional(element) || /^[0-9]{3}[\s][0-9]{3}[\s][0-9]{3}$/i.test(value);
    });
/********************************/

/** EXTENDER JQUERY VALIDATION **/
$.extend($.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Rellena este campo.",
    email: "Ingrese una dirección de correo válida.",
    url: "Ingrese una URL válida.",
    date: "Ingrese una fecha válida.",
    dateISO: "Ingrese una fecha (ISO) válida.",
    number: "Ingrese un número válido.",
    digits: "Ingrese sólo dígitos.",
    creditcard: "Ingrese un número de tarjeta válido.",
    equalTo: "Ingrese el mismo valor de nuevo.",
    extension: "Ingrese un valor con una extensión aceptada.",
    maxlength: $.validator.format("No escriba más de {0} caracteres."),
    minlength: $.validator.format("No escriba menos de {0} caracteres."),
    rangelength: $.validator.format("Ingrese un valor entre {0} y {1} caracteres."),
    range: $.validator.format("Ingrese un valor entre {0} y {1}."),
    max: $.validator.format("Ingrese un valor menor o igual a {0}."),
    min: $.validator.format("Ingrese un valor mayor o igual a {0}."),
    nifES: "Ingrese un NIF válido.",
    nieES: "Ingrese un NIE válido.",
    cifES: "Ingrese un CIF válido.",
    alphanumeric: "Ingrese solo letras, numeros y sub guion",
    telefono_fijo: "Ingrese un numero de telefono",
    telefono_celular: "Ingrese un numero de celular",
    alMenosUno: "Ingrese al menos un valor",
    telefono_rpm: "Ingrese solo (# o *) y números"
});

$.validator.setDefaults({
    errorClass: 'text-danger',
    errorElement: 'p',
});
/***********************************/
